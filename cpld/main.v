`include "hardware.v"
`include "vga.v"
`include "rom.v"

module clock(input CLK64MHZ, output [2:0] CNT);
	reg [2:0] _count = 0;

	always @(posedge CLK64MHZ)begin
		if(_count == 4)begin _count <= 0; end else begin _count <= _count + 1; end
	end

	assign CNT = _count;
endmodule

module main(input CLK64MHZ, input BUTTON, output LED1, output LED2, output LED3,
		output [2:0] VGARGB, output [2:0] VGAB, output VGAH, output VGAV,
		output MRWR, output MCE0R, output MCE1R, output MOER, output MSEMR, output [15:0] MA, inout [7:0] MD,
		output MRWL, output MCE0L, output MCE1L, output MOEL, output MSEML, output MA14L, output MA15L, output MMS,
		output ZXRST, inout ZXROMCS, input ZXWR, input ZXRD, input ZXMRQ, input ZXIORQ, input [15:0] ZXA, inout [7:0] ZXD,
		output EECS, output EESCK, output EESI, output EEHOLD, output EEWP, input EESO,
		output ETHSCK, output ETHCS, output ETHMOSI, input ETHMISO, input ETHINT, input ETHLEDA, input ETHLEDB,
		output SDPOW, output SDCS, output SDDI, output SDSCK, input SDDO, input SDCD,
		output AUX0, output AUX2, output AUX4, output AUX6, output AUX8,
		input AUX1, input AUX3, input AUX5, input AUX7, output AUX9);
	wire _rst;
	wire [2:0] _cnt;
	wire _zxmwr, _zxmrd, _zxiowr, _zxiord;
	wire [15:0] _ma_vga;
	wire [15:0] _ma_readrom;
	wire _rom_ready;
	wire _zxrom_enable;
	wire _ee_cs;
	wire _ee_readrom_cs;
	wire _ee_readrom_mosi;
	wire _ee_readrom_sck;
	wire _spi_mosi;
	wire _spi_sck;

	clock cl(CLK64MHZ, _cnt);
	readrom rr(_rst, _cnt[2], EESO, _ee_readrom_cs, _ee_readrom_mosi, _ee_readrom_sck, MRWR, _ma_readrom, MD, _rom_ready);
	vga vg(_rom_ready, CLK64MHZ, _cnt, _zxiowr, ZXA[0], ZXD[2:0], _ma_vga, MD, VGARGB, VGAB, VGAH, VGAV);
	hardware hw(_rst, _zxiowr, ZXA, ZXD, _zxrom_enable, SDPOW, LED2, LED3, SDCS, ETHCS, _ee_cs, _spi_mosi, _spi_sck);

	assign _rst = BUTTON & AUX1;
	assign MA = _rom_ready ? _ma_vga : _ma_readrom;

	assign _zxmwr = ZXWR | ZXMRQ;
	assign _zxmrd = ZXRD | ZXMRQ;
	assign _zxiowr = ZXWR | ZXIORQ;
	assign _zxiord = ZXRD | ZXIORQ;

	assign MMS = 1;
	assign MCE0R = 0;
	assign MCE1R = 1;
	assign MOER = !_rom_ready;
	assign MSEMR = 1;

	assign MRWL = _zxmwr;
	assign MCE0L = 0;
	assign MCE1L = 1;
	assign MOEL = !_zxrom_enable & !ZXA[15] & !ZXA[14] & !_zxmrd ? 0 : 1;
	assign MSEML = 1;

	assign MA14L = ZXA[14];
	assign MA15L = ZXA[15];

	assign ZXRST = !_rst | !_rom_ready;
	assign ZXROMCS = _zxrom_enable ? 1'bz : 1'b1;

	assign LED1 = _rst;

	assign SDDI = !_spi_mosi;
	assign SDSCK = !_spi_sck;

	assign ETHMOSI = _spi_mosi;
	assign ETHSCK = _spi_sck;

	assign EECS = AUX1 ? (_rom_ready ? _ee_cs : _ee_readrom_cs) : AUX3;
	assign EESI = AUX1 ? (_rom_ready ? _spi_mosi : _ee_readrom_mosi) : AUX5;
	assign EESCK = AUX1 ? (_rom_ready ? _spi_sck : _ee_readrom_sck) : AUX7;
	assign EEHOLD = 1;
	assign EEWP = !AUX1;
	assign AUX9 = EESO;

	assign ZXD[5:0] = (!_zxiord & (ZXA[0] == 1)) ? {AUX5, AUX7, SDCD, EESO, SDDO, ETHMISO} : 6'bz;
endmodule

