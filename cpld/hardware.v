module hardware(input RST, input ZXIOWR, input [15:0] ZXA, input [4:0] ZXD, output ZXROMENABLE, output SDPOW, output LED2, output LED3, output SDCS, output ETHCS, output EECS, output SPIMOSI, output SPISCK);
	reg _zxrom_enable = 0;
	reg _sd_pow = 0;
	reg _led2 = 0;
	reg _led3 = 0;
	reg _sd_cs = 1;
	reg _eth_cs = 1;
	reg _ee_cs = 1;
	reg _spi_mosi = 0;
	reg _spi_sck = 0;

	always @(negedge RST or negedge ZXIOWR)begin
		if(!RST)begin
			_zxrom_enable <= 0;
			_sd_pow <= 0;
			_led2 <= 0;
			_led3 <= 0;
			_sd_cs <= 1;
			_eth_cs <= 1;
		end else begin
			if(ZXA[1:0] == 1)begin
				_zxrom_enable <= ZXD[3];
				_sd_pow <= ZXD[2];
				_led2 <= ZXD[1];
				_led3 <= ZXD[0];
			end else if(ZXA[1:0] == 3)begin
				_ee_cs <= ZXD[4];
				_sd_cs <= ZXD[3];
				_eth_cs <= ZXD[2];
				_spi_mosi <= ZXD[1];
				_spi_sck <= ZXD[0];
			end
		end
	end

	assign ZXROMENABLE = _zxrom_enable;
	assign SDPOW = !_sd_pow;
	assign LED2 = _led2 & RST;
	assign LED3 = _led3 & RST;
	assign SDCS = !_sd_cs;
	assign ETHCS = _eth_cs;
	assign EECS = _ee_cs;
	assign SPIMOSI = _spi_mosi;
	assign SPISCK = _spi_sck;
endmodule

