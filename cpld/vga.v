module flash(input VGAV, output FLASH);
	reg [4:0] _count;

	always @(posedge VGAV)begin
		_count <= _count + 1;
	end

	assign FLASH = _count[4];
endmodule

module vga_gen(input CLK64MHZ, input [2:0] CNT, output [2:0] VGARGB, output [2:0] VGAB, output VGAH, output VGAV, output [8:0] VGAX, output [8:0] VGAY, input [3:0] PIXEL);
	reg [8:0] _counth = 0;
	reg [9:0] _countv = 0;
	reg _vgah, _vgav;
	reg [2:0] _vgargb;
	reg _vgab;

	always @(posedge CLK64MHZ)begin
		if(CNT == 0)begin
			if(_counth == 324)begin
				if(_countv == 525)begin
					_countv <= 0;
				end else begin
					_countv <= _countv + 1;
				end
			end
			if(_counth == 400)begin
				_counth <= 0;
			end else begin
				_counth <= _counth + 1;
			end
		end else if(CNT == 4)begin
			_vgah <= !((_counth >= 328) && (_counth < 376));
			_vgav <= !((_countv >= 490) && (_countv < 492));
			if((_counth < 320) && (_countv < 480))begin
				_vgargb <= PIXEL[2:0];
				_vgab <= PIXEL[3] & (PIXEL[2] | PIXEL[1] | PIXEL[0]);
			end else begin
				_vgargb <= 3'b000;
				_vgab <= 0;
			end
		end
	end

	assign VGAH = _vgah;
	assign VGAV = _vgav;
	assign VGARGB = _vgargb;
	assign VGAB = {_vgab, _vgab, _vgab};
	assign VGAX[8:0] = _counth[8:0];
	assign VGAY[8:0] = _countv[8:0];
endmodule

module get_pixel(input ENABLE, input CLK64MHZ, input [2:0] CNT, input [8:0] VGAX, input [8:0] VGAY, input FLASH, input [2:0] BORDER, output [15:0] MA, input [7:0] MD, output [3:0] PIXEL);
	reg [3:0] _pixel;
	reg [12:0] _ma;
	reg [4:0] _x;
	reg [7:0] _y;
	reg [7:0] _pixels_fetch;
	reg [7:0] _attrs_fetch;
	reg [7:0] _pixels;
	reg [7:0] _attrs;

	always @(posedge CLK64MHZ)begin
		if(CNT == 1)begin
			if(VGAX[2:0] == 3'b000)begin
				_x <= (VGAX - 23) >> 3;
				_y <= (VGAY - 48) >> 1;
				_pixels <= {_pixels_fetch[0], _pixels_fetch[1], _pixels_fetch[2], _pixels_fetch[3], _pixels_fetch[4], _pixels_fetch[5], _pixels_fetch[6], _pixels_fetch[7]};
				_attrs <= _attrs_fetch;
			end else if(VGAX[2:0] == 3'b100)begin
				_y[4:0] <= (VGAY - 48) >> 4;
			end
		end else if(CNT == 2)begin
			if(VGAX[2:0] == 3'b000)begin
				_ma <= {_y[7:6],_y[2:0],_y[5:3],_x[4:0]};
			end else if(VGAX[2:0] == 3'b010)begin
				_pixels_fetch <= MD;
			end else if(VGAX[2:0] == 3'b100)begin
				_ma <= {3'b110, _y[4:0],_x[4:0]};
			end else if(VGAX[2:0] == 3'b110)begin
				_attrs_fetch <= MD;
			end
		end else if(CNT == 3)begin
			if((VGAX >= 32) && (VGAX < 288) && (VGAY >= 48) && (VGAY < 432))begin
				if(_pixels[VGAX[2:0]] ^ (_attrs[7] & FLASH))begin
					_pixel <= {_attrs[6], _attrs[2:0]};
				end else begin
					_pixel <= {_attrs[6], _attrs[5:3]};
				end
			end else begin
				_pixel <= {1'b0, BORDER[2:0]};
			end
		end
	end

	assign PIXEL = ENABLE ? _pixel : 4'b0111;
	assign MA = {3'b010, _ma};
endmodule

module border(input ENABLE, input ZXIOWR, input ZXA0, input [2:0] ZXD, output [2:0] BORDER);
	reg [2:0] _border;

	always @(negedge ENABLE or negedge ZXIOWR)begin
		if(!ENABLE)begin
			_border <= 3'b111;
		end else begin
			if(!ZXA0)begin
				_border <= ZXD;
			end
		end
	end

	assign BORDER = _border;
endmodule

module vga(input ENABLE, input CLK64MHZ, input [2:0] CNT, input ZXIOWR, input ZXA0, input [2:0] ZXD, output [15:0] MA, input [7:0] MD,
		output [2:0] VGARGB, output [2:0] VGAB, output VGAH, output VGAV);
	wire [8:0] _x;
	wire [8:0] _y;
	wire [3:0] _pixel;
	wire _flash;
	wire [2:0] _border;

	flash fl(VGAV, _flash);
	border br(ENABLE, ZXIOWR, ZXA0, ZXD, _border);
	vga_gen vg(CLK64MHZ, CNT, VGARGB, VGAB, VGAH, VGAV, _x, _y, _pixel);
	get_pixel gp(ENABLE, CLK64MHZ, CNT, _x, _y, _flash, _border, MA, MD, _pixel);
endmodule

