module readrom(input RST, input CNT, input EESO, output EECS, output EEMOSI, output EESCK, output MWR, output [15:0] MA, inout [7:0] MD, output ROMREADY);
	reg [9:0] _address = 0;
	reg [6:0] _count;
	reg [1:0] _state = 0;
	reg _rom_ready = 0;
	reg [2:0] _ee; // 2 - CS, 1 - SI, 0 - SCK
	reg [7:0] _data;
	reg _mwr = 1;
	reg _screen = 0;

	always @(negedge RST or posedge CNT)begin
		if(!RST)begin
			_state <= 0;
		end else begin
			if(_state == 0)begin
				_state <= 1;
				_count <= 0;
				_rom_ready <= 0;
				_address <= 0;
				_mwr <= 1;
				_screen <= 0;
			end else if(_state == 1)begin
				_count <= _count + 1;
				case(_count)
					1:  _ee <= 3'b100;
					2:  _ee <= 3'b000;
					3:  _ee <= 3'b001;
					4:  _ee <= 3'b000;
					5:  _ee <= 3'b001;
					6:  _ee <= 3'b000;
					7:  _ee <= 3'b001;
					8:  _ee <= 3'b000;
					9:  _ee <= 3'b001;
					10: _ee <= 3'b000;
					11: _ee <= 3'b001;
					12: _ee <= 3'b000;
					13: _ee <= 3'b001;
					14: _ee <= 3'b000;
					15: _ee <= 3'b010;
					16: _ee <= 3'b011;
					17: _ee <= 3'b010;
					18: _ee <= 3'b011;
					19: _ee <= 3'b010;
					20: _ee <= 3'b000;
					21: _ee <= 3'b001;
					22: _ee <= 3'b000;
					23: _ee <= 3'b001;
					24: _ee <= 3'b000;
					25: _ee <= 3'b001;
					26: _ee <= 3'b000;
					27: _ee <= 3'b001;
					28: _ee <= 3'b000;
					29: _ee <= 3'b001;
					30: _ee <= 3'b000;
					31: _ee <= 3'b001;
					32: _ee <= 3'b000;
					33: _ee <= 3'b001;
					34: _ee <= 3'b000;
					35: _ee <= 3'b001;
					36: _ee <= 3'b000;
					37: _ee <= 3'b001;
					38: _ee <= 3'b000;
					39: _ee <= 3'b001;
					40: _ee <= 3'b000;
					41: _ee <= 3'b001;
					42: _ee <= 3'b000;
					43: _ee <= 3'b001;
					44: _ee <= 3'b000;
					45: _ee <= 3'b001;
					46: _ee <= 3'b000;
					47: _ee <= 3'b001;
					48: _ee <= 3'b000;
					49: _ee <= 3'b001;
					50: _ee <= 3'b000;
					51: _ee <= 3'b001;
					52: _ee <= 3'b000;
					53: _ee <= 3'b001;
					54: _ee <= 3'b000;
					55: _ee <= 3'b001;
					56: _ee <= 3'b000;
					57: _ee <= 3'b001;
					58: _ee <= 3'b000;
					59: _ee <= 3'b001;
					60: _ee <= 3'b000;
					61: _ee <= 3'b001;
					62: _ee <= 3'b000;
					63: _ee <= 3'b001;
					64: _ee <= 3'b000;
					65: _ee <= 3'b001;
					66: _ee <= 3'b000;
					67: _ee <= 3'b001;
					68: _ee <= 3'b000;
					69: begin
						_state <= 2;
						_count <= 0;
					end
				endcase
			end else if(_state == 2)begin
				_count <= _count + 1;
				case(_count)
					1:  _ee <= 3'b001;
					2:  begin _data[7] <= EESO; _ee <= 3'b000; end
					3:  _ee <= 3'b001;
					4:  begin _data[6] <= EESO; _ee <= 3'b000; end
					5:  _ee <= 3'b001;
					6:  begin _data[5] <= EESO; _ee <= 3'b000; end
					7:  _ee <= 3'b001;
					8:  begin _data[4] <= EESO; _ee <= 3'b000; end
					9:  _ee <= 3'b001;
					10: begin _data[3] <= EESO; _ee <= 3'b000; end
					11: _ee <= 3'b001;
					12: begin _data[2] <= EESO; _ee <= 3'b000; end
					13: _ee <= 3'b001;
					14: begin _data[1] <= EESO; _ee <= 3'b000; end
					15: _ee <= 3'b001;
					16: begin _data[0] <= EESO; _ee <= 3'b000; end
					17: _mwr <= 0;
					18: _mwr <= 1;
					19: begin _screen <= 1; _data <= 8'b00111111; end
					20: _mwr <= 0;
					21: _mwr <= 1;
					22: begin
						_screen <= 0;
						if(_address == 1023)begin
							_state <= 3;
						end else begin
							_count <= 0;
							_address <= _address + 1;
						end
					end
				endcase
			end else if(_state == 3)begin
				_rom_ready <= 1;
			end
		end
	end

	assign EECS = _ee[2];
	assign EEMOSI = _ee[1];
	assign EESCK = _ee[0];
	assign MWR = _mwr;
	assign MA = {1'b0, _screen, 1'b0, _screen, _screen, 1'b0, _address[9:0]};
	assign MD = _rom_ready ? 8'bz : _data;
	assign ROMREADY = _rom_ready;
endmodule

