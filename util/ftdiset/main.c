#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ftdi.h>

static uint16_t get_bit(char *s, uint8_t channel);

int main(int argc, char *argv[]){
	struct ftdi_context *ftdi;
	uint16_t vendor, product, output, mask, bit;
	uint8_t buffer[3];
	int c, ret;
	vendor = 0x0403;
	product = 0x6010;
	output = mask = 0;
	opterr = 0;
	while((c = getopt(argc, argv, "v:p:0:1:")) != -1){
		if(c == 'v'){
			vendor = strtoull(optarg, NULL, 0);
		}else if(c == 'p'){
			product = strtoull(optarg, NULL, 0);
		}else if(c == '0'){
			bit = get_bit(optarg, 0);
			if(bit == 0){
				fprintf(stderr, "error: invalid pin: %s\n", optarg);
				return(1);
			}
			output &= ~bit;
			mask |= bit;
		}else if(c == '1'){
			bit = get_bit(optarg, 0);
			if(bit == 0){
				fprintf(stderr, "error: invalid pin: %s\n", optarg);
				return(1);
			}
			output |= bit;
			mask |= bit;
		}
	}
	if((ftdi = ftdi_new()) == NULL){
		fprintf(stderr, "error: ftdi_new\n");
		return(1);
	}
	ret = ftdi_usb_open(ftdi, vendor, product);
	if((ret < 0) && (ret != -5)){
		fprintf(stderr, "error: ftdi_usb_open: %d\n", ret);
		ftdi_deinit(ftdi);
		return(1);
	}
	ftdi_set_interface(ftdi, INTERFACE_A);
	ftdi_set_bitmode(ftdi, 0x00, BITMODE_MPSSE);
	ret = 1;
	buffer[0] = 0x80;
	buffer[1] = (output >> 0) & 0xFF;
	buffer[2] = (mask >> 0) & 0xFF;
	if(ftdi_write_data(ftdi, buffer, 3) != 3){
		fprintf(stderr, "error: ftdi_write_data\n");
		goto error;
	}
	buffer[0] = 0x82;
	buffer[1] = (output >> 8) & 0xFF;
	buffer[2] = (mask >> 8) & 0xFF;
	if(ftdi_write_data(ftdi, buffer, 3) != 3){
		fprintf(stderr, "error: ftdi_write_data\n");
		goto error;
	}
	usleep(5000);
	ret = 0;
error:
	ftdi_usb_close(ftdi);
	ftdi_deinit(ftdi);
	return(ret);
}

static uint16_t get_bit(char *s, uint8_t channel){
	if(channel == 0){
		if(!strcasecmp(s, "ad0"))return(0x0001);
		if(!strcasecmp(s, "ad1"))return(0x0002);
		if(!strcasecmp(s, "ad2"))return(0x0004);
		if(!strcasecmp(s, "ad3"))return(0x0008);
		if(!strcasecmp(s, "ad4"))return(0x0010);
		if(!strcasecmp(s, "ad5"))return(0x0020);
		if(!strcasecmp(s, "ad6"))return(0x0040);
		if(!strcasecmp(s, "ad7"))return(0x0080);

		if(!strcasecmp(s, "ac0"))return(0x0100);
		if(!strcasecmp(s, "ac1"))return(0x0200);
		if(!strcasecmp(s, "ac2"))return(0x0400);
		if(!strcasecmp(s, "ac3"))return(0x0800);
		if(!strcasecmp(s, "ac4"))return(0x1000);
		if(!strcasecmp(s, "ac5"))return(0x2000);
		if(!strcasecmp(s, "ac6"))return(0x4000);
		if(!strcasecmp(s, "ac7"))return(0x8000);
	}
	if(!strcasecmp(s, "txd") || !strcasecmp(s, "tck") || !strcasecmp(s, "sk") || !strcasecmp(s, "clk") || !strcasecmp(s, "sclk") || !strcasecmp(s, "sck"))return(0x0001);
	if(!strcasecmp(s, "rxd") || !strcasecmp(s, "tdi") || !strcasecmp(s, "do") || !strcasecmp(s, "si") || !strcasecmp(s, "mosi"))return(0x0002);
	if(!strcasecmp(s, "rts") || !strcasecmp(s, "tdo") || !strcasecmp(s, "di") || !strcasecmp(s, "so") || !strcasecmp(s, "miso"))return(0x0004);
	if(!strcasecmp(s, "cts") || !strcasecmp(s, "tms") || !strcasecmp(s, "cs"))return(0x0008);
	if(!strcasecmp(s, "dtr") || !strcasecmp(s, "gpiol0") || !strcasecmp(s, "trst"))return(0x0010);
	if(!strcasecmp(s, "dsr") || !strcasecmp(s, "gpiol1"))return(0x0020);
	if(!strcasecmp(s, "dcd") || !strcasecmp(s, "gpiol2"))return(0x0040);
	if(!strcasecmp(s, "ri")  || !strcasecmp(s, "gpiol3"))return(0x0080);

	if(!strcasecmp(s, "gpioh0"))return(0x0100);
	if(!strcasecmp(s, "gpioh1"))return(0x0200);
	if(!strcasecmp(s, "gpioh2"))return(0x0400);
	if(!strcasecmp(s, "gpioh3"))return(0x0800);
	if(!strcasecmp(s, "gpioh4"))return(0x1000);
	if(!strcasecmp(s, "gpioh5"))return(0x2000);
	if(!strcasecmp(s, "gpioh6"))return(0x4000);
	if(!strcasecmp(s, "gpioh7"))return(0x8000);

	return(0);
}

