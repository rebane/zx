#include "spi.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpsse.h>

struct spi_struct{
	struct mpsse_context *mpsse;
};

void *spi_open(uint16_t vid, uint16_t pid){
	struct spi_struct *spi;
	spi = malloc(sizeof(struct spi_struct));
	if(spi == NULL)return(NULL);
	spi->mpsse = MPSSE(SPI0, ONE_HUNDRED_KHZ, MSB);
	if(spi->mpsse == NULL){
		free(spi);
		return(NULL);
	}
	PinLow(spi->mpsse, GPIOL0);
	PinLow(spi->mpsse, GPIOL1);
	PinLow(spi->mpsse, GPIOL2);
	PinLow(spi->mpsse, GPIOL3);
	return(spi);
}

void spi_close(void *spi, uint8_t mosi, uint8_t clk){ // TODO: set mosi & clk after reset release
	usleep(5000);
	PinHigh(((struct spi_struct *)spi)->mpsse, GPIOL0);
	PinHigh(((struct spi_struct *)spi)->mpsse, GPIOL1);
	PinHigh(((struct spi_struct *)spi)->mpsse, GPIOL2);
	PinHigh(((struct spi_struct *)spi)->mpsse, GPIOL3);
	usleep(5000);
	Close(((struct spi_struct *)spi)->mpsse);
}

void spi_transfer(void *spi, const void *wbuf, uint16_t wlen, void *rbuf, uint16_t rlen){
	void *data;
	Start(((struct spi_struct *)spi)->mpsse);
	Write(((struct spi_struct *)spi)->mpsse, (void *)wbuf, wlen);
	data = Read(((struct spi_struct *)spi)->mpsse, rlen);
	Stop(((struct spi_struct *)spi)->mpsse);
	memcpy(rbuf, data, rlen);
	free(data);
}

