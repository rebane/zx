#ifndef _SPI_H_
#define _SPI_H_

#include <stdint.h>

void *spi_open(uint16_t vid, uint16_t pid);
void spi_close(void *spi, uint8_t mosi, uint8_t clk);
void spi_transfer(void *spi, const void *wbuf, uint16_t wlen, void *rbuf, uint16_t rlen);

#endif

