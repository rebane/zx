#include "spi.h"
#include <stdlib.h>
#include <stdio.h>
#include <ftdi.h>

struct spi_struct{
	struct ftdi_context *ftdi;
};

static void spi_cs(void *spi, uint8_t active);
static uint8_t spi_transfer_byte(void *spi, uint8_t in);

void *spi_open(uint16_t vid, uint16_t pid){
	struct spi_struct *spi;
	int ret;
	spi = malloc(sizeof(struct spi_struct));
	if(spi == NULL)return(NULL);
	if((spi->ftdi = ftdi_new()) == 0){
		free(spi);
		return(NULL);
	}
	ret = ftdi_usb_open(spi->ftdi, vid, pid);
	if((ret < 0) && (ret != -5)){
		ftdi_deinit(spi->ftdi);
		free(spi);
		return(NULL);
	}
	ftdi_set_interface(spi->ftdi, INTERFACE_A);
	ftdi_set_bitmode(spi->ftdi, 0xFB, BITMODE_BITBANG);
	ftdi_set_baudrate(spi->ftdi, 460800);
	ftdi_write_data(spi->ftdi, (uint8_t *)"\xBF", 1);
	usleep(5000);
	return(spi);
}

void spi_close(void *spi, uint8_t mosi, uint8_t clk){
	usleep(5000);
	ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xFF", 1);
	usleep(5000);
	ftdi_disable_bitbang(((struct spi_struct *)spi)->ftdi);
	ftdi_usb_close(((struct spi_struct *)spi)->ftdi);
	ftdi_deinit(((struct spi_struct *)spi)->ftdi);
	free(spi);
}

void spi_transfer(void *spi, const void *wbuf, uint16_t wlen, void *rbuf, uint16_t rlen){
	uint16_t i;
	spi_cs(spi, 1);
	for(i = 0; i < wlen; i++)spi_transfer_byte(spi, ((uint8_t *)wbuf)[i]);
	for(i = 0; i < rlen; i++)((uint8_t *)rbuf)[i] = spi_transfer_byte(spi, 0x00);
	spi_cs(spi, 0);
}

static void spi_cs(void *spi, uint8_t active){
	if(active){
		ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB6", 1);
	}else{
		ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xBE", 1);
	}
}

static uint8_t spi_transfer_byte(void *spi, uint8_t in){
	uint8_t i, c, out;
	out = 0;
	for(i = 7; i < 8; i--){
		if(in & (1 << i)){
			ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB6", 1);
			ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB7", 1);
			ftdi_read_pins(((struct spi_struct *)spi)->ftdi, &c);
			if(c & (1 << 2))out |= (1 << i);
			ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB6", 1);
		}else{
			ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB4", 1);
			ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB5", 1);
			ftdi_read_pins(((struct spi_struct *)spi)->ftdi, &c);
			if(c & (1 << 2))out |= (1 << i);
			ftdi_write_data(((struct spi_struct *)spi)->ftdi, (uint8_t *)"\xB4", 1);
		}
	}
	return(out);
}

