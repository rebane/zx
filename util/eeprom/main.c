#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include "spi.h"

uint8_t buffer[1024 * 1024];

uint8_t eeprom_write_enable(void *spi, int count);
uint8_t eeprom_wait_write(void *spi, int count);
uint8_t eeprom_read_status(void *spi);

int main(int argc, char *argv[]){
	void *spi;
	int ret, fd, c, mode, verify, erase;
	char *file;
	ssize_t i, l, n, j, offset;
	uint8_t spi_buf[1024];

	opterr = 0;
	mode = 0;
	verify = 0;
	erase = 0;
	offset = 0;
	while((c = getopt(argc, argv, "her:w:vo:")) != -1){
		if(c == 'h'){
			printf("help\n");
			return(0);
		}else if(c == 'e'){
			erase = 1;
		}else if(c == 'r'){
			mode = 1;
			file = optarg;
		}else if(c == 'w'){
			mode = 2;
			file = optarg;
		}else if(c == 'v'){
			verify = 1;
		}else if(c == 'o'){
			offset = strtoll(optarg, NULL, 10);
		}else{
			fprintf(stderr, "invalid command line\n");
			return(1);
		}
	}
	if(!mode && !erase){
		fprintf(stderr, "invalid command line\n");
		return(1);
	}
	if(offset % 256){
		fprintf(stderr, "invalid offset\n");
		return(1);
	}

	spi = spi_open(0x0403, 0x6010);
	if(spi == NULL){
		fprintf(stderr, "ftdi open error\n");
		return(1);
	}

	spi_transfer(spi, "\xAB\x00\x00\x00", 4, spi_buf, 1);
	printf("Chip ID: 0x%02X\n", (unsigned int)spi_buf[0]);

	ret = 0;
	fd = -1;

	if(erase){
		printf("erasing...\n");
		if(!eeprom_wait_write(spi, 10)){
			printf("\n");
			fprintf(stderr, "chip write in progress\n");
			ret = 1;
			goto ready;
		}
		if(!eeprom_write_enable(spi, 10)){
			printf("\n");
			fprintf(stderr, "write enable error\n");
			ret = 1;
			goto ready;
		}
		spi_transfer(spi, "\xC7", 1, NULL, 0);
		if(!eeprom_wait_write(spi, 10)){
			printf("\n");
			fprintf(stderr, "chip write in progress\n");
			ret = 1;
			goto ready;
		}
	}

	if(mode == 1){
		printf("reading...\n");
		if(!eeprom_wait_write(spi, 10)){
			fprintf(stderr, "chip write in progress\n");
			ret = 1;
			goto ready;
		}
		spi_buf[0] = 0x03;
		spi_buf[1] = (((0 + offset) >> 16) & 0xFF);
		spi_buf[2] = (((0 + offset) >> 8) & 0xFF);
		spi_buf[3] = (((0 + offset) >> 0) & 0xFF);
		spi_transfer(spi, spi_buf, 4, spi_buf, 50);
	}else if(mode == 2){
		fd = open(file, O_RDONLY);
		if(fd < 0){
			fprintf(stderr, "open: %s\n", strerror(errno));
			ret = 1;
			goto ready;
		}
		l = read(fd, buffer, 1024 * 1024);
		if(l < 0){
			fprintf(stderr, "read: %s\n", strerror(errno));
			ret = 1;
			goto ready;
		}
		printf("read %ld byte(s) from file \"%s\"\n", (long int)l, file);
		printf("writing...\n");
		printf("\rwrote 0 byte(s)");
		fflush(stdout);
		for(i = 0; i < l; i += 256){
			n = l - i;
			if(n > 256)n = 256;
			if(!eeprom_wait_write(spi, 10)){
				printf("\n");
				fprintf(stderr, "chip write in progress\n");
				ret = 1;
				goto ready;
			}
			if(!eeprom_write_enable(spi, 10)){
				printf("\n");
				fprintf(stderr, "write enable error\n");
				ret = 1;
				goto ready;
			}
			spi_buf[0] = 0x02;
			spi_buf[1] = (((i + offset) >> 16) & 0xFF);
			spi_buf[2] = (((i + offset) >> 8) & 0xFF);
			spi_buf[3] = (((i + offset) >> 0) & 0xFF);
			memcpy(&spi_buf[4], &buffer[i], n);
			spi_transfer(spi, spi_buf, n + 4, NULL, 0);
			printf("\rwrote %ld byte(s)", (long int)(i + n));
			fflush(stdout);
		}
		printf("\n");
		if(verify){
			printf("verifing...\n");
			printf("\rverified 0 byte(s)");
			fflush(stdout);
			for(i = 0; i < l; i += 1024){
				n = l - i;
				if(n > 1024)n = 1024;
				if(!eeprom_wait_write(spi, 10)){
					fprintf(stderr, "chip write in progress\n");
					ret = 1;
					goto ready;
				}
				spi_buf[0] = 0x03;
				spi_buf[1] = (((i + offset) >> 16) & 0xFF);
				spi_buf[2] = (((i + offset) >> 8) & 0xFF);
				spi_buf[3] = (((i + offset) >> 0) & 0xFF);
				spi_transfer(spi, spi_buf, 4, spi_buf, n);
				for(j = 0; j < n; j++){
					if(spi_buf[j] != buffer[i + j]){
						printf("\rverified %ld byte(s)\n", (long int)(i + j));
						fprintf(stderr, "verify error: read: %02X, original: %02X\n", (unsigned int)spi_buf[j], (unsigned int)buffer[i + j]);
						ret = 1;
						goto ready;
					}
				}
				printf("\rverified %ld byte(s)", (long int)(i + n));
				fflush(stdout);
			}
			printf("\n");
		}
		if(!eeprom_wait_write(spi, 10)){
			fprintf(stderr, "chip write in progress\n");
			ret = 1;
			goto ready;
		}
	}
ready:
	if(fd >= 0)close(fd);
	spi_close(spi, 1, 1);
	return(ret);
}

uint8_t eeprom_write_enable(void *spi, int count){
	uint8_t s;
	int i;
	for(i = 0; i < count; i++){
		s = eeprom_read_status(spi);
		if(s & 0x02)return(1);
		spi_transfer(spi, "\x06", 1, NULL, 0);
		if(i)usleep(5000);
	}
	return(0);
}

uint8_t eeprom_wait_write(void *spi, int count){
	uint8_t s;
	int i;
	for(i = 0; i < count; i++){
		s = eeprom_read_status(spi);
		if(!(s & 0x01))return(1);
		usleep(5000);
	}
	return(0);
}

uint8_t eeprom_read_status(void *spi){
	uint8_t status;
	spi_transfer(spi, "\x05", 1, &status, 1);
	return(status);
}

