#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "z80.h"
#include "hw.h"
#include "zx.h"
#include "mtimer.h"
#include "encx24j600.h"
#include "ethernet.h"
#include "ip.h"
#include "udp.h"
#include "dhcp.h"
#include "debug.h"

#define DHCP_XID      0x12345678
#define MAC_BROADCAST ((void *)0x3900)

uint8_t ethernet_packet[1600], *ip_packet;
uint8_t dhcp_server[4];
uint8_t my_ip[4], my_nm[4], my_gw[4], my_ns[4];

// sudo /usr/sbin/arping -i eth0 00:04:A3:FC:9E:81
uint16_t dhcp_ok(uint16_t len);
void print_link();

int main(){
	uint16_t len;
	uint8_t event, ok, sent, try;
	mtimer_t t;
	dhcp_option_t option;

	zx_init(NULL);
	zx_wrap(ZX_WRAP_ON);
	zx_border(ZX_BORDER_MAGENTA);
	mtimer_init(zx_timer_get);

	printf("Starting ethernet demo...\n");
	ip_packet = &ethernet_packet[ETHERNET_PAYLOAD_START];

	hw_spi_cs_disable_eth();

	encx24j600_init();

	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X\n", (unsigned int)encx24j600_mac[0], (unsigned int)encx24j600_mac[1], (unsigned int)encx24j600_mac[2], (unsigned int)encx24j600_mac[3], (unsigned int)encx24j600_mac[4], (unsigned int)encx24j600_mac[5]);

	encx24j600_filter_dhcp();

dhcp_start:
	// DHCP discover
	mtimer_timeout_set(&t, 500);
	sent = 0;
	while(1){
		if(encx24j600_link){
			if(mtimer_timeout(&t)){
				printf("Sending DHCP discover...\n");
				len = dhcp_write_discover(ip_packet, DHCP_XID, encx24j600_mac);
				len = ethernet_write(ethernet_packet, MAC_BROADCAST, encx24j600_mac, ETHERNET_TYPE_IPV4, ip_packet, len);
				encx24j600_send(ethernet_packet, len);
				sent = 1;
				mtimer_timeout_set(&t, 5000);
			}
		}else{
			mtimer_timeout_set(&t, 1000);
		}
		event = encx24j600_poll();
		if(event == ENCX24J600_EVENT_LINK){
			print_link();
		}else if(event == ENCX24J600_EVENT_RX){
			len = encx24j600_recv(ethernet_packet, 1600);
		}else{
			continue;
		}
		if(!sent)continue;
		len = dhcp_ok(len);
		if(!len)continue;
		my_ip[0] = my_ip[1] = my_ip[2] = my_ip[3] = 0;
		my_nm[0] = my_nm[1] = my_nm[2] = my_nm[3] = 0;
		my_gw[0] = my_gw[1] = my_gw[2] = my_gw[3] = 0;
		my_ns[0] = my_ns[1] = my_ns[2] = my_ns[3] = 0;
		ok = 0;
		dhcp_yiaddr(my_ip, ip_packet);
		dhcp_siaddr(dhcp_server, ip_packet);
		for(dhcp_option_first(ip_packet, len, &option); dhcp_option_valid(&option); dhcp_option_next(ip_packet, len, &option)){
			if(option.option == 53){ // DHCP message type
				if(option.len == 1){
					if(((uint8_t *)option.param)[0] == 2)ok = 1;
				}
			}else if(option.option == 1){ // subnet mask
				if(option.len == 4){
					my_nm[0] = ((uint8_t *)option.param)[0];
					my_nm[1] = ((uint8_t *)option.param)[1];
					my_nm[2] = ((uint8_t *)option.param)[2];
					my_nm[3] = ((uint8_t *)option.param)[3];
				}
			}else if(option.option == 3){ // router
				if(option.len >= 4){
					my_gw[0] = ((uint8_t *)option.param)[0];
					my_gw[1] = ((uint8_t *)option.param)[1];
					my_gw[2] = ((uint8_t *)option.param)[2];
					my_gw[3] = ((uint8_t *)option.param)[3];
				}
			}else if(option.option == 6){ // nameserver
				if(option.len >= 4){
					my_ns[0] = ((uint8_t *)option.param)[0];
					my_ns[1] = ((uint8_t *)option.param)[1];
					my_ns[2] = ((uint8_t *)option.param)[2];
					my_ns[3] = ((uint8_t *)option.param)[3];
				}
			}else if(option.option == 54){ // DHCP server
				if(option.len >= 4){
					dhcp_server[0] = ((uint8_t *)option.param)[0];
					dhcp_server[1] = ((uint8_t *)option.param)[1];
					dhcp_server[2] = ((uint8_t *)option.param)[2];
					dhcp_server[3] = ((uint8_t *)option.param)[3];
				}
			}
		}
		if(!ok)continue;
		printf("DHCP offer from %u.%u.%u.%u\n", (unsigned int)dhcp_server[0], (unsigned int)dhcp_server[1], (unsigned int)dhcp_server[2], (unsigned int)dhcp_server[3]);
		break;
	}

	// DHCP request
	mtimer_timeout_set(&t, 0);
	sent = 0;
	try = 0;
	while(1){
		if(encx24j600_link){
			if(mtimer_timeout(&t)){
				try++;
				if(try > 5)goto dhcp_start;
				printf("Sending DHCP request...\n");
				len = dhcp_write_request(ip_packet, DHCP_XID, encx24j600_mac, my_ip, dhcp_server);
				len = ethernet_write(ethernet_packet, MAC_BROADCAST, encx24j600_mac, ETHERNET_TYPE_IPV4, ip_packet, len);
				encx24j600_send(ethernet_packet, len);
				sent = 1;
				mtimer_timeout_set(&t, 5000);
			}
		}else{
			goto dhcp_start;
		}
		event = encx24j600_poll();
		if(event == ENCX24J600_EVENT_LINK){
			print_link();
		}else if(event == ENCX24J600_EVENT_RX){
			len = encx24j600_recv(ethernet_packet, 1600);
		}else{
			continue;
		}
		if(!sent)continue;
		len = dhcp_ok(len);
		if(!len)continue;
		ok = 0;
		for(dhcp_option_first(ip_packet, len, &option); dhcp_option_valid(&option); dhcp_option_next(ip_packet, len, &option)){
			// printf("OP: %u\n", (unsigned int)option.option);
			if(option.option == 53){ // DHCP message type
				if(option.len == 1){
					if(((uint8_t *)option.param)[0] == 5)ok = 1;
				}
			}
		}
		if(!ok)continue;
		printf("DHCP ack from %u.%u.%u.%u\n", (unsigned int)dhcp_server[0], (unsigned int)dhcp_server[1], (unsigned int)dhcp_server[2], (unsigned int)dhcp_server[3]);
		break;
	}
	printf("  IP:         %u.%u.%u.%u\n", (unsigned int)my_ip[0], (unsigned int)my_ip[1], (unsigned int)my_ip[2], (unsigned int)my_ip[3]);
	printf("  NETMASK:    %u.%u.%u.%u\n", (unsigned int)my_nm[0], (unsigned int)my_nm[1], (unsigned int)my_nm[2], (unsigned int)my_nm[3]);
	if((my_gw[0] != 0) || (my_gw[1] != 0) || (my_gw[2] != 0) || (my_gw[3] != 0)){
		printf("  GATEWAY:    %u.%u.%u.%u\n", (unsigned int)my_gw[0], (unsigned int)my_gw[1], (unsigned int)my_gw[2], (unsigned int)my_gw[3]);
	}
	if((my_ns[0] != 0) || (my_ns[1] != 0) || (my_ns[2] != 0) || (my_ns[3] != 0)){
		printf("  NAMESERVER: %u.%u.%u.%u\n", (unsigned int)my_ns[0], (unsigned int)my_ns[1], (unsigned int)my_ns[2], (unsigned int)my_ns[3]);
	}

	encx24j600_filter_normal();
	while(1){
		mtimer_sleep(1000);
		sprintf(&ip_packet[UDP_PAYLOAD_START], "UPTIME: %llu\n", (unsigned long long int)mtimer_get(NULL));
		debug_write(ethernet_packet, my_ip, &ip_packet[UDP_PAYLOAD_START], strlen(&ip_packet[UDP_PAYLOAD_START]));
	}
}

uint16_t dhcp_ok(uint16_t len){
	uint16_t i;
	if(len < (ETHERNET_HEADER_LEN + IP_HEADER_LEN + UDP_HEADER_LEN + DHCP_HEADER_LEN))return(0);
	// ethernet check
	if(ethernet_type(ethernet_packet) != ETHERNET_TYPE_IPV4)return(0);
	len -= ETHERNET_HEADER_LEN;
	// ip check
	if(ip_version(ip_packet) != IP_VERSION_IPV4)return(0);
	if(ip_protocol(ip_packet) != IP_PROTOCOL_UDP)return(0);
	i = ip_len(ip_packet);
	if(i > len)return(0);
	len = i;
	if(len < (IP_HEADER_LEN + UDP_HEADER_LEN + DHCP_HEADER_LEN))return(0);
	// udp check
	if(udp_dst_port(ip_packet) != DHCP_CLIENT_PORT)return(0);
	if(udp_src_port(ip_packet) != DHCP_SERVER_PORT)return(0);
	i = udp_len(ip_packet) + IP_HEADER_LEN;
	if(i > len)return(0);
	len = i;
	if(len < (IP_HEADER_LEN + UDP_HEADER_LEN + DHCP_HEADER_LEN))return(0);
	// dhcp check
	if(dhcp_op(ip_packet) != DHCP_OP_REPLY)return(0);
	if(dhcp_htype(ip_packet) != DHCP_HTYPE_ETHERNET)return(0);
	if(dhcp_hlen(ip_packet) != 6)return(0);
	if(dhcp_xid(ip_packet) != DHCP_XID)return(0);
	if(!dhcp_chaddr_is(ip_packet, encx24j600_mac))return(0);
	if(!dhcp_magic_cookie_ok(ip_packet))return(0);
	// chk check
	if(!ip_chk_ok(ip_packet))return(0);
	if(!udp_chk_ok(ip_packet))return(0);
	return(len);
}

void print_link(){
	if(encx24j600_link == ENCX24J600_LINK_100_FD){
		printf("LINK UP, 100Mbps, FULL DUPLEX\n");
	}else if(encx24j600_link == ENCX24J600_LINK_10_FD){
		printf("LINK UP, 10Mbps, FULL DUPLEX\n");
	}else if(encx24j600_link == ENCX24J600_LINK_100_HD){
		printf("LINK UP, 100Mbps, HALF DUPLEX\n");
	}else if(encx24j600_link == ENCX24J600_LINK_10_HD){
		printf("LINK UP, 10Mbps, HALF DUPLEX\n");
	}else{
		printf("LINK DOWN\n");
	}
}

