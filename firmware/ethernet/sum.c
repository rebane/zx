#include <stdio.h>
#include <stdint.h>

int main(){
	uint32_t sum;
	uint16_t i, l;
	uint8_t *data = "\xFF\xFF\xFF\xFF\xFF\xFF\x08\x00\x45\x00\x11\x00\x00\x00\x00\x00\x00\x00\x44\x00\x43\x02\x01\x06\x00";
	// uint8_t *data = "\x89\xAB\xCD";
	sum = 0;
	l = 25;
	for(i = 0; i < l; i += 2){
		if((i + 1) >= l){
			sum += ((uint16_t)data[i] << 8);
		}else{
			sum += ((uint16_t)data[i] << 8) | (uint16_t)data[i + 1];
		}
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	sum = (~sum) & 0xFFFF;
	printf("0x%04X\n", (unsigned int)sum);
	return(0);
}

