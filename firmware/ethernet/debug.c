#include "debug.h"
#include <string.h>
#include "encx24j600.h"
#include "ethernet.h"
#include "udp.h"

#define IP_BROADCAST  ((void *)0x3900)
#define MAC_BROADCAST ((void *)0x3900)

void debug_write(void *dest, const void *my_ip, const void *buf, uint16_t count){
	if(count > (1500 - 43))count = 1500 - 43;
	count = udp_write(&((uint8_t *)dest)[ETHERNET_PAYLOAD_START], IP_BROADCAST, my_ip, 9997, 9998, buf, count);
	count = ethernet_write(dest, MAC_BROADCAST, encx24j600_mac, ETHERNET_TYPE_IPV4, &((uint8_t *)dest)[ETHERNET_HEADER_LEN], count);
	encx24j600_send(dest, count);
}

