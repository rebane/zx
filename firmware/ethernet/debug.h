#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <stdint.h>

void debug_write(void *dest, const void *my_ip, const void *buf, uint16_t count);

#endif

