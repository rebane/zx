#include "ethernet.h"
#include <string.h>

void *ethernet_broadcast = "\xFF\xFF\xFF\xFF\xFF\xFF";

uint16_t ethernet_write(void *dest, const void *dest_mac, const void *src_mac, uint16_t type, const void *buf, uint16_t count){
	uint16_t i;
	for(i = 0; i < 6; i++){
		((uint8_t *)dest)[ETHERNET_HEADER_START + 0 + i] = ((uint8_t *)dest_mac)[i];
		((uint8_t *)dest)[ETHERNET_HEADER_START + 6 + i] = ((uint8_t *)src_mac)[i];
	}
	((uint8_t *)dest)[ETHERNET_HEADER_START + 12] = (type >> 8) & 0xFF;
	((uint8_t *)dest)[ETHERNET_HEADER_START + 13] = (type >> 0) & 0xFF;
	memmove(&((uint8_t *)dest)[ETHERNET_PAYLOAD_START], buf, count);
	return(ETHERNET_HEADER_LEN + count);
}

uint16_t ethernet_type(const void *buf){
	return(((uint16_t)((uint8_t *)buf)[12] << 8) | ((uint8_t *)buf)[13]);
}

