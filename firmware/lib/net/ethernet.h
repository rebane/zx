#ifndef _ETHERNET_H_
#define _ETHERNET_H_

#include <stdint.h>

#define ETHERNET_HEADER_START  0
#define ETHERNET_HEADER_LEN    14
#define ETHERNET_HEADER_END    ((ETHERNET_HEADER_START) + (ETHERNET_HEADER_LEN) - 1)
#define ETHERNET_PAYLOAD_START ((ETHERNET_HEADER_END) + 1)

#define ETHERNET_TYPE_IPV4     0x0800
#define ETHERNET_TYPE_ARP      0x0806

extern void *ethernet_broadcast;

uint16_t ethernet_write(void *dest, const void *dest_mac, const void *src_mac, uint16_t type, const void *buf, uint16_t count);
uint16_t ethernet_type(const void *buf);

#endif

