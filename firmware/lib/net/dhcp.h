#ifndef _DHCP_H_
#define _DHCP_H_

#include <stdint.h>

#define DHCP_HEADER_START   (UDP_PAYLOAD_START)
#define DHCP_HEADER_LEN     240
#define DHCP_HEADER_END     ((DHCP_HEADER_START) + (DHCP_HEADER_LEN) - 1)
#define DHCP_OPTIONS_START  ((DHCP_HEADER_END) + 1)

#define DHCP_SERVER_PORT    67
#define DHCP_CLIENT_PORT    68

#define DHCP_OP_REQUEST     1
#define DHCP_OP_REPLY       2

#define DHCP_HTYPE_ETHERNET 1

#define DHCP_MAGIC_COOKIE   0x63825363

struct dhcp_option_struct{
	uint8_t option;
	uint8_t len;
	const void *param;
	uint16_t next;
};

typedef struct dhcp_option_struct dhcp_option_t;

uint16_t dhcp_write(void *dest, uint32_t xid, const void *siaddr, const void *chaddr, const void *buf, uint16_t count);
uint16_t dhcp_write_discover(void *dest, uint32_t xid, const void *chaddr);
uint16_t dhcp_write_request(void *dest, uint32_t xid, const void *chaddr, const void *yiaddr, const void *siaddr);
uint8_t dhcp_op(const char *buf);
uint8_t dhcp_htype(const char *buf);
uint8_t dhcp_hlen(const char *buf);
uint32_t dhcp_xid(const char *buf);
void dhcp_yiaddr(void *ip, const char *buf);
void dhcp_siaddr(void *ip, const char *buf);
uint8_t dhcp_chaddr_is(const char *buf, const char *chaddr);
uint8_t dhcp_magic_cookie_ok(const char *buf);

void dhcp_option_first(const char *buf, uint16_t len, dhcp_option_t *option);
uint8_t dhcp_option_valid(dhcp_option_t *option);
void dhcp_option_next(const char *buf, uint16_t len, dhcp_option_t *option);

#endif

