#include "ip.h"
#include <string.h>

void *ip_broadcast = "\xFF\xFF\xFF\xFF";
void *ip_zero = "\x00\x00\x00\x00"; 

uint16_t ip_write(void *dest, const void *dest_ip, const void *src_ip, uint8_t protocol, const void *buf, uint16_t count){
	uint32_t sum;
	uint16_t i;
	((uint8_t *)dest)[IP_HEADER_START + 0] = (IP_VERSION_IPV4 << 4) | 5;
	((uint8_t *)dest)[IP_HEADER_START + 1] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 2] = ((IP_HEADER_LEN + count) >> 8) & 0xFF;
	((uint8_t *)dest)[IP_HEADER_START + 3] = ((IP_HEADER_LEN + count) >> 0) & 0xFF;
	((uint8_t *)dest)[IP_HEADER_START + 4] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 5] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 6] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 7] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 8] = 64;
	((uint8_t *)dest)[IP_HEADER_START + 9] = protocol;
	((uint8_t *)dest)[IP_HEADER_START + 10] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 11] = 0x00;
	((uint8_t *)dest)[IP_HEADER_START + 12] = ((uint8_t *)src_ip)[0];
	((uint8_t *)dest)[IP_HEADER_START + 13] = ((uint8_t *)src_ip)[1];
	((uint8_t *)dest)[IP_HEADER_START + 14] = ((uint8_t *)src_ip)[2];
	((uint8_t *)dest)[IP_HEADER_START + 15] = ((uint8_t *)src_ip)[3];
	((uint8_t *)dest)[IP_HEADER_START + 16] = ((uint8_t *)dest_ip)[0];
	((uint8_t *)dest)[IP_HEADER_START + 17] = ((uint8_t *)dest_ip)[1];
	((uint8_t *)dest)[IP_HEADER_START + 18] = ((uint8_t *)dest_ip)[2];
	((uint8_t *)dest)[IP_HEADER_START + 19] = ((uint8_t *)dest_ip)[3];
	sum = 0;
	for(i = (IP_HEADER_START + 0); i < (IP_HEADER_START + IP_HEADER_LEN); i += 2){
		sum += ((uint16_t)((uint8_t *)dest)[i] << 8) | (uint16_t)((uint8_t *)dest)[i + 1];
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	sum = (~sum) & 0xFFFF;
	((uint8_t *)dest)[IP_HEADER_START + 10] = (sum >> 8) & 0xFF;
	((uint8_t *)dest)[IP_HEADER_START + 11] = (sum >> 0) & 0xFF;
	memmove(&((uint8_t *)dest)[IP_PAYLOAD_START], buf, count);
	return(IP_HEADER_LEN + count);
}

uint8_t ip_version(const void *buf){
	return(((uint8_t *)buf)[IP_HEADER_START + 0] >> 4);
}

uint16_t ip_len(const void *buf){
	return(((uint16_t)((uint8_t *)buf)[IP_HEADER_START + 2] << 8) | ((uint8_t *)buf)[IP_HEADER_START + 3]);
}

uint8_t ip_protocol(const void *buf){
	return(((uint8_t *)buf)[IP_HEADER_START + 9]);
}

uint8_t ip_chk_ok(const void *buf){
	uint32_t sum;
	uint16_t i;
	sum = 0;
	for(i = (IP_HEADER_START + 0); i < (IP_HEADER_START + IP_HEADER_LEN); i += 2){
		if(i == (IP_HEADER_START + 10))continue;
		sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	sum = (~sum) & 0xFFFF;
	return(sum == (((uint16_t)((uint8_t *)buf)[IP_HEADER_START + 10] << 8) | ((uint8_t *)buf)[IP_HEADER_START + 11]));
}

