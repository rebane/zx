#include "udp.h"
#include <string.h>
#include "ip.h"

uint16_t udp_write(void *dest, const void *dest_ip, const void *src_ip, uint16_t dest_port, uint16_t src_port, const void *buf, uint16_t count){
#ifdef UDP_USE_CHK
	uint16_t sum;
#endif
	((uint8_t *)dest)[UDP_HEADER_START + 0] = (src_port >> 8) & 0xFF;
	((uint8_t *)dest)[UDP_HEADER_START + 1] = (src_port >> 0) & 0xFF;
	((uint8_t *)dest)[UDP_HEADER_START + 2] = (dest_port >> 8) & 0xFF;
	((uint8_t *)dest)[UDP_HEADER_START + 3] = (dest_port >> 0) & 0xFF;
	((uint8_t *)dest)[UDP_HEADER_START + 4] = ((UDP_HEADER_LEN + count) >> 8) & 0xFF;
	((uint8_t *)dest)[UDP_HEADER_START + 5] = ((UDP_HEADER_LEN + count) >> 0) & 0xFF;
	memmove(&((uint8_t *)dest)[UDP_PAYLOAD_START], buf, count);
#ifdef UDP_USE_CHK
	sum = udp_chk(dest_ip, src_ip, dest);
	((uint8_t *)dest)[UDP_HEADER_START + 6] = (sum >> 8) & 0xFF;
	((uint8_t *)dest)[UDP_HEADER_START + 7] = (sum >> 0) & 0xFF;
#else
	((uint8_t *)dest)[UDP_HEADER_START + 6] = 0x00;
	((uint8_t *)dest)[UDP_HEADER_START + 7] = 0x00;
#endif
	return(ip_write(dest, dest_ip, src_ip, IP_PROTOCOL_UDP, &((uint8_t *)dest)[UDP_HEADER_START], UDP_HEADER_LEN + count));
}

uint16_t udp_dst_port(const void *buf){
	return(((uint16_t)((uint8_t *)buf)[UDP_HEADER_START + 2] << 8) | ((uint8_t *)buf)[UDP_HEADER_START + 3]);
}

uint16_t udp_src_port(const void *buf){
	return(((uint16_t)((uint8_t *)buf)[UDP_HEADER_START + 0] << 8) | ((uint8_t *)buf)[UDP_HEADER_START + 1]);
}

uint16_t udp_len(const void *buf){
	return(((uint16_t)((uint8_t *)buf)[UDP_HEADER_START + 4] << 8) | ((uint8_t *)buf)[UDP_HEADER_START + 5]);
}

uint16_t udp_chk(const void *dest_ip, const void *src_ip, const void *buf){
	uint32_t sum;
	uint16_t i, l;
	sum = 0;
	sum += ((uint16_t)((uint8_t *)src_ip)[0] << 8) | (uint16_t)((uint8_t *)src_ip)[1];
	sum += ((uint16_t)((uint8_t *)src_ip)[2] << 8) | (uint16_t)((uint8_t *)src_ip)[3];
	sum += ((uint16_t)((uint8_t *)dest_ip)[0] << 8) | (uint16_t)((uint8_t *)dest_ip)[1];
	sum += ((uint16_t)((uint8_t *)dest_ip)[2] << 8) | (uint16_t)((uint8_t *)dest_ip)[3];
	sum += (uint16_t)IP_PROTOCOL_UDP;
	l = ((uint16_t)((uint8_t *)buf)[UDP_HEADER_START + 4] << 8) | ((uint8_t *)buf)[UDP_HEADER_START + 5];
	sum += l;
	for(i = (UDP_HEADER_START + 0); i < (UDP_HEADER_START + l); i += 2){
		if(i == (UDP_HEADER_START + 6))continue;
		if((i + 1) >= (UDP_HEADER_START + l)){
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8);
		}else{
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
		}
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	return((~sum) & 0xFFFF);
}

uint8_t udp_chk_ok(const void *buf){
	uint16_t sum;
	// UDP checksum computation is optional for IPv4. If a checksum is not used it should be set to the value zero.
	sum = ((uint16_t)((uint8_t *)buf)[UDP_HEADER_START + 6] << 8) | ((uint8_t *)buf)[UDP_HEADER_START + 7];
	if(sum == 0x0000)return(1);
	return(sum == (udp_chk(&((uint8_t *)buf)[IP_HEADER_START + 16], &((uint8_t *)buf)[IP_HEADER_START + 12], buf)));
}

