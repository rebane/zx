#include "dhcp.h"
#include "udp.h"

uint16_t dhcp_write(void *dest, uint32_t xid, const void *siaddr, const void *chaddr, const void *buf, uint16_t count){
	uint16_t i;
	((uint8_t *)dest)[DHCP_HEADER_START + 0] = DHCP_OP_REQUEST;
	((uint8_t *)dest)[DHCP_HEADER_START + 1] = DHCP_HTYPE_ETHERNET;
	((uint8_t *)dest)[DHCP_HEADER_START + 2] = 0x06; // HLEN
	((uint8_t *)dest)[DHCP_HEADER_START + 3] = 0x00; // HOPS
	((uint8_t *)dest)[DHCP_HEADER_START + 4] = (xid >> 24) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 5] = (xid >> 16) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 6] = (xid >> 8) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 7] = (xid >> 0) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 8] = 0x00; // SECS
	((uint8_t *)dest)[DHCP_HEADER_START + 9] = 0x00;
	((uint8_t *)dest)[DHCP_HEADER_START + 10] = 0x80; // FLAGS
	((uint8_t *)dest)[DHCP_HEADER_START + 11] = 0x00;
	for(i = 0; i < 8; i++){ // CIADDR, YIADDR
		((uint8_t *)dest)[DHCP_HEADER_START + 12 + i] = 0x00;
	}
	((uint8_t *)dest)[DHCP_HEADER_START + 20] = ((uint8_t *)siaddr)[0];
	((uint8_t *)dest)[DHCP_HEADER_START + 21] = ((uint8_t *)siaddr)[1];
	((uint8_t *)dest)[DHCP_HEADER_START + 22] = ((uint8_t *)siaddr)[2];
	((uint8_t *)dest)[DHCP_HEADER_START + 23] = ((uint8_t *)siaddr)[3];
	((uint8_t *)dest)[DHCP_HEADER_START + 24] = 0;
	((uint8_t *)dest)[DHCP_HEADER_START + 25] = 0;
	((uint8_t *)dest)[DHCP_HEADER_START + 26] = 0;
	((uint8_t *)dest)[DHCP_HEADER_START + 27] = 0;
	for(i = 0; i < 6; i++){ // CHADDR
		((uint8_t *)dest)[DHCP_HEADER_START + 28 + i] = ((uint8_t *)chaddr)[i];
	}
	for(i = 0; i < (10 + 192); i++){ // CHADDR + BOOTP legacy
		((uint8_t *)dest)[DHCP_HEADER_START + 34 + i] = 0x00;
	}
	((uint8_t *)dest)[DHCP_HEADER_START + 236] = (DHCP_MAGIC_COOKIE >> 24) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 237] = (DHCP_MAGIC_COOKIE >> 16) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 238] = (DHCP_MAGIC_COOKIE >> 8) & 0xFF;
	((uint8_t *)dest)[DHCP_HEADER_START + 239] = (DHCP_MAGIC_COOKIE >> 0) & 0xFF;
	if(&((uint8_t *)dest)[DHCP_OPTIONS_START] != buf){
		for(i = 0; i < count; i++){
			((uint8_t *)dest)[DHCP_OPTIONS_START + i] = ((uint8_t *)buf)[i];
		}
	}
	// return(udp_write(dest, ip_broadcast, ip_zero, 67, 68, &((uint8_t *)dest)[DHCP_HEADER_START], DHCP_HEADER_LEN + count));
	return(udp_write(dest, "\xFF\xFF\xFF\xFF", "\x00\x00\x00\x00", DHCP_SERVER_PORT, DHCP_CLIENT_PORT, &((uint8_t *)dest)[DHCP_HEADER_START], DHCP_HEADER_LEN + count));
}

uint16_t dhcp_write_discover(void *dest, uint32_t xid, const void *chaddr){
	((uint8_t *)dest)[DHCP_OPTIONS_START + 0] = 53;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 1] = 1;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 2] = 1;

	((uint8_t *)dest)[DHCP_OPTIONS_START + 3] = 55;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 4] = 3;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 5] = 1;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 6] = 3;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 7] = 6;

	((uint8_t *)dest)[DHCP_OPTIONS_START + 8] = 0xFF;
	return(dhcp_write(dest, xid, "\x00\x00\x00\x00", chaddr, &((uint8_t *)dest)[DHCP_OPTIONS_START + 0], 9));
}

uint16_t dhcp_write_request(void *dest, uint32_t xid, const void *chaddr, const void *yiaddr, const void *siaddr){
	((uint8_t *)dest)[DHCP_OPTIONS_START + 0] = 53;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 1] = 1;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 2] = 3;

	((uint8_t *)dest)[DHCP_OPTIONS_START + 3] = 50;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 4] = 4;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 5] = ((uint8_t *)yiaddr)[0];
	((uint8_t *)dest)[DHCP_OPTIONS_START + 6] = ((uint8_t *)yiaddr)[1];
	((uint8_t *)dest)[DHCP_OPTIONS_START + 7] = ((uint8_t *)yiaddr)[2];
	((uint8_t *)dest)[DHCP_OPTIONS_START + 8] = ((uint8_t *)yiaddr)[3];

	((uint8_t *)dest)[DHCP_OPTIONS_START + 9] = 54;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 10] = 4;
	((uint8_t *)dest)[DHCP_OPTIONS_START + 11] = ((uint8_t *)siaddr)[0];
	((uint8_t *)dest)[DHCP_OPTIONS_START + 12] = ((uint8_t *)siaddr)[1];
	((uint8_t *)dest)[DHCP_OPTIONS_START + 13] = ((uint8_t *)siaddr)[2];
	((uint8_t *)dest)[DHCP_OPTIONS_START + 14] = ((uint8_t *)siaddr)[3];

	((uint8_t *)dest)[DHCP_OPTIONS_START + 15] = 0xFF;
	return(dhcp_write(dest, xid, siaddr, chaddr, &((uint8_t *)dest)[DHCP_OPTIONS_START + 0], 16));
}

uint8_t dhcp_op(const char *buf){
	return(((uint8_t *)buf)[DHCP_HEADER_START + 0]);
}

uint8_t dhcp_htype(const char *buf){
	return(((uint8_t *)buf)[DHCP_HEADER_START + 1]);
}

uint8_t dhcp_hlen(const char *buf){
	return(((uint8_t *)buf)[DHCP_HEADER_START + 2]);
}

uint32_t dhcp_xid(const char *buf){
	return(((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 4] << 24) | ((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 5] << 16) | ((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 6] << 8) | ((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 7] << 0));
}

void dhcp_yiaddr(void *ip, const char *buf){
	((uint8_t *)ip)[0] = ((uint8_t *)buf)[DHCP_HEADER_START + 16];
	((uint8_t *)ip)[1] = ((uint8_t *)buf)[DHCP_HEADER_START + 17];
	((uint8_t *)ip)[2] = ((uint8_t *)buf)[DHCP_HEADER_START + 18];
	((uint8_t *)ip)[3] = ((uint8_t *)buf)[DHCP_HEADER_START + 19];
}

void dhcp_siaddr(void *ip, const char *buf){
	((uint8_t *)ip)[0] = ((uint8_t *)buf)[DHCP_HEADER_START + 20];
	((uint8_t *)ip)[1] = ((uint8_t *)buf)[DHCP_HEADER_START + 21];
	((uint8_t *)ip)[2] = ((uint8_t *)buf)[DHCP_HEADER_START + 22];
	((uint8_t *)ip)[3] = ((uint8_t *)buf)[DHCP_HEADER_START + 23];
}

uint8_t dhcp_chaddr_is(const char *buf, const char *chaddr){
	uint8_t i;
	for(i = 0; i < 6; i++){
		if(((uint8_t *)buf)[DHCP_HEADER_START + 28 + i] != ((uint8_t *)chaddr)[i])return(0);
	}
	return(1);
}

uint8_t dhcp_magic_cookie_ok(const char *buf){
	return((((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 236] << 24) | ((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 237] << 16) | ((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 238] << 8) | ((uint32_t)((uint8_t *)buf)[DHCP_HEADER_START + 239] << 0)) == DHCP_MAGIC_COOKIE);
}

void dhcp_option_first(const char *buf, uint16_t len, dhcp_option_t *option){
	option->next = DHCP_OPTIONS_START;
	dhcp_option_next(buf, len, option);
}

void dhcp_option_next(const char *buf, uint16_t len, dhcp_option_t *option){
	if(len < (option->next + 2)){
		option->option = 0xFF;
		return;
	}
	option->len = ((uint8_t *)buf)[option->next + 1];
	if(len < (option->next + option->len + 2)){
		option->option = 0xFF;
		return;
	}
	option->option = ((uint8_t *)buf)[option->next];
	option->param = &((uint8_t *)buf)[option->next + 2];
	option->next += option->len + 2;
}

uint8_t dhcp_option_valid(dhcp_option_t *option){
	return(option->option != 0xFF);
}

