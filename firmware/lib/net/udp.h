#ifndef _UDP_H_
#define _UDP_H_

#include <stdint.h>
#include "ip.h"

// #define UDP_USE_CHK

#define UDP_HEADER_START  (IP_PAYLOAD_START)
#define UDP_HEADER_LEN    8
#define UDP_HEADER_END    ((UDP_HEADER_START) + (UDP_HEADER_LEN) - 1)
#define UDP_PAYLOAD_START ((UDP_HEADER_END) + 1)

uint16_t udp_write(void *dest, const void *dest_ip, const void *src_ip, uint16_t dest_port, uint16_t src_port, const void *buf, uint16_t count);
uint16_t udp_chk(const void *dest_ip, const void *src_ip, const void *buf);
uint16_t udp_dst_port(const void *buf);
uint16_t udp_src_port(const void *buf);
uint16_t udp_len(const void *buf);
uint8_t udp_chk_ok(const void *buf);

#endif

