#ifndef _IP_H_
#define _IP_H_

#include <stdint.h>

#define IP_HEADER_START  0
#define IP_HEADER_LEN    20
#define IP_HEADER_END    ((IP_HEADER_START) + (IP_HEADER_LEN) - 1)
#define IP_PAYLOAD_START ((IP_HEADER_END) + 1)

#define IP_VERSION_IPV4  4
#define IP_PROTOCOL_UDP  0x11

extern void *ip_broadcast, *ip_zero;

uint16_t ip_write(void *dest, const void *dest_ip, const void *src_ip, uint8_t protocol, const void *buf, uint16_t count);
uint8_t ip_version(const void *buf);
uint16_t ip_len(const void *buf);
uint8_t ip_protocol(const void *buf);
uint8_t ip_chk_ok(const void *buf);

#endif

