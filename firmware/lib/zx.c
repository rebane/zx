#include "zx.h"
#include <string.h>
#include "z80.h"

#define zx_console_x           zx_data_base[0]
#define zx_console_y           zx_data_base[1]
#define zx_console_ink         zx_data_base[2]
#define zx_console_paper       zx_data_base[3]
#define zx_console_bright      zx_data_base[4]
#define zx_console_flash       zx_data_base[5]
#define zx_console_border      zx_data_base[6]
#define zx_console_inverse     zx_data_base[7]
#define zx_console_over        zx_data_base[8]
#define zx_console_cursor      zx_data_base[9]
#define zx_console_cursor_attr zx_data_base[10]
#define zx_console_wrap        zx_data_base[11]

static uint8_t *zx_data_base, *zx_font_base;
static uint8_t *(*zx_font_get)(uint16_t c);
#ifndef ZX_DISABLE_UTF8
static uint8_t zx_utf8_len, zx_utf8_loc, zx_utf8_buf[4];
#endif
#ifndef ZX_DISABLE_TIMER
static volatile uint32_t zx_msec_div_20;
#endif
#ifndef ZX_DISABLE_KEYBOARD
static volatile uint8_t zx_kb_state, zx_kb_timer;
static volatile uint8_t zx_kb_fifo_in, zx_kb_fifo_out, zx_kb_fifo_len, zx_kb_fifo[16];
#ifndef ZX_DISABLE_KEYMAP
static const uint8_t zx_keymap[8][4][5] = {
	{{0x09, 0x00, 0x00, 0x00, 0x00}, { 'B',  'N',  'M', 0x00, 0x00}, { '*',  ',',  '.', 0x00, 0x00}, { 'b',  'n',  'm', 0x00, 0x00}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, { 'T',  'R',  'E',  'W',  'Q'}, { '>',  '<', 0x00, 0x00, 0x00}, { 't',  'r',  'e',  'w',  'q'}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, { 'G',  'F',  'D',  'S',  'A'}, { '}',  '{', '\\',  '|',  '~'}, { 'g',  'f',  'd',  's',  'a'}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, {0x00, 0x00, 0x00, 0x00, 0x1B}, { '%',  '$',  '#',  '@',  '!'}, { '5',  '4',  '3',  '2',  '1'}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, { 'V',  'C',  'X',  'Z', 0x00}, { '/',  '?', 0xA3,  ':', 0x00}, { 'v',  'c',  'x',  'z',  ' '}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, { 'Y',  'U',  'I',  'O',  'P'}, { '[',  ']', 0x00,  ';', '\"'}, { 'y',  'u',  'i',  'o',  'p'}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, { 'H',  'J',  'K',  'L', '\n'}, { '^',  '-',  '+',  '=', 0x00}, { 'h',  'j',  'k',  'l', '\n'}},
	{{0x00, 0x00, 0x00, 0x00, 0x00}, {0x00, 0x00, 0x00, 0x00, 0x08}, { '&', '\'',  '(',  ')',  '_'}, { '6',  '7',  '8',  '9',  '0'}}
};
#endif
#endif

static void zx_draw_cursor(uint8_t on);
static uint8_t zx_attr_make(uint8_t ink, uint8_t paper, uint8_t bright, uint8_t flash);
static uint8_t zx_contrast(uint8_t color);
static void zx_interrupt();

void zx_init(uint8_t *(*font_get)(uint16_t c)){
	zx_data_base = (uint8_t *)ZX_CONSOLE;
	zx_font_base = (uint8_t *)ZX_ROM_FONT;
	zx_font_get = font_get;
#ifndef ZX_DISABLE_UTF8
	zx_utf8_len = 0;
#endif
#if !defined (ZX_DISABLE_TIMER) && !defined (ZX_DISABLE_KEYBOARD)
	__asm__("di");
#ifndef ZX_DISABLE_TIMER
	zx_msec_div_20 = 0;
#endif
#ifndef ZX_DISABLE_KEYBOARD
	zx_kb_state = zx_kb_timer = 0;
	zx_kb_fifo_in = zx_kb_fifo_out = zx_kb_fifo_len = 0;
#endif
	*(uint8_t *)0xFFF4 = 0xC3;
	*(void **)0xFFF5 = (void *)zx_interrupt;
	*(uint8_t *)0xFFFF = 0x18;
	__asm__("ld a, #0x39");
	__asm__("ld i, a");
	__asm__("im 2");
	__asm__("ei");
#endif
}

#ifndef ZX_DISABLE_TIMER
uint32_t zx_timer_get(uint16_t *msec){
	uint32_t msec_div_20;
	__asm__("di");
	msec_div_20 = zx_msec_div_20;
	__asm__("ei");
	if(msec != ((void *)0))*msec = (msec_div_20 % 50) * 20;
	return(msec_div_20 / 50);
}
#endif

#ifndef ZX_DISABLE_FONT
void zx_font(void *font_base){
	zx_font_base = (uint8_t *)font_base;
}
#endif

#ifndef ZX_DISABLE_DEFAULT
void zx_default(){
	zx_console_x = 0;
	zx_console_y = 0;
	zx_console_ink = ZX_INK_DEFAULT;
	zx_console_paper = ZX_PAPER_DEFAULT;
	zx_console_bright = ZX_BRIGHT_DEFAULT;
	zx_console_flash = ZX_FLASH_DEFAULT;
#ifndef ZX_DISABLE_INVERSE
	zx_console_inverse = ZX_INVERSE_DEFAULT;
#endif
#ifndef ZX_DISABLE_OVER
	zx_console_over = ZX_OVER_DEFAULT;
#endif
#ifndef ZX_DISABLE_CURSOR
	zx_console_cursor = ZX_CURSOR_DEFAULT;
	zx_console_cursor_attr = ZX_CURSOR_ATTR_DEFAULT;
#endif
	zx_console_wrap = ZX_WRAP_DEFAULT;
}
#endif

#ifndef ZX_DISABLE_CLS
void zx_cls(){
	uint8_t attr;
	zx_console_x = zx_console_y = 0;
	attr = zx_attr_make(zx_console_ink, zx_console_paper, zx_console_bright, zx_console_flash);
	memset((void *)ZX_FRAMEBUFFER_PIXELS, 0x00, ZX_FRAMEBUFFER_PIXELS_LEN);
	memset((void *)ZX_FRAMEBUFFER_ATTRS, attr, ZX_FRAMEBUFFER_ATTRS_LEN);
#ifndef ZX_DISABLE_CURSOR
	zx_console_cursor_attr = attr;
	zx_draw_cursor(1);
#endif
}
#endif

#ifndef ZX_DISABLE_BORDER
void zx_border(uint8_t border){
	zx_console_border = (border & 0x07);
	out(ZX_PORT_ULA, zx_console_border);
}
#endif

#ifndef ZX_DISABLE_INK
void zx_ink(uint8_t ink){
	zx_console_ink = ink;
}
#endif

#ifndef ZX_DISABLE_PAPER
void zx_paper(uint8_t paper){
	zx_console_paper = paper;
}
#endif

#ifndef ZX_DISABLE_BRIGHT
void zx_bright(uint8_t bright){
	zx_console_bright = bright;
}
#endif

#ifndef ZX_DISABLE_FLASH
void zx_flash(uint8_t flash){
	zx_console_flash = flash;
}
#endif

#ifndef ZX_DISABLE_OVER
void zx_over(uint8_t over){
	zx_console_over = over;
}
#endif

#ifndef ZX_DISABLE_INVERSE
void zx_inverse(uint8_t inverse){
	zx_console_inverse = inverse;
}
#endif

#ifndef ZX_DISABLE_COLOR
void zx_color(uint8_t ink, uint8_t paper, uint8_t bright, uint8_t flash){
	zx_console_ink = ink;
	zx_console_paper = paper;
	zx_console_bright = bright;
	zx_console_flash = flash;
}
#endif

#ifndef ZX_DISABLE_AT
void zx_at(uint8_t x, uint8_t y){
#ifndef ZX_DISABLE_CURSOR
	zx_draw_cursor(0);
#endif
	zx_console_x = x;
	if(zx_console_x > 31)zx_console_x = 31;
	zx_console_y = y;
	if(zx_console_y > 23)zx_console_y = 23;
#ifndef ZX_DISABLE_CURSOR
	zx_console_cursor_attr = *(uint8_t *)(ZX_FRAMEBUFFER_ATTRS + zx_console_x + ((uint16_t)zx_console_y * 32));
	zx_draw_cursor(1);
#endif
}
#endif

#ifndef ZX_DISABLE_CURSOR
void zx_cursor(uint8_t cursor){
	zx_console_cursor = cursor;
	zx_draw_cursor(1);
}
#endif

#ifndef ZX_DISABLE_WRAP
void zx_wrap(uint8_t wrap){
	zx_console_wrap = wrap;
}
#endif

#ifndef ZX_DISABLE_ATTR
uint8_t zx_attr(uint8_t x, uint8_t y){
	if(x > 31)x = 31;
	if(y > 23)y = 23;
	return(*(uint8_t *)(ZX_FRAMEBUFFER_ATTRS + x + ((uint16_t)y * 32)));
}
#endif

#ifndef ZX_DISABLE_CURSOR
static void zx_draw_cursor(uint8_t on){
	if(zx_console_cursor && on){
		*(uint8_t *)(ZX_FRAMEBUFFER_ATTRS + zx_console_x + ((uint16_t)zx_console_y * 32)) = (zx_console_cursor_attr & 0xC0) | ((zx_console_cursor_attr & 0x38) >> 3) | ((zx_console_cursor_attr & 0x07) << 3);
	}else{
		*(uint8_t *)(ZX_FRAMEBUFFER_ATTRS + zx_console_x + ((uint16_t)zx_console_y * 32)) = zx_console_cursor_attr;
	}
}
#endif

static uint8_t zx_attr_make(uint8_t ink, uint8_t paper, uint8_t bright, uint8_t flash){
	return((ink & 0x07) | ((paper & 0x07) << 3) | ((bright & 0x01) << 6) | ((flash & 0x01) << 7));
}

static uint8_t zx_contrast(uint8_t color){
	if(color < ZX_COLOR_GREEN)return(ZX_COLOR_WHITE);
	return(ZX_COLOR_BLACK);
}

void zx_putchar(uint16_t c){
	uint8_t i, *f, s, ink, paper, bright, flash, *ptr;
	uint16_t p;
#ifndef ZX_DISABLE_CURSOR
	zx_draw_cursor(0);
#endif
	if(c == '\n'){
		zx_console_x = 0;
		zx_console_y++;
	}else if(c == '\r'){
		zx_console_x = 0;
	}else if(c == 0x08){
		if(zx_console_x)zx_console_x--;
	}else if((c > 31) && (c != 127)){
		if(zx_font_get != NULL){
			f = zx_font_get(c);
		}else{
			f = NULL;
		}
		if(f == NULL){
			if(c == 163){        // £
				f = &zx_font_base[96 * 8];
			}else if(c == 169){  // ©
				f = &zx_font_base[127 * 8];
			}else if(c == 180){  // ´
				f = &zx_font_base[39 * 8];
			}else if(c == 8593){ // ↑
				f = &zx_font_base[94 * 8];
			}else if((c < 127) && (c != 39) && (c != 94) && (c != 96)){
				f = &zx_font_base[c * 8];
			}
		}
		if(f == NULL)f = &zx_font_base[(uint16_t)'?' * 8];
		if(zx_console_y < 8){
			p = ZX_FRAMEBUFFER_PIXELS + 0x0000 + ((uint16_t)zx_console_y * 32);
		}else if(zx_console_y < 16){
			p = ZX_FRAMEBUFFER_PIXELS + 0x0800 + (((uint16_t)zx_console_y - 8) * 32);
		}else{
			p = ZX_FRAMEBUFFER_PIXELS + 0x1000 + (((uint16_t)zx_console_y - 16) * 32);
		}
		ptr = (uint8_t *)(ZX_FRAMEBUFFER_ATTRS + zx_console_x + ((uint16_t)zx_console_y * 32));
		ink = zx_console_ink;
		paper = zx_console_paper;
		bright = zx_console_bright;
		flash = zx_console_flash;
#ifndef ZX_DISABLE_TRANSPARENT
		if(ink == ZX_INK_TRANSPARENT)ink = (*ptr & 0x07);
		if(paper == ZX_PAPER_TRANSPARENT)paper = ((*ptr >> 3) & 0x07);
		if(bright == ZX_BRIGHT_TRANSPARENT)bright = ((*ptr >> 6) & 0x01);
		if(flash == ZX_FLASH_TRANSPARENT)flash = (*ptr >> 7);
#endif
#ifndef ZX_DISABLE_CONTRAST
		if((ink == ZX_INK_CONTRAST) && (paper == ZX_PAPER_CONTRAST)){
			ink = ZX_INK_DEFAULT;
			paper = ZX_PAPER_DEFAULT;
		}
		if(ink == ZX_INK_CONTRAST)ink = zx_contrast(paper);
		if(paper == ZX_PAPER_CONTRAST)paper = zx_contrast(ink);
#endif
		*ptr = zx_attr_make(ink, paper, bright, flash);
		p += zx_console_x;
		for(i = 0; i < 8; i++){
			s = f[i];
#ifndef ZX_DISABLE_INVERSE
			if(zx_console_inverse)s = s ^ 0xFF;
#endif
#ifndef ZX_DISABLE_OVER
			if(zx_console_over)s = s ^ *(uint8_t *)(p + ((uint16_t)i * 256));
#endif
			*(uint8_t *)(p + ((uint16_t)i * 256)) = s;
		}
		zx_console_x++;
		if(zx_console_x > 31){
			zx_console_x = 0;
			if(zx_console_wrap)zx_console_y++;
		}
	}
	if(zx_console_y > 23){
		zx_console_y = 23;
		for(i = 0; i < 8; i++){
			memcpy((void *)(ZX_FRAMEBUFFER_PIXELS + 0x0000 + ((uint16_t)i * 256)), (void *)(ZX_FRAMEBUFFER_PIXELS + 0x0020 + ((uint16_t)i * 256)), 32 * 7);
			memcpy((void *)(ZX_FRAMEBUFFER_PIXELS + 0x00E0 + ((uint16_t)i * 256)), (void *)(ZX_FRAMEBUFFER_PIXELS + 0x0800 + ((uint16_t)i * 256)), 32);
			memcpy((void *)(ZX_FRAMEBUFFER_PIXELS + 0x0800 + ((uint16_t)i * 256)), (void *)(ZX_FRAMEBUFFER_PIXELS + 0x0820 + ((uint16_t)i * 256)), 32 * 7);
			memcpy((void *)(ZX_FRAMEBUFFER_PIXELS + 0x08E0 + ((uint16_t)i * 256)), (void *)(ZX_FRAMEBUFFER_PIXELS + 0x1000 + ((uint16_t)i * 256)), 32);
			memcpy((void *)(ZX_FRAMEBUFFER_PIXELS + 0x1000 + ((uint16_t)i * 256)), (void *)(ZX_FRAMEBUFFER_PIXELS + 0x1020 + ((uint16_t)i * 256)), 32 * 7);
			memset((void *)(ZX_FRAMEBUFFER_PIXELS + 0x10E0 + ((uint16_t)i * 256)), 0x00, 32);
		}
		memcpy((void *)ZX_FRAMEBUFFER_ATTRS, (void *)(ZX_FRAMEBUFFER_ATTRS + 32), (ZX_FRAMEBUFFER_ATTRS_LEN - 32));
		// memset((void *)0x5AE0, zx_attr_make(zx_console_ink, zx_console_paper, zx_console_bright, zx_console_flash), 32);
	}
#ifndef ZX_DISABLE_CURSOR
	zx_console_cursor_attr = *(uint8_t *)(ZX_FRAMEBUFFER_ATTRS + zx_console_x + ((uint16_t)zx_console_y * 32));
	zx_draw_cursor(1);
#endif
}

#ifndef ZX_DISABLE_UTF8
void zx_putchar_utf8(uint8_t c){
	if(zx_utf8_len){
		zx_utf8_loc++;
		zx_utf8_buf[zx_utf8_loc] = c;
		if(zx_utf8_loc == zx_utf8_len){
			if(zx_utf8_len == 1){
				zx_putchar(((uint16_t)(zx_utf8_buf[0] & 0x1F) << 6) | (zx_utf8_buf[1] & 0x3F));
			}else if(zx_utf8_len == 2){
				zx_putchar(((uint16_t)(zx_utf8_buf[0] & 0x0F) << 12) | ((uint16_t)(zx_utf8_buf[1] & 0x3F) << 6) | (zx_utf8_buf[2] & 0x3F));
			}else{
				zx_putchar('?');
			}
			zx_utf8_len = 0;
		}
	}else{
		if((c & 0xE0) == 0xC0){
			zx_utf8_buf[0] = c;
			zx_utf8_len = 1;
			zx_utf8_loc = 0;
		}else if((c & 0xF0) == 0xE0){
			zx_utf8_buf[0] = c;
			zx_utf8_len = 2;
			zx_utf8_loc = 0;
		}else if((c & 0xF8) == 0xF0){
			zx_utf8_buf[0] = c;
			zx_utf8_len = 3;
			zx_utf8_loc = 0;
		}else{ // etc...
			zx_putchar(c);
		}
	}
}
#endif

void putchar(int c){
#ifndef ZX_DISABLE_UTF8
	zx_putchar_utf8((uint8_t)c);
#else
	zx_putchar((uint8_t)c);
#endif
}

#ifndef ZX_DISABLE_KEYBOARD
uint8_t zx_getkey(){
	uint8_t c;
	if(!zx_kb_fifo_len)return(0);
	__asm__("di");
	c = zx_kb_fifo[zx_kb_fifo_out];
	zx_kb_fifo_len--;
	__asm__("ei");
	zx_kb_fifo_out++;
	if(zx_kb_fifo_out == 16)zx_kb_fifo_out = 0;
	return(c);
}
#ifndef ZX_DISABLE_KEYMAP
uint8_t zx_getchar(uint8_t c){
	if(c == 0)return(0);
	return(zx_keymap[(c >> 3) & 0x07][c >> 6][(c & 0x07) - 1]);
}
#endif
#endif

#if !defined (ZX_DISABLE_TIMER) && !defined (ZX_DISABLE_KEYBOARD)
static void zx_interrupt() __naked{
	__asm
	di
	push af
#ifndef ZX_DISABLE_TIMER
	push iy
	ld	iy,#_zx_msec_div_20
	inc	0 (iy)
	jr	NZ,00001$
	ld	iy,#_zx_msec_div_20
	inc	1 (iy)
	jr	NZ,00001$
	ld	iy,#_zx_msec_div_20
	inc	2 (iy)
	jr	NZ,00001$
	ld	iy,#_zx_msec_div_20
	inc	3 (iy)
00001$:
	pop iy
#endif
#ifndef ZX_DISABLE_KEYBOARD
	push bc
	push de
	push hl

	ld c, #0xFE
	ld b, #0xFE
	in a, (c)
	rra
	ld d, a
	rl l
	ld b, #0x7F
	in a, (c)
	rra
	rl d
	rra
	rl l
	// B, N, M
	ld e, #3
	ld h, #0
00002$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00002$
	// Z, X, C, V, SPACE
	ld e, #5
	inc h
	ld a, d
00003$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00003$
	// A, S, D, F, G
	ld e, #5
	inc h
	ld b, #0xFD
	in a, (c)
00004$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00004$
	// H, J, K, L, ENTER
	ld e, #5
	inc h
	ld b, #0xBF
	in a, (c)
00005$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00005$
	// Q, W, E, R, T
	ld e, #5
	inc h
	ld b, #0xFB
	in a, (c)
00006$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00006$
	// Y, U, I, O, P
	ld e, #5
	inc h
	ld b, #0xDF
	in a, (c)
00007$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00007$
	// 1, 2, 3, 4, 5
	ld e, #5
	inc h
	ld b, #0xF7
	in a, (c)
00008$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00008$
	// 6, 7, 8, 9, 0
	ld e, #5
	inc h
	ld b, #0xEF
	in a, (c)
00009$:
	srl a
	jr nc, #00010$
	dec e
	jr nz, #00009$

00010$:
	rr h
	rl l
	rr h
	rl l
	rr h
	rl l
	rl e
	rl e
	rl e
	rl e
	rl e
	rl e
	rl l
	rl e
	rl l
	rl e
	rl l

	// if(zx_kb_state != l){ zx_kb_state = l; zx_kb_timer = 0; }
	ld a, (#_zx_kb_state)
	sub a, l
	jr z, 00011$
	ld a, l
	ld (#_zx_kb_state), a
	xor a
	ld (#_zx_kb_timer), a
	jp 00013$
00011$:
	// if(zx_kb_state & 0x07)...
	ld a, l
	and a, #0x07
	jp z, 00013$
	// zx_kb_timer++;
	ld hl, #_zx_kb_timer
	inc (hl)
	// if(zx_kb_timer == ...)
	ld a, (#_zx_kb_timer)
	sub a, #2
	jr z, 00012$
	// if(zx_kb_timer == ...){ zx_kb_timer = ...; }
	ld a, (#_zx_kb_timer)
	sub a, #30
	jr nz, 00013$
	ld a, #25
	ld (#_zx_kb_timer), a
00012$:
	// if(zx_kb_fifo_len != 16)...
	ld a, (#_zx_kb_fifo_len)
	sub a, #0x10
	jr z, 00013$
	// zx_kb_fifo[zx_kb_fifo_in] = zx_kb_state;
	ld de, #_zx_kb_fifo
	ld hl, (_zx_kb_fifo_in)
	ld h, #0x00
	add hl, de
	ld a, (#_zx_kb_state)
	ld (hl),a
	// zx_kb_fifo_in++;
	ld hl, #_zx_kb_fifo_in
	inc (hl)
	// zx_kb_len++;
	ld hl, #_zx_kb_fifo_len
	inc (hl)
	// if(zx_kb_fifo_in == 16)zx_kb_fifo_in = 0;
	ld a, (#_zx_kb_fifo_in)
	sub a, #0x10
	jr nz, 00013$
	ld hl, #_zx_kb_fifo_in
	ld (hl), #0x00
00013$:
	pop hl
	pop de
	pop bc
#endif
	pop af
	ei
	reti
	__endasm;
}
#endif

/* interrupts:
https://chuntey.wordpress.com/2013/10/02/how-to-write-zx-spectrum-games-chapter-17/
http://www.animatez.co.uk/computers/zx-spectrum/interrupts/
*/

/*
The 50 Hz interrupt is synchronized with the video signal generation by
the ULA; both the interrupt and the video signal are generated by it.
Many programs use the interrupt to synchronize with the frame cycle.
Some use it to generate fantastic effects, such as full-screen characters,
full-screen horizon (Aquaplane) or pixel colour (Uridium, for instance)
Very many modern programs use the fact that the screen is "written"
(or "fired") to the CRT in a finite time to do as much time-consuming
screen calculations as possible without causing character flickering:
although the ULA has started displaying the screen for this frame already,
the electron beam will for a moment not "pass" this or that part of the
screen so it's safe to change something there. So the exact time in the
1/50 second time-slice at which the screen is updated is very important.
Each line takes exactly 224 T states.

After an interrupt occurs, 64 line times (14336 T states; see below for
exact timings) pass before the first byte of the screen (16384) is displayed.
At least the last 48 of these are actual border-lines; the others may be
either border or vertical retrace.

Then the 192 screen+border lines are displayed, followed by 56 border lines
again. Note that this means that a frame is (64+192+56)*224=69888 T states
long, which means that the '50 Hz' interrupt is actually
a 3.5MHz/69888=50.08 Hz interrupt. This fact can be seen by taking a clock
program, and running it for an hour, after which it will be the expected
6 seconds fast. However, on a real Spectrum, the frequency of the
interrupt varies slightly as the Spectrum gets hot; the reason for this
is unknown, but placing a cooler onto the ULA has been observed to remove
this effect.
*/

