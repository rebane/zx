#ifndef _Z80_H_
#define _Z80_H_

#include <stdint.h>

void jp(uint16_t addr);
uint8_t in(uint16_t addr);
void out(uint16_t addr, uint8_t val);

#endif

