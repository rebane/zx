#include "zx.h"

void main() __naked{
	__asm

	.macro bytes_0x00_16
	.byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	.endm

	.macro bytes_0x00_256
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	.endm

	.macro bytes_0x38_128
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.byte 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38
	.endm

	// SNA header
	.byte 0x00 // I
	.word 0x0000, 0x0000, 0x0000, 0x0000 // HL', DE', BC', AF'
	.word 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 // HL, DE, BC, IY, IX
	.byte 0x00 // Interrupt (bit 2 contains IFF2, 1 = EI, 0 = DI)
	.byte 0x00 // R
	.word 0x0000, (0x5C00 - 2) // AF, SP
	.byte 0x00 // IntMode (0 = IM0, 1 = IM1, 2 = IM2)
	.byte 0x07 // Border

	// pixels, 256 * 24 bytes
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256
	bytes_0x00_256

	// attrs, 128 * 6 bytes
	bytes_0x38_128
	bytes_0x38_128
	bytes_0x38_128
	bytes_0x38_128
	bytes_0x38_128
	bytes_0x38_128

	// console
	.byte 0x00                   // ZX_CONSOLE_X
	.byte 0x00                   // ZX_CONSOLE_Y
	.byte ZX_INK_DEFAULT         // ZX_CONSOLE_INK
	.byte ZX_PAPER_DEFAULT       // ZX_CONSOLE_PAPER
	.byte ZX_BRIGHT_DEFAULT      // ZX_CONSOLE_BRIGHT
	.byte ZX_FLASH_DEFAULT       // ZX_CONSOLE_FLASH
	.byte ZX_BORDER_DEFAULT      // ZX_CONSOLE_BORDER
	.byte ZX_INVERSE_DEFAULT     // ZX_CONSOLE_INVERSE
	.byte ZX_OVER_DEFAULT        // ZX_CONSOLE_OVER
	.byte ZX_CURSOR_DEFAULT      // ZX_CONSOLE_CURSOR
	.byte ZX_CURSOR_ATTR_DEFAULT // ZX_CONSOLE_ATTR_CURSOR
	.byte ZX_WRAP_DEFAULT        // ZX_CONSOLE_WRAP
	.byte 0x00, 0x00, 0x00, 0x00 // padding
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	bytes_0x00_16
	.byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // padding
	.word 0x5C00 // this word is in fake stack and points to start address

	__endasm;
}

