#include "z80.h"
#include <stdint.h>

void jp(uint16_t addr) __naked{
	(void)addr;

	__asm

	ld hl, #2
	add hl, sp
	ld c, (hl)
	inc hl
	ld h, (hl)
	ld l, c
	jp (hl)

	__endasm;
}

uint8_t in(uint16_t addr) __naked{
	(void)addr;

	__asm

	ld hl, #2
	add hl, sp
	ld c, (hl)
	inc hl
	ld b, (hl)
	in l, (c)
	ret

	__endasm;
}

void out(uint16_t addr, uint8_t val) __naked{
	(void)addr;
	(void)val;

	__asm

	ld hl, #2
	add hl, sp
	ld c, (hl)
	inc hl
	ld b, (hl)
	inc hl
	ld a, (hl)
	out (c), a
	ret

	__endasm;
}

