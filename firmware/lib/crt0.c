void crt0() __naked{
	__asm

	// disable interrupts
	di

	// set stack pointer
	ld sp, #STACK_LOC

        // initialise global variables
	ld	bc, #l__INITIALIZER
	ld	a, b
	or	a, c
	jr	Z, 00001$
	ld	de, #s__INITIALIZED
	ld	hl, #s__INITIALIZER
	ldir
00001$:
	jp	_main

	__endasm;
}

