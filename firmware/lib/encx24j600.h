#ifndef _ENCX24J600_H_
#define _ENCX24J600_H_

#include <stdint.h>

//Receive and transmit buffers
#define ENCX24J600_TX_BUFFER_START 0x0000
#define ENCX24J600_TX_BUFFER_STOP  0x17FE
#define ENCX24J600_RX_BUFFER_START 0x1800
#define ENCX24J600_RX_BUFFER_STOP  0x5FFE

//SPI command set
#define ENCX24J600_CMD_B0SEL       0xC0 //Bank 0 Select
#define ENCX24J600_CMD_B1SEL       0xC2 //Bank 1 Select
#define ENCX24J600_CMD_B2SEL       0xC4 //Bank 2 Select
#define ENCX24J600_CMD_B3SEL       0xC6 //Bank 3 Select
#define ENCX24J600_CMD_SETETHRST   0xCA //System Reset
#define ENCX24J600_CMD_FCDISABLE   0xE0 //Flow Control Disable
#define ENCX24J600_CMD_FCSINGLE    0xE2 //Flow Control Single
#define ENCX24J600_CMD_FCMULTIPLE  0xE4 //Flow Control Multiple
#define ENCX24J600_CMD_FCCLEAR     0xE6 //Flow Control Clear
#define ENCX24J600_CMD_SETPKTDEC   0xCC //Decrement Packet Counter
#define ENCX24J600_CMD_DMASTOP     0xD2 //DMA Stop
#define ENCX24J600_CMD_DMACKSUM    0xD8 //DMA Start Checksum
#define ENCX24J600_CMD_DMACKSUMS   0xDA //DMA Start Checksum with Seed
#define ENCX24J600_CMD_DMACOPY     0xDC //DMA Start Copy
#define ENCX24J600_CMD_DMACOPYS    0xDE //DMA Start Copy and Checksum with Seed
#define ENCX24J600_CMD_SETTXRTS    0xD4 //Request Packet Transmission
#define ENCX24J600_CMD_ENABLERX    0xE8 //Enable RX
#define ENCX24J600_CMD_DISABLERX   0xEA //Disable RX
#define ENCX24J600_CMD_SETEIE      0xEC //Enable Interrupts
#define ENCX24J600_CMD_CLREIE      0xEE //Disable Interrupts
#define ENCX24J600_CMD_RBSEL       0xC8 //Read Bank Select
#define ENCX24J600_CMD_WGPRDPT     0x60 //Write EGPRDPT
#define ENCX24J600_CMD_RGPRDPT     0x62 //Read EGPRDPT
#define ENCX24J600_CMD_WRXRDPT     0x64 //Write ERXRDPT
#define ENCX24J600_CMD_RRXRDPT     0x66 //Read ERXRDPT
#define ENCX24J600_CMD_WUDARDPT    0x68 //Write EUDARDPT
#define ENCX24J600_CMD_RUDARDPT    0x6A //Read EUDARDPT
#define ENCX24J600_CMD_WGPWRPT     0x6C //Write EGPWRPT
#define ENCX24J600_CMD_RGPWRPT     0x6E //Read EGPWRPT
#define ENCX24J600_CMD_WRXWRPT     0x70 //Write ERXWRPT
#define ENCX24J600_CMD_RRXWRPT     0x72 //Read ERXWRPT
#define ENCX24J600_CMD_WUDAWRPT    0x74 //Write EUDAWRPT
#define ENCX24J600_CMD_RUDAWRPT    0x76 //Read EUDAWRPT
#define ENCX24J600_CMD_RCR         0x00 //Read Control Register
#define ENCX24J600_CMD_WCR         0x40 //Write Control Register
#define ENCX24J600_CMD_RCRU        0x20 //Read Control Register Unbanked
#define ENCX24J600_CMD_WCRU        0x22 //Write Control Register Unbanked
#define ENCX24J600_CMD_BFS         0x80 //Bit Field Set
#define ENCX24J600_CMD_BFC         0xA0 //Bit Field Clear
#define ENCX24J600_CMD_BFSU        0x24 //Bit Field Set Unbanked
#define ENCX24J600_CMD_BFCU        0x26 //Bit Field Clear Unbanked
#define ENCX24J600_CMD_RGPDATA     0x28 //Read EGPDATA
#define ENCX24J600_CMD_WGPDATA     0x2A //Write EGPDATA
#define ENCX24J600_CMD_RRXDATA     0x2C //Read ERXDATA
#define ENCX24J600_CMD_WRXDATA     0x2E //Write ERXDATA
#define ENCX24J600_CMD_RUDADATA    0x30 //Read EUDADATA
#define ENCX24J600_CMD_WUDADATA    0x32 //Write EUDADATA

//ENCX24J600 registers
#define ENCX24J600_REG_ETXST       0x00
#define ENCX24J600_REG_ETXLEN      0x02
#define ENCX24J600_REG_ERXST       0x04
#define ENCX24J600_REG_ERXTAIL     0x06
#define ENCX24J600_REG_ERXHEAD     0x08
#define ENCX24J600_REG_EDMAST      0x0A
#define ENCX24J600_REG_EDMALEN     0x0C
#define ENCX24J600_REG_EDMADST     0x0E
#define ENCX24J600_REG_EDMACS      0x10
#define ENCX24J600_REG_ETXSTAT     0x12
#define ENCX24J600_REG_ETXWIRE     0x14
#define ENCX24J600_REG_EUDAST      0x16
#define ENCX24J600_REG_EUDAND      0x18
#define ENCX24J600_REG_ESTAT       0x1A
#define ENCX24J600_REG_EIR         0x1C
#define ENCX24J600_REG_ECON1       0x1E
#define ENCX24J600_REG_EHT1        0x20
#define ENCX24J600_REG_EHT2        0x22
#define ENCX24J600_REG_EHT3        0x24
#define ENCX24J600_REG_EHT4        0x26
#define ENCX24J600_REG_EPMM1       0x28
#define ENCX24J600_REG_EPMM2       0x2A
#define ENCX24J600_REG_EPMM3       0x2C
#define ENCX24J600_REG_EPMM4       0x2E
#define ENCX24J600_REG_EPMCS       0x30
#define ENCX24J600_REG_EPMO        0x32
#define ENCX24J600_REG_ERXFCON     0x34
#define ENCX24J600_REG_MACON1      0x40
#define ENCX24J600_REG_MACON2      0x42
#define ENCX24J600_REG_MABBIPG     0x44
#define ENCX24J600_REG_MAIPG       0x46
#define ENCX24J600_REG_MACLCON     0x48
#define ENCX24J600_REG_MAMXFL      0x4A
#define ENCX24J600_REG_MICMD       0x52
#define ENCX24J600_REG_MIREGADR    0x54
#define ENCX24J600_REG_MAADR3      0x60
#define ENCX24J600_REG_MAADR2      0x62
#define ENCX24J600_REG_MAADR1      0x64
#define ENCX24J600_REG_MIWR        0x66
#define ENCX24J600_REG_MIRD        0x68
#define ENCX24J600_REG_MISTAT      0x6A
#define ENCX24J600_REG_EPAUS       0x6C
#define ENCX24J600_REG_ECON2       0x6E
#define ENCX24J600_REG_ERXWM       0x70
#define ENCX24J600_REG_EIE         0x72
#define ENCX24J600_REG_EIDLED      0x74
#define ENCX24J600_REG_EGPDATA     0x80
#define ENCX24J600_REG_ERXDATA     0x82
#define ENCX24J600_REG_EUDADATA    0x84
#define ENCX24J600_REG_EGPRDPT     0x86
#define ENCX24J600_REG_EGPWRPT     0x88
#define ENCX24J600_REG_ERXRDPT     0x8A
#define ENCX24J600_REG_ERXWRPT     0x8C
#define ENCX24J600_REG_EUDARDPT    0x8E
#define ENCX24J600_REG_EUDAWRPT    0x90

//ENCX24J600 PHY registers
#define ENCX24J600_PHY_REG_PHCON1  0x00
#define ENCX24J600_PHY_REG_PHSTAT1 0x01
#define ENCX24J600_PHY_REG_PHANA   0x04
#define ENCX24J600_PHY_REG_PHANLPA 0x05
#define ENCX24J600_PHY_REG_PHANE   0x06
#define ENCX24J600_PHY_REG_PHCON2  0x11
#define ENCX24J600_PHY_REG_PHSTAT2 0x1B
#define ENCX24J600_PHY_REG_PHSTAT3 0x1F

//ESTAT register
#define ENCX24J600_ESTAT_INT                  0x8000
#define ENCX24J600_ESTAT_FCIDLE               0x4000
#define ENCX24J600_ESTAT_RXBUSY               0x2000
#define ENCX24J600_ESTAT_CLKRDY               0x1000
#define ENCX24J600_ESTAT_R11                  0x0800
#define ENCX24J600_ESTAT_PHYDPX               0x0400
#define ENCX24J600_ESTAT_R9                   0x0200
#define ENCX24J600_ESTAT_PHYLNK               0x0100
#define ENCX24J600_ESTAT_PKTCNT               0x00FF

//EIR register
#define ENCX24J600_EIR_CRYPTEN                0x8000
#define ENCX24J600_EIR_MODEXIF                0x4000
#define ENCX24J600_EIR_HASHIF                 0x2000
#define ENCX24J600_EIR_AESIF                  0x1000
#define ENCX24J600_EIR_LINKIF                 0x0800
#define ENCX24J600_EIR_R10                    0x0400
#define ENCX24J600_EIR_R9                     0x0200
#define ENCX24J600_EIR_R8                     0x0100
#define ENCX24J600_EIR_R7                     0x0080
#define ENCX24J600_EIR_PKTIF                  0x0040
#define ENCX24J600_EIR_DMAIF                  0x0020
#define ENCX24J600_EIR_R4                     0x0010
#define ENCX24J600_EIR_TXIF                   0x0008
#define ENCX24J600_EIR_TXABTIF                0x0004
#define ENCX24J600_EIR_RXABTIF                0x0002
#define ENCX24J600_EIR_PCFULIF                0x0001

//ECON1 register
#define ENCX24J600_ECON1_MODEXST              0x8000
#define ENCX24J600_ECON1_HASHEN               0x4000
#define ENCX24J600_ECON1_HASHOP               0x2000
#define ENCX24J600_ECON1_HASHLST              0x1000
#define ENCX24J600_ECON1_AESST                0x0800
#define ENCX24J600_ECON1_AESOP1               0x0400
#define ENCX24J600_ECON1_AESOP0               0x0200
#define ENCX24J600_ECON1_PKTDEC               0x0100
#define ENCX24J600_ECON1_FCOP1                0x0080
#define ENCX24J600_ECON1_FCOP0                0x0040
#define ENCX24J600_ECON1_DMAST                0x0020
#define ENCX24J600_ECON1_DMACPY               0x0010
#define ENCX24J600_ECON1_DMACSSD              0x0008
#define ENCX24J600_ECON1_DMANOCS              0x0004
#define ENCX24J600_ECON1_TXRTS                0x0002
#define ENCX24J600_ECON1_RXEN                 0x0001

//ETXSTAT register
#define ENCX24J600_ETXSTAT_R12                0x1000
#define ENCX24J600_ETXSTAT_R11                0x0800
#define ENCX24J600_ETXSTAT_LATECOL            0x0400
#define ENCX24J600_ETXSTAT_MAXCOL             0x0200
#define ENCX24J600_ETXSTAT_EXDEFER            0x0100
#define ENCX24J600_ETXSTAT_DEFER              0x0080
#define ENCX24J600_ETXSTAT_R6                 0x0040
#define ENCX24J600_ETXSTAT_R5                 0x0020
#define ENCX24J600_ETXSTAT_CRCBAD             0x0010
#define ENCX24J600_ETXSTAT_COLCNT             0x000F

//ERXFCON register
#define ENCX24J600_ERXFCON_HTEN               0x8000
#define ENCX24J600_ERXFCON_MPEN               0x4000
#define ENCX24J600_ERXFCON_NOTPM              0x1000
#define ENCX24J600_ERXFCON_PMEN3              0x0800
#define ENCX24J600_ERXFCON_PMEN2              0x0400
#define ENCX24J600_ERXFCON_PMEN1              0x0200
#define ENCX24J600_ERXFCON_PMEN0              0x0100
#define ENCX24J600_ERXFCON_CRCEEN             0x0080
#define ENCX24J600_ERXFCON_CRCEN              0x0040
#define ENCX24J600_ERXFCON_RUNTEEN            0x0020
#define ENCX24J600_ERXFCON_RUNTEN             0x0010
#define ENCX24J600_ERXFCON_UCEN               0x0008
#define ENCX24J600_ERXFCON_NOTMEEN            0x0004
#define ENCX24J600_ERXFCON_MCEN               0x0002
#define ENCX24J600_ERXFCON_BCEN               0x0001

//MACON1 register
#define ENCX24J600_MACON1_R15                 0x8000
#define ENCX24J600_MACON1_R14                 0x4000
#define ENCX24J600_MACON1_R11                 0x0800
#define ENCX24J600_MACON1_R10                 0x0400
#define ENCX24J600_MACON1_R9                  0x0200
#define ENCX24J600_MACON1_R8                  0x0100
#define ENCX24J600_MACON1_LOOPBK              0x0010
#define ENCX24J600_MACON1_R3                  0x0008
#define ENCX24J600_MACON1_RXPAUS              0x0004
#define ENCX24J600_MACON1_PASSALL             0x0002
#define ENCX24J600_MACON1_R0                  0x0001

//MACON2 register
#define ENCX24J600_MACON2_DEFER               0x4000
#define ENCX24J600_MACON2_BPEN                0x2000
#define ENCX24J600_MACON2_NOBKOFF             0x1000
#define ENCX24J600_MACON2_R9                  0x0200
#define ENCX24J600_MACON2_R8                  0x0100
#define ENCX24J600_MACON2_PADCFG2             0x0080
#define ENCX24J600_MACON2_PADCFG1             0x0040
#define ENCX24J600_MACON2_PADCFG0             0x0020
#define ENCX24J600_MACON2_TXCRCEN             0x0010
#define ENCX24J600_MACON2_PHDREN              0x0008
#define ENCX24J600_MACON2_HFRMEN              0x0004
#define ENCX24J600_MACON2_R1                  0x0002
#define ENCX24J600_MACON2_FULDPX              0x0001

//MABBIPG register
#define ENCX24J600_MABBIPG_BBIPG              0x007F

//MAIPG register
#define ENCX24J600_MAIPG_R14                  0x4000
#define ENCX24J600_MAIPG_R13                  0x2000
#define ENCX24J600_MAIPG_R12                  0x1000
#define ENCX24J600_MAIPG_R11                  0x0800
#define ENCX24J600_MAIPG_R10                  0x0400
#define ENCX24J600_MAIPG_R9                   0x0200
#define ENCX24J600_MAIPG_R8                   0x0100
#define ENCX24J600_MAIPG_IPG                  0x007F

//MACLCON register
#define ENCX24J600_MACLCON_R13                0x2000
#define ENCX24J600_MACLCON_R12                0x1000
#define ENCX24J600_MACLCON_R11                0x0800
#define ENCX24J600_MACLCON_R10                0x0400
#define ENCX24J600_MACLCON_R9                 0x0200
#define ENCX24J600_MACLCON_R8                 0x0100
#define ENCX24J600_MACLCON_MAXRET             0x000F

//MICMD register
#define ENCX24J600_MICMD_MIISCAN              0x0002
#define ENCX24J600_MICMD_MIIRD                0x0001

//MIREGADR register
#define ENCX24J600_MIREGADR_R12               0x1000
#define ENCX24J600_MIREGADR_R11               0x0800
#define ENCX24J600_MIREGADR_R10               0x0400
#define ENCX24J600_MIREGADR_R9                0x0200
#define ENCX24J600_MIREGADR_R8                0x0100
#define ENCX24J600_MIREGADR_PHREG             0x001F

//MISTAT register
#define ENCX24J600_MISTAT_R3                  0x0008
#define ENCX24J600_MISTAT_NVALID              0x0004
#define ENCX24J600_MISTAT_SCAN                0x0002
#define ENCX24J600_MISTAT_BUSY                0x0001

//ECON2 register
#define ENCX24J600_ECON2_ETHEN                0x8000
#define ENCX24J600_ECON2_STRCH                0x4000
#define ENCX24J600_ECON2_TXMAC                0x2000
#define ENCX24J600_ECON2_SHA1MD5              0x1000
#define ENCX24J600_ECON2_COCON3               0x0800
#define ENCX24J600_ECON2_COCON2               0x0400
#define ENCX24J600_ECON2_COCON1               0x0200
#define ENCX24J600_ECON2_COCON0               0x0100
#define ENCX24J600_ECON2_AUTOFC               0x0080
#define ENCX24J600_ECON2_TXRST                0x0040
#define ENCX24J600_ECON2_RXRST                0x0020
#define ENCX24J600_ECON2_ETHRST               0x0010
#define ENCX24J600_ECON2_MODLEN1              0x0008
#define ENCX24J600_ECON2_MODLEN0              0x0004
#define ENCX24J600_ECON2_AESLEN1              0x0002
#define ENCX24J600_ECON2_AESLEN0              0x0001

//ERXWM register
#define ENCX24J600_ERXWM_RXFWM                0xFF00
#define ENCX24J600_ERXWM_RXEWM                0x00FF

//EIE register
#define ENCX24J600_EIE_INTIE                  0x8000
#define ENCX24J600_EIE_MODEXIE                0x4000
#define ENCX24J600_EIE_HASHIE                 0x2000
#define ENCX24J600_EIE_AESIE                  0x1000
#define ENCX24J600_EIE_LINKIE                 0x0800
#define ENCX24J600_EIE_R10                    0x0400
#define ENCX24J600_EIE_R9                     0x0200
#define ENCX24J600_EIE_R8                     0x0100
#define ENCX24J600_EIE_R7                     0x0080
#define ENCX24J600_EIE_PKTIE                  0x0040
#define ENCX24J600_EIE_DMAIE                  0x0020
#define ENCX24J600_EIE_R4                     0x0010
#define ENCX24J600_EIE_TXIE                   0x0008
#define ENCX24J600_EIE_TXABTIE                0x0004
#define ENCX24J600_EIE_RXABTIE                0x0002
#define ENCX24J600_EIE_PCFULIE                0x0001

//EIDLED register
#define ENCX24J600_EIDLED_LACFG3              0x8000
#define ENCX24J600_EIDLED_LACFG2              0x4000
#define ENCX24J600_EIDLED_LACFG1              0x2000
#define ENCX24J600_EIDLED_LACFG0              0x1000
#define ENCX24J600_EIDLED_LBCFG3              0x0800
#define ENCX24J600_EIDLED_LBCFG2              0x0400
#define ENCX24J600_EIDLED_LBCFG1              0x0200
#define ENCX24J600_EIDLED_LBCFG0              0x0100
#define ENCX24J600_EIDLED_DEVID               0x00FF

//PHCON1 register
#define ENCX24J600_PHCON1_PRST                0x8000
#define ENCX24J600_PHCON1_PLOOPBK             0x4000
#define ENCX24J600_PHCON1_SPD100              0x2000
#define ENCX24J600_PHCON1_ANEN                0x1000
#define ENCX24J600_PHCON1_PSLEEP              0x0800
#define ENCX24J600_PHCON1_RENEG               0x0200
#define ENCX24J600_PHCON1_PFULDPX             0x0100

//PHSTAT1 register
#define ENCX24J600_PHSTAT1_FULL100            0x4000
#define ENCX24J600_PHSTAT1_HALF100            0x2000
#define ENCX24J600_PHSTAT1_FULL10             0x1000
#define ENCX24J600_PHSTAT1_HALF10             0x0800
#define ENCX24J600_PHSTAT1_ANDONE             0x0020
#define ENCX24J600_PHSTAT1_LRFAULT            0x0010
#define ENCX24J600_PHSTAT1_ANABLE             0x0008
#define ENCX24J600_PHSTAT1_LLSTAT             0x0004
#define ENCX24J600_PHSTAT1_EXTREGS            0x0001

//PHANA register
#define ENCX24J600_PHANA_ADNP                 0x8000
#define ENCX24J600_PHANA_ADFAULT              0x2000
#define ENCX24J600_PHANA_ADPAUS1              0x0800
#define ENCX24J600_PHANA_ADPAUS0              0x0400
#define ENCX24J600_PHANA_AD100FD              0x0100
#define ENCX24J600_PHANA_AD100                0x0080
#define ENCX24J600_PHANA_AD10FD               0x0040
#define ENCX24J600_PHANA_AD10                 0x0020
#define ENCX24J600_PHANA_ADIEEE4              0x0010
#define ENCX24J600_PHANA_ADIEEE3              0x0008
#define ENCX24J600_PHANA_ADIEEE2              0x0004
#define ENCX24J600_PHANA_ADIEEE1              0x0002
#define ENCX24J600_PHANA_ADIEEE0              0x0001

//PHANLPA register
#define ENCX24J600_PHANLPA_LPNP               0x8000
#define ENCX24J600_PHANLPA_LPACK              0x4000
#define ENCX24J600_PHANLPA_LPFAULT            0x2000
#define ENCX24J600_PHANLPA_LPPAUS1            0x0800
#define ENCX24J600_PHANLPA_LPPAUS0            0x0400
#define ENCX24J600_PHANLPA_LP100T4            0x0200
#define ENCX24J600_PHANLPA_LP100FD            0x0100
#define ENCX24J600_PHANLPA_LP100              0x0080
#define ENCX24J600_PHANLPA_LP10FD             0x0040
#define ENCX24J600_PHANLPA_LP10               0x0020
#define ENCX24J600_PHANLPA_LPIEEE             0x001F
#define ENCX24J600_PHANLPA_LPIEEE4            0x0010
#define ENCX24J600_PHANLPA_LPIEEE3            0x0008
#define ENCX24J600_PHANLPA_LPIEEE2            0x0004
#define ENCX24J600_PHANLPA_LPIEEE1            0x0002
#define ENCX24J600_PHANLPA_LPIEEE0            0x0001

//PHANE register
#define ENCX24J600_PHANE_PDFLT                0x0010
#define ENCX24J600_PHANE_LPARCD               0x0002
#define ENCX24J600_PHANE_LPANABL              0x0001

//PHCON2 register
#define ENCX24J600_PHCON2_EDPWRDN             0x2000
#define ENCX24J600_PHCON2_EDTHRES             0x0800
#define ENCX24J600_PHCON2_FRCLNK              0x0004
#define ENCX24J600_PHCON2_EDSTAT              0x0002

//PHSTAT2 register
#define ENCX24J600_PHSTAT2_PLRITY             0x0010

//PHSTAT3 register
#define ENCX24J600_PHSTAT3_SPDDPX2            0x0010
#define ENCX24J600_PHSTAT3_SPDDPX1            0x0008
#define ENCX24J600_PHSTAT3_SPDDPX0            0x0004

//Receive status vector
#define ENCX24J600_RSV_UNICAST_FILTER         0x00100000
#define ENCX24J600_RSV_PATTERN_MATCH_FILTER   0x00080000
#define ENCX24J600_RSV_MAGIC_PACKET_FILTER    0x00040000
#define ENCX24J600_RSV_HASH_FILTER            0x00020000
#define ENCX24J600_RSV_NOT_ME_FILTER          0x00010000
#define ENCX24J600_RSV_RUNT_FILTER            0x00008000
#define ENCX24J600_RSV_VLAN_TYPE              0x00004000
#define ENCX24J600_RSV_UNKNOWN_OPCODE         0x00002000
#define ENCX24J600_RSV_PAUSE_CONTROL_FRAME    0x00001000
#define ENCX24J600_RSV_CONTROL_FRAME          0x00000800
#define ENCX24J600_RSV_DRIBBLE_NIBBLE         0x00000400
#define ENCX24J600_RSV_BROADCAST_PACKET       0x00000200
#define ENCX24J600_RSV_MULTICAST_PACKET       0x00000100
#define ENCX24J600_RSV_RECEIVED_OK            0x00000080
#define ENCX24J600_RSV_LENGTH_OUT_OF_RANGE    0x00000040
#define ENCX24J600_RSV_LENGTH_CHECK_ERROR     0x00000020
#define ENCX24J600_RSV_CRC_ERROR              0x00000010
#define ENCX24J600_RSV_CARRIER_EVENT          0x00000004
#define ENCX24J600_RSV_PACKET_IGNORED         0x00000001

#define ENCX24J600_EVENT_NONE                 0
#define ENCX24J600_EVENT_LINK                 1
#define ENCX24J600_EVENT_RX                   2

#define ENCX24J600_LINK_DOWN                  0
#define ENCX24J600_LINK_100_FD                1
#define ENCX24J600_LINK_10_FD                 2
#define ENCX24J600_LINK_100_HD                3
#define ENCX24J600_LINK_10_HD                 4

struct encx24j600_rsv_struct{
	uint16_t next_packet;
	uint16_t len;
	uint32_t rxstat;
};

extern uint8_t encx24j600_mac[6];
extern uint8_t encx24j600_link;

void encx24j600_init();
void encx24j600_filter_normal();
void encx24j600_filter_dhcp();
uint8_t encx24j600_poll();
uint16_t encx24j600_recv(void *buf, uint16_t count);
void encx24j600_send(const void *buf, uint16_t count);

#endif

