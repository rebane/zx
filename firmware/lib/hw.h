#ifndef _HW_H_
#define _HW_H_

#include <stdint.h>

// ports
#define HW_PORT_OUTPUT              (0x0001)
#define HW_PORT_SPI                 (0x0003)
#define HW_PORT_INPUT               (0x0001)

// bits
#define HW_PORT_OUTPUT_LED3         (0x01 << 0)
#define HW_PORT_OUTPUT_LED2         (0x01 << 1)
#define HW_PORT_OUTPUT_SD           (0x01 << 2)
#define HW_PORT_OUTPUT_ROM          (0x01 << 3)

#define HW_PORT_SPI_SCK             (0x01 << 0)
#define HW_PORT_SPI_MOSI            (0x01 << 1)
#define HW_PORT_SPI_ETH             (0x01 << 2)
#define HW_PORT_SPI_SD              (0x01 << 3)
#define HW_PORT_SPI_EE              (0x01 << 4)

#define HW_PORT_INPUT_ETH_MISO      (0x01 << 0)
#define HW_PORT_INPUT_SD_MISO       (0x01 << 1)
#define HW_PORT_INPUT_EE_MISO       (0x01 << 2)
#define HW_PORT_INPUT_SD_CD         (0x01 << 3)
#define HW_PORT_INPUT_EXT_SCK       (0x01 << 4)
#define HW_PORT_INPUT_EXT_MOSI      (0x01 << 5)

// meaning
#define HW_PORT_OUTPUT_LED3_OFF     (0x00)
#define HW_PORT_OUTPUT_LED3_ON      (HW_PORT_OUTPUT_LED3)
#define HW_PORT_OUTPUT_LED2_OFF     (0x00)
#define HW_PORT_OUTPUT_LED2_ON      (HW_PORT_OUTPUT_LED2)
#define HW_PORT_OUTPUT_SD_DISABLE   (0x00)
#define HW_PORT_OUTPUT_SD_ENABLE    (HW_PORT_OUTPUT_SD)
#define HW_PORT_OUTPUT_ROM_DISABLE  (0x00)
#define HW_PORT_OUTPUT_ROM_ENABLE   (HW_PORT_OUTPUT_ROM)

#define HW_PORT_SPI_SCK_HIGH        (HW_PORT_SPI_SCK)
#define HW_PORT_SPI_SCK_LOW         (0x00)
#define HW_PORT_SPI_MOSI_HIGH       (HW_PORT_SPI_MOSI)
#define HW_PORT_SPI_MOSI_LOW        (0x00)
#define HW_PORT_SPI_ETH_DISABLE     (HW_PORT_SPI_ETH)
#define HW_PORT_SPI_ETH_ENABLE      (0x00)
#define HW_PORT_SPI_SD_DISABLE      (HW_PORT_SPI_SD)
#define HW_PORT_SPI_SD_ENABLE       (0x00)
#define HW_PORT_SPI_EE_DISABLE      (HW_PORT_SPI_EE)
#define HW_PORT_SPI_EE_ENABLE       (0x00)

#define HW_PORT_INPUT_ETH_MISO_HIGH (HW_PORT_INPUT_ETH_MISO)
#define HW_PORT_INPUT_ETH_MISO_LOW  (0x00)
#define HW_PORT_INPUT_SD_MISO_HIGH  (HW_PORT_INPUT_SD_MISO)
#define HW_PORT_INPUT_SD_MISO_LOW   (0x00)
#define HW_PORT_INPUT_EE_MISO_HIGH  (HW_PORT_INPUT_EE_MISO)
#define HW_PORT_INPUT_EE_MISO_LOW   (0x00)
#define HW_PORT_INPUT_SD_CD_PRESENT (HW_PORT_INPUT_SD_CD)
#define HW_PORT_INPUT_SD_CD_MISSING (0x00)
#define HW_PORT_INPUT_EXT_SCK_HIGH  (HW_PORT_INPUT_EXT_SCK)
#define HW_PORT_INPUT_EXT_SCK_LOW   (0x00)
#define HW_PORT_INPUT_EXT_MOSI_HIGH (HW_PORT_INPUT_EXT_MOSI)
#define HW_PORT_INPUT_EXT_MOSI_LOW  (0x00)

void hw_spi_cs_enable_eeprom();
void hw_spi_cs_disable_eeprom();
void hw_spi_read_eeprom(void *buf, uint16_t count);
void hw_spi_write_eeprom(const void *buf, uint16_t count);

void hw_spi_cs_enable_sd();
void hw_spi_cs_disable_sd();
void hw_spi_read_sd(void *buf, uint16_t count);
void hw_spi_write_sd(const void *buf, uint16_t count);
void hw_spi_sd_release();

void hw_spi_cs_enable_eth();
void hw_spi_cs_disable_eth();
void hw_spi_read_eth(void *buf, uint16_t count);
void hw_spi_write_eth(const void *buf, uint16_t count);
void hw_spi_write_byte_eth(uint8_t byte);

#endif

