#include "hw.h"

void hw_spi_cs_enable_eth() __naked{
	__asm

	ld a, #(HW_PORT_SPI_EE_DISABLE | HW_PORT_SPI_SD_DISABLE | HW_PORT_SPI_ETH_ENABLE | HW_PORT_SPI_MOSI_HIGH | HW_PORT_SPI_SCK_LOW)
	out (HW_PORT_SPI), a
	ret

	__endasm;
}

void hw_spi_cs_disable_eth() __naked{
	__asm

	ld a, #(HW_PORT_SPI_EE_DISABLE | HW_PORT_SPI_SD_DISABLE | HW_PORT_SPI_ETH_ENABLE | HW_PORT_SPI_MOSI_HIGH | HW_PORT_SPI_SCK_LOW)
	out (HW_PORT_SPI), a
	ld a, #(HW_PORT_SPI_EE_DISABLE | HW_PORT_SPI_SD_DISABLE | HW_PORT_SPI_ETH_DISABLE | HW_PORT_SPI_MOSI_HIGH | HW_PORT_SPI_SCK_LOW)
	out (HW_PORT_SPI), a
	ret

	__endasm;
}

void hw_spi_read_eth(void *buf, uint16_t count) __naked{
	(void)buf;
	(void)count;

	__asm

	.macro hw_spi_read_bit_eth
	ld a, #(HW_PORT_SPI_EE_DISABLE | HW_PORT_SPI_SD_DISABLE | HW_PORT_SPI_ETH_ENABLE | HW_PORT_SPI_MOSI_HIGH | HW_PORT_SPI_SCK_HIGH)
	out (HW_PORT_SPI), a
	dec a
	out (HW_PORT_SPI), a
	in a, (HW_PORT_INPUT)
	rra
	rl c
	.endm

	ld hl, #2
	add hl, sp
	ld c, (hl)
	inc hl
	ld b, (hl)
	inc hl
	ld e, (hl)
	inc hl
	ld d, (hl)
	ld a, d
	or e
	jp z, #00002$
	ld h, b
	ld l, c
	// hl - address
	// de - len
00001$:
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth
	hw_spi_read_bit_eth

	ld (hl), c

	inc hl
	dec de
	ld a, d
	or e
	jp nz, #00001$
00002$:
	ret

	__endasm;
}

void hw_spi_write_eth(const void *buf, uint16_t count) __naked{
	(void)buf;
	(void)count;

	__asm

	.macro hw_spi_write_bit_eth
	xor a
	rl c
	rla
	rlca
	add a, #(HW_PORT_SPI_EE_DISABLE | HW_PORT_SPI_SD_DISABLE | HW_PORT_SPI_ETH_ENABLE | HW_PORT_SPI_SCK_LOW)
	out (HW_PORT_SPI), a
	inc a
	out (HW_PORT_SPI), a
	.endm

	ld hl, #2
	add hl, sp
	ld c, (hl)
	inc hl
	ld b, (hl)
	inc hl
	ld e, (hl)
	inc hl
	ld d, (hl)
	ld a, d
	or e
	jp z, #00004$
	ld h, b
	ld l, c
	// hl - address
	// de - len
00003$:
	ld c, (hl)

	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth

	inc hl
	dec de
	ld a, d
	or e
	jp nz, #00003$
00004$:
	ret

	__endasm;
}

void hw_spi_write_byte_eth(uint8_t byte) __naked{
	(void)byte;

	__asm

	ld hl, #2
	add hl, sp
	ld c, (hl)

	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth
	hw_spi_write_bit_eth

	ret

	__endasm;
}

