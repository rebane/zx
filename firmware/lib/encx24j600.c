#include "encx24j600.h"
#include <stdio.h>
#include "hw.h"
#include "z80.h"

uint8_t encx24j600_mac[6]; // my mac: 00:04:A3:FC:9E:81
uint8_t encx24j600_link;

static uint16_t encx24j600_rx_ptr;

static uint8_t encx24j600_reset();
static void encx24j600_cmd(uint8_t cmd);
static uint16_t encx24j600_reg_read(uint8_t address);
static void encx24j600_reg_write(uint8_t address, uint16_t value);
static void encx24j600_reg_bits_clear(uint8_t address, uint16_t mask);
static void encx24j600_reg_bits_set(uint8_t address, uint16_t mask);
static void encx24j600_raw_read(uint8_t cmd, void *buf, uint16_t count);
static void encx24j600_raw_write(uint8_t cmd, const void *buf, uint16_t count);
static uint16_t encx24j600_phy_reg_read(uint8_t address);
static void encx24j600_phy_reg_write(uint8_t address, uint16_t value);
static void encx24j600_reset_tx();
static void encx24j600_update_link();

void encx24j600_init(){
	uint16_t maadr, phstat1, timeout;

	encx24j600_link = ENCX24J600_LINK_DOWN;

	encx24j600_reset();

	/* read MAC address */
	maadr = encx24j600_reg_read(ENCX24J600_REG_MAADR1);
	encx24j600_mac[0] = (maadr >> 0) & 0xFF;
	encx24j600_mac[1] = (maadr >> 8) & 0xFF;
	maadr = encx24j600_reg_read(ENCX24J600_REG_MAADR2);
	encx24j600_mac[2] = (maadr >> 0) & 0xFF;
	encx24j600_mac[3] = (maadr >> 8) & 0xFF;
	maadr = encx24j600_reg_read(ENCX24J600_REG_MAADR3);
	encx24j600_mac[4] = (maadr >> 0) & 0xFF;
	encx24j600_mac[5] = (maadr >> 8) & 0xFF;

	/* PHY Leds: link status, LEDA: Link State + collision events, LEDB: Link State + transmit/receive events */
	encx24j600_reg_write(ENCX24J600_REG_EIDLED, 0xCB00);

	/* loopback disabled */
	encx24j600_reg_write(ENCX24J600_REG_MACON1, 0x0009);

	/* interpacket gap value */
	encx24j600_reg_write(ENCX24J600_REG_MAIPG, 0x0C12);

	/* write the auto negotiation pattern */
	encx24j600_phy_reg_write(ENCX24J600_PHY_REG_PHANA, 0x05E1);

	/* start auto negotiation */
	encx24j600_phy_reg_write(ENCX24J600_PHY_REG_PHCON1, ENCX24J600_PHCON1_ANEN | ENCX24J600_PHCON1_RENEG);

	/* set rx filter */
	encx24j600_reg_bits_clear(ENCX24J600_REG_MACON1, ENCX24J600_MACON1_PASSALL);
	encx24j600_reg_write(ENCX24J600_REG_ERXFCON, ENCX24J600_ERXFCON_CRCEN);

	/* set max framelen */
	encx24j600_reg_write(ENCX24J600_REG_MAMXFL, 1518);

	/* reset TX */
	encx24j600_reset_tx();
	encx24j600_reg_bits_clear(ENCX24J600_REG_EIR, ENCX24J600_EIR_TXIF | ENCX24J600_EIR_TXABTIF);
	encx24j600_reg_write(ENCX24J600_REG_EGPWRPT, 0x0000);

	/* reset RX */
	encx24j600_cmd(ENCX24J600_CMD_DISABLERX);
	encx24j600_reg_write(ENCX24J600_REG_ERXST, 0x0600);
	encx24j600_reg_write(ENCX24J600_REG_ERXRDPT, 0x0600);
	encx24j600_rx_ptr = 0x0600;
	encx24j600_reg_write(ENCX24J600_REG_ERXTAIL, 0x6000 - 2);
	encx24j600_reg_write(ENCX24J600_REG_EUDAST, 0x6000);
	encx24j600_reg_write(ENCX24J600_REG_EUDAND, 0x6000 + 1);
	encx24j600_reg_bits_clear(ENCX24J600_REG_EIR, ENCX24J600_EIR_PCFULIF | ENCX24J600_EIR_RXABTIF | ENCX24J600_EIR_TXABTIF | ENCX24J600_EIR_TXIF | ENCX24J600_EIR_PKTIF);
	encx24j600_cmd(ENCX24J600_CMD_ENABLERX);

	/* wait for auto negotiation */
	timeout = 500;
	do{
		phstat1 = encx24j600_phy_reg_read(ENCX24J600_PHY_REG_PHSTAT1);
	}while(!(phstat1 & ENCX24J600_PHSTAT1_ANDONE) && --timeout);
}

void encx24j600_filter_normal(){
	encx24j600_reg_write(ENCX24J600_REG_ERXFCON, ENCX24J600_ERXFCON_CRCEN | ENCX24J600_ERXFCON_UCEN | ENCX24J600_ERXFCON_BCEN);
}

void encx24j600_filter_dhcp(){
	encx24j600_reg_write(ENCX24J600_REG_EPMO, 0);
	//                                                |                                     ||                                        ||                                   |
	// DD:DD:DD:DD:DD:DD SS:SS:SS:SS:SS:SS 0800 | 4500 llll idid flfo tl11 hcrc s-----ip 00000000 | 0044 0043 llll ucrc | 02010600 xdxdxdxd 0000 0000 00000000 youripad ....
	// xx xx xx xx xx xx                   xxxx | xxxx                  xx               xxxxxxxx | xxxx xxxx             xxxxxxxx                    
	encx24j600_reg_write(ENCX24J600_REG_EPMM1, 0xF03F);
	encx24j600_reg_write(ENCX24J600_REG_EPMM2, 0xC080);
	encx24j600_reg_write(ENCX24J600_REG_EPMM3, 0x3C3F);
	encx24j600_reg_write(ENCX24J600_REG_EPMM4, 0x0000);
	encx24j600_reg_write(ENCX24J600_REG_EPMCS, 0x19F7);
	encx24j600_reg_write(ENCX24J600_REG_ERXFCON, ENCX24J600_ERXFCON_CRCEN | ENCX24J600_ERXFCON_PMEN0);
}

uint8_t encx24j600_poll(){
	uint16_t eir;
	eir = encx24j600_reg_read(ENCX24J600_REG_EIR);
	if(eir & ENCX24J600_EIR_LINKIF){
		encx24j600_update_link();
		encx24j600_reg_bits_clear(ENCX24J600_REG_EIR, ENCX24J600_EIR_LINKIF);
		return(ENCX24J600_EVENT_LINK);
	}
	if(eir & ENCX24J600_EIR_PKTIF){
		return(ENCX24J600_EVENT_RX);
	}
	return(ENCX24J600_EVENT_NONE);
}

uint16_t encx24j600_recv(void *buf, uint16_t count){
	struct encx24j600_rsv_struct rsv;
	uint16_t len, newrxtail;
	len = 0;
	encx24j600_reg_write(ENCX24J600_REG_ERXRDPT, encx24j600_rx_ptr);
	encx24j600_raw_read(ENCX24J600_CMD_RRXDATA, &rsv, sizeof(rsv));
	if((rsv.rxstat & ENCX24J600_RSV_RECEIVED_OK) && (rsv.len < 1518)){
		len = rsv.len;
		if(len > count)len = count;
		encx24j600_raw_read(ENCX24J600_CMD_RRXDATA, buf, len);
	}
	encx24j600_rx_ptr = rsv.next_packet;
	newrxtail = encx24j600_rx_ptr - 2;
	if(newrxtail == 0x0600)newrxtail = 0x6000 - 2;
	encx24j600_cmd(ENCX24J600_CMD_SETPKTDEC);
	encx24j600_reg_write(ENCX24J600_REG_ERXTAIL, newrxtail);
	return(len);
}

void encx24j600_send(const void *buf, uint16_t count){
	if(encx24j600_reg_read(ENCX24J600_REG_EIR) & ENCX24J600_EIR_TXABTIF){
		encx24j600_reset_tx();
	}
	encx24j600_reg_write(ENCX24J600_REG_EGPWRPT, 0x0000);
	encx24j600_raw_write(ENCX24J600_CMD_WGPDATA, buf, count);
	encx24j600_reg_write(ENCX24J600_REG_ETXST, 0x0000);
	encx24j600_reg_write(ENCX24J600_REG_ETXLEN, count);
	encx24j600_cmd(ENCX24J600_CMD_SETTXRTS);
}

static void encx24j600_reset_tx(){
	encx24j600_reg_bits_set(ENCX24J600_REG_ECON2, ENCX24J600_ECON2_TXRST);
	encx24j600_reg_bits_clear(ENCX24J600_REG_ECON2, ENCX24J600_ECON2_TXRST);
}

static void encx24j600_update_link(){
        uint16_t phstat3, estat;

	phstat3 = encx24j600_phy_reg_read(ENCX24J600_PHY_REG_PHSTAT3);
	phstat3 = (phstat3 >> 2) & 0x07;
	if(phstat3 == 0x06){
		encx24j600_link = ENCX24J600_LINK_100_FD;
	}else if(phstat3 == 0x05){
		encx24j600_link = ENCX24J600_LINK_10_FD;
	}else if(phstat3 == 0x02){
		encx24j600_link = ENCX24J600_LINK_100_HD;
	}else if(phstat3 == 0x01){
		encx24j600_link = ENCX24J600_LINK_10_HD;
	}else{
		encx24j600_link = ENCX24J600_LINK_DOWN;
	}
	estat = encx24j600_reg_read(ENCX24J600_REG_ESTAT);
	if(estat & ENCX24J600_ESTAT_PHYLNK){
		if(estat & ENCX24J600_ESTAT_PHYDPX){
			encx24j600_reg_write(ENCX24J600_REG_MACON2, ENCX24J600_MACON2_DEFER | ENCX24J600_MACON2_PADCFG2 | ENCX24J600_MACON2_PADCFG0 | ENCX24J600_MACON2_TXCRCEN | ENCX24J600_MACON2_R1 | ENCX24J600_MACON2_FULDPX);
			encx24j600_reg_write(ENCX24J600_REG_MABBIPG, 0x0015);
		}else{
			encx24j600_reg_write(ENCX24J600_REG_MACON2, ENCX24J600_MACON2_DEFER | ENCX24J600_MACON2_PADCFG2 | ENCX24J600_MACON2_PADCFG0 | ENCX24J600_MACON2_TXCRCEN | ENCX24J600_MACON2_R1);
			encx24j600_reg_write(ENCX24J600_REG_MABBIPG, 0x0012);
			/* Max retransmittions attempt  */
			encx24j600_reg_write(ENCX24J600_REG_MACLCON, 0x370F);
		}
	}
}

static uint8_t encx24j600_reset(){
	uint16_t eudast, timeout;

	timeout = 10;
	do{
		encx24j600_reg_write(ENCX24J600_REG_EUDAST, 0x1234);
		eudast = encx24j600_reg_read(ENCX24J600_REG_EUDAST);
	}while((eudast != 0x1234) && --timeout);
	if(timeout == 0)return(0);

	timeout = 10;
	while(!(encx24j600_reg_read(ENCX24J600_REG_ESTAT) & ENCX24J600_ESTAT_CLKRDY) && --timeout);
	if(timeout == 0)return(0);

	encx24j600_reg_bits_set(ENCX24J600_REG_ECON2, ENCX24J600_ECON2_ETHRST);

	if(encx24j600_reg_read(ENCX24J600_REG_EUDAST) != 0x0000)return(0);

	return(1);
}

static void encx24j600_cmd(uint8_t cmd){
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(cmd);
	hw_spi_cs_disable_eth();
}

static uint16_t encx24j600_reg_read(uint8_t address){
	uint16_t value;
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(ENCX24J600_CMD_RCRU);
	hw_spi_write_byte_eth(address);
	hw_spi_read_eth(&value, 2);
	hw_spi_cs_disable_eth();
	return(value);
}

static void encx24j600_reg_write(uint8_t address, uint16_t value){
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(ENCX24J600_CMD_WCRU);
	hw_spi_write_byte_eth(address);
	hw_spi_write_eth(&value, 2);
	hw_spi_cs_disable_eth();
}

static void encx24j600_reg_bits_clear(uint8_t address, uint16_t mask){
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(ENCX24J600_CMD_BFCU);
	hw_spi_write_byte_eth(address);
	hw_spi_write_eth(&mask, 2);
	hw_spi_cs_disable_eth();
}

static void encx24j600_reg_bits_set(uint8_t address, uint16_t mask){
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(ENCX24J600_CMD_BFSU);
	hw_spi_write_byte_eth(address);
	hw_spi_write_eth(&mask, 2);
	hw_spi_cs_disable_eth();
}

static void encx24j600_raw_read(uint8_t cmd, void *buf, uint16_t count){
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(cmd);
	hw_spi_read_eth(buf, count);
	hw_spi_cs_disable_eth();
}

static void encx24j600_raw_write(uint8_t cmd, const void *buf, uint16_t count){
	hw_spi_cs_enable_eth();
	hw_spi_write_byte_eth(cmd);
	hw_spi_write_eth(buf, count);
	hw_spi_cs_disable_eth();
}

static uint16_t encx24j600_phy_reg_read(uint8_t address){
	encx24j600_reg_write(ENCX24J600_REG_MIREGADR, ENCX24J600_MIREGADR_R8 | address);
	encx24j600_reg_write(ENCX24J600_REG_MICMD, ENCX24J600_MICMD_MIIRD);
	while(encx24j600_reg_read(ENCX24J600_REG_MISTAT) & ENCX24J600_MISTAT_BUSY);
	encx24j600_reg_write(ENCX24J600_REG_MICMD, 0x00);
	return(encx24j600_reg_read(ENCX24J600_REG_MIRD));
}

static void encx24j600_phy_reg_write(uint8_t address, uint16_t value){
	encx24j600_reg_write(ENCX24J600_REG_MIREGADR, ENCX24J600_MIREGADR_R8 | address);
	encx24j600_reg_write(ENCX24J600_REG_MIWR, value);
	while(encx24j600_reg_read(ENCX24J600_REG_MISTAT) & ENCX24J600_MISTAT_BUSY);
}

