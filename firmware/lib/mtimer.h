#ifndef _MTIMER_H_
#define _MTIMER_H_

#include <stdint.h>

struct mtimer_t{
	uint32_t sec;
	uint16_t msec;
};

typedef struct mtimer_t mtimer_t;

void mtimer_init(uint32_t (*timer_get)(uint16_t *msec));
uint32_t mtimer_get(mtimer_t *mtimer);
void mtimer_timeout_set(mtimer_t *mtimer, uint32_t msec);
uint8_t mtimer_timeout(mtimer_t *mtimer);
void mtimer_sleep(uint32_t msec);

#endif

