#include "mmc.h"
#include <string.h>
#include "z80.h"
#include "hw.h"
#include "diskio.h"

static uint16_t mmc_len;
static uint32_t mmc_sector;
static uint8_t mmc_card_type, mmc_sector_valid, mmc_buffer[512];

static uint8_t mmc_cmd(uint8_t cmd, uint32_t arg);
static uint16_t mmc_get(uint8_t *buffer, uint16_t len);

void mmc_init(void){
	hw_spi_cs_disable_sd();
	mmc_card_type = mmc_sector_valid = 0;
}

uint8_t mmc_insert(void){
	uint8_t buf[4], cmd;
	uint16_t i;
	for(i = 0; i < 10; i++)hw_spi_sd_release();
	if(mmc_cmd(CMD0, 0) != 1)return(0);
	if(mmc_cmd(CMD8, 0x1AA) == 1){
		hw_spi_read_sd(buf, 4);
		if((buf[2] == 0x01) && (buf[3] == 0xAA)){
			for(i = 0; i < 1000; i++){
				if(mmc_cmd(ACMD41, (((uint32_t)1) << 30)) == 0)break;
			}
			if((i < 1000) && mmc_cmd(CMD58, 0) == 0){
				hw_spi_read_sd(buf, 4);
				mmc_card_type = (buf[0] & 0x40) ? (CT_SD2 | CT_BLOCK) : CT_SD2;
			}
		}
	}else{
		if(mmc_cmd(ACMD41, 0) <= 1){
			mmc_card_type = CT_SD1;
			cmd = ACMD41;
		}else{
			mmc_card_type = CT_MMC;
			cmd = CMD1;
		}
		for(i = 0; i < 1000; i++){
			if(mmc_cmd(cmd, 0) == 0)break;
		}
		if((i == 1000) || (mmc_cmd(CMD16, 512) != 0))mmc_card_type = 0;
	}
	hw_spi_sd_release();
	return(mmc_card_type);
}

uint16_t mmc_read_register(uint8_t cmd, uint8_t *buf, uint16_t count){
	uint16_t c;
	if(!mmc_card_type)return(0);
	mmc_cmd(cmd, 0);
	c = mmc_get(buf, count);
	hw_spi_sd_release();
	return(c);
}

uint8_t mmc_read_sector(uint32_t sector){
	if(!mmc_card_type)return(0);
	if(!(mmc_card_type & CT_BLOCK))sector *= 512;
	if(mmc_sector_valid && (mmc_sector == sector))return(1);
	if(mmc_cmd(CMD17, sector)){
		hw_spi_sd_release();
		return(0);
	}
	mmc_sector_valid = 0;
	if(!mmc_get(mmc_buffer, 512)){
		hw_spi_sd_release();
		return(0);
	}
	mmc_sector_valid = 1;
	mmc_sector = sector;
	hw_spi_sd_release();
	return(1);
}

uint16_t mmc_read(void *buf, uint32_t sector, uint16_t offset, uint16_t count){
	uint16_t i;
	if(!mmc_read_sector(sector))return(0);
	for(i = 0; i < count; i++)((uint8_t *)buf)[i] = mmc_buffer[offset + i];
	return(count);
}

static uint8_t mmc_cmd(uint8_t cmd, uint32_t arg){
	uint8_t i, buf[5];
	if(cmd & 0x80){
		cmd &= 0x7F;
		buf[0] = mmc_cmd(CMD55, 0);
		if(buf[0] > 1)return(buf[0]);
	}
	hw_spi_sd_release();
	hw_spi_read_sd(buf, 1);
	buf[0] = cmd;
	buf[1] = (arg >> 24);
	buf[2] = (arg >> 16);
	buf[3] = (arg >> 8);
	buf[4] = (arg >> 0);
	hw_spi_write_sd(buf, 5);
	buf[0] = 0x01;
	if(cmd == CMD0){ buf[0] = 0x95; }else if(cmd == CMD8){ buf[0] = 0x87; }
	hw_spi_write_sd(buf, 1);
	for(i = 0; i < 10; i++){
		hw_spi_read_sd(buf, 1);
		if(!(buf[0] & 0x80))break;
	}
	return(buf[0]);
}

static uint16_t mmc_get(uint8_t *buf, uint16_t count){
	uint16_t i;
	uint8_t c;
	for(i = 0; i < 1000; i++){
		hw_spi_read_sd(&c, 1);
		if(c == 0xFE){
			hw_spi_read_sd(buf, count);
			return(count);
		}
		if(c != 0xFF)return(0);
	}
	return(0);
}

DSTATUS disk_initialize(){
        return(0);
}

DRESULT disk_readp(
	BYTE* Buffer,       /* Pointer to the read buffer */
	DWORD SectorNumber, /* Sector number */
	WORD Offset,        /* Byte offset in the sector to start to read */
	WORD Count          /* Number of bytes to read */
){
	WORD res;
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_ENABLE | HW_PORT_OUTPUT_LED2_ON | HW_PORT_OUTPUT_LED3_ON));
	res = mmc_read(Buffer, SectorNumber, Offset, Count);
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_ENABLE | HW_PORT_OUTPUT_LED2_ON | HW_PORT_OUTPUT_LED3_OFF));
        if(res == Count)return(RES_OK);
        return(RES_NOTRDY);
}

