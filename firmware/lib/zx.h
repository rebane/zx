#ifndef _ZX_H_
#define _ZX_H_

#include <stdint.h>

#define ZX_COLOR_BLACK              (0)
#define ZX_COLOR_BLUE               (1)
#define ZX_COLOR_RED                (2)
#define ZX_COLOR_MAGENTA            (3)
#define ZX_COLOR_GREEN              (4)
#define ZX_COLOR_CYAN               (5)
#define ZX_COLOR_YELLOW             (6)
#define ZX_COLOR_WHITE              (7)
#define ZX_COLOR_TRANSPARENT        (8)
#define ZX_COLOR_CONTRAST           (9)

#define ZX_BORDER_BLACK             (ZX_COLOR_BLACK)
#define ZX_BORDER_BLUE              (ZX_COLOR_BLUE)
#define ZX_BORDER_RED               (ZX_COLOR_RED)
#define ZX_BORDER_MAGENTA           (ZX_COLOR_MAGENTA)
#define ZX_BORDER_GREEN             (ZX_COLOR_GREEN)
#define ZX_BORDER_CYAN              (ZX_COLOR_CYAN)
#define ZX_BORDER_YELLOW            (ZX_COLOR_YELLOW)
#define ZX_BORDER_WHITE             (ZX_COLOR_WHITE)
#define ZX_BORDER_DEFAULT           (ZX_BORDER_WHITE)

#define ZX_INK_BLACK                (ZX_COLOR_BLACK)
#define ZX_INK_BLUE                 (ZX_COLOR_BLUE)
#define ZX_INK_RED                  (ZX_COLOR_RED)
#define ZX_INK_MAGENTA              (ZX_COLOR_MAGENTA)
#define ZX_INK_GREEN                (ZX_COLOR_GREEN)
#define ZX_INK_CYAN                 (ZX_COLOR_CYAN)
#define ZX_INK_YELLOW               (ZX_COLOR_YELLOW)
#define ZX_INK_WHITE                (ZX_COLOR_WHITE)
#define ZX_INK_TRANSPARENT          (ZX_COLOR_TRANSPARENT)
#define ZX_INK_CONTRAST             (ZX_COLOR_CONTRAST)
#define ZX_INK_DEFAULT              (ZX_INK_BLACK)

#define ZX_PAPER_BLACK              (ZX_COLOR_BLACK)
#define ZX_PAPER_BLUE               (ZX_COLOR_BLUE)
#define ZX_PAPER_RED                (ZX_COLOR_RED)
#define ZX_PAPER_MAGENTA            (ZX_COLOR_MAGENTA)
#define ZX_PAPER_GREEN              (ZX_COLOR_GREEN)
#define ZX_PAPER_CYAN               (ZX_COLOR_CYAN)
#define ZX_PAPER_YELLOW             (ZX_COLOR_YELLOW)
#define ZX_PAPER_WHITE              (ZX_COLOR_WHITE)
#define ZX_PAPER_TRANSPARENT        (ZX_COLOR_TRANSPARENT)
#define ZX_PAPER_CONTRAST           (ZX_COLOR_CONTRAST)
#define ZX_PAPER_DEFAULT            (ZX_PAPER_WHITE)

#define ZX_BRIGHT_OFF               (0)
#define ZX_BRIGHT_ON                (1)
#define ZX_BRIGHT_TRANSPARENT       (8)
#define ZX_BRIGHT_DEFAULT           (ZX_BRIGHT_OFF)

#define ZX_FLASH_OFF                (0)
#define ZX_FLASH_ON                 (1)
#define ZX_FLASH_TRANSPARENT        (8)
#define ZX_FLASH_DEFAULT            (ZX_FLASH_OFF)

#define ZX_INVERSE_OFF              (0)
#define ZX_INVERSE_ON               (1)
#define ZX_INVERSE_DEFAULT          (ZX_INVERSE_OFF)

#define ZX_OVER_OFF                 (0)
#define ZX_OVER_ON                  (1)
#define ZX_OVER_DEFAULT             (ZX_OVER_OFF)

#define ZX_CURSOR_OFF               (0)
#define ZX_CURSOR_ON                (1)
#define ZX_CURSOR_DEFAULT           (ZX_CURSOR_OFF)
#define ZX_CURSOR_ATTR_DEFAULT      ((ZX_INK_DEFAULT & 0x07) | ((ZX_PAPER_DEFAULT & 0x07) << 3))

#define ZX_WRAP_OFF                 (0)
#define ZX_WRAP_ON                  (1)
#define ZX_WRAP_DEFAULT             (ZX_WRAP_ON)

#define ZX_PORT_ULA                 (0x00FE)
#define ZX_PORT_KB_15               (0xF7FE)
#define ZX_PORT_KB_60               (0xEFFE)
#define ZX_PORT_KB_QT               (0xFBFE)
#define ZX_PORT_KB_YP               (0xDFFE)
#define ZX_PORT_KB_AG               (0xFDFE)
#define ZX_PORT_KB_HE               (0xBFFE)
#define ZX_PORT_KB_SV               (0xFEFE)
#define ZX_PORT_KB_BS               (0x7FFE)

#define ZX_PORT_ULA_BORDER          (0x07 << 0)
#define ZX_PORT_ULA_BORDER_BLACK    (ZX_BORDER_BLACK << 0)
#define ZX_PORT_ULA_BORDER_BLUE     (ZX_BORDER_BLUE << 0)
#define ZX_PORT_ULA_BORDER_RED      (ZX_BORDER_RED << 0)
#define ZX_PORT_ULA_BORDER_MAGENTA  (ZX_BORDER_MAGENTA << 0)
#define ZX_PORT_ULA_BORDER_GREEN    (ZX_BORDER_GREEN << 0)
#define ZX_PORT_ULA_BORDER_CYAN     (ZX_BORDER_CYAN << 0)
#define ZX_PORT_ULA_BORDER_YELLOW   (ZX_BORDER_YELLOW << 0)
#define ZX_PORT_ULA_BORDER_WHITE    (ZX_BORDER_WHITE << 0)
#define ZX_PORT_ULA_BORDER_DEFAULT  (ZX_BORDER_DEFAULT << 0)
#define ZX_PORT_ULA_MIC             (0x01 << 3)
#define ZX_PORT_ULA_MIC_ON          (ZX_PORT_ULA_MIC)
#define ZX_PORT_ULA_MIC_OFF         (0x00)
#define ZX_PORT_ULA_MIC_DEFAULT     (ZX_PORT_ULA_MIC_OFF)
#define ZX_PORT_ULA_EAR_OUT         (0x01 << 4)
#define ZX_PORT_ULA_EAR_OUT_ON      (ZX_PORT_ULA_EAR_OUT)
#define ZX_PORT_ULA_EAR_OUT_OFF     (0x00)
#define ZX_PORT_ULA_EAR_OUT_DEFAULT (ZX_PORT_ULA_EAR_OUT_OFF)
#define ZX_PORT_ULA_EAR_IN          (0x01 << 5)
#define ZX_PORT_ULA_EAR_IN_ON       (ZX_PORT_ULA_EAR_IN)
#define ZX_PORT_ULA_EAR_IN_OFF      (0x00)
#define ZX_PORT_ULA_DEFAULT         (ZX_PORT_ULA_BORDER_DEFAULT | ZX_PORT_ULA_MIC_DEFAULT | ZX_PORT_ULA_EAR_OUT_DEFAULT)

#define ZX_FRAMEBUFFER              (0x4000)
#define ZX_FRAMEBUFFER_LEN          (0x1B00)
#define ZX_FRAMEBUFFER_PIXELS       (0x4000)
#define ZX_FRAMEBUFFER_PIXELS_LEN   (0x1800)
#define ZX_FRAMEBUFFER_ATTRS        (0x5800)
#define ZX_FRAMEBUFFER_ATTRS_LEN    (0x0300)
#define ZX_ROM_FONT                 (0x3C00)
#define ZX_ROM_FONT_LEN             (0x0400)
#define ZX_CONSOLE                  (0x5B00)

#define ZX_CONSOLE_X                (0x5B00)
#define ZX_CONSOLE_Y                (0x5B01)
#define ZX_CONSOLE_INK              (0x5B02)
#define ZX_CONSOLE_PAPER            (0x5B03)
#define ZX_CONSOLE_BRIGHT           (0x5B04)
#define ZX_CONSOLE_FLASH            (0x5B05)
#define ZX_CONSOLE_BORDER           (0x5B06)
#define ZX_CONSOLE_INVERSE          (0x5B07)
#define ZX_CONSOLE_OVER             (0x5B08)
#define ZX_CONSOLE_CURSOR           (0x5B09)
#define ZX_CONSOLE_CURSOR_ATTR      (0x5B0A)
#define ZX_CONSOLE_WRAP             (0x5B0B)

void zx_init(uint8_t *(*font)(uint16_t c));
uint32_t zx_timer_get(uint16_t *msec);
void zx_font(void *font_base);
void zx_default();

void zx_cls();
void zx_border(uint8_t border);
void zx_ink(uint8_t ink);
void zx_paper(uint8_t paper);
void zx_flash(uint8_t flash);
void zx_bright(uint8_t bright);
void zx_over(uint8_t over);
void zx_inverse(uint8_t inverse);
void zx_color(uint8_t ink, uint8_t paper, uint8_t bright, uint8_t flash);
void zx_at(uint8_t x, uint8_t y);
void zx_cursor(uint8_t cursor);
void zx_wrap(uint8_t wrap);
uint8_t zx_attr(uint8_t x, uint8_t y);

uint8_t zx_getkey();
uint8_t zx_getchar(uint8_t c);

void putchar(int c);

#endif

