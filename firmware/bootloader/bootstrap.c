#include "hw.h"
#include "zx.h"

void main() __naked{
	__asm

	.macro set address, val
	ld a, #val
	ld (address), a
	.endm

	// disable interrupts
	di

	// rom disable, sd disable, leds off
	ld a, #(HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF)
	out (HW_PORT_OUTPUT), a

	// spi idle
	ld a, #(HW_PORT_SPI_EE_DISABLE | HW_PORT_SPI_SD_DISABLE | HW_PORT_SPI_ETH_DISABLE | HW_PORT_SPI_MOSI_LOW | HW_PORT_SPI_SCK_LOW)
	out (HW_PORT_SPI), a

	// border
	ld a, #(ZX_PORT_ULA_DEFAULT)
	out (ZX_PORT_ULA), a

	// mask screen
	ld hl, #(ZX_FRAMEBUFFER_ATTRS)
	ld (hl), #(((ZX_PAPER_DEFAULT & 0x07) << 3) | ((ZX_PAPER_DEFAULT & 0x07) << 0))
	ld de, #(ZX_FRAMEBUFFER_ATTRS + 1)
	ld bc, #(ZX_FRAMEBUFFER_ATTRS_LEN - 1)
	ldir

	// clear pixels for first 8 rows
	ld hl, #(ZX_FRAMEBUFFER_PIXELS)
	ld (hl), #0x00
	ld de, #(ZX_FRAMEBUFFER_PIXELS + 1)
	ld bc, #((32 * 8 * 8) - 1)
	ldir

	// reset attributes for first 8 rows
	ld hl, #(ZX_FRAMEBUFFER_ATTRS)
	ld (hl), #0x38
	ld (hl), #(((ZX_FLASH_DEFAULT & 0x01) << 7) | ((ZX_BRIGHT_DEFAULT & 0x01) << 6) | ((ZX_PAPER_DEFAULT & 0x07) << 3) | ((ZX_INK_DEFAULT & 0x07) << 0))
	ld de, #(ZX_FRAMEBUFFER_ATTRS + 1)
	ld bc, #((32 * 8) - 1)
	ldir

	// reset console parameters
	set ^/ZX_CONSOLE_X/, ^/0/
	set ^/ZX_CONSOLE_Y/, ^/0/
	set ^/ZX_CONSOLE_INK/, ^/ZX_INK_DEFAULT/
	set ^/ZX_CONSOLE_PAPER/, ^/ZX_PAPER_DEFAULT/
	set ^/ZX_CONSOLE_BRIGHT/, ^/ZX_BRIGHT_DEFAULT/
	set ^/ZX_CONSOLE_FLASH/, ^/ZX_FLASH_DEFAULT/
	set ^/ZX_CONSOLE_BORDER/, ^/ZX_BORDER_DEFAULT/
	set ^/ZX_CONSOLE_INVERSE/, ^/ZX_INVERSE_DEFAULT/
	set ^/ZX_CONSOLE_OVER/, ^/ZX_OVER_DEFAULT/
	set ^/ZX_CONSOLE_CURSOR/, ^/ZX_CURSOR_DEFAULT/
//	set ^/ZX_CONSOLE_CURSOR_ATTR/, ^/(((ZX_FLASH_DEFAULT & 0x01) << 7) | ((ZX_BRIGHT_DEFAULT & 0x01) << 6) | ((ZX_PAPER_DEFAULT & 0x07) << 3) | ((ZX_INK_DEFAULT & 0x07) << 0))/
	ld a, (((ZX_FLASH_DEFAULT & 0x01) << 7) | ((ZX_BRIGHT_DEFAULT & 0x01) << 6) | ((ZX_PAPER_DEFAULT & 0x07) << 3) | ((ZX_INK_DEFAULT & 0x07) << 0))
	ld (ZX_CONSOLE_CURSOR_ATTR), a
	set ^/ZX_CONSOLE_WRAP/, ^/ZX_WRAP_DEFAULT/

	// copy remaining code to ram
	ld hl, #00001$
	ld de, #(CODE_LOC)
	ld bc, #1024 // it's impossible to calculate exact amount if compiled separately, but 1024 is maximum
	ldir

	// jump to ram code
	jp CODE_LOC
00001$:

	__endasm;
}

