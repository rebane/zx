#include <stdint.h>
#include <string.h>
#include "z80.h"
#include "hw.h"
#include "zx.h"

static void clear_and_jump(uint16_t address);
static void do_clear_and_jump() __naked;
static void message(char *text);

void main(){
	out(HW_PORT_OUTPUT, HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF);
	if((in(HW_PORT_INPUT) & (HW_PORT_INPUT_SD_CD | HW_PORT_INPUT_EXT_SCK | HW_PORT_INPUT_EXT_MOSI)) == (HW_PORT_INPUT_SD_CD_MISSING | HW_PORT_INPUT_EXT_SCK_HIGH | HW_PORT_INPUT_EXT_MOSI_HIGH)){
		clear_and_jump(0x0000);
	}
	hw_spi_cs_disable_eeprom();
	hw_spi_cs_enable_eeprom();
	if((in(HW_PORT_INPUT) & HW_PORT_INPUT_EXT_SCK) == HW_PORT_INPUT_EXT_SCK_LOW){
		hw_spi_write_eeprom("\x03\x00\xC0\x00", 4);
		if((in(HW_PORT_INPUT) & HW_PORT_INPUT_EXT_MOSI) == HW_PORT_INPUT_EXT_MOSI_LOW){ // 16384 bytes of SNA from address 0xC000 to address 0x5C00
			message("Loading partial SNA...");
			hw_spi_read_eeprom((void *)0x5C00, 16384);
		}else{ // 41984 bytes of SNA from address 0xC000 to address 0x5C00
			message("Loading SNA...");
			hw_spi_read_eeprom((void *)0x5C00, 41984);
		}
		hw_spi_cs_disable_eeprom();
		// clear pixels for first 8 rows
		__asm__("ld hl, #(0x4000)");
		__asm__("ld (hl), #0x00");
		__asm__("ld de, #(0x4000 + 1)");
		__asm__("ld bc, #((32 * 8 * 8) - 1)");
		__asm__("ldir");
		clear_and_jump(0x5C00);
	}
	*(uint8_t *)0x5B01 = 1; // ZX_CONSOLE_Y = 1;
	if((in(HW_PORT_INPUT) & HW_PORT_INPUT_EXT_MOSI) == HW_PORT_INPUT_EXT_MOSI_LOW){ // ROM2
		message("Loading ROM2...");
		out(HW_PORT_OUTPUT, HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF);
		hw_spi_write_eeprom("\x03\x00\x80\x00", 4);
	}else{ // DEFAULT ROM
		message("Loading...");
		out(HW_PORT_OUTPUT, HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF);
		hw_spi_write_eeprom("\x03\x00\x40\x00", 4);
	}
	hw_spi_read_eeprom((void *)0, 16384);
	hw_spi_cs_disable_eeprom();
	clear_and_jump(0x0000);
}

static void clear_and_jump(uint16_t address) __naked{
	*(uint8_t *)0x5B80 = (address >> 0) & 0xFF;
	*(uint8_t *)0x5B81 = (address >> 8) & 0xFF;
	memcpy((void *)0x5B82, do_clear_and_jump, 0x80 - 2); // TODO: how to calculate exact amount
	__asm__("jp 0x5B82");
}

static void do_clear_and_jump() __naked{
	// clear screen
	__asm__("ld hl, #(0x4000 + (32 * 8 * 8))");
	__asm__("ld (hl), #0x00");
	__asm__("ld de, #(0x4000 + (32 * 8 * 8) + 1)");
	__asm__("ld bc, #((32 * 8 * 16) - 1)");
	__asm__("ldir");

	// reset attributes
	__asm__("ld hl, #(0x5800 + (32 * 8))");
	__asm__("ld (hl), #(0x38)");
	__asm__("ld de, #(0x5800 + (32 * 8) + 1)");
	__asm__("ld bc, #((32 * 8 * 2) - 1)");
	__asm__("ldir");

	// jump, TODO: set register for original ROM ("NEW" command or cold boot)
	__asm__("ld hl, (0x5B80)");
	__asm__("jp (hl)");
}

static void message(char *text){
	uint8_t i, j;
	for(i = 0; text[i]; i++){
		for(j = 0; j < 8; j++){
			((uint8_t *)ZX_FRAMEBUFFER + i)[256 * j] = ((uint8_t *)ZX_ROM_FONT)[((uint16_t)text[i] * 8) + j];
		}
	}
}

