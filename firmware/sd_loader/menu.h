#ifndef _MENU_H_
#define _MENU_H_

#include <stdint.h>

int16_t menu(char *title, int16_t count, void (*get)(int16_t n, uint8_t *item), uint8_t esc);

#endif

