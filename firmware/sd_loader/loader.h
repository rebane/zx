#ifndef _LOADER_H_
#define _LOADER_H_

#include <stdint.h>

uint8_t loader_init(char *name);
void loader_progress(uint8_t p);
uint16_t loader_read(void *buf, uint16_t count);

#endif

