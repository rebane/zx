#include "loader.h"
#include <string.h>
#include "z80.h"
#include "hw.h"
#include "zx.h"
#include "pff.h"

uint8_t loader_init(char *name){
	if(pf_open(name))return(0);
	memset((void *)ZX_FRAMEBUFFER_ATTRS, 0x3F, 736);
	memset((void *)0x5AE0, 0x2D, 32);
	return(1);
}

void loader_progress(uint8_t p){
	memset((void *)0x5AE0, 0x00, p);
}

uint16_t loader_read(void *buf, uint16_t count){
	WORD br;
	if(pf_read(buf, count, &br))return(0);
	if(br != count)return(0);
	return(count);
}

