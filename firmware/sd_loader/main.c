#include <stdint.h>
#include <string.h>
#include "z80.h"
#include "zx.h"
#include "hw.h"
#include "mmc.h"
#include "pff.h"
#include "menu.h"
#include "rom_loader.h"
#include "sna_loader.h"

static uint8_t mode;
static struct{
	char name[8];
}*files = (void *)0x6000;
static FATFS fs;
static const char *ext;

static uint8_t sd_open();
static void sd_close();
static void menu_main(int16_t n, uint8_t *item);
static void menu_files(int16_t n, uint8_t *item);
static int16_t read_files(uint16_t len);
static void print_loading(char *name, uint8_t error);
static void printf(char *s); // use own printf, without formatting
static uint8_t *font_get(uint16_t c);
static void basic_loader_start();
static void do_install_ram_font();
static void install_ram_font(void *font_base);

void main(){
	int16_t l;
	char filename[13];
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF));
	zx_init(font_get);
	install_ram_font((void *)0x5C00);
	zx_wrap(ZX_WRAP_OFF);
	if(sd_open()){
		print_loading("AUTOEXEC.SNA", 0);
		if(!pf_open("AUTOEXEC.SNA")){
			sna_loader_load("AUTOEXEC.SNA");
			print_loading("AUTOEXEC.SNA", 1);
		}else{
			printf("not found\n");
		}
		print_loading("AUTOEXEC.ROM", 0);
		if(!pf_open("AUTOEXEC.ROM")){
			rom_loader_load("AUTOEXEC.ROM");
			print_loading("AUTOEXEC.ROM", 1);
		}else{
			printf("not found\n");
		}
	}
	sd_close();
	while(1){
		mode = menu("Select", 3, menu_main, 0);
		if(mode == 2){
			memcpy((void *)0x5B80, basic_loader_start, 0x80);
			__asm
			jp 0x5B80
			__endasm;
		}
		zx_default();
		zx_cls();
		zx_wrap(ZX_WRAP_OFF);
		for(l = 0; l < 13; l++)filename[l] = 0;
		if(!sd_open())goto done;
		if(mode == 0){
			ext = "SNA";
			l = read_files(49179);
		}else{
			ext = "ROM";
			l = read_files(16384);
		}
		if(l < 1)goto done;
		printf("Press BREAK to cancel\n");
		l = menu("Files", l, menu_files, 1);
		if(l < 0)goto done;
		menu_files(l, filename);
		if(mode == 0){
			sna_loader_load(filename);
		}else{
			rom_loader_load(filename);
		}
		print_loading(filename, 1);
done:
		sd_close();
	}
}

static uint8_t sd_open(){
	uint8_t i, s;
	if((in(HW_PORT_INPUT) & HW_PORT_INPUT_SD_CD) == HW_PORT_INPUT_SD_CD_MISSING){
		printf("SD card not inserted\n");
		return(0);
	}
	printf("Detecting SD card... ");
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_ENABLE | HW_PORT_OUTPUT_LED2_ON | HW_PORT_OUTPUT_LED3_OFF));
	mmc_init();
	for(i = 0; i < 5; i++){
		s = mmc_insert();
		if(s)break;
	}
	if(i == 5){
		printf("error\n");
		return(0);
	}
	printf("type: ");
	if(s & CT_SD2){
		printf("SD,v2\n");
	}else if(s & CT_SD1){
		printf("SD,v1\n");
	}else{
		printf("MMC\n");
	}
	printf("Mounting SD card... ");
	if(pf_mount(&fs)){
		printf("error\n");
		return(0);
	}
	printf("done\n");
	return(1);
}

static void sd_close(){
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF));
}

static void menu_files(int16_t n, uint8_t *item){
	uint8_t i;
	for(i = 0; (i < 8) && files[n].name[i]; i++)item[i] = files[n].name[i];
	item[i++] = '.';
	item[i++] = ext[0];
	item[i++] = ext[1];
	item[i] = ext[2];
}

static void menu_main(int16_t n, uint8_t *item){
	if(n == 0){
		memcpy(item, "SNA Loader", 10);
	}else if(n == 1){
		memcpy(item, "ROM Loader", 10);
	}else{
		memcpy(item, "48 BASIC", 8);
	}
}

static int16_t read_files(uint16_t len){
	DIR dp;
	FILINFO fno;
	uint16_t i;
	int16_t l;
	uint8_t s;
	printf("Reading *.");
	printf((void *)ext);
	printf(" files... ");
	if(pf_opendir(&dp, "/")){
		printf("error\n");
		return(-1);
	}
	l = 0;
	while(1){
		if(pf_readdir(&dp, &fno)){
			printf("error\n");
			return(-1);
		}
		if(fno.fname[0] == 0)break;
		i = strlen(fno.fname);
		if(!(fno.fattrib & AM_DIR) && (fno.fsize == len) && (i > 4) && (fno.fname[i - 4] == '.') && (fno.fname[i - 3] == ext[0]) && (fno.fname[i - 2] == ext[1]) && (fno.fname[i - 1] == ext[2])){
			for(s = 0; s < (i - 4); s++)files[l].name[s] = fno.fname[s];
			for( ; s < 12; s++)files[l].name[s] = 0;
			l++;
			if(l == 5119)break;
		}
	}
	if(!l){
		printf("not found\n");
	}else{
		printf("done\n");
	}
	return(l);
}

static void print_loading(char *name, uint8_t error){
	if(error){
		install_ram_font((void *)0x5C00);
		zx_default();
		zx_cls();
		zx_wrap(ZX_WRAP_OFF);
	}
	printf("Loading ");
	printf(name);
	printf("...");
	if(error)printf(" error\n");
}

static void printf(char *s){
	for( ; *s; s++)putchar(*s);
}

static uint8_t *font_get(uint16_t c){
	if(c == 0x80)return("\xFE\xFC\xF8\xF0\xE0\xC0\x80\x00"); // ◤ - black upper left traingle
	if(c == 0x81)return("\x80\x80\x80\x80\x80\x80\x80\x80"); // ▌ - left half block
	if(c == 0x82)return("\x80\x80\x80\x80\x80\x80\x80\xFF"); // ▙ - quadrant upper left and lower left and lower right
	if(c == 0x83)return("\x00\x00\x00\x00\x00\x00\x00\xFF"); // ▄ - lower half block
	if(c == 0x84)return("\x01\x01\x01\x01\x01\x01\x01\xFF"); // ▟ - quadrant upper right and lower left and lower right
	if(c == 0x85)return("\x01\x01\x01\x01\x01\x01\x01\x01"); // ▐ - right half block
	return(NULL);
}

static void basic_loader_start() __naked{
	__asm
	ld a, #(HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF)
	out (HW_PORT_OUTPUT), a
	jp 0x0000
	__endasm;
}

static void do_install_ram_font() __naked{
	__asm
	out (HW_PORT_OUTPUT), a
	ldir
	xor a
	out (HW_PORT_OUTPUT), a
	ret
	__endasm;
}

static void install_ram_font(void *font_base){
	memcpy((void *)0x5B80, do_install_ram_font, 0x80); // TODO: how to calculate exact amount
	__asm
	ld hl, #2
	add hl, sp
	ld e, (hl)
	inc hl
	ld d, (hl)
	ld hl, #(ZX_ROM_FONT)
	ld bc, #(ZX_ROM_FONT_LEN)
	ld a, #(HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF);
	// TODO: push interrupt mask
	di
	call 0x5B80
	// TODO: restore interrupt mask
	__endasm;
	zx_font(font_base);
}

