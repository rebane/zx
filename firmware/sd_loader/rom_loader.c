#include "rom_loader.h"
#include <string.h>
#include "loader.h"
#include "z80.h"
#include "hw.h"
#include "zx.h"
#include "pff.h"

static void rom_loader_start();

void rom_loader_load(char *name){
	uint8_t i;
	if(!loader_init(name))return;
	for(i = 0; i < 32; i++){
		loader_progress(i);
		if(!loader_read((void *)((uint16_t)0x6000 + ((uint16_t)i * 512)), 512))return;
	}
	loader_progress(32);
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF));
	memcpy((void *)0x5B80, rom_loader_start, 0x80); // TODO: how to calculate exact amount
	__asm
	di
	ld hl, #0x6000
	ld de, #0x0000
	ld bc, #16384
	jp 0x5B80
	__endasm;
}

static void rom_loader_start() __naked{
	__asm
	ldir
	jp 0x0000
	__endasm;
}

