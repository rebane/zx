#include "sna_loader.h"
#include <string.h>
#include "loader.h"
#include "z80.h"
#include "hw.h"
#include "zx.h"
#include "pff.h"

static uint8_t sna_loader_header[29], sna_loader_attrs[768];

void sna_loader_load(char *name){
	uint8_t i, *sp;
	if(!loader_init(name))return;
	if(!loader_read(sna_loader_header, 27))return;
	loader_progress(1);
	for(i = 0; i < 6; i++){
		if(!loader_read((void *)((uint16_t)0x4000 + ((uint16_t)i * 1024)), 1024))return;
		loader_progress(i + 2);
	}
	if(!loader_read(sna_loader_attrs, 768))return;
	loader_progress(8);
	for(i = 0; i < 24; i++){
		if(!loader_read((void *)((uint16_t)0x5B00 + ((uint16_t)i * 1760)), 1760))return;
		loader_progress(i + 8);
	}
	loader_progress(32);
	out(HW_PORT_OUTPUT, (HW_PORT_OUTPUT_ROM_DISABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF));

	zx_border(sna_loader_header[26] & 0x07);
	memcpy((void *)0x5800, sna_loader_attrs, 768);
	sp = (uint8_t *)(*(uint16_t *)&sna_loader_header[23]);
	*(uint16_t *)&sna_loader_header[27] = (uint16_t)(sp - 7);
	sp[-7] = 0xD3; sp[-6] = 0x01;                // out (0x01), a; 0x01 = HW_PORT_OUTPUT, a = (HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF)
	sp[-5] = 0x3E; sp[-4] = sna_loader_header[22];      // ld a, sna_loader_header[22]
	sp[-3] = (sna_loader_header[19] & 1) ? 0xFB : 0xF3; // ei / di
	sp[-2] = 0xED; sp[-1] = 0x45;                // retn
	if(sna_loader_header[25] == 0){
		__asm
		im 0
		__endasm;
	}else if(sna_loader_header[25] == 1){
		__asm
		im 1
		__endasm;
	}else{
		__asm
		im 2
		__endasm;
	}

	__asm
	ld hl, (_sna_loader_header + 27) // write jump address
	ld (00001$), hl

	ld hl, #_sna_loader_header + 7   // AF'
	ld sp, hl
	pop af
	__endasm;
	__asm__("ex af, af\'");   // warning: missing terminating ' character
	__asm
	ld a, (_sna_loader_header + 5)   // BC'
	ld b, a
	ld a, (_sna_loader_header + 6)
	ld c, a
	ld de, (_sna_loader_header + 3)  // DE'
	ld hl, (_sna_loader_header + 1)  // HL'
	exx
	ld ix, (_sna_loader_header + 17) // IX
	ld iy, (_sna_loader_header + 15) // IY
	ld de, (_sna_loader_header + 11) // DE
	ld a, (_sna_loader_header + 0)   // I
	ld i, a
	ld a, (_sna_loader_header + 20)  // R
	ld r, a
	ld hl, #_sna_loader_header + 21  // F
	ld sp, hl
	pop af
	ld hl, (_sna_loader_header + 13) // BC
	ld b, h
	ld c, l
	ld sp, (_sna_loader_header + 23) // SP
	ld hl, (_sna_loader_header + 9)  // HL
	ld a, #(HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED2_OFF | HW_PORT_OUTPUT_LED3_OFF)
	.byte 0xC3                // jump
	00001$:
	.ds 2
	__endasm;
}

