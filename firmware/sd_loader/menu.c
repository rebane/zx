#include "menu.h"
#include <string.h>
#include "z80.h"
#include "zx.h"

static char *menu_copyright = "\xA9 2016 Rebane, rebane@alkohol.ee";
static void (*menu_get)(int16_t n, uint8_t *item);

static void menu_list(uint8_t j, int16_t i, int16_t l);

int16_t menu(char *title, int16_t count, void (*get)(int16_t n, uint8_t *item), uint8_t esc){
	int16_t i;
	uint8_t j, s, keyboard_state, keyboard_count;

	menu_get = get;

	zx_at(0, 23);
	zx_color(ZX_INK_BLACK, ZX_PAPER_WHITE, ZX_BRIGHT_OFF, ZX_FLASH_OFF);
	for(i = 0; menu_copyright[i]; i++)putchar(menu_copyright[i]);

	zx_at(9, 9);
	zx_color(ZX_INK_WHITE, ZX_PAPER_BLACK, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	for(i = 0; (i < 8) && title[i]; i++)putchar(title[i]);
	for( ; i < 8; i++)putchar(' ');
	zx_color(ZX_INK_BLACK, ZX_PAPER_RED, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	putchar(0x80);
	zx_color(ZX_INK_RED, ZX_PAPER_YELLOW, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	putchar(0x80);
	zx_color(ZX_INK_YELLOW, ZX_PAPER_GREEN, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	putchar(0x80);
	zx_color(ZX_INK_GREEN, ZX_PAPER_CYAN, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	putchar(0x80);
	zx_color(ZX_INK_CYAN, ZX_PAPER_BLACK, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	putchar(0x80);
	putchar(' ');

	menu_list(0, 0, count);

	zx_at(9, 14);
	zx_color(ZX_INK_BLACK, ZX_PAPER_WHITE, ZX_BRIGHT_ON, ZX_FLASH_OFF);
	putchar(0x82);
	for(i = 0; i < 12; i++)putchar(0x83);
	putchar(0x84);
	i = 0;
	j = s = 0;
	keyboard_state = keyboard_count = 0;

	while(1){
		s = ((in(ZX_PORT_KB_BS) << 3) & 0x08) | ((in(ZX_PORT_KB_60) >> 2) & 0x06) | (in(ZX_PORT_KB_HE) & 0x01);
		if(keyboard_state == s){
			if(keyboard_count < 254)keyboard_count++;
		}else{
			keyboard_state = s;
			keyboard_count = 0;
		}
		if(keyboard_count == 8){
			if(keyboard_state == 0x0B){
				if(i < (count - 1)){
					i++;
					if((j < (count - 1)) && (j < 3))j++;
				}else{
					i = 0;
					j = 0;
				}
			}else if(keyboard_state == 0x0D){
				if(i){
					i--;
					if(j)j--;
				}else{
					i = (count - 1);
					j = (count - 1);
					if(j > 3)j = 3;
				}
			}else if(keyboard_state == 0x0E){
				// if(!count)return(-1);
				return(i);
			}else if(esc && (keyboard_state == 0x07)){
				return(-1);
			}
			menu_list(j, i, count);
		}
	}
}

static void menu_list(uint8_t j, int16_t i, int16_t count){
	uint8_t t, u, buffer[15];
	i -= j;
	for(t = 0; t < 4; t++){
		memcpy(buffer, "\x81            \x85", 14);
		if(/*count && */(i < count))menu_get(i, &buffer[1]);
		if(/*count && */(t == j)){
			zx_color(ZX_INK_BLACK, ZX_PAPER_CYAN, ZX_BRIGHT_ON, ZX_FLASH_OFF);
		}else{
			zx_color(ZX_INK_BLACK, ZX_PAPER_WHITE, ZX_BRIGHT_ON, ZX_FLASH_OFF);
		}
		zx_at(9, 10 + t);
		for(u = 0; u < 14; u++)putchar(buffer[u]);
		i++;
	}
}

