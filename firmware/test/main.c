#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "z80.h"
#include "hw.h"
#include "zx.h"
#include "mtimer.h"

static uint8_t utf8_getchar();
static uint8_t *font_get(uint16_t c);

int main(){
	uint8_t c;

	zx_init(font_get);
	zx_default();
	zx_border(ZX_BORDER_CYAN);
	zx_cursor(ZX_CURSOR_ON);
	mtimer_init(zx_timer_get);

	while(1){
		c = utf8_getchar();
		if(c == 0)continue;
		if(c == 0x1B){    // ESC
			zx_cls();
			continue;
		}
		if(c == 0x08){    // DELETE
			putchar(0x08);
			putchar(' ');
		}
		putchar(c);
	}
}

static uint8_t caps_lock = 0, utf8_getchar_buffer[4], utf8_getchar_len = 0;

static uint8_t utf8_getchar(){
	uint8_t k;
	if(utf8_getchar_len)return(utf8_getchar_buffer[--utf8_getchar_len]);
	k = zx_getkey();
	if(k == 0x8B){       // sym + e (euro)
		utf8_getchar_buffer[0] = 0xAC;
		utf8_getchar_buffer[1] = 0x82;
		utf8_getchar_len = 2;
		return(0xE2);
	}else if(k == 0x5C){ // caps + 2 (caps lock)
		if(caps_lock){
			caps_lock = 0;
			out(HW_PORT_OUTPUT, HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED3_OFF);
		}else{
			caps_lock = 1;
			out(HW_PORT_OUTPUT, HW_PORT_OUTPUT_ROM_ENABLE | HW_PORT_OUTPUT_SD_DISABLE | HW_PORT_OUTPUT_LED3_ON);
		}
		return(0);
	}
	k = zx_getchar(k);
	if(caps_lock){
		if(islower(k)){
			k = toupper(k);
		}else if(isupper(k)){
			k = tolower(k);
		}
	}
	return(k);
}

static uint8_t *font_get(uint16_t c){
	if(c == 0x0027)return("\x00\x08\x08\x00\x00\x00\x00\x00"); // ' - apostrophe
	if(c == 0x005E)return("\x00\x10\x28\x44\x00\x00\x00\x00"); // ^ - caret
	if(c == 0x20AC)return("\x00\x3C\x42\xF8\xF8\x42\x3C\x00"); // € - euro
	return(NULL);
}

