<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="11" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="7" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Flex-Kleb" color="1" fill="7" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE">
<packages>
<package name="HOLE-M2.5">
<hole x="0" y="0" drill="2.6"/>
<polygon width="0.2" layer="41">
<vertex x="-2.5" y="0" curve="180"/>
<vertex x="2.5" y="0" curve="180"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-2.5" y="0" curve="-180"/>
<vertex x="2.5" y="0" curve="-180"/>
</polygon>
<circle x="0" y="0" radius="2.5" width="0.2" layer="22"/>
<circle x="0" y="0" radius="2.5" width="0.2" layer="21"/>
<circle x="0" y="0" radius="2.5" width="0.2" layer="51"/>
</package>
<package name="HOLE-M3">
<hole x="0" y="0" drill="3.2"/>
<polygon width="0.2" layer="41">
<vertex x="-3" y="0" curve="180"/>
<vertex x="3" y="0" curve="180"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-3" y="0" curve="-180"/>
<vertex x="3" y="0" curve="-180"/>
</polygon>
<circle x="0" y="0" radius="3" width="0.2" layer="22"/>
<circle x="0" y="0" radius="3" width="0.2" layer="21"/>
<circle x="0" y="0" radius="3" width="0.2" layer="51"/>
</package>
<package name="HOLE-M4">
<hole x="0" y="0" drill="4.2"/>
<polygon width="0.2" layer="41">
<vertex x="-3.5" y="0" curve="180"/>
<vertex x="3.5" y="0" curve="180"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-3.5" y="0" curve="-180"/>
<vertex x="3.5" y="0" curve="-180"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.2" layer="22"/>
<circle x="0" y="0" radius="3.5" width="0.2" layer="21"/>
<circle x="0" y="0" radius="3.5" width="0.2" layer="51"/>
</package>
<package name="TACTILE-TH-90">
<pad name="GND@1" x="-3.5" y="-1.3" drill="1.3"/>
<pad name="GND@2" x="3.5" y="-1.3" drill="1.3"/>
<pad name="A" x="-2.25" y="1.2" drill="1"/>
<pad name="B" x="2.25" y="1.2" drill="1"/>
<wire x1="-4.2" y1="3.7" x2="-4.2" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="4.2" y2="-2.8" width="0.2" layer="21"/>
<wire x1="4.2" y1="-2.8" x2="4.2" y2="3.7" width="0.2" layer="21"/>
<wire x1="4.2" y1="3.7" x2="-4.2" y2="3.7" width="0.2" layer="21"/>
<wire x1="-1.6" y1="3.7" x2="-1.6" y2="9.5" width="0.2" layer="51"/>
<wire x1="-1.6" y1="9.5" x2="1.6" y2="9.5" width="0.2" layer="51"/>
<wire x1="1.6" y1="9.5" x2="1.6" y2="3.7" width="0.2" layer="51"/>
<text x="-1.8" y="-4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.6" y1="3.7" x2="-4.2" y2="3.7" width="0.2" layer="51"/>
<wire x1="-4.2" y1="3.7" x2="-4.2" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-4.2" y1="-2.8" x2="4.2" y2="-2.8" width="0.2" layer="51"/>
<wire x1="4.2" y1="-2.8" x2="4.2" y2="3.7" width="0.2" layer="51"/>
<wire x1="4.2" y1="3.7" x2="1.6" y2="3.7" width="0.2" layer="51"/>
<text x="-1.5" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="TACTILE-PANASONIC-EVQX">
<smd name="A@1" x="-1.85" y="2.2" dx="0.6" dy="1" layer="1"/>
<smd name="B@1" x="1.85" y="2.2" dx="0.6" dy="1" layer="1"/>
<smd name="A@2" x="-1.85" y="-2.2" dx="0.6" dy="1" layer="1"/>
<smd name="B@2" x="1.85" y="-2.2" dx="0.6" dy="1" layer="1"/>
<smd name="GND@1" x="-2.525" y="0" dx="0.45" dy="1" layer="1"/>
<smd name="GND@2" x="2.525" y="0" dx="0.45" dy="1" layer="1"/>
<wire x1="-2.5" y1="0.8" x2="-2.5" y2="2.5" width="0.2" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.4" y2="2.5" width="0.2" layer="21"/>
<wire x1="-1.3" y1="2.5" x2="1.3" y2="2.5" width="0.2" layer="21"/>
<wire x1="2.4" y1="2.5" x2="2.6" y2="2.5" width="0.2" layer="21"/>
<wire x1="2.6" y1="2.5" x2="2.6" y2="0.8" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-0.8" x2="-2.5" y2="-2.5" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.4" y2="-2.5" width="0.2" layer="21"/>
<wire x1="-1.3" y1="-2.5" x2="1.3" y2="-2.5" width="0.2" layer="21"/>
<wire x1="2.4" y1="-2.5" x2="2.6" y2="-2.5" width="0.2" layer="21"/>
<wire x1="2.6" y1="-2.5" x2="2.6" y2="-0.8" width="0.2" layer="21"/>
<text x="-1.7" y="3.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="2.5" x2="2.6" y2="2.5" width="0.2" layer="51"/>
<wire x1="2.6" y1="2.5" x2="2.6" y2="-2.5" width="0.2" layer="51"/>
<wire x1="2.6" y1="-2.5" x2="-2.5" y2="-2.5" width="0.2" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.2" layer="51"/>
</package>
<package name="MICRO-SD">
<wire x1="1.6" y1="7.6" x2="8.6" y2="7.6" width="0.2" layer="21"/>
<wire x1="8.6" y1="-7.6" x2="1.8" y2="-7.6" width="0.2" layer="21"/>
<wire x1="0" y1="3" x2="0" y2="5.6" width="0.2" layer="21"/>
<smd name="1" x="0" y="2.25" dx="1.6" dy="0.7" layer="1"/>
<smd name="2" x="0" y="1.15" dx="1.6" dy="0.7" layer="1"/>
<smd name="3" x="0" y="0.05" dx="1.6" dy="0.7" layer="1"/>
<smd name="4" x="0" y="-1.05" dx="1.6" dy="0.7" layer="1"/>
<smd name="5" x="0" y="-2.15" dx="1.6" dy="0.7" layer="1"/>
<smd name="6" x="0" y="-3.25" dx="1.6" dy="0.7" layer="1"/>
<smd name="7" x="0" y="-4.35" dx="1.6" dy="0.7" layer="1"/>
<smd name="8" x="0" y="-5.45" dx="1.6" dy="0.7" layer="1"/>
<smd name="9" x="0" y="-6.55" dx="1.6" dy="0.7" layer="1"/>
<smd name="G1" x="0.6" y="-7.75" dx="1.6" dy="1.2" layer="1"/>
<smd name="G2" x="10.1" y="-7.75" dx="2.2" dy="1.2" layer="1"/>
<smd name="G4" x="0.5" y="6.85" dx="1.4" dy="1.6" layer="1"/>
<smd name="G3" x="10.1" y="7.75" dx="2.2" dy="1.2" layer="1"/>
<text x="3.4" y="8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="0" y1="-7.557" x2="14.986" y2="7.557" layer="39"/>
<hole x="10.5" y="-4.95" drill="0.9"/>
<hole x="10.5" y="3.05" drill="0.9"/>
<rectangle x1="4.4" y1="-6.1" x2="6.4" y2="2.9" layer="41"/>
<text x="5.3" y="-0.6" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="15" y1="7.6" x2="15" y2="4.9" width="0.2" layer="51"/>
<wire x1="15" y1="4.9" x2="14.802" y2="3.898" width="0.2" layer="51" curve="-20.8555"/>
<wire x1="14.802" y1="3.898" x2="14" y2="-0.623" width="0.2" layer="51" curve="20.8543"/>
<wire x1="14" y1="-0.623" x2="14" y2="-7.6" width="0.2" layer="51"/>
<wire x1="0" y1="7.6" x2="15" y2="7.6" width="0.2" layer="51"/>
<wire x1="0" y1="7.6" x2="0" y2="-7.6" width="0.2" layer="51"/>
<wire x1="0" y1="-7.6" x2="14" y2="-7.6" width="0.2" layer="51"/>
</package>
<package name="2X5_1.27_IDC">
<wire x1="6.6" y1="2.7" x2="-6.6" y2="2.7" width="0.2" layer="21"/>
<wire x1="-6.6" y1="2.7" x2="-6.6" y2="-2.7" width="0.2" layer="21"/>
<pad name="6" x="0" y="0.635" drill="0.6"/>
<pad name="5" x="0" y="-0.635" drill="0.6"/>
<pad name="8" x="1.27" y="0.635" drill="0.6"/>
<pad name="7" x="1.27" y="-0.635" drill="0.6"/>
<pad name="10" x="2.54" y="0.635" drill="0.6"/>
<pad name="9" x="2.54" y="-0.635" drill="0.6"/>
<pad name="4" x="-1.27" y="0.635" drill="0.6"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.6"/>
<pad name="2" x="-2.54" y="0.635" drill="0.6"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.6" first="yes"/>
<text x="-1.4" y="3.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-6.6" y1="-2.7" x2="-1.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.7" x2="1.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="1.2" y1="-2.7" x2="6.6" y2="-2.7" width="0.2" layer="21"/>
<wire x1="6.6" y1="-2.7" x2="6.6" y2="2.7" width="0.2" layer="21"/>
<wire x1="-6.6" y1="2.7" x2="6.6" y2="2.7" width="0.2" layer="51"/>
<wire x1="6.6" y1="2.7" x2="6.6" y2="-2.7" width="0.2" layer="51"/>
<wire x1="6.6" y1="-2.7" x2="1.2" y2="-2.7" width="0.2" layer="51"/>
<wire x1="1.2" y1="-2.7" x2="-1.2" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-2.7" x2="-6.6" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-6.6" y1="-2.7" x2="-6.6" y2="2.7" width="0.2" layer="51"/>
<wire x1="-5.6" y1="1.8" x2="5.6" y2="1.8" width="0.2" layer="51"/>
<wire x1="5.6" y1="1.8" x2="5.6" y2="-1.8" width="0.2" layer="51"/>
<wire x1="5.6" y1="-1.8" x2="1.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="1.2" y1="-1.8" x2="1.2" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-2.7" x2="-1.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-1.8" x2="-5.6" y2="-1.8" width="0.2" layer="51"/>
<wire x1="-5.6" y1="-1.8" x2="-5.6" y2="1.8" width="0.2" layer="51"/>
<wire x1="-5.6" y1="1.8" x2="5.6" y2="1.8" width="0.2" layer="21"/>
<wire x1="5.6" y1="1.8" x2="5.6" y2="-1.8" width="0.2" layer="21"/>
<wire x1="5.6" y1="-1.8" x2="1.2" y2="-1.8" width="0.2" layer="21"/>
<wire x1="1.2" y1="-1.8" x2="1.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-5.6" y1="1.8" x2="-5.6" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-5.6" y1="-1.8" x2="-1.2" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-1.8" x2="-1.2" y2="-2.7" width="0.2" layer="21"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-DIODE">
<smd name="C" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="A" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-0.3" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.9" y1="0.3" x2="-0.9" y2="0" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-0.9" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-0.6" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0.3" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.6" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="-0.3" x2="-0.9" y2="0" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0" y2="0.1" width="0.127" layer="21"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.3" x2="0.1" y2="-0.3" width="0.127" layer="21"/>
</package>
<package name="LED3MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="2.2" width="0.2" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.2" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.1" y2="0.4" width="0.127" layer="51"/>
<wire x1="-1.1" y1="0.4" x2="-1.1" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-0.4" x2="-1.6" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0.2" x2="-1.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-0.2" x2="-1.6" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.3" y2="0.1" width="0.127" layer="51"/>
<wire x1="-1.3" y1="0.1" x2="-1.3" y2="0" width="0.127" layer="51"/>
<wire x1="-1.3" y1="0" x2="-1.3" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-0.1" x2="-1.6" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.3" y2="0" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.4" x2="-1.7" y2="-0.4" width="0.127" layer="51"/>
<text x="-0.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED5MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="3.2" width="0.2" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.2" width="0.2" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.4" x2="-1.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.4" x2="-2.1" y2="0" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.7" y2="0.2" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.2" x2="-1.7" y2="-0.2" width="0.127" layer="51"/>
<wire x1="-1.7" y1="-0.2" x2="-2.1" y2="0" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0.1" x2="-1.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0" x2="-1.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-1.8" y1="-0.1" x2="-2.1" y2="0" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.8" y2="0" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-2.2" y2="-0.4" width="0.127" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED2MM-SMD">
<smd name="C" x="-1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1" x2="1.6" y2="1" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-1" x2="1.6" y2="-1" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.7" x2="-0.7" y2="-0.7" width="0.2" layer="21"/>
<text x="-1.2" y="1.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="1" x2="-2.1" y2="1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="1" x2="2.5" y2="1" width="0.2" layer="51"/>
<wire x1="2.5" y1="1" x2="2.5" y2="-1" width="0.2" layer="51"/>
<wire x1="2.5" y1="-1" x2="-2.1" y2="-1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="-1" x2="-2.5" y2="-1" width="0.2" layer="51"/>
<wire x1="-2.5" y1="-1" x2="-2.5" y2="1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.2" layer="51"/>
<text x="-1.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED5MM_ANGLE">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.8" y="-4.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="51"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="51"/>
<text x="-1.8" y="-5.2" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3.2" y1="-2" x2="-1.8" y2="-2" width="0.2" layer="51"/>
<wire x1="-1.8" y1="-2" x2="-0.7" y2="-2" width="0.2" layer="51"/>
<wire x1="-0.7" y1="-2" x2="0.7" y2="-2" width="0.2" layer="51"/>
<wire x1="0.7" y1="-2" x2="1.8" y2="-2" width="0.2" layer="51"/>
<wire x1="1.8" y1="-2" x2="3.2" y2="-2" width="0.2" layer="51"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-2.8" width="0.2" layer="51"/>
<wire x1="3.2" y1="-2.8" x2="2.8" y2="-2.8" width="0.2" layer="51"/>
<wire x1="2.8" y1="-2.8" x2="2.8" y2="-7.2" width="0.2" layer="51"/>
<wire x1="2.8" y1="-7.2" x2="-2.8" y2="-7.2" width="0.2" layer="51" curve="-180"/>
<wire x1="-2.8" y1="-7.2" x2="-2.8" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-2.8" y1="-2.8" x2="-3.2" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-3.2" y1="-2.8" x2="-3.2" y2="-2" width="0.2" layer="51"/>
<wire x1="-3.2" y1="-2" x2="3.2" y2="-2" width="0.2" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-2.8" width="0.2" layer="21"/>
<wire x1="3.2" y1="-2.8" x2="2.8" y2="-2.8" width="0.2" layer="21"/>
<wire x1="2.8" y1="-2.8" x2="2.8" y2="-7.2" width="0.2" layer="21"/>
<wire x1="2.8" y1="-7.2" x2="-2.8" y2="-7.2" width="0.2" layer="21" curve="-180"/>
<wire x1="-2.8" y1="-7.2" x2="-2.8" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-2.8" y1="-2.8" x2="-3.2" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-2.8" x2="-3.2" y2="-2" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-2" x2="-1.8" y2="0.5" width="0.2" layer="51"/>
<wire x1="-1.8" y1="0.5" x2="-0.7" y2="0.5" width="0.2" layer="51"/>
<wire x1="-0.7" y1="0.5" x2="-0.7" y2="-2" width="0.2" layer="51"/>
<wire x1="0.7" y1="-2" x2="0.7" y2="0.5" width="0.2" layer="51"/>
<wire x1="0.7" y1="0.5" x2="1.8" y2="0.5" width="0.2" layer="51"/>
<wire x1="1.8" y1="0.5" x2="1.8" y2="-2" width="0.2" layer="51"/>
</package>
<package name="SOIC-8-WIDE-FLASH">
<smd name="1" x="-1.905" y="-3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="2" x="-0.635" y="-3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="3" x="0.635" y="-3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="4" x="1.905" y="-3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="5" x="1.905" y="3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="6" x="0.635" y="3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="7" x="-0.635" y="3.2655" dx="0.6" dy="2" layer="1"/>
<smd name="8" x="-1.905" y="3.2655" dx="0.6" dy="2" layer="1"/>
<text x="-3" y="-1.8" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<circle x="-1.92" y="-1.4" radius="0.282840625" width="0.2" layer="21"/>
<polygon width="0.127" layer="41">
<vertex x="1.4" y="-2.8"/>
<vertex x="2.4" y="-2.8"/>
<vertex x="2.4" y="-3.6"/>
<vertex x="1.4" y="-3.6"/>
</polygon>
<text x="-2.7" y="-2.2" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.6" y1="-2.1" x2="-2.6" y2="2.1" width="0.2" layer="51"/>
<wire x1="-2.6" y1="2.1" x2="-2.3" y2="2.1" width="0.2" layer="51"/>
<wire x1="-2.3" y1="2.1" x2="-2.3" y2="4.3" width="0.2" layer="51"/>
<wire x1="-2.3" y1="4.3" x2="-1.6" y2="4.3" width="0.2" layer="51"/>
<wire x1="-1.6" y1="4.3" x2="-1.6" y2="2.1" width="0.2" layer="51"/>
<wire x1="-1.6" y1="2.1" x2="-1" y2="2.1" width="0.2" layer="51"/>
<wire x1="-1" y1="2.1" x2="-1" y2="4.3" width="0.2" layer="51"/>
<wire x1="-1" y1="4.3" x2="-0.3" y2="4.3" width="0.2" layer="51"/>
<wire x1="-0.3" y1="4.3" x2="-0.3" y2="2.1" width="0.2" layer="51"/>
<wire x1="-0.3" y1="2.1" x2="0.3" y2="2.1" width="0.2" layer="51"/>
<wire x1="0.3" y1="2.1" x2="0.3" y2="4.3" width="0.2" layer="51"/>
<wire x1="0.3" y1="4.3" x2="1" y2="4.3" width="0.2" layer="51"/>
<wire x1="1" y1="4.3" x2="1" y2="2.1" width="0.2" layer="51"/>
<wire x1="1" y1="2.1" x2="1.6" y2="2.1" width="0.2" layer="51"/>
<wire x1="1.6" y1="2.1" x2="1.6" y2="4.3" width="0.2" layer="51"/>
<wire x1="1.6" y1="4.3" x2="2.3" y2="4.3" width="0.2" layer="51"/>
<wire x1="2.3" y1="4.3" x2="2.3" y2="2.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="2.1" x2="2.6" y2="2.1" width="0.2" layer="51"/>
<wire x1="2.6" y1="2.1" x2="2.6" y2="-2.1" width="0.2" layer="51"/>
<wire x1="2.6" y1="-2.1" x2="2.3" y2="-2.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="-2.1" x2="2.3" y2="-4.3" width="0.2" layer="51"/>
<wire x1="2.3" y1="-4.3" x2="1.6" y2="-4.3" width="0.2" layer="51"/>
<wire x1="1.6" y1="-4.3" x2="1.6" y2="-2.1" width="0.2" layer="51"/>
<wire x1="1.6" y1="-2.1" x2="1" y2="-2.1" width="0.2" layer="51"/>
<wire x1="1" y1="-2.1" x2="1" y2="-4.3" width="0.2" layer="51"/>
<wire x1="1" y1="-4.3" x2="0.3" y2="-4.3" width="0.2" layer="51"/>
<wire x1="0.3" y1="-4.3" x2="0.3" y2="-2.1" width="0.2" layer="51"/>
<wire x1="0.3" y1="-2.1" x2="-0.3" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-2.1" x2="-0.3" y2="-4.3" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-4.3" x2="-1" y2="-4.3" width="0.2" layer="51"/>
<wire x1="-1" y1="-4.3" x2="-1" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-1" y1="-2.1" x2="-1.6" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-2.1" x2="-1.6" y2="-4.3" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-4.3" x2="-2.3" y2="-4.3" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-4.3" x2="-2.3" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-2.1" x2="-2.6" y2="-2.1" width="0.2" layer="51"/>
<circle x="-2" y="-1.6" radius="0.282840625" width="0.2" layer="51"/>
<wire x1="-2.54" y1="2.005" x2="2.54" y2="2.005" width="0.2" layer="21"/>
<wire x1="2.54" y1="2.005" x2="2.54" y2="-2.005" width="0.2" layer="21"/>
<wire x1="2.54" y1="-2.005" x2="-2.54" y2="-2.005" width="0.2" layer="21"/>
<wire x1="-2.54" y1="2.005" x2="-2.54" y2="-2.005" width="0.2" layer="21"/>
</package>
<package name="USON-4X4">
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.2" layer="51"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.2" layer="51"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.2" layer="51"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.2" layer="51"/>
<circle x="-1.5" y="1.5" radius="0.2" width="0" layer="51"/>
<text x="-1.9" y="2.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="1" x="-1.95" y="1.2" dx="0.84" dy="0.32" layer="1"/>
<smd name="2" x="-1.95" y="0.4" dx="0.84" dy="0.32" layer="1"/>
<smd name="3" x="-1.95" y="-0.4" dx="0.84" dy="0.32" layer="1"/>
<smd name="4" x="-1.95" y="-1.2" dx="0.84" dy="0.32" layer="1"/>
<smd name="5" x="1.95" y="-1.2" dx="0.84" dy="0.32" layer="1" rot="R180"/>
<smd name="6" x="1.95" y="-0.4" dx="0.84" dy="0.32" layer="1" rot="R180"/>
<smd name="7" x="1.95" y="0.4" dx="0.84" dy="0.32" layer="1" rot="R180"/>
<smd name="8" x="1.95" y="1.2" dx="0.84" dy="0.32" layer="1" rot="R180"/>
<smd name="PAD" x="0" y="0" dx="2.3" dy="3" layer="1" rot="R180" cream="no"/>
<text x="-1.8" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2" y1="1.7" x2="-2" y2="2" width="0.2" layer="21"/>
<wire x1="-2" y1="2" x2="-1.7" y2="2" width="0.2" layer="21"/>
<wire x1="-1.7" y1="2" x2="2" y2="2" width="0.2" layer="21"/>
<wire x1="2" y1="2" x2="2" y2="1.7" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="-2" width="0.2" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.2" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="-1.7" width="0.2" layer="21"/>
<text x="-2.1" y="2.1" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<wire x1="-2" y1="1.7" x2="-1.7" y2="2" width="0.2" layer="21"/>
<polygon width="0.2" layer="31">
<vertex x="-0.5" y="-0.7"/>
<vertex x="-0.5" y="0.7"/>
<vertex x="0.5" y="0.7"/>
<vertex x="0.5" y="-0.7"/>
</polygon>
</package>
<package name="WSON-6X5">
<wire x1="-3" y1="-2.5" x2="3" y2="-2.5" width="0.2" layer="51"/>
<wire x1="3" y1="2.5" x2="-3" y2="2.5" width="0.2" layer="51"/>
<circle x="-2.5" y="2" radius="0.2" width="0" layer="51"/>
<text x="-1.7" y="3.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3" y1="2.5" x2="-3" y2="-2.5" width="0.2" layer="51"/>
<wire x1="3" y1="2.5" x2="3" y2="-2.5" width="0.2" layer="51"/>
<wire x1="-3" y1="2.5" x2="-2.7" y2="2.5" width="0.2" layer="21"/>
<wire x1="-2.7" y1="2.5" x2="3" y2="2.5" width="0.2" layer="21"/>
<wire x1="-3" y1="-2.5" x2="3" y2="-2.5" width="0.2" layer="21"/>
<smd name="1" x="-2.845" y="1.905" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="2" x="-2.845" y="0.635" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="3" x="-2.845" y="-0.635" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="4" x="-2.845" y="-1.905" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="5" x="2.845" y="-1.905" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="6" x="2.845" y="-0.635" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="7" x="2.845" y="0.635" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="8" x="2.845" y="1.905" dx="1.04" dy="0.44" layer="1" roundness="70"/>
<smd name="PAD" x="0" y="0" dx="4" dy="3" layer="1" rot="R90" cream="no"/>
<text x="-3.1" y="2.6" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<wire x1="-3" y1="2.5" x2="-3" y2="2.4" width="0.2" layer="21"/>
<wire x1="-3" y1="2.4" x2="-2.7" y2="2.5" width="0.2" layer="21"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<polygon width="0.2" layer="31">
<vertex x="-1" y="1.3"/>
<vertex x="-1" y="-1.3"/>
<vertex x="1" y="-1.3"/>
<vertex x="1" y="1.3"/>
</polygon>
</package>
<package name="SOT-23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-1.6" y1="-1.9" x2="1.6" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.9" x2="1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.9" x2="0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="0.6" y1="1.9" x2="0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="0.6" y1="0.7" x2="-0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-0.6" y1="0.7" x2="-0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-0.6" y1="1.9" x2="-1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-1.6" y1="1.9" x2="-1.6" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-0.4" y1="0.8" x2="0.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.2" layer="21"/>
</package>
<package name="SOT-353-5">
<text x="-1.6" y="-1.6" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="1" x="-0.65" y="-0.96" dx="1.19" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0" y="-0.96" dx="1.19" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="0.65" y="-0.96" dx="1.19" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="0.65" y="0.96" dx="1.19" dy="0.4" layer="1" rot="R90"/>
<smd name="5" x="-0.65" y="0.96" dx="1.19" dy="0.4" layer="1" rot="R90"/>
<wire x1="-1.2" y1="1.1" x2="-1.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-0.1" y1="1.1" x2="0.1" y2="1.1" width="0.2" layer="21"/>
<wire x1="1.2" y1="1.1" x2="1.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-1.2" y1="1.6" x2="-0.5" y2="1.6" width="0.2" layer="51"/>
<wire x1="-0.5" y1="1.6" x2="-0.5" y2="0.5" width="0.2" layer="51"/>
<wire x1="-0.5" y1="0.5" x2="0.4" y2="0.5" width="0.2" layer="51"/>
<wire x1="0.4" y1="0.5" x2="0.4" y2="1.6" width="0.2" layer="51"/>
<wire x1="0.4" y1="1.6" x2="1.2" y2="1.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="1.6" x2="1.2" y2="-1.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-1.6" x2="-1.2" y2="1.6" width="0.2" layer="51"/>
<text x="-0.9" y="-0.6" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
</package>
<package name="1812[4532-METRIC]">
<smd name="P$1" x="-2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<smd name="P$2" x="2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<text x="-1.1" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.6" y1="2.3" x2="3.6" y2="2.3" width="0.2" layer="21"/>
<wire x1="3.6" y1="2.3" x2="3.6" y2="-2.3" width="0.2" layer="21"/>
<wire x1="3.6" y1="-2.3" x2="-3.6" y2="-2.3" width="0.2" layer="21"/>
<wire x1="-3.6" y1="-2.3" x2="-3.6" y2="2.3" width="0.2" layer="21"/>
<wire x1="-3.4" y1="2.1" x2="3.4" y2="2.1" width="0.2" layer="51"/>
<wire x1="3.4" y1="2.1" x2="3.4" y2="-2.1" width="0.2" layer="51"/>
<wire x1="3.4" y1="-2.1" x2="-3.4" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-3.4" y1="-2.1" x2="-3.4" y2="2.1" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-0OHM-ON">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<polygon width="0" layer="29">
<vertex x="-1.2" y="0.6"/>
<vertex x="1.2" y="0.6"/>
<vertex x="1.2" y="-0.6"/>
<vertex x="-1.2" y="-0.6"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="-0.4" y="0.4"/>
<vertex x="0" y="0"/>
<vertex x="-0.4" y="-0.4"/>
<vertex x="-1" y="-0.4"/>
<vertex x="-1" y="0.4"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0.15" y="0.4"/>
<vertex x="1" y="0.4"/>
<vertex x="1" y="-0.4"/>
<vertex x="0.15" y="-0.4"/>
<vertex x="0.45" y="-0.1"/>
<vertex x="0.45" y="0.1"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-0.3" y="0.4"/>
<vertex x="-0.3" y="-0.4"/>
<vertex x="0.3" y="-0.4"/>
<vertex x="0.3" y="0.4"/>
</polygon>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
</package>
<package name="1206[3216-METRIC]">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<text x="-1.2" y="1.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<wire x1="-2.3" y1="-1.1" x2="2.3" y2="-1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="-1.1" x2="2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="1.1" x2="-2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="-2.3" y1="1.1" x2="-2.3" y2="-1.1" width="0.2" layer="51"/>
<text x="-1.5" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="1.3" x2="2.5" y2="1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.3" x2="2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="-1.3" x2="-2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.3" x2="-2.5" y2="1.3" width="0.2" layer="21"/>
</package>
<package name="0402[1005-METRIC]">
<smd name="P$1" x="-0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="P$2" x="0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.6" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.85" y1="0.5" x2="-0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="0.5" x2="-0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="0805[2012-METRIC]">
<smd name="P$1" x="-1.1" y="0" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="1.1" y="0" dx="1.4" dy="1" layer="1" rot="R90"/>
<text x="-1.6" y="1.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="21"/>
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="21"/>
</package>
<package name="0201[0603-METRIC]">
<smd name="P$1" x="-0.25" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="P$2" x="0.25" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<text x="-1.6" y="0.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.5" y="-0.15" size="0.3048" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.75" y1="0.45" x2="0.75" y2="0.45" width="0.2" layer="21"/>
<wire x1="0.75" y1="0.45" x2="0.75" y2="-0.45" width="0.2" layer="21"/>
<wire x1="0.75" y1="-0.45" x2="-0.75" y2="-0.45" width="0.2" layer="21"/>
<wire x1="-0.75" y1="-0.45" x2="-0.75" y2="0.45" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.4" x2="0.7" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.7" y1="0.4" x2="0.7" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.7" y1="-0.4" x2="-0.7" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.7" y1="-0.4" x2="-0.7" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="SOT-23">
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-1.6" y1="-1.9" x2="1.6" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.9" x2="1.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.7" x2="0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="0.6" y1="0.7" x2="0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="0.6" y1="1.9" x2="-0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-0.6" y1="1.9" x2="-0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-0.6" y1="0.7" x2="-1.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.3" y="-0.7" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.8" y1="0.7" x2="-1.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.5" y1="0.7" x2="-1.5" y2="-0.1" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.1" width="0.2" layer="21"/>
</package>
<package name="TO-92">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.2" layer="21"/>
<pad name="3" x="1.27" y="0" drill="0.8" diameter="1.4"/>
<pad name="2" x="0" y="1.905" drill="0.8" diameter="1.4"/>
<pad name="1" x="-1.27" y="0" drill="0.8" diameter="1.5"/>
<text x="1.9" y="-2.1" size="0.8128" layer="25" font="vector" rot="R180">&gt;Name</text>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.2" layer="51"/>
<wire x1="2.1" y1="-1.6" x2="-2.1" y2="-1.6" width="0.2" layer="51" curve="258"/>
<text x="-1.8" y="0" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="2.1" y1="-1.6" x2="-2.1" y2="-1.6" width="0.2" layer="21" curve="258"/>
</package>
<package name="SOT-323">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1" y1="-0.1" x2="1" y2="0.5" width="0.2" layer="21"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.1" width="0.2" layer="21"/>
<smd name="3" x="0" y="0.9" dx="0.5" dy="1" layer="1"/>
<smd name="1" x="-0.65" y="-0.9" dx="0.5" dy="1" layer="1"/>
<smd name="2" x="0.65" y="-0.9" dx="0.5" dy="1" layer="1"/>
<text x="1.5" y="-0.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1" y1="0.5" x2="-0.6" y2="0.5" width="0.2" layer="21"/>
<wire x1="1" y1="0.5" x2="0.6" y2="0.5" width="0.2" layer="21"/>
<wire x1="-1" y1="-1.3" x2="-0.4" y2="-1.3" width="0.2" layer="51"/>
<wire x1="-0.4" y1="-1.3" x2="-0.4" y2="-0.5" width="0.2" layer="51"/>
<wire x1="-0.4" y1="-0.5" x2="0.4" y2="-0.5" width="0.2" layer="51"/>
<wire x1="0.4" y1="-0.5" x2="0.4" y2="-1.3" width="0.2" layer="51"/>
<wire x1="0.4" y1="-1.3" x2="1" y2="-1.3" width="0.2" layer="51"/>
<wire x1="1" y1="-1.3" x2="1" y2="0.5" width="0.2" layer="51"/>
<wire x1="1" y1="0.5" x2="0.3" y2="0.5" width="0.2" layer="51"/>
<wire x1="0.3" y1="0.5" x2="0.3" y2="1.3" width="0.2" layer="51"/>
<wire x1="0.3" y1="1.3" x2="-0.3" y2="1.3" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1.3" x2="-0.3" y2="0.5" width="0.2" layer="51"/>
<wire x1="-0.3" y1="0.5" x2="-1" y2="0.5" width="0.2" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-1.3" width="0.2" layer="51"/>
<text x="-0.8" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2512[6332-METRIC]">
<text x="-1.2" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-2.6" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<smd name="P$2" x="2.6" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<wire x1="-3.9" y1="-1.9" x2="3.9" y2="-1.9" width="0.2" layer="51"/>
<wire x1="3.9" y1="-1.9" x2="3.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="3.9" y1="1.9" x2="-3.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="-3.9" y1="1.9" x2="-3.9" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.5" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-4.1" y1="2.1" x2="4.1" y2="2.1" width="0.2" layer="21"/>
<wire x1="4.1" y1="2.1" x2="4.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="4.1" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="2.1" width="0.2" layer="21"/>
</package>
<package name="PROG_THT">
<pad name="1" x="-5.08" y="1.27" drill="1.016" shape="square"/>
<pad name="3" x="-2.54" y="1.27" drill="1.016"/>
<pad name="5" x="0" y="1.27" drill="1.016"/>
<pad name="7" x="2.54" y="1.27" drill="1.016"/>
<text x="-2.5" y="3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<pad name="2" x="-5.08" y="-1.27" drill="1.016"/>
<pad name="4" x="-2.54" y="-1.27" drill="1.016"/>
<pad name="6" x="0" y="-1.27" drill="1.016"/>
<pad name="8" x="2.54" y="-1.27" drill="1.016"/>
<pad name="9" x="5.08" y="1.27" drill="1.016"/>
<pad name="10" x="5.08" y="-1.27" drill="1.016"/>
<wire x1="-6.3" y1="2.5" x2="6.3" y2="2.5" width="0.2" layer="51"/>
<wire x1="6.3" y1="2.5" x2="6.3" y2="-2.5" width="0.2" layer="51"/>
<wire x1="6.3" y1="-2.5" x2="-6.3" y2="-2.5" width="0.2" layer="51"/>
<wire x1="-6.3" y1="-2.5" x2="-6.3" y2="2.5" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-5.7" y1="1.9" x2="-4.4" y2="1.9" width="0.2" layer="51"/>
<wire x1="-4.4" y1="1.9" x2="-4.4" y2="0.6" width="0.2" layer="51"/>
<wire x1="-4.4" y1="0.6" x2="-5.7" y2="0.6" width="0.2" layer="51"/>
<wire x1="-5.7" y1="0.6" x2="-5.7" y2="1.9" width="0.2" layer="51"/>
<wire x1="-6.3" y1="2.5" x2="-4" y2="2.5" width="0.2" layer="21"/>
<wire x1="-3.8" y1="2.5" x2="6.3" y2="2.5" width="0.2" layer="21"/>
<wire x1="6.3" y1="2.5" x2="6.3" y2="-2.5" width="0.2" layer="21"/>
<wire x1="6.3" y1="-2.5" x2="-6.3" y2="-2.5" width="0.2" layer="21"/>
<wire x1="-6.3" y1="-2.5" x2="-6.3" y2="0" width="0.2" layer="21"/>
<circle x="-5.08" y="1.27" radius="1.3" width="0.2" layer="22"/>
<wire x1="-6.3" y1="0" x2="-6.3" y2="2.5" width="0.2" layer="21"/>
<wire x1="-6.3" y1="0" x2="-7" y2="0" width="0.2" layer="21"/>
<wire x1="-7" y1="0" x2="-7" y2="3" width="0.2" layer="21"/>
<wire x1="-7" y1="3" x2="-7" y2="3.2" width="0.2" layer="21"/>
<wire x1="-7" y1="3.2" x2="-4" y2="3.2" width="0.2" layer="21"/>
<wire x1="-4" y1="3.2" x2="-3.8" y2="3.2" width="0.2" layer="21"/>
<wire x1="-3.8" y1="3.2" x2="-3.8" y2="2.5" width="0.2" layer="21"/>
<wire x1="-3.8" y1="2.5" x2="-4" y2="2.5" width="0.2" layer="21"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.2" layer="21"/>
<wire x1="-4" y1="3.2" x2="-4" y2="3" width="0.2" layer="21"/>
<wire x1="-4" y1="3" x2="-6.8" y2="3" width="0.2" layer="21"/>
<wire x1="-7" y1="3" x2="-6.8" y2="3" width="0.2" layer="21"/>
<wire x1="-6.8" y1="3" x2="-6.8" y2="0.1" width="0.2" layer="21"/>
<wire x1="-6.8" y1="0.1" x2="-6.6" y2="0.1" width="0.2" layer="21"/>
<wire x1="-6.6" y1="0.1" x2="-6.5" y2="0.1" width="0.2" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="2.5" width="0.2" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.5" y2="2.5" width="0.2" layer="21"/>
<wire x1="-6.5" y1="2.5" x2="-6.5" y2="2.7" width="0.2" layer="21"/>
<wire x1="-6.5" y1="2.7" x2="-4.1" y2="2.7" width="0.2" layer="21"/>
<wire x1="-4.1" y1="2.7" x2="-4.1" y2="2.9" width="0.2" layer="21"/>
<wire x1="-4.1" y1="2.9" x2="-6.6" y2="2.9" width="0.2" layer="21"/>
<wire x1="-6.6" y1="2.9" x2="-6.6" y2="0.1" width="0.2" layer="21"/>
</package>
<package name="2X5_1.27_IDC90">
<wire x1="-6.6" y1="4" x2="-6.6" y2="1.7" width="0.2" layer="21"/>
<pad name="6" x="0" y="0.635" drill="0.6"/>
<pad name="5" x="0" y="-0.635" drill="0.6"/>
<pad name="8" x="1.27" y="0.635" drill="0.6"/>
<pad name="7" x="1.27" y="-0.635" drill="0.6"/>
<pad name="10" x="2.54" y="0.635" drill="0.6"/>
<pad name="9" x="2.54" y="-0.635" drill="0.6"/>
<pad name="4" x="-1.27" y="0.635" drill="0.6"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.6"/>
<pad name="2" x="-2.54" y="0.635" drill="0.6"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.6" first="yes"/>
<text x="2.3" y="-1.7" size="0.8128" layer="25" font="vector" rot="R180">&gt;Name</text>
<wire x1="-6.6" y1="1.7" x2="6.6" y2="1.7" width="0.2" layer="21"/>
<wire x1="6.6" y1="1.7" x2="6.6" y2="4" width="0.2" layer="21"/>
<wire x1="-6.6" y1="6.6" x2="6.6" y2="6.6" width="0.2" layer="51"/>
<wire x1="6.6" y1="6.6" x2="6.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="6.6" y1="1.7" x2="2.84" y2="1.7" width="0.2" layer="51"/>
<wire x1="2.84" y1="1.7" x2="2.84" y2="-1.2" width="0.2" layer="51"/>
<wire x1="2.84" y1="-1.2" x2="2.24" y2="-1.2" width="0.2" layer="51"/>
<wire x1="2.24" y1="-1.2" x2="2.24" y2="1.7" width="0.2" layer="51"/>
<wire x1="2.24" y1="1.7" x2="1.57" y2="1.7" width="0.2" layer="51"/>
<wire x1="1.57" y1="1.7" x2="1.57" y2="-1.2" width="0.2" layer="51"/>
<wire x1="1.57" y1="-1.2" x2="0.97" y2="-1.2" width="0.2" layer="51"/>
<wire x1="0.97" y1="-1.2" x2="0.97" y2="1.7" width="0.2" layer="51"/>
<wire x1="0.97" y1="1.7" x2="0.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="0.3" y1="1.7" x2="0.3" y2="-1.2" width="0.2" layer="51"/>
<wire x1="0.3" y1="-1.2" x2="-0.3" y2="-1.2" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-1.2" x2="-0.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1.7" x2="-0.97" y2="1.7" width="0.2" layer="51"/>
<wire x1="-0.97" y1="1.7" x2="-0.97" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-0.97" y1="-1.1" x2="-1.57" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-1.57" y1="-1.1" x2="-1.57" y2="1.7" width="0.2" layer="51"/>
<wire x1="-1.57" y1="1.7" x2="-2.24" y2="1.7" width="0.2" layer="51"/>
<wire x1="-2.24" y1="1.7" x2="-2.24" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-2.24" y1="-1.1" x2="-2.84" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-2.84" y1="-1.1" x2="-2.84" y2="1.7" width="0.2" layer="51"/>
<wire x1="-2.84" y1="1.7" x2="-6.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="-6.6" y1="1.7" x2="-6.6" y2="6.6" width="0.2" layer="51"/>
<text x="-1.4" y="4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SOD-323">
<smd name="C" x="-1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<wire x1="-1.2" y1="0.7" x2="1.2" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-0.7" x2="1.2" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-0.5" y1="0.4" x2="-0.5" y2="-0.4" width="0.2" layer="21"/>
<text x="-1.3" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.1" y1="0" x2="0.4" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.4" x2="0.4" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.4" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.3" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.2" x2="0.3" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.2" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.2" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.1" x2="0.2" y2="0" width="0.127" layer="21"/>
<wire x1="0.2" y1="0" x2="0.2" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.1" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-1.8" y1="0.4" x2="-1.4" y2="0.4" width="0.2" layer="51"/>
<wire x1="-1.4" y1="0.4" x2="-1.4" y2="0.8" width="0.2" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="51"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="0.4" width="0.2" layer="51"/>
<wire x1="1.4" y1="0.4" x2="1.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="1.8" y1="0.4" x2="1.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="1.8" y1="-0.4" x2="1.4" y2="-0.4" width="0.2" layer="51"/>
<wire x1="1.4" y1="-0.4" x2="1.4" y2="-0.8" width="0.2" layer="51"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-1.4" y1="-0.4" x2="-1.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-1.8" y1="-0.4" x2="-1.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.2" y2="0" width="0.1" layer="51"/>
<wire x1="-1.2" y1="0" x2="-1.2" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-1.2" y1="0" x2="-1.2" y2="0.3" width="0.1" layer="51"/>
<wire x1="-1.2" y1="0" x2="-0.8" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.3" x2="-0.8" y2="0" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0" x2="-0.8" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.3" x2="-1.2" y2="0" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0" x2="-0.5" y2="0" width="0.1" layer="51"/>
<text x="-0.4" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SMA-DIODE">
<wire x1="-2.2" y1="1.8" x2="2.2" y2="1.8" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.8" x2="-2.2" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-2.2" y1="1.8" x2="-2.2" y2="1.3" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-1.8" x2="-2.2" y2="-1.3" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.8" x2="2.2" y2="-1.3" width="0.2" layer="21" curve="-1.90441"/>
<wire x1="2.2" y1="1.8" x2="2.2" y2="1.3" width="0.2" layer="21"/>
<smd name="C" x="-2" y="0" dx="2" dy="2" layer="1"/>
<smd name="A" x="2" y="0" dx="2" dy="2" layer="1"/>
<text x="-1.2" y="2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-1.5" y1="1.25" x2="-0.75" y2="1.75" layer="21"/>
<rectangle x1="-1.5" y1="-1.75" x2="-0.75" y2="-1.25" layer="21"/>
<wire x1="0.1" y1="0" x2="0.6" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.6" y1="0.4" x2="0.6" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.4" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.5" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.2" x2="0.5" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.5" y1="-0.2" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.4" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.1" x2="0.4" y2="0" width="0.127" layer="21"/>
<wire x1="0.4" y1="0" x2="0.4" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.4" y2="0" width="0.127" layer="21"/>
<wire x1="-2.9" y1="0.9" x2="-2.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="-2.2" y1="0.9" x2="-2.2" y2="1.8" width="0.2" layer="51"/>
<wire x1="-2.2" y1="1.8" x2="2.2" y2="1.8" width="0.2" layer="51"/>
<wire x1="2.2" y1="1.8" x2="2.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="2.2" y1="0.9" x2="2.9" y2="0.9" width="0.2" layer="51"/>
<wire x1="2.9" y1="0.9" x2="2.9" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.9" y1="-0.9" x2="2.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.2" y1="-0.9" x2="2.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="2.2" y1="-1.8" x2="-2.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-1.8" x2="-2.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-0.9" x2="-2.9" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-2.9" y1="-0.9" x2="-2.9" y2="0.9" width="0.2" layer="51"/>
<rectangle x1="-1.5" y1="1.25" x2="-0.75" y2="1.75" layer="51"/>
<rectangle x1="-1.5" y1="-1.75" x2="-0.75" y2="-1.25" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="MICROMELF">
<smd name="C" x="-0.95" y="0" dx="0.9" dy="1.4" layer="1"/>
<smd name="A" x="0.95" y="0" dx="0.9" dy="1.4" layer="1"/>
<text x="-1.2" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.3" y1="0" x2="0.2" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.4" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0" y2="0.1" width="0.127" layer="21"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1" x2="1.7" y2="1" width="0.2" layer="21"/>
<wire x1="1.7" y1="1" x2="1.7" y2="-1" width="0.2" layer="21"/>
<wire x1="1.7" y1="-1" x2="-1.7" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-1" x2="-1.7" y2="1" width="0.2" layer="21"/>
<wire x1="-1.6" y1="0.9" x2="-1.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.9" x2="1.6" y2="0.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.9" x2="1.6" y2="-0.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-0.9" x2="-1.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-0.9" x2="-1.6" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-0.9" x2="-1.6" y2="0.9" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.9" x2="-1.2" y2="-0.9" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="POWER_DI_123">
<smd name="C" x="-0.95" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="A" x="1.6" y="0" dx="0.9" dy="1.4" layer="1"/>
<text x="-1.6" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="1" x2="-1.4" y2="1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="1.2" x2="1.4" y2="1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="1.2" x2="1.4" y2="1" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-1" x2="-1.4" y2="-1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-1.2" x2="1.4" y2="-1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="-1.2" x2="1.4" y2="-1" width="0.2" layer="21"/>
<wire x1="0.4" y1="0" x2="0.9" y2="-0.4" width="0.2" layer="21"/>
<wire x1="0.9" y1="-0.4" x2="0.9" y2="0.4" width="0.2" layer="21"/>
<wire x1="0.9" y1="0.4" x2="0.4" y2="0" width="0.2" layer="21"/>
<wire x1="0.4" y1="0" x2="0.6" y2="-0.1" width="0.2" layer="21"/>
<wire x1="0.6" y1="-0.1" x2="0.8" y2="-0.2" width="0.2" layer="21"/>
<wire x1="0.8" y1="-0.2" x2="0.8" y2="0.2" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.2" x2="0.6" y2="-0.1" width="0.2" layer="21"/>
<text x="-0.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2" y1="1.2" x2="-2" y2="-1.2" width="0.2" layer="51"/>
<wire x1="-2" y1="-1.2" x2="2" y2="-1.2" width="0.2" layer="51"/>
<wire x1="2" y1="-1.2" x2="2" y2="1.2" width="0.2" layer="51"/>
<wire x1="2" y1="1.2" x2="-2" y2="1.2" width="0.2" layer="51"/>
<wire x1="-1.7" y1="0" x2="-1.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0.4" x2="-1.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0" x2="-1.5" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0" x2="-1" y2="0.4" width="0.1" layer="51"/>
<wire x1="-1" y1="0.4" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.4" x2="-1.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-0.8" y2="0" width="0.1" layer="51"/>
</package>
<package name="0402[1005-METRIC]-DIODE">
<smd name="C" x="-0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="A" x="0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.3" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.85" y1="0.5" x2="-0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="0.5" x2="-0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.1" y1="0.2" x2="-0.1" y2="0" width="0.2" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.1" y2="-0.2" width="0.2" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="0.1" y2="0.2" width="0.2" layer="21"/>
<wire x1="-0.65" y1="0" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.55" y1="0" x2="-0.35" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0.2" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.6" y2="-0.2" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.4" y2="0.2" width="0.1" layer="51"/>
<wire x1="-0.4" y1="0.2" x2="-0.4" y2="-0.2" width="0.1" layer="51"/>
<wire x1="-0.4" y1="-0.2" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.55" y2="0" width="0.1" layer="51"/>
<wire x1="-0.55" y1="0" x2="-0.45" y2="0.1" width="0.1" layer="51"/>
<wire x1="-0.45" y1="0.1" x2="-0.45" y2="-0.1" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="SOD-123">
<description>SOD-123 0.91x1.22 mm pad</description>
<wire x1="-0.1" y1="0" x2="0.6" y2="0.4" width="0.2" layer="21"/>
<wire x1="0.6" y1="0.4" x2="0.6" y2="-0.4" width="0.2" layer="21"/>
<wire x1="0.6" y1="-0.4" x2="-0.1" y2="0" width="0.2" layer="21"/>
<wire x1="-1.3" y1="0.9" x2="1.3" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.3" y1="-0.9" x2="-1.3" y2="-0.9" width="0.2" layer="21"/>
<smd name="A" x="1.65" y="0" dx="0.9" dy="1.2" layer="1"/>
<smd name="C" x="-1.65" y="0" dx="0.9" dy="1.2" layer="1"/>
<text x="-1.2" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-0.9" y1="-0.8" x2="-0.4" y2="0.8" layer="21"/>
<wire x1="0" y1="0" x2="0.5" y2="0.2" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.2" x2="0.5" y2="0" width="0.2" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.2" width="0.2" layer="21"/>
<wire x1="0.5" y1="-0.2" x2="0.2" y2="0" width="0.2" layer="21"/>
<wire x1="0.2" y1="0" x2="0.5" y2="0" width="0.2" layer="21"/>
<wire x1="1.3" y1="0" x2="2" y2="0.4" width="0.2" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-0.4" width="0.2" layer="51"/>
<wire x1="2" y1="-0.4" x2="1.3" y2="0" width="0.2" layer="51"/>
<wire x1="-2.4" y1="0.9" x2="2.4" y2="0.9" width="0.2" layer="51"/>
<wire x1="2.4" y1="-0.9" x2="-2.4" y2="-0.9" width="0.2" layer="51"/>
<wire x1="1.4" y1="0" x2="1.9" y2="0.2" width="0.2" layer="51"/>
<wire x1="1.9" y1="0.2" x2="1.9" y2="0" width="0.2" layer="51"/>
<wire x1="1.9" y1="0" x2="1.9" y2="-0.2" width="0.2" layer="51"/>
<wire x1="1.9" y1="-0.2" x2="1.6" y2="0" width="0.2" layer="51"/>
<wire x1="1.6" y1="0" x2="1.9" y2="0" width="0.2" layer="51"/>
<wire x1="-2.4" y1="0.9" x2="-2.4" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.4" y1="-0.9" x2="2.4" y2="0.9" width="0.2" layer="51"/>
<wire x1="1.3" y1="0.4" x2="1.3" y2="-0.4" width="0.2" layer="51"/>
<text x="-2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SOD-110-R">
<smd name="A" x="0.95" y="0" dx="0.8" dy="1" layer="1"/>
<smd name="C" x="-0.95" y="0" dx="0.8" dy="1" layer="1"/>
<text x="-1.5" y="1.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="-0.3" y2="0.8" width="0.2" layer="21"/>
<wire x1="-0.3" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-0.3" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-0.3" y1="-0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-0.3" y1="0.8" x2="-0.3" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="-1.4" y1="0" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="0.3" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-0.6" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0.3" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.6" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="-0.3" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.3" y2="0" width="0.1" layer="51"/>
<text x="-0.1" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SOT-523">
<smd name="3" x="0" y="0.645" dx="0.4" dy="0.51" layer="1"/>
<smd name="2" x="0.5" y="-0.645" dx="0.4" dy="0.51" layer="1"/>
<smd name="1" x="-0.5" y="-0.645" dx="0.4" dy="0.51" layer="1"/>
<text x="-1.2" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-0.8" y1="-1" x2="-0.2" y2="-1" width="0.2" layer="51"/>
<wire x1="-0.2" y1="-1" x2="-0.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="-0.2" y1="-0.6" x2="0.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="0.2" y1="-0.6" x2="0.2" y2="-1" width="0.2" layer="51"/>
<wire x1="0.2" y1="-1" x2="0.8" y2="-1" width="0.2" layer="51"/>
<wire x1="0.8" y1="-1" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.3" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="1" width="0.2" layer="51"/>
<wire x1="0.3" y1="1" x2="-0.3" y2="1" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1" x2="-0.3" y2="0.4" width="0.2" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-1" width="0.2" layer="51"/>
<text x="-0.5" y="-0.3" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.5" y1="0.4" x2="-0.8" y2="0.4" width="0.2" layer="21"/>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-0.1" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.1" width="0.2" layer="21"/>
</package>
<package name="SOT-223">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.2" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.2" layer="21"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.2" layer="21"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.2" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1"/>
<text x="-3.7" y="-1.8" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.2" layer="51"/>
</package>
<package name="DFN-2020-6">
<polygon width="0.01" layer="1">
<vertex x="-1.15" y="0.85"/>
<vertex x="1.15" y="0.85"/>
<vertex x="1.15" y="0.45"/>
<vertex x="0.8" y="0.45"/>
<vertex x="0.8" y="0.2"/>
<vertex x="1.15" y="0.2"/>
<vertex x="1.15" y="-0.2"/>
<vertex x="-1.15" y="-0.2"/>
<vertex x="-1.15" y="0.2"/>
<vertex x="-0.8" y="0.2"/>
<vertex x="-0.8" y="0.45"/>
<vertex x="-1.15" y="0.45"/>
</polygon>
<rectangle x1="-0.505" y1="-0.007" x2="0.505" y2="0.657" layer="31"/>
<rectangle x1="-1.15" y1="0.45" x2="-0.8" y2="0.85" layer="31"/>
<rectangle x1="-1.15" y1="-0.2" x2="-0.8" y2="0.2" layer="31"/>
<rectangle x1="0.8" y1="0.45" x2="1.15" y2="0.85" layer="31"/>
<rectangle x1="0.8" y1="-0.2" x2="1.15" y2="0.2" layer="31"/>
<polygon width="0.01" layer="29">
<vertex x="-1.25" y="0.95"/>
<vertex x="1.25" y="0.95"/>
<vertex x="1.25" y="0.375"/>
<vertex x="0.9" y="0.375"/>
<vertex x="0.9" y="0.275"/>
<vertex x="1.25" y="0.275"/>
<vertex x="1.25" y="-0.3"/>
<vertex x="-1.25" y="-0.3"/>
<vertex x="-1.25" y="0.275"/>
<vertex x="-0.9" y="0.275"/>
<vertex x="-0.9" y="0.375"/>
<vertex x="-1.25" y="0.375"/>
</polygon>
<wire x1="-1.2" y1="1.2" x2="1.2" y2="1.2" width="0.2" layer="51"/>
<wire x1="1.2" y1="1.2" x2="1.2" y2="-1.2" width="0.2" layer="51"/>
<wire x1="1.2" y1="-1.2" x2="-1.2" y2="-1.2" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-1.2" x2="-1.2" y2="1.2" width="0.2" layer="51"/>
<circle x="-0.8" y="0.8" radius="0.1" width="0.2" layer="51"/>
<text x="-1.7" y="1.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="D" x="0" y="0.325" dx="0.5" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="G" x="-0.9" y="-0.65" dx="0.5" dy="0.4" layer="1"/>
<smd name="S1" x="0" y="-0.65" dx="0.92" dy="0.285" layer="1"/>
<smd name="S2" x="0.9" y="-0.65" dx="0.5" dy="0.4" layer="1"/>
<wire x1="-1.4" y1="1.3" x2="1.4" y2="1.3" width="0.2" layer="21"/>
<wire x1="1.4" y1="1.3" x2="1.4" y2="-1.3" width="0.2" layer="21"/>
<wire x1="1.4" y1="-1.3" x2="-1.4" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-1.3" x2="-1.4" y2="1.3" width="0.2" layer="21"/>
<text x="-0.8" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
</package>
<package name="POWERPAK-1212-8">
<wire x1="-2" y1="2" x2="2" y2="2" width="0.2" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.2" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.2" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.2" layer="51"/>
<circle x="1.5" y="1.5" radius="0.1" width="0.2" layer="51"/>
<text x="-1.7" y="2.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2.3" y1="2.3" x2="2.3" y2="2.3" width="0.2" layer="21"/>
<wire x1="2.3" y1="2.3" x2="2.3" y2="-2.3" width="0.2" layer="21"/>
<wire x1="2.3" y1="-2.3" x2="-2.3" y2="-2.3" width="0.2" layer="21"/>
<wire x1="-2.3" y1="-2.3" x2="-2.3" y2="2.3" width="0.2" layer="21"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<smd name="PAD" x="0" y="-0.5575" dx="2.235" dy="1.725" layer="1" cream="no"/>
<smd name="5" x="-0.99" y="-1.55" dx="0.405" dy="0.76" layer="1"/>
<smd name="6" x="-0.33" y="-1.55" dx="0.405" dy="0.76" layer="1"/>
<smd name="7" x="0.33" y="-1.55" dx="0.405" dy="0.76" layer="1"/>
<smd name="8" x="0.99" y="-1.55" dx="0.405" dy="0.76" layer="1"/>
<smd name="4" x="-0.99" y="1.435" dx="0.405" dy="0.99" layer="1"/>
<smd name="3" x="-0.33" y="1.435" dx="0.405" dy="0.99" layer="1"/>
<smd name="2" x="0.33" y="1.435" dx="0.405" dy="0.99" layer="1"/>
<smd name="1" x="0.99" y="1.435" dx="0.405" dy="0.99" layer="1"/>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="0"/>
<vertex x="0.6" y="0"/>
<vertex x="0.6" y="-0.6"/>
<vertex x="-0.6" y="-0.6"/>
</polygon>
</package>
<package name="TO-252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<smd name="TAB" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<text x="-3.8" y="-2.5" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-2.9" y1="5.5" x2="-3.2" y2="5.5" width="0.2" layer="21"/>
<wire x1="-3.2" y1="5.5" x2="-3.2" y2="-5.6" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-5.6" x2="-3" y2="-5.6" width="0.2" layer="21"/>
<wire x1="-1.5" y1="-5.6" x2="1.5" y2="-5.6" width="0.2" layer="21"/>
<wire x1="3" y1="-5.6" x2="3.2" y2="-5.6" width="0.2" layer="21"/>
<wire x1="3.2" y1="-5.6" x2="3.2" y2="5.5" width="0.2" layer="21"/>
<wire x1="3.2" y1="5.5" x2="2.9" y2="5.5" width="0.2" layer="21"/>
<wire x1="-2.7" y1="-5.6" x2="-1.7" y2="-5.6" width="0.2" layer="51"/>
<wire x1="-1.7" y1="-5.6" x2="-1.7" y2="-3" width="0.2" layer="51"/>
<wire x1="-1.7" y1="-3" x2="1.7" y2="-3" width="0.2" layer="51"/>
<wire x1="1.7" y1="-3" x2="1.7" y2="-5.6" width="0.2" layer="51"/>
<wire x1="1.7" y1="-5.6" x2="2.7" y2="-5.6" width="0.2" layer="51"/>
<wire x1="2.7" y1="-5.6" x2="2.7" y2="5.7" width="0.2" layer="51"/>
<wire x1="2.7" y1="5.7" x2="-2.7" y2="5.7" width="0.2" layer="51"/>
<wire x1="-2.7" y1="5.7" x2="-2.7" y2="-5.6" width="0.2" layer="51"/>
<text x="-1.9" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="12X12">
<smd name="1" x="3.75" y="0" dx="11" dy="4" layer="1" rot="R270"/>
<smd name="2" x="-3.75" y="0" dx="11" dy="4" layer="1" rot="R270"/>
<text x="-1.8" y="6.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.2" layer="51"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.2" layer="51"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.2" layer="51"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.2" layer="51"/>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.2" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.2" layer="21"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.2" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.2" layer="21"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="5X5">
<smd name="1" x="-1.85" y="0" dx="5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="1.85" y="0" dx="5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.8" y="3.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3" y1="2.7" x2="3" y2="2.7" width="0.2" layer="51"/>
<wire x1="3" y1="2.7" x2="3" y2="-2.7" width="0.2" layer="51"/>
<wire x1="3" y1="-2.7" x2="-3" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-3" y1="-2.7" x2="-3" y2="2.7" width="0.2" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3" y1="2.7" x2="3" y2="2.7" width="0.2" layer="21"/>
<wire x1="3" y1="2.7" x2="3" y2="-2.7" width="0.2" layer="21"/>
<wire x1="3" y1="-2.7" x2="-3" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-3" y1="-2.7" x2="-3" y2="2.7" width="0.2" layer="21"/>
</package>
<package name="3.5/7.5">
<pad name="P$1" x="-1.75" y="0" drill="0.7"/>
<pad name="P$2" x="1.75" y="0" drill="0.7"/>
<circle x="0" y="0" radius="3.8" width="0.2" layer="21"/>
<text x="-1.7" y="4.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="3.8" width="0.2" layer="51"/>
</package>
<package name="PS8D43">
<smd name="1" x="-4" y="0" dx="3" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="4" y="0" dx="3" dy="1.8" layer="1" rot="R90"/>
<wire x1="-3.7" y1="1.8" x2="3.7" y2="1.8" width="0.2" layer="21" curve="-130"/>
<wire x1="3.7" y1="-1.8" x2="-3.7" y2="-1.8" width="0.2" layer="21" curve="-130"/>
<text x="-1.7" y="4.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="4.159325" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="7X7">
<smd name="1" x="-2.525" y="0" dx="2.4" dy="6.4" layer="1"/>
<smd name="2" x="2.525" y="0" dx="2.4" dy="6.4" layer="1"/>
<text x="-1.8" y="3.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-4" y1="3.6" x2="4" y2="3.6" width="0.2" layer="21"/>
<wire x1="4" y1="3.6" x2="4" y2="-3.6" width="0.2" layer="21"/>
<wire x1="4" y1="-3.6" x2="-4" y2="-3.6" width="0.2" layer="21"/>
<wire x1="-4" y1="-3.6" x2="-4" y2="3.6" width="0.2" layer="21"/>
<wire x1="-4" y1="3.6" x2="-4" y2="-3.6" width="0.2" layer="51"/>
<wire x1="-4" y1="-3.6" x2="4" y2="-3.6" width="0.2" layer="51"/>
<wire x1="4" y1="-3.6" x2="4" y2="3.6" width="0.2" layer="51"/>
<wire x1="4" y1="3.6" x2="-4" y2="3.6" width="0.2" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="3X3">
<smd name="1" x="-0.95" y="0" dx="1.2" dy="3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.2" dy="3" layer="1"/>
<text x="-1.7" y="2.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.85" y1="1.85" x2="1.85" y2="1.85" width="0.2" layer="21"/>
<wire x1="1.85" y1="1.85" x2="1.85" y2="-1.85" width="0.2" layer="21"/>
<wire x1="1.85" y1="-1.85" x2="-1.85" y2="-1.85" width="0.2" layer="21"/>
<wire x1="-1.85" y1="-1.85" x2="-1.85" y2="1.85" width="0.2" layer="21"/>
<wire x1="-1.85" y1="1.85" x2="1.85" y2="1.85" width="0.2" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.85" y2="-1.85" width="0.2" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="-1.85" y2="-1.85" width="0.2" layer="51"/>
<wire x1="-1.85" y1="-1.85" x2="-1.85" y2="1.85" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="8X8">
<smd name="1" x="-2.75" y="0" dx="8" dy="2.5" layer="1" rot="R90"/>
<smd name="2" x="2.75" y="0" dx="8" dy="2.5" layer="1" rot="R90"/>
<text x="-1.9" y="4.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-4.2" y1="4.2" x2="4.2" y2="4.2" width="0.2" layer="51"/>
<wire x1="4.2" y1="4.2" x2="4.2" y2="-4.2" width="0.2" layer="51"/>
<wire x1="4.2" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2" layer="51"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="4.2" width="0.2" layer="51"/>
<wire x1="-4.2" y1="4.2" x2="4.2" y2="4.2" width="0.2" layer="21"/>
<wire x1="4.2" y1="4.2" x2="4.2" y2="-4.2" width="0.2" layer="21"/>
<wire x1="4.2" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="4.2" width="0.2" layer="21"/>
<text x="-1.9" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="4X4">
<smd name="1" x="-1.35" y="0" dx="1.2" dy="4" layer="1"/>
<smd name="2" x="1.35" y="0" dx="1.2" dy="4" layer="1"/>
<text x="-1.6" y="2.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2.2" y1="2.3" x2="2.2" y2="2.3" width="0.2" layer="21"/>
<wire x1="2.2" y1="2.3" x2="2.2" y2="-2.3" width="0.2" layer="21"/>
<wire x1="2.2" y1="-2.3" x2="-2.2" y2="-2.3" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-2.3" x2="-2.2" y2="2.3" width="0.2" layer="21"/>
<wire x1="-2.2" y1="2.3" x2="2.2" y2="2.3" width="0.2" layer="51"/>
<wire x1="2.2" y1="2.3" x2="2.2" y2="-2.3" width="0.2" layer="51"/>
<wire x1="2.2" y1="-2.3" x2="-2.2" y2="-2.3" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-2.3" x2="-2.2" y2="2.3" width="0.2" layer="51"/>
<text x="-1.6" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="6X6">
<smd name="1" x="-2.15" y="0" dx="6" dy="2" layer="1" rot="R90"/>
<smd name="2" x="2.15" y="0" dx="6" dy="2" layer="1" rot="R90"/>
<text x="-1.7" y="3.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.4" y1="3.2" x2="3.4" y2="3.2" width="0.2" layer="21"/>
<wire x1="3.4" y1="3.2" x2="3.4" y2="-3.2" width="0.2" layer="21"/>
<wire x1="3.4" y1="-3.2" x2="-3.4" y2="-3.2" width="0.2" layer="21"/>
<wire x1="-3.4" y1="-3.2" x2="-3.4" y2="3.2" width="0.2" layer="21"/>
<wire x1="-3.4" y1="3.2" x2="3.4" y2="3.2" width="0.2" layer="51"/>
<wire x1="3.4" y1="3.2" x2="3.4" y2="-3.2" width="0.2" layer="51"/>
<wire x1="3.4" y1="-3.2" x2="-3.4" y2="-3.2" width="0.2" layer="51"/>
<wire x1="-3.4" y1="-3.2" x2="-3.4" y2="3.2" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2X2">
<smd name="1" x="-0.65" y="0" dx="0.7" dy="2" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="2" layer="1"/>
<wire x1="-1.3" y1="-1.3" x2="1.3" y2="-1.3" width="0.2" layer="21"/>
<wire x1="1.3" y1="-1.3" x2="1.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="1.3" y1="1.3" x2="-1.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="-1.3" y1="1.3" x2="-1.3" y2="-1.3" width="0.2" layer="21"/>
<text x="-1.8" y="1.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.3" y1="1.3" x2="1.3" y2="1.3" width="0.2" layer="51"/>
<wire x1="1.3" y1="1.3" x2="1.3" y2="-1.3" width="0.2" layer="51"/>
<wire x1="1.3" y1="-1.3" x2="-1.3" y2="-1.3" width="0.2" layer="51"/>
<wire x1="-1.3" y1="-1.3" x2="-1.3" y2="1.3" width="0.2" layer="51"/>
<text x="-1.1" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
</package>
<package name="5/8">
<pad name="P$1" x="-2.5" y="0" drill="1"/>
<pad name="P$2" x="2.5" y="0" drill="1"/>
<circle x="0" y="0" radius="4.1" width="0.2" layer="21"/>
<text x="-1.7" y="4.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="4.1" width="0.2" layer="51"/>
</package>
<package name="2.4X1.4">
<smd name="1" x="-0.8" y="0" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="1.4" y2="-0.9" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.9" x2="-1.4" y2="0.9" width="0.2" layer="21"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.2" layer="21"/>
<text x="-1.8" y="1.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.9" x2="1.4" y2="0.9" width="0.2" layer="51"/>
<wire x1="1.4" y1="0.9" x2="1.4" y2="-0.9" width="0.2" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="-1.4" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-1.4" y1="-0.9" x2="-1.4" y2="0.9" width="0.2" layer="51"/>
<text x="-1.1" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
</package>
<package name="3.2X2.5">
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2" layer="21"/>
<wire x1="2" y1="0.3" x2="2" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2" layer="21"/>
<wire x1="-2" y1="0.3" x2="-2" y2="-0.3" width="0.2" layer="21"/>
<smd name="1" x="-1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="3" x="1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="4" x="-1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<text x="-1" y="2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.2" layer="51"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.2" layer="51"/>
<text x="-1.3" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2X1.6">
<wire x1="-0.6" y1="1.2" x2="0.6" y2="1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.3" x2="1.4" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.6" y1="-1.2" x2="-0.6" y2="-1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-1.4" y2="-0.3" width="0.2" layer="21"/>
<smd name="1" x="-0.75" y="-0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="3" x="0.75" y="0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="4" x="-0.75" y="0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="2" x="0.75" y="-0.6" dx="0.7" dy="0.6" layer="1"/>
<text x="-1.5" y="1.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="-1.2" x2="1.4" y2="-1.2" width="0.2" layer="51"/>
<wire x1="1.4" y1="-1.2" x2="1.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="1.4" y1="1.2" x2="-1.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="-1.4" y1="1.2" x2="-1.4" y2="-1.2" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="7X5">
<wire x1="-3.9" y1="2.7" x2="3.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="3.9" y1="0.3" x2="3.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.7" x2="-3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="0.3" x2="-3.9" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-3" y="-1.27" dx="2" dy="1.4" layer="1"/>
<smd name="3" x="3" y="1.27" dx="2" dy="1.4" layer="1"/>
<smd name="4" x="-3" y="1.27" dx="2" dy="1.4" layer="1"/>
<smd name="2" x="3" y="-1.27" dx="2" dy="1.4" layer="1"/>
<text x="-1.4" y="3.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.9" y1="2.7" x2="-3.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="2.7" x2="3.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="-2.3" x2="-3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="2.7" x2="-3.9" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-3.9" y1="-2.7" x2="3.9" y2="-2.7" width="0.2" layer="51"/>
<wire x1="3.9" y1="-2.7" x2="3.9" y2="2.7" width="0.2" layer="51"/>
<wire x1="3.9" y1="2.7" x2="-3.9" y2="2.7" width="0.2" layer="51"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="RESONATOR">
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="51"/>
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="51"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="51"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="0.7" dy="1.6" layer="1"/>
<smd name="G" x="0" y="0" dx="0.7" dy="1.6" layer="1"/>
<smd name="2" x="1.2" y="0" dx="0.7" dy="1.6" layer="1"/>
<text x="-1.6" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="21"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="21"/>
<text x="-1.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="FIDUCIAL">
<polygon width="0" layer="29">
<vertex x="1" y="0" curve="180"/>
<vertex x="-1" y="0" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="-1"/>
<vertex x="0" y="-0.6" curve="-180"/>
<vertex x="0" y="0.6"/>
<vertex x="0" y="1" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="1"/>
<vertex x="0" y="0.6" curve="-180"/>
<vertex x="0" y="-0.6"/>
<vertex x="0" y="-1" curve="180"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0" y="-0.4" curve="180"/>
<vertex x="0" y="0.4" curve="180"/>
</polygon>
<circle x="0" y="0" radius="0.9" width="0.2" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="51"/>
</package>
<package name="FIDUCIAL_BORDER">
<polygon width="0" layer="29">
<vertex x="1" y="0" curve="180"/>
<vertex x="-1" y="0" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="-1"/>
<vertex x="0" y="-0.6" curve="-180"/>
<vertex x="0" y="0.6"/>
<vertex x="0" y="1" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="1"/>
<vertex x="0" y="0.6" curve="-180"/>
<vertex x="0" y="-0.6"/>
<vertex x="0" y="-1" curve="180"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0" y="-0.4" curve="180"/>
<vertex x="0" y="0.4" curve="180"/>
</polygon>
<circle x="0" y="0" radius="0.9" width="0.2" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="51"/>
<polygon width="0.2" layer="1">
<vertex x="0" y="1.1" curve="180"/>
<vertex x="0" y="-1.1"/>
<vertex x="0" y="-2.3" curve="-180"/>
<vertex x="0" y="2.3"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0" y="-1.1" curve="180"/>
<vertex x="0" y="1.1"/>
<vertex x="0" y="2.3" curve="-180"/>
<vertex x="0" y="-2.3"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="HOLE">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SWITCH">
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="B" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<text x="-3.048" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="3.048" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="CHASSIS">
<pin name="GND" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="0" y1="0" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.016" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="2.032" y1="0" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="-0.254" width="0.127" layer="94"/>
<wire x1="0.762" y1="-0.254" x2="1.27" y2="-0.254" width="0.127" layer="94"/>
</symbol>
<symbol name="MICRO_SD">
<wire x1="-12.7" y1="11.43" x2="-12.7" y2="-11.43" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-11.43" x2="7.62" y2="-11.43" width="0.254" layer="94"/>
<wire x1="7.62" y1="-11.43" x2="10.16" y2="-11.43" width="0.254" layer="94"/>
<wire x1="10.16" y1="-11.43" x2="12.7" y2="-11.43" width="0.254" layer="94"/>
<wire x1="12.7" y1="-11.43" x2="12.7" y2="13.97" width="0.254" layer="94"/>
<wire x1="12.7" y1="13.97" x2="10.16" y2="13.97" width="0.254" layer="94"/>
<wire x1="10.16" y1="13.97" x2="7.62" y2="13.97" width="0.254" layer="94"/>
<wire x1="7.62" y1="13.97" x2="6.35" y2="13.97" width="0.254" layer="94"/>
<wire x1="6.35" y1="13.97" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="3.81" y2="12.7" width="0.254" layer="94"/>
<wire x1="3.81" y1="12.7" x2="3.81" y2="13.97" width="0.254" layer="94"/>
<wire x1="3.81" y1="13.97" x2="1.27" y2="13.97" width="0.254" layer="94"/>
<wire x1="1.27" y1="13.97" x2="-1.27" y2="11.43" width="0.254" layer="94"/>
<wire x1="-1.27" y1="11.43" x2="-12.7" y2="11.43" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-2.54" y="0" size="1.778" layer="96" font="vector">&gt;Value</text>
<pin name="DAT2" x="-15.24" y="10.16" length="short" direction="pas"/>
<pin name="DAT3" x="-15.24" y="7.62" length="short" direction="pas"/>
<pin name="CMD" x="-15.24" y="5.08" length="short" direction="pas"/>
<pin name="VDD" x="-15.24" y="2.54" length="short" direction="pas"/>
<pin name="CLK" x="-15.24" y="0" length="short" direction="pas"/>
<pin name="VSS" x="-15.24" y="-2.54" length="short" direction="pas"/>
<pin name="DAT0" x="-15.24" y="-5.08" length="short" direction="pas"/>
<pin name="DAT1" x="-15.24" y="-7.62" length="short" direction="pas"/>
<pin name="/CD" x="-15.24" y="-10.16" length="short" direction="pas"/>
<pin name="G4" x="7.62" y="15.24" length="point" direction="pas" rot="R270"/>
<pin name="G3" x="10.16" y="15.24" length="point" direction="pas" rot="R270"/>
<pin name="G1" x="7.62" y="-12.7" length="point" direction="pas" rot="R90"/>
<pin name="G2" x="10.16" y="-12.7" length="point" direction="pas" rot="R90"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="-11.43" width="0.1778" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="-11.43" width="0.1778" layer="94"/>
<wire x1="7.62" y1="15.24" x2="7.62" y2="13.97" width="0.1778" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="13.97" width="0.1778" layer="94"/>
</symbol>
<symbol name="LED">
<pin name="C" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="-1.016" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.381" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="1.651" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.27" x2="2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.381" x2="1.651" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.651" y1="0.127" x2="2.032" y2="-0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="1.905" y2="0.127" width="0.254" layer="94"/>
<text x="-0.508" y="-1.778" size="1.778" layer="95" font="vector" rot="MR270">&gt;Name</text>
<text x="-0.508" y="1.778" size="1.778" layer="96" font="vector" rot="R90">&gt;Value</text>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SPI_FLASH">
<pin name="CS" x="-10.16" y="2.54" length="short"/>
<pin name="MISO" x="-10.16" y="0" length="short"/>
<pin name="WP" x="-10.16" y="-2.54" length="short"/>
<pin name="GND" x="-10.16" y="-5.08" length="short" direction="pwr"/>
<pin name="MOSI" x="10.16" y="-5.08" length="short" rot="R180"/>
<pin name="SCK" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="HLD" x="10.16" y="0" length="short" rot="R180"/>
<pin name="VCC" x="10.16" y="2.54" length="short" direction="pwr" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="-6.35" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="SOT23-5">
<pin name="4" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="5" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.858" y="1.524" size="1.778" layer="95" font="vector">&gt;P1</text>
<text x="-6.858" y="-1.016" size="1.778" layer="95" font="vector">&gt;P2</text>
<text x="-6.858" y="-3.556" size="1.778" layer="95" font="vector">&gt;P3</text>
<text x="6.858" y="-3.556" size="1.778" layer="95" font="vector" rot="MR0">&gt;P4</text>
<text x="6.858" y="1.524" size="1.778" layer="95" font="vector" rot="MR0">&gt;P5</text>
<text x="-4.318" y="6.604" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-4.318" y="-8.636" size="1.778" layer="95" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="CAPACITOR">
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<rectangle x1="-0.762" y1="-2.032" x2="-0.254" y2="2.032" layer="94"/>
<rectangle x1="0.254" y1="-2.032" x2="0.762" y2="2.032" layer="94"/>
<text x="-1.27" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="1.27" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="TRANSISTOR-NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="5.08" y="-5.08" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="5.08" y="3.556" size="1.778" layer="95" font="vector">&gt;Name</text>
</symbol>
<symbol name="RESISTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="-2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.048" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="3.048" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="PROG_SPI_AVR">
<pin name="GND" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="MOSI" x="-10.16" y="5.08" length="short" direction="pas"/>
<pin name="SCK" x="10.16" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="CS" x="-10.16" y="2.54" length="short" direction="pas"/>
<pin name="MISO" x="10.16" y="0" length="short" direction="pas" rot="R180"/>
<pin name="RST" x="-10.16" y="0" length="short" direction="pas"/>
<pin name="TX" x="10.16" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="5V" x="-10.16" y="-2.54" length="short" direction="pas"/>
<pin name="RX" x="10.16" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="3V3" x="-10.16" y="-5.08" length="short" direction="pas"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95" font="vector">&gt;Name</text>
</symbol>
<symbol name="DIODE">
<pin name="C" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="A" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="1.27" y1="1.016" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.016" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.016" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.778" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="1.778" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="MOSFET-P">
<wire x1="-3.6576" y1="-2.413" x2="-3.6576" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-2.0066" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.905" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="1.905" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="1.905" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="3.429" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0.254" x2="-0.254" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.254" y1="0" x2="-1.143" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="3.556" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-5.08" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.54" layer="94"/>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="0" visible="off" length="point" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="FERRITE/INDUCTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.048" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="3.048" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-2.667" y1="-1.016" x2="2.667" y2="0.508" layer="94"/>
<rectangle x1="-2.667" y1="0.762" x2="2.667" y2="1.016" layer="94"/>
</symbol>
<symbol name="REGULATOR_EN_BYP">
<pin name="VIN" x="-10.16" y="2.54" length="short" direction="pas" swaplevel="1"/>
<pin name="EN" x="-10.16" y="-2.54" length="short" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-7.62" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="BYP" x="10.16" y="-2.54" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="VOUT" x="10.16" y="2.54" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="6.35" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-5.08" y="-13.97" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="OSCILLATOR">
<pin name="VCC" x="-10.16" y="2.54" length="short" direction="pas"/>
<pin name="EN" x="-10.16" y="-2.54" length="short" direction="pas"/>
<pin name="OUT" x="10.16" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="10.16" y="-2.54" length="short" direction="pas" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-7.62" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
<symbol name="CRYSTAL-GND">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96" font="vector">&gt;Value</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-2.54" visible="off" length="point"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.04726875" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.803215625" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.567959375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.359209375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.127" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HOLE" prefix="HOLE">
<gates>
<gate name="G$1" symbol="HOLE" x="0" y="0"/>
</gates>
<devices>
<device name="-M2.5" package="HOLE-M2.5">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M3" package="HOLE-M3">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M4" package="HOLE-M4">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE-SWITCH-GND" prefix="SW" uservalue="yes">
<description>TACTILE SWITCH WITH GND</description>
<gates>
<gate name="SW" symbol="SWITCH" x="0" y="0"/>
<gate name="GND" symbol="CHASSIS" x="-2.54" y="-5.08" addlevel="request"/>
</gates>
<devices>
<device name="-TH-90" package="TACTILE-TH-90">
<connects>
<connect gate="GND" pin="GND" pad="GND@1 GND@2"/>
<connect gate="SW" pin="A" pad="A"/>
<connect gate="SW" pin="B" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-EVQX" package="TACTILE-PANASONIC-EVQX">
<connects>
<connect gate="GND" pin="GND" pad="GND@1 GND@2"/>
<connect gate="SW" pin="A" pad="A@1 A@2"/>
<connect gate="SW" pin="B" pad="B@1 B@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO_SD" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="MICRO_SD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MICRO-SD">
<connects>
<connect gate="G$1" pin="/CD" pad="9"/>
<connect gate="G$1" pin="CLK" pad="5"/>
<connect gate="G$1" pin="CMD" pad="3"/>
<connect gate="G$1" pin="DAT0" pad="7"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="1"/>
<connect gate="G$1" pin="DAT3" pad="2"/>
<connect gate="G$1" pin="G1" pad="G1"/>
<connect gate="G$1" pin="G2" pad="G2"/>
<connect gate="G$1" pin="G3" pad="G3"/>
<connect gate="G$1" pin="G4" pad="G4"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>LED</description>
<gates>
<gate name="LED" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]-DIODE">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM" package="LED3MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM" package="LED5MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-SMD" package="LED2MM-SMD">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM_ANGLE" package="LED5MM_ANGLE">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPI_FLASH">
<gates>
<gate name="G$1" symbol="SPI_FLASH" x="0" y="0"/>
</gates>
<devices>
<device name="-SOIC8" package="SOIC-8-WIDE-FLASH">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="HLD" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-USON4X4" package="USON-4X4">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4 PAD"/>
<connect gate="G$1" pin="HLD" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-WSON-6X5" package="WSON-6X5">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4 PAD"/>
<connect gate="G$1" pin="HLD" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOT-X-5" prefix="U">
<gates>
<gate name="G$1" symbol="SOT23-5" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT23-5" package="SOT-23-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT353-5" package="SOT-353-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="C" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1812[4532-METRIC]" package="1812[4532-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603[1608-METRIC]-0OHM" package="0603[1608-METRIC]-0OHM-ON">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR-NPN" prefix="T" uservalue="yes">
<description>TRANSISTOR NPN</description>
<gates>
<gate name="T" symbol="TRANSISTOR-NPN" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT-23" package="SOT-23">
<connects>
<connect gate="T" pin="B" pad="1"/>
<connect gate="T" pin="C" pad="3"/>
<connect gate="T" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-92" package="TO-92">
<connects>
<connect gate="T" pin="B" pad="2"/>
<connect gate="T" pin="C" pad="3"/>
<connect gate="T" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-323" package="SOT-323">
<connects>
<connect gate="T" pin="B" pad="1"/>
<connect gate="T" pin="C" pad="3"/>
<connect gate="T" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>RESISTOR</description>
<gates>
<gate name="R" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2512[6332-METRIC]" package="2512[6332-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PROG_SPI_AVR">
<gates>
<gate name="G$1" symbol="PROG_SPI_AVR" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="PROG_THT">
<connects>
<connect gate="G$1" pin="3V3" pad="10"/>
<connect gate="G$1" pin="5V" pad="8"/>
<connect gate="G$1" pin="CS" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="MISO" pad="5"/>
<connect gate="G$1" pin="MOSI" pad="2"/>
<connect gate="G$1" pin="RST" pad="6"/>
<connect gate="G$1" pin="RX" pad="9"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="TX" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.27_IDC" package="2X5_1.27_IDC">
<connects>
<connect gate="G$1" pin="3V3" pad="10"/>
<connect gate="G$1" pin="5V" pad="8"/>
<connect gate="G$1" pin="CS" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="MISO" pad="5"/>
<connect gate="G$1" pin="MOSI" pad="2"/>
<connect gate="G$1" pin="RST" pad="6"/>
<connect gate="G$1" pin="RX" pad="9"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="TX" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.27_IDC90" package="2X5_1.27_IDC90">
<connects>
<connect gate="G$1" pin="3V3" pad="10"/>
<connect gate="G$1" pin="5V" pad="8"/>
<connect gate="G$1" pin="CS" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="MISO" pad="5"/>
<connect gate="G$1" pin="MOSI" pad="2"/>
<connect gate="G$1" pin="RST" pad="6"/>
<connect gate="G$1" pin="RX" pad="9"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="TX" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>DIODE</description>
<gates>
<gate name="D" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]-DIODE">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD-323" package="SOD-323">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMA" package="SMA-DIODE">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICROMELF" package="MICROMELF">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-POWER-DI-123" package="POWER_DI_123">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]-DIODE">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD-123" package="SOD-123">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD-110-R" package="SOD-110-R">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-P" prefix="T" uservalue="yes">
<description>MOSFET-P</description>
<gates>
<gate name="T" symbol="MOSFET-P" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT-23" package="SOT-23">
<connects>
<connect gate="T" pin="D" pad="3"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-523" package="SOT-523">
<connects>
<connect gate="T" pin="D" pad="3"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-223" package="SOT-223">
<connects>
<connect gate="T" pin="D" pad="2"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DFN-2020-6" package="DFN-2020-6">
<connects>
<connect gate="T" pin="D" pad="D"/>
<connect gate="T" pin="G" pad="G"/>
<connect gate="T" pin="S" pad="S1 S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-POWERPAK-1212-8" package="POWERPAK-1212-8">
<connects>
<connect gate="T" pin="D" pad="5 6 7 8 PAD"/>
<connect gate="T" pin="G" pad="4"/>
<connect gate="T" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-252" package="TO-252">
<connects>
<connect gate="T" pin="D" pad="TAB"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE/INDUCTOR" prefix="L" uservalue="yes">
<description>FERRITE/INDUCTOR</description>
<gates>
<gate name="L" symbol="FERRITE/INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-12X12" package="12X12">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5X5" package="5X5">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.5/7.5" package="3.5/7.5">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PS8D43" package="PS8D43">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7X7" package="7X7">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3X3" package="3X3">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-8X8" package="8X8">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4X4" package="4X4">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6X6" package="6X6">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2X2" package="2X2">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5/8" package="5/8">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.4X1.4" package="2.4X1.4">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR_EN_BYP" prefix="U" uservalue="yes">
<description>REGULATOR</description>
<gates>
<gate name="U" symbol="REGULATOR_EN_BYP" x="0" y="0"/>
</gates>
<devices>
<device name="-TPS7933" package="SOT-23-5">
<connects>
<connect gate="U" pin="BYP" pad="4"/>
<connect gate="U" pin="EN" pad="3"/>
<connect gate="U" pin="GND" pad="2"/>
<connect gate="U" pin="VIN" pad="1"/>
<connect gate="U" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OSCILLATOR">
<gates>
<gate name="G$1" symbol="OSCILLATOR" x="0" y="0"/>
</gates>
<devices>
<device name="-3.2X2.5" package="3.2X2.5">
<connects>
<connect gate="G$1" pin="EN" pad="1"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-GND" prefix="Q">
<description>CRYSTAL</description>
<gates>
<gate name="Q" symbol="CRYSTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="-3.2X2.5" package="3.2X2.5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2X1.6" package="2X1.6">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7X5" package="7X5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RESONATOR" package="RESONATOR">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="2"/>
<connect gate="Q" pin="GND" pad="G"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FIDUCIAL">
<description>FIDUCIAL</description>
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-BORDER" package="FIDUCIAL_BORDER">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="1747981-1">
<packages>
<package name="TE_1747981-1">
<text x="-2" y="2.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-7.7" y1="0.9" x2="-7.7" y2="-0.9" width="0" layer="46"/>
<wire x1="-7.7" y1="-0.9" x2="-6.8" y2="-0.9" width="0" layer="46" curve="180"/>
<wire x1="-6.8" y1="-0.9" x2="-6.8" y2="0.9" width="0" layer="46"/>
<wire x1="-6.8" y1="0.9" x2="-7.7" y2="0.9" width="0" layer="46" curve="180"/>
<wire x1="-7.7" y1="-6.56" x2="-6.8" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="-6.8" y1="-5.36" x2="-7.7" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="-7.7" y1="-6.56" x2="-6.8" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="-6.8" y1="-5.36" x2="-7.7" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="-7.7" y1="-6.56" x2="-6.8" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="-6.8" y1="-5.36" x2="-7.7" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="-7.7" y1="-5.36" x2="-7.7" y2="-6.56" width="0" layer="46"/>
<wire x1="-7.7" y1="-6.56" x2="-6.8" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="-6.8" y1="-6.56" x2="-6.8" y2="-5.36" width="0" layer="46"/>
<wire x1="-6.8" y1="-5.36" x2="-7.7" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="6.8" y1="0.9" x2="6.8" y2="-0.9" width="0" layer="46"/>
<wire x1="6.8" y1="-0.9" x2="7.7" y2="-0.9" width="0" layer="46" curve="180"/>
<wire x1="7.7" y1="-0.9" x2="7.7" y2="0.9" width="0" layer="46"/>
<wire x1="7.7" y1="0.9" x2="6.8" y2="0.9" width="0" layer="46" curve="180"/>
<wire x1="6.8" y1="-6.56" x2="7.7" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="7.7" y1="-5.36" x2="6.8" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="6.8" y1="-6.56" x2="7.7" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="7.7" y1="-5.36" x2="6.8" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="6.8" y1="-6.56" x2="7.7" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="7.7" y1="-5.36" x2="6.8" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="6.8" y1="-5.36" x2="6.8" y2="-6.56" width="0" layer="46"/>
<wire x1="6.8" y1="-6.56" x2="7.7" y2="-6.56" width="0" layer="46" curve="180"/>
<wire x1="7.7" y1="-6.56" x2="7.7" y2="-5.36" width="0" layer="46"/>
<wire x1="7.7" y1="-5.36" x2="6.8" y2="-5.36" width="0" layer="46" curve="180"/>
<wire x1="-7.5" y1="1.5" x2="7.5" y2="1.5" width="0.2" layer="51"/>
<wire x1="7.5" y1="1.5" x2="7.5" y2="-9.545" width="0.2" layer="51"/>
<wire x1="7.5" y1="-9.545" x2="7.5" y2="-9.6" width="0.127" layer="51"/>
<wire x1="7.5" y1="-9.6" x2="7.5" y2="-10" width="0.2" layer="51"/>
<wire x1="7.5" y1="-10" x2="-7.5" y2="-10" width="0.2" layer="51"/>
<wire x1="-7.5" y1="-10" x2="-7.5" y2="1.5" width="0.2" layer="51"/>
<wire x1="-7.5" y1="-2" x2="-7.5" y2="-4.1" width="0.2" layer="21"/>
<wire x1="7.5" y1="-2" x2="7.5" y2="-4.1" width="0.2" layer="21"/>
<wire x1="-6.1" y1="1.5" x2="-5" y2="1.5" width="0.2" layer="21"/>
<wire x1="6.1" y1="1.5" x2="5" y2="1.5" width="0.2" layer="21"/>
<smd name="1" x="4.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="4" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="3.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="3" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="2.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="2" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="1.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="1" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-0.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="-1" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="-1.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="-2" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="-2.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="-3" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="-3.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="-4" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="-4.5" y="0.76" dx="2.6" dy="0.3" layer="1" rot="R90"/>
<pad name="S1" x="-7.25" y="0" drill="0.9" diameter="1.7" shape="long" rot="R90"/>
<pad name="S2" x="-7.25" y="-5.96" drill="0.9" diameter="1.5" shape="long" rot="R90"/>
<pad name="S3" x="7.25" y="0" drill="0.9" diameter="1.7" shape="long" rot="R90"/>
<pad name="S4" x="7.25" y="-5.96" drill="0.9" diameter="1.5" shape="long" rot="R90"/>
<text x="-2" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<polygon width="0.2" layer="2">
<vertex x="-8" y="0.85" curve="-180"/>
<vertex x="-6.5" y="0.85"/>
<vertex x="-6.5" y="-0.85" curve="-180"/>
<vertex x="-8" y="-0.85"/>
</polygon>
<polygon width="0.2" layer="2">
<vertex x="6.5" y="0.85" curve="-180"/>
<vertex x="8" y="0.85"/>
<vertex x="8" y="-0.85" curve="-180"/>
<vertex x="6.5" y="-0.85"/>
</polygon>
<polygon width="0.2" layer="2">
<vertex x="6.6" y="-5.2" curve="-180"/>
<vertex x="7.9" y="-5.2"/>
<vertex x="7.9" y="-6.7" curve="-180"/>
<vertex x="6.6" y="-6.7"/>
</polygon>
<polygon width="0.2" layer="2">
<vertex x="-7.9" y="-5.2" curve="-180"/>
<vertex x="-6.6" y="-5.2"/>
<vertex x="-6.6" y="-6.7" curve="-180"/>
<vertex x="-7.9" y="-6.7"/>
</polygon>
<polygon width="0.2" layer="15">
<vertex x="-8" y="0.85" curve="-180"/>
<vertex x="-6.5" y="0.85"/>
<vertex x="-6.5" y="-0.85" curve="-180"/>
<vertex x="-8" y="-0.85"/>
</polygon>
<polygon width="0.2" layer="15">
<vertex x="6.5" y="0.85" curve="-180"/>
<vertex x="8" y="0.85"/>
<vertex x="8" y="-0.85" curve="-180"/>
<vertex x="6.5" y="-0.85"/>
</polygon>
<polygon width="0.2" layer="15">
<vertex x="6.6" y="-5.2" curve="-180"/>
<vertex x="7.9" y="-5.2"/>
<vertex x="7.9" y="-6.7" curve="-180"/>
<vertex x="6.6" y="-6.7"/>
</polygon>
<polygon width="0.2" layer="15">
<vertex x="-7.9" y="-5.2" curve="-180"/>
<vertex x="-6.6" y="-5.2"/>
<vertex x="-6.6" y="-6.7" curve="-180"/>
<vertex x="-7.9" y="-6.7"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="1747981-1">
<wire x1="-5.08" y1="27.94" x2="-5.08" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-27.94" x2="5.08" y2="-27.94" width="0.254" layer="94"/>
<wire x1="5.08" y1="-27.94" x2="5.08" y2="27.94" width="0.254" layer="94"/>
<wire x1="5.08" y1="27.94" x2="-5.08" y2="27.94" width="0.254" layer="94"/>
<text x="-2.54" y="27.94" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-30.48" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
<pin name="1" x="-7.62" y="25.4" length="short" direction="pas"/>
<pin name="2" x="-7.62" y="22.86" length="short" direction="pas"/>
<pin name="3" x="-7.62" y="20.32" length="short" direction="pas"/>
<pin name="4" x="-7.62" y="17.78" length="short" direction="pas"/>
<pin name="5" x="-7.62" y="15.24" length="short" direction="pas"/>
<pin name="6" x="-7.62" y="12.7" length="short" direction="pas"/>
<pin name="7" x="-7.62" y="10.16" length="short" direction="pas"/>
<pin name="8" x="-7.62" y="7.62" length="short" direction="pas"/>
<pin name="9" x="-7.62" y="5.08" length="short" direction="pas"/>
<pin name="10" x="-7.62" y="2.54" length="short" direction="pas"/>
<pin name="11" x="-7.62" y="0" length="short" direction="pas"/>
<pin name="12" x="-7.62" y="-2.54" length="short" direction="pas"/>
<pin name="13" x="-7.62" y="-5.08" length="short" direction="pas"/>
<pin name="14" x="-7.62" y="-7.62" length="short" direction="pas"/>
<pin name="15" x="-7.62" y="-10.16" length="short" direction="pas"/>
<pin name="16" x="-7.62" y="-12.7" length="short" direction="pas"/>
<pin name="17" x="-7.62" y="-15.24" length="short" direction="pas"/>
<pin name="18" x="-7.62" y="-17.78" length="short" direction="pas"/>
<pin name="19" x="-7.62" y="-20.32" length="short" direction="pas"/>
<pin name="SHIELD" x="-7.62" y="-25.4" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1747981-1" prefix="J">
<description>EMBOSS PACKAGE HDMI CONNECTOR &lt;a href="https://pricing.snapeda.com/parts/1747981-1/TE%20Connectivity/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="1747981-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TE_1747981-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="SHIELD" pad="S1 S2 S3 S4"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="COMMENT" value="1747981-1"/>
<attribute name="DESCRIPTION" value=" HDMI Connectors Product Highlights: Size = Standard Type A Series Receptacle 1 Ports Terminate To Printed Circuit Board "/>
<attribute name="MF" value="TE Connectivity"/>
<attribute name="MP" value="1747981-1"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/1747981-1/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="we-rj45_2">
<packages>
<package name="WE-RJ45">
<smd name="P$1" x="-6.63" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$2" x="-5.61" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$3" x="-4.59" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$4" x="-3.57" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$5" x="-2.55" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$6" x="-1.53" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$7" x="-0.51" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$8" x="0.51" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$9" x="1.53" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$10" x="2.55" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$11" x="3.57" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$12" x="4.59" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$13" x="5.61" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<smd name="P$14" x="6.63" y="-13.68" dx="2.89" dy="0.71" layer="1" rot="R90"/>
<hole x="-3.5" y="-10.2" drill="1.4"/>
<hole x="3.5" y="-10.2" drill="1.4"/>
<smd name="P$15" x="-9.5" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<smd name="P$16" x="9.5" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<wire x1="-7.8" y1="-8" x2="-7.8" y2="-15" width="0.2" layer="21"/>
<wire x1="7.8" y1="-15" x2="7.8" y2="-8" width="0.2" layer="21"/>
<wire x1="-7.8" y1="8.4" x2="7.8" y2="8.4" width="0.2" layer="51"/>
<wire x1="7.8" y1="8.4" x2="7.8" y2="1.7" width="0.2" layer="51"/>
<wire x1="7.8" y1="1.7" x2="10.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="10.6" y1="1.7" x2="10.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="10.6" y1="-1.7" x2="7.8" y2="-1.7" width="0.2" layer="51"/>
<wire x1="7.8" y1="-1.7" x2="7.8" y2="-15" width="0.2" layer="51"/>
<wire x1="7.8" y1="-15" x2="-7.8" y2="-15" width="0.2" layer="51"/>
<wire x1="-7.8" y1="-15" x2="-7.8" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-7.8" y1="-1.7" x2="-10.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-10.6" y1="-1.7" x2="-10.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="-10.6" y1="1.7" x2="-7.8" y2="1.7" width="0.2" layer="51"/>
<wire x1="-7.8" y1="1.7" x2="-7.8" y2="8.4" width="0.2" layer="51"/>
<text x="2.2" y="-15.5" size="0.8128" layer="25" font="vector" rot="R180">&gt;Name</text>
<text x="-1.7" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-7.08" y1="-12.4" x2="-7.08" y2="-14.9" width="0.2" layer="41"/>
<wire x1="6" y1="-12.4" x2="6" y2="-14.9" width="0.2" layer="41"/>
<wire x1="5.2" y1="-12.4" x2="5.2" y2="-14.9" width="0.2" layer="41"/>
<wire x1="5" y1="-12.4" x2="5" y2="-14.9" width="0.2" layer="41"/>
<wire x1="4.2" y1="-12.4" x2="4.2" y2="-14.9" width="0.2" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="WE-RJ45">
<pin name="LEDG-" x="-7.62" y="15.24" visible="pin" length="short"/>
<pin name="LEDG+" x="-7.62" y="12.7" visible="pin" length="short"/>
<pin name="TRD0+" x="-7.62" y="10.16" visible="pin" length="short"/>
<pin name="TRD0-" x="-7.62" y="7.62" visible="pin" length="short"/>
<pin name="TRD1+" x="-7.62" y="5.08" visible="pin" length="short"/>
<pin name="TRD1-" x="-7.62" y="2.54" visible="pin" length="short"/>
<pin name="TRD2+" x="-7.62" y="0" visible="pin" length="short"/>
<pin name="TRD2-" x="-7.62" y="-2.54" visible="pin" length="short"/>
<pin name="TRD3+" x="-7.62" y="-5.08" visible="pin" length="short"/>
<pin name="TRD3-" x="-7.62" y="-7.62" visible="pin" length="short"/>
<pin name="VCC" x="-7.62" y="-10.16" visible="pin" length="short" direction="pwr"/>
<pin name="GND" x="-7.62" y="-12.7" visible="pin" length="short" direction="pwr"/>
<pin name="LEDY-" x="-7.62" y="-15.24" visible="pin" length="short"/>
<pin name="LEDY+" x="-7.62" y="-17.78" visible="pin" length="short"/>
<pin name="CHS@1" x="5.08" y="-22.86" visible="pin" length="short" direction="pwr" rot="R90"/>
<pin name="CHS@2" x="5.08" y="20.32" visible="pin" length="short" direction="pwr" rot="R270"/>
<wire x1="-5.08" y1="17.78" x2="7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="-5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.32" x2="-5.08" y2="17.78" width="0.254" layer="94"/>
<text x="-5.08" y="20.32" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-22.86" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="WE-RJ45">
<gates>
<gate name="G$1" symbol="WE-RJ45" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WE-RJ45">
<connects>
<connect gate="G$1" pin="CHS@1" pad="P$15"/>
<connect gate="G$1" pin="CHS@2" pad="P$16"/>
<connect gate="G$1" pin="GND" pad="P$12"/>
<connect gate="G$1" pin="LEDG+" pad="P$2"/>
<connect gate="G$1" pin="LEDG-" pad="P$1"/>
<connect gate="G$1" pin="LEDY+" pad="P$14"/>
<connect gate="G$1" pin="LEDY-" pad="P$13"/>
<connect gate="G$1" pin="TRD0+" pad="P$3"/>
<connect gate="G$1" pin="TRD0-" pad="P$4"/>
<connect gate="G$1" pin="TRD1+" pad="P$5"/>
<connect gate="G$1" pin="TRD1-" pad="P$6"/>
<connect gate="G$1" pin="TRD2+" pad="P$7"/>
<connect gate="G$1" pin="TRD2-" pad="P$8"/>
<connect gate="G$1" pin="TRD3+" pad="P$9"/>
<connect gate="G$1" pin="TRD3-" pad="P$10"/>
<connect gate="G$1" pin="VCC" pad="P$11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_POWER">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<pin name="+5V" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+9V">
<pin name="+9V" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+1V2">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+1V2" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+5V0A">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.508" y="-5.715" size="1.016" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V0A" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND_">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="+5V_">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3_">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+9V" prefix="+9V_">
<gates>
<gate name="G$1" symbol="+9V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+1V2" prefix="+1V2_">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+1V2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V0A" prefix="+5V0A_">
<gates>
<gate name="G$1" symbol="+5V0A" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="74LV8T245QWRKSRQ1">
<packages>
<package name="VQFN">
<smd name="1" x="-0.75" y="2.65" dx="0.25" dy="0.6" layer="1"/>
<smd name="2" x="-1.65" y="2.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-1.65" y="1.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-1.65" y="1.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-1.65" y="0.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="-1.65" y="0.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-1.65" y="-0.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="-1.65" y="-0.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="9" x="-1.65" y="-1.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="-1.65" y="-1.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="-1.65" y="-2.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="-0.75" y="-2.65" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="13" x="0.75" y="-2.65" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="14" x="1.65" y="-2.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="15" x="1.65" y="-1.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="16" x="1.65" y="-1.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="17" x="1.65" y="-0.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="18" x="1.65" y="-0.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="19" x="1.65" y="0.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="20" x="1.65" y="0.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<wire x1="-1.9" y1="2.7" x2="-1.9" y2="2.9" width="0.2" layer="21"/>
<wire x1="-1.9" y1="2.9" x2="-1.2" y2="2.9" width="0.2" layer="21"/>
<wire x1="1.2" y1="2.9" x2="1.9" y2="2.9" width="0.2" layer="21"/>
<wire x1="1.9" y1="2.9" x2="1.9" y2="2.7" width="0.2" layer="21"/>
<wire x1="1.9" y1="-2.7" x2="1.9" y2="-2.9" width="0.2" layer="21"/>
<wire x1="1.9" y1="-2.9" x2="1.2" y2="-2.9" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.9" x2="-1.9" y2="-2.9" width="0.2" layer="21"/>
<wire x1="-1.9" y1="-2.9" x2="-1.9" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-1.9" y1="2.7" x2="-1.2" y2="2.9" width="0.2" layer="21"/>
<text x="-2" y="3" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<wire x1="-1.9" y1="2.9" x2="-1.9" y2="-2.9" width="0.2" layer="51"/>
<wire x1="-1.9" y1="-2.9" x2="1.9" y2="-2.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="-2.9" x2="1.9" y2="2.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="2.9" x2="-1.9" y2="2.9" width="0.2" layer="51"/>
<circle x="-1.1" y="2.1" radius="0.2" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<text x="-1.1" y="3.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="21" x="1.65" y="1.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="22" x="1.65" y="1.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="23" x="1.65" y="2.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="24" x="0.75" y="2.65" dx="0.25" dy="0.6" layer="1"/>
<smd name="G1" x="0" y="0" dx="2" dy="4" layer="1" cream="no"/>
<smd name="G2" x="-0.25" y="2.45" dx="0.25" dy="1" layer="1"/>
<smd name="G3" x="0.25" y="2.45" dx="0.25" dy="1" layer="1"/>
<smd name="G4" x="-0.25" y="-2.45" dx="0.25" dy="1" layer="1" rot="R180"/>
<smd name="G5" x="0.25" y="-2.45" dx="0.25" dy="1" layer="1" rot="R180"/>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="1.6"/>
<vertex x="-0.6" y="0.9"/>
<vertex x="-0.3" y="0.9"/>
<vertex x="-0.3" y="1.6"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.3" y="1.6"/>
<vertex x="0.3" y="0.9"/>
<vertex x="0.6" y="0.9"/>
<vertex x="0.6" y="1.6"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="0.3"/>
<vertex x="-0.6" y="-0.4"/>
<vertex x="-0.3" y="-0.4"/>
<vertex x="-0.3" y="0.3"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.3" y="0.3"/>
<vertex x="0.3" y="-0.4"/>
<vertex x="0.6" y="-0.4"/>
<vertex x="0.6" y="0.3"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="-0.9"/>
<vertex x="-0.6" y="-1.6"/>
<vertex x="-0.3" y="-1.6"/>
<vertex x="-0.3" y="-0.9"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.3" y="-0.9"/>
<vertex x="0.3" y="-1.6"/>
<vertex x="0.6" y="-1.6"/>
<vertex x="0.6" y="-0.9"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="SN74LXC8T245RHLR">
<pin name="VCCA" x="-10.16" y="12.7" length="short" direction="pas"/>
<pin name="DIR" x="-10.16" y="10.16" length="short" direction="pas"/>
<pin name="A1" x="-10.16" y="7.62" length="short" direction="pas"/>
<pin name="A2" x="-10.16" y="5.08" length="short" direction="pas"/>
<pin name="A3" x="-10.16" y="2.54" length="short" direction="pas"/>
<pin name="A4" x="-10.16" y="0" length="short" direction="pas"/>
<pin name="A5" x="-10.16" y="-2.54" length="short" direction="pas"/>
<pin name="A6" x="-10.16" y="-5.08" length="short" direction="pas"/>
<pin name="A7" x="-10.16" y="-7.62" length="short" direction="pas"/>
<pin name="A8" x="-10.16" y="-10.16" length="short" direction="pas"/>
<pin name="GND@11" x="-10.16" y="-12.7" length="short" direction="pas"/>
<pin name="GND@12" x="-10.16" y="-15.24" length="short" direction="pas"/>
<pin name="GND@13" x="10.16" y="-12.7" length="short" direction="pas" rot="R180"/>
<pin name="B8" x="10.16" y="-10.16" length="short" direction="pas" rot="R180"/>
<pin name="B7" x="10.16" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="B6" x="10.16" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="B5" x="10.16" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="B4" x="10.16" y="0" length="short" direction="pas" rot="R180"/>
<pin name="B3" x="10.16" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="B2" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="B1" x="10.16" y="7.62" length="short" direction="pas" rot="R180"/>
<pin name="/OE" x="10.16" y="10.16" length="short" direction="pas" rot="R180"/>
<pin name="VCCB@23" x="10.16" y="12.7" length="short" direction="pas" rot="R180"/>
<pin name="VCCB@24" x="10.16" y="15.24" length="short" direction="pas" rot="R180"/>
<pin name="GND@25" x="10.16" y="-15.24" length="short" direction="pas" rot="R180"/>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<text x="-5.08" y="20.32" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-20.32" size="1.778" layer="95" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SN74LXC8T245">
<gates>
<gate name="G$1" symbol="SN74LXC8T245RHLR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN">
<connects>
<connect gate="G$1" pin="/OE" pad="22"/>
<connect gate="G$1" pin="A1" pad="3"/>
<connect gate="G$1" pin="A2" pad="4"/>
<connect gate="G$1" pin="A3" pad="5"/>
<connect gate="G$1" pin="A4" pad="6"/>
<connect gate="G$1" pin="A5" pad="7"/>
<connect gate="G$1" pin="A6" pad="8"/>
<connect gate="G$1" pin="A7" pad="9"/>
<connect gate="G$1" pin="A8" pad="10"/>
<connect gate="G$1" pin="B1" pad="21"/>
<connect gate="G$1" pin="B2" pad="20"/>
<connect gate="G$1" pin="B3" pad="19"/>
<connect gate="G$1" pin="B4" pad="18"/>
<connect gate="G$1" pin="B5" pad="17"/>
<connect gate="G$1" pin="B6" pad="16"/>
<connect gate="G$1" pin="B7" pad="15"/>
<connect gate="G$1" pin="B8" pad="14"/>
<connect gate="G$1" pin="DIR" pad="2"/>
<connect gate="G$1" pin="GND@11" pad="11"/>
<connect gate="G$1" pin="GND@12" pad="12"/>
<connect gate="G$1" pin="GND@13" pad="13"/>
<connect gate="G$1" pin="GND@25" pad="G1 G2 G3 G4 G5"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB@23" pad="23"/>
<connect gate="G$1" pin="VCCB@24" pad="24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SN74LXC8T245">
<packages>
<package name="VQFN">
<smd name="1" x="-0.75" y="2.65" dx="0.25" dy="0.6" layer="1"/>
<smd name="2" x="-1.65" y="2.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-1.65" y="1.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-1.65" y="1.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-1.65" y="0.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="-1.65" y="0.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-1.65" y="-0.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="-1.65" y="-0.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="9" x="-1.65" y="-1.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="-1.65" y="-1.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="-1.65" y="-2.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="-0.75" y="-2.65" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="13" x="0.75" y="-2.65" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="14" x="1.65" y="-2.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="15" x="1.65" y="-1.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="16" x="1.65" y="-1.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="17" x="1.65" y="-0.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="18" x="1.65" y="-0.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="19" x="1.65" y="0.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="20" x="1.65" y="0.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<wire x1="-1.9" y1="2.7" x2="-1.9" y2="2.9" width="0.2" layer="21"/>
<wire x1="-1.9" y1="2.9" x2="-1.2" y2="2.9" width="0.2" layer="21"/>
<wire x1="1.2" y1="2.9" x2="1.9" y2="2.9" width="0.2" layer="21"/>
<wire x1="1.9" y1="2.9" x2="1.9" y2="2.7" width="0.2" layer="21"/>
<wire x1="1.9" y1="-2.7" x2="1.9" y2="-2.9" width="0.2" layer="21"/>
<wire x1="1.9" y1="-2.9" x2="1.2" y2="-2.9" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.9" x2="-1.9" y2="-2.9" width="0.2" layer="21"/>
<wire x1="-1.9" y1="-2.9" x2="-1.9" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-1.9" y1="2.7" x2="-1.2" y2="2.9" width="0.2" layer="21"/>
<text x="-2" y="3" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<wire x1="-1.9" y1="2.9" x2="-1.9" y2="-2.9" width="0.2" layer="51"/>
<wire x1="-1.9" y1="-2.9" x2="1.9" y2="-2.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="-2.9" x2="1.9" y2="2.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="2.9" x2="-1.9" y2="2.9" width="0.2" layer="51"/>
<circle x="-1.1" y="2.1" radius="0.2" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<text x="-1.1" y="3.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="21" x="1.65" y="1.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="22" x="1.65" y="1.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="23" x="1.65" y="2.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="24" x="0.75" y="2.65" dx="0.25" dy="0.6" layer="1"/>
<smd name="G1" x="0" y="0" dx="2" dy="4" layer="1" cream="no"/>
<smd name="G2" x="-0.25" y="2.45" dx="0.25" dy="1" layer="1"/>
<smd name="G3" x="0.25" y="2.45" dx="0.25" dy="1" layer="1"/>
<smd name="G4" x="-0.25" y="-2.45" dx="0.25" dy="1" layer="1" rot="R180"/>
<smd name="G5" x="0.25" y="-2.45" dx="0.25" dy="1" layer="1" rot="R180"/>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="1.6"/>
<vertex x="-0.6" y="1"/>
<vertex x="-0.3" y="1"/>
<vertex x="-0.3" y="1.6"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.3" y="1.6"/>
<vertex x="0.3" y="1"/>
<vertex x="0.6" y="1"/>
<vertex x="0.6" y="1.6"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="0.3"/>
<vertex x="-0.6" y="-0.3"/>
<vertex x="-0.3" y="-0.3"/>
<vertex x="-0.3" y="0.3"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.3" y="0.3"/>
<vertex x="0.3" y="-0.3"/>
<vertex x="0.6" y="-0.3"/>
<vertex x="0.6" y="0.3"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="-0.6" y="-1"/>
<vertex x="-0.6" y="-1.6"/>
<vertex x="-0.3" y="-1.6"/>
<vertex x="-0.3" y="-1"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.3" y="-1"/>
<vertex x="0.3" y="-1.6"/>
<vertex x="0.6" y="-1.6"/>
<vertex x="0.6" y="-1"/>
</polygon>
<wire x1="-1.8" y1="2" x2="-1.5" y2="2" width="0.25" layer="41"/>
<wire x1="-1.8" y1="1.5" x2="-1.5" y2="1.5" width="0.25" layer="41"/>
<wire x1="-1.8" y1="1" x2="-1.5" y2="1" width="0.25" layer="41"/>
<wire x1="-1.8" y1="0.5" x2="-1.5" y2="0.5" width="0.25" layer="41"/>
<wire x1="-1.8" y1="0" x2="-1.5" y2="0" width="0.25" layer="41"/>
<wire x1="-1.8" y1="-0.5" x2="-1.5" y2="-0.5" width="0.25" layer="41"/>
<wire x1="-1.8" y1="-1" x2="-1.5" y2="-1" width="0.25" layer="41"/>
<wire x1="-1.8" y1="-1.5" x2="-1.5" y2="-1.5" width="0.25" layer="41"/>
<wire x1="-1.8" y1="-2" x2="-1.5" y2="-2" width="0.25" layer="41"/>
<wire x1="-1.8" y1="-2.5" x2="-1.5" y2="-2.5" width="0.25" layer="41"/>
<wire x1="-1" y1="-2.8" x2="-1" y2="-2.5" width="0.25" layer="41"/>
<wire x1="-0.5" y1="-2.8" x2="-0.5" y2="-2.2" width="0.25" layer="41"/>
<wire x1="0" y1="-2.8" x2="0" y2="-2.2" width="0.25" layer="41"/>
<wire x1="0.5" y1="-2.8" x2="0.5" y2="-2.2" width="0.25" layer="41"/>
<wire x1="1" y1="-2.8" x2="1" y2="-2.5" width="0.25" layer="41"/>
<wire x1="1.8" y1="-2.5" x2="1.5" y2="-2.5" width="0.25" layer="41"/>
<wire x1="1.8" y1="-2" x2="1.5" y2="-2" width="0.25" layer="41"/>
<wire x1="1.8" y1="-1.5" x2="1.5" y2="-1.5" width="0.25" layer="41"/>
<wire x1="1.8" y1="-1" x2="1.5" y2="-1" width="0.25" layer="41"/>
<wire x1="1.8" y1="-0.5" x2="1.5" y2="-0.5" width="0.25" layer="41"/>
<wire x1="1.8" y1="0" x2="1.5" y2="0" width="0.25" layer="41"/>
<wire x1="1.8" y1="0.5" x2="1.5" y2="0.5" width="0.25" layer="41"/>
<wire x1="1.8" y1="1" x2="1.5" y2="1" width="0.25" layer="41"/>
<wire x1="1.8" y1="1.5" x2="1.5" y2="1.5" width="0.25" layer="41"/>
<wire x1="1.8" y1="2" x2="1.5" y2="2" width="0.25" layer="41"/>
<wire x1="1.8" y1="2.5" x2="1.5" y2="2.5" width="0.25" layer="41"/>
<wire x1="1" y1="2.8" x2="1" y2="2.5" width="0.25" layer="41"/>
<wire x1="0.5" y1="2.8" x2="0.5" y2="2.2" width="0.25" layer="41"/>
<wire x1="0" y1="2.8" x2="0" y2="2.2" width="0.25" layer="41"/>
<wire x1="-0.5" y1="2.8" x2="-0.5" y2="2.2" width="0.25" layer="41"/>
<wire x1="-1" y1="2.8" x2="-1" y2="2.5" width="0.25" layer="41"/>
<wire x1="-1.8" y1="2.5" x2="-1.5" y2="2.5" width="0.25" layer="41"/>
<wire x1="-1.1" y1="-0.3" x2="-1.1" y2="0.4" width="0.2" layer="41"/>
<wire x1="1.1" y1="0.4" x2="1.1" y2="-0.3" width="0.2" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="SN74LXC8T245RHLR">
<pin name="VCCA" x="-10.16" y="15.24" length="short" direction="pas"/>
<pin name="DIR" x="-10.16" y="10.16" length="short" direction="pas"/>
<pin name="A1" x="-10.16" y="7.62" length="short" direction="pas"/>
<pin name="A2" x="-10.16" y="5.08" length="short" direction="pas"/>
<pin name="A3" x="-10.16" y="2.54" length="short" direction="pas"/>
<pin name="A4" x="-10.16" y="0" length="short" direction="pas"/>
<pin name="A5" x="-10.16" y="-2.54" length="short" direction="pas"/>
<pin name="A6" x="-10.16" y="-5.08" length="short" direction="pas"/>
<pin name="A7" x="-10.16" y="-7.62" length="short" direction="pas"/>
<pin name="A8" x="-10.16" y="-10.16" length="short" direction="pas"/>
<pin name="GND@11" x="-10.16" y="-12.7" length="short" direction="pas"/>
<pin name="GND@12" x="-10.16" y="-15.24" length="short" direction="pas"/>
<pin name="GND@13" x="10.16" y="-12.7" length="short" direction="pas" rot="R180"/>
<pin name="B8" x="10.16" y="-10.16" length="short" direction="pas" rot="R180"/>
<pin name="B7" x="10.16" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="B6" x="10.16" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="B5" x="10.16" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="B4" x="10.16" y="0" length="short" direction="pas" rot="R180"/>
<pin name="B3" x="10.16" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="B2" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="B1" x="10.16" y="7.62" length="short" direction="pas" rot="R180"/>
<pin name="/OE" x="-10.16" y="12.7" length="short" direction="pas"/>
<pin name="VCCB@23" x="10.16" y="12.7" length="short" direction="pas" rot="R180"/>
<pin name="VCCB@24" x="10.16" y="15.24" length="short" direction="pas" rot="R180"/>
<pin name="GND@25" x="10.16" y="-15.24" length="short" direction="pas" rot="R180"/>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<text x="-5.08" y="20.32" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-20.32" size="1.778" layer="95" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SN74LXC8T245">
<gates>
<gate name="G$1" symbol="SN74LXC8T245RHLR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN">
<connects>
<connect gate="G$1" pin="/OE" pad="22"/>
<connect gate="G$1" pin="A1" pad="3"/>
<connect gate="G$1" pin="A2" pad="4"/>
<connect gate="G$1" pin="A3" pad="5"/>
<connect gate="G$1" pin="A4" pad="6"/>
<connect gate="G$1" pin="A5" pad="7"/>
<connect gate="G$1" pin="A6" pad="8"/>
<connect gate="G$1" pin="A7" pad="9"/>
<connect gate="G$1" pin="A8" pad="10"/>
<connect gate="G$1" pin="B1" pad="21"/>
<connect gate="G$1" pin="B2" pad="20"/>
<connect gate="G$1" pin="B3" pad="19"/>
<connect gate="G$1" pin="B4" pad="18"/>
<connect gate="G$1" pin="B5" pad="17"/>
<connect gate="G$1" pin="B6" pad="16"/>
<connect gate="G$1" pin="B7" pad="15"/>
<connect gate="G$1" pin="B8" pad="14"/>
<connect gate="G$1" pin="DIR" pad="2"/>
<connect gate="G$1" pin="GND@11" pad="11"/>
<connect gate="G$1" pin="GND@12" pad="12"/>
<connect gate="G$1" pin="GND@13" pad="13"/>
<connect gate="G$1" pin="GND@25" pad="G1 G2 G3 G4 G5"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB@23" pad="23"/>
<connect gate="G$1" pin="VCCB@24" pad="24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_HEADER_THT_2.54X1N_MALE">
<packages>
<package name="HEADER_THT_2.54X1N_MALE-02">
<pad name="1" x="-1.27" y="0" drill="1"/>
<pad name="2" x="1.27" y="0" drill="1"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.2" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.2" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.2" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.2" layer="51"/>
<text x="-1" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1" y="-0.5" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="HEADER_THT_2.54X1N_MALE-02">
<pin name="1" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="1.824" size="1.4224" layer="95" font="vector">1</text>
<text x="-1.778" y="-0.716" size="1.4224" layer="95" font="vector">2</text>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="0" y="-5.08" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER_THT_2.54X1N_MALE-02" prefix="JP" uservalue="yes">
<description>"HEADER_THT_2.54X1N_MALE-02"</description>
<gates>
<gate name="JP" symbol="HEADER_THT_2.54X1N_MALE-02" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="HEADER_THT_2.54X1N_MALE-02">
<connects>
<connect gate="JP" pin="1" pad="1"/>
<connect gate="JP" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-2.54"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_HEADER_THT_2.54X2_MALE">
<packages>
<package name="HEADER_THT_2.54X2_MALE-02">
<pad name="1" x="-1.27" y="0" drill="1" shape="square"/>
<pad name="2" x="1.27" y="0" drill="1"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.2" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.2" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.2" layer="51"/>
<wire x1="-1.905" y1="-0.635" x2="-0.635" y2="-0.635" width="0.2" layer="51"/>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="0.635" width="0.2" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.905" y2="0.635" width="0.2" layer="51"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="-0.635" width="0.2" layer="51"/>
<text x="-1" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1" y="-0.5" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="HEADER_THT_2.54X2_MALE-02">
<pin name="1" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="1.824" size="1.4224" layer="95" font="vector">1</text>
<text x="-1.778" y="-0.716" size="1.4224" layer="95" font="vector">2</text>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="0" y="-5.08" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER_THT_2.54X2_MALE-02" prefix="JP" uservalue="yes">
<description>"HEADER_THT_2.54X2_MALE-02"</description>
<gates>
<gate name="JP" symbol="HEADER_THT_2.54X2_MALE-02" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="HEADER_THT_2.54X2_MALE-02">
<connects>
<connect gate="JP" pin="1" pad="1"/>
<connect gate="JP" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-2.54"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ICE40HX4K-TQ144">
<packages>
<package name="QFP50P2200X2200X160-144N">
<text x="-1.5" y="11.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="10" y1="-10" x2="-10" y2="-10" width="0.2" layer="51"/>
<wire x1="10" y1="10" x2="-10" y2="10" width="0.2" layer="51"/>
<wire x1="10" y1="-10" x2="10" y2="10" width="0.2" layer="51"/>
<wire x1="-10" y1="-10" x2="-10" y2="10" width="0.2" layer="51"/>
<smd name="37" x="-8.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="38" x="-8.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="39" x="-7.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="40" x="-7.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="41" x="-6.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="42" x="-6.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="43" x="-5.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="44" x="-5.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="45" x="-4.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="46" x="-4.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="47" x="-3.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="48" x="-3.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="49" x="-2.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="50" x="-2.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="51" x="-1.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="52" x="-1.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="53" x="-0.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="54" x="-0.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="55" x="0.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="56" x="0.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="57" x="1.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="58" x="1.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="59" x="2.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="60" x="2.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="61" x="3.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="62" x="3.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="63" x="4.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="64" x="4.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="65" x="5.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="66" x="5.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="67" x="6.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="68" x="6.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="69" x="7.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="70" x="7.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="71" x="8.25" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="72" x="8.75" y="-10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="109" x="8.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="110" x="8.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="111" x="7.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="112" x="7.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="113" x="6.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="114" x="6.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="115" x="5.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="116" x="5.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="117" x="4.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="118" x="4.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="119" x="3.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="120" x="3.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="121" x="2.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="122" x="2.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="123" x="1.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="124" x="1.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="125" x="0.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="126" x="0.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="127" x="-0.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="128" x="-0.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="129" x="-1.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="130" x="-1.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="131" x="-2.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="132" x="-2.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="133" x="-3.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="134" x="-3.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="135" x="-4.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="136" x="-4.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="137" x="-5.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="138" x="-5.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="139" x="-6.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="140" x="-6.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="141" x="-7.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="142" x="-7.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="143" x="-8.25" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="144" x="-8.75" y="10.67" dx="1.47" dy="0.28" layer="1" rot="R90"/>
<smd name="1" x="-10.67" y="8.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="2" x="-10.67" y="8.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="3" x="-10.67" y="7.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="4" x="-10.67" y="7.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="5" x="-10.67" y="6.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="6" x="-10.67" y="6.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="7" x="-10.67" y="5.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="8" x="-10.67" y="5.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="9" x="-10.67" y="4.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="10" x="-10.67" y="4.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="11" x="-10.67" y="3.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="12" x="-10.67" y="3.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="13" x="-10.67" y="2.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="14" x="-10.67" y="2.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="15" x="-10.67" y="1.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="16" x="-10.67" y="1.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="17" x="-10.67" y="0.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="18" x="-10.67" y="0.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="19" x="-10.67" y="-0.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="20" x="-10.67" y="-0.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="21" x="-10.67" y="-1.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="22" x="-10.67" y="-1.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="23" x="-10.67" y="-2.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="24" x="-10.67" y="-2.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="25" x="-10.67" y="-3.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="26" x="-10.67" y="-3.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="27" x="-10.67" y="-4.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="28" x="-10.67" y="-4.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="29" x="-10.67" y="-5.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="30" x="-10.67" y="-5.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="31" x="-10.67" y="-6.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="32" x="-10.67" y="-6.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="33" x="-10.67" y="-7.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="34" x="-10.67" y="-7.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="35" x="-10.67" y="-8.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="36" x="-10.67" y="-8.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="73" x="10.67" y="-8.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="74" x="10.67" y="-8.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="75" x="10.67" y="-7.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="76" x="10.67" y="-7.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="77" x="10.67" y="-6.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="78" x="10.67" y="-6.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="79" x="10.67" y="-5.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="80" x="10.67" y="-5.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="81" x="10.67" y="-4.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="82" x="10.67" y="-4.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="83" x="10.67" y="-3.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="84" x="10.67" y="-3.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="85" x="10.67" y="-2.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="86" x="10.67" y="-2.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="87" x="10.67" y="-1.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="88" x="10.67" y="-1.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="89" x="10.67" y="-0.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="90" x="10.67" y="-0.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="91" x="10.67" y="0.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="92" x="10.67" y="0.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="93" x="10.67" y="1.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="94" x="10.67" y="1.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="95" x="10.67" y="2.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="96" x="10.67" y="2.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="97" x="10.67" y="3.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="98" x="10.67" y="3.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="99" x="10.67" y="4.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="100" x="10.67" y="4.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="101" x="10.67" y="5.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="102" x="10.67" y="5.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="103" x="10.67" y="6.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="104" x="10.67" y="6.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="105" x="10.67" y="7.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="106" x="10.67" y="7.75" dx="1.47" dy="0.28" layer="1"/>
<smd name="107" x="10.67" y="8.25" dx="1.47" dy="0.28" layer="1"/>
<smd name="108" x="10.67" y="8.75" dx="1.47" dy="0.28" layer="1"/>
<text x="-1.5" y="-0.5" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-10" y1="9.3" x2="-10" y2="10" width="0.2" layer="21"/>
<wire x1="-10" y1="10" x2="-9.3" y2="10" width="0.2" layer="21"/>
<wire x1="-9.3" y1="10" x2="-10" y2="9.3" width="0.2" layer="21"/>
<wire x1="9.3" y1="10" x2="10" y2="10" width="0.2" layer="21"/>
<wire x1="10" y1="10" x2="10" y2="9.3" width="0.2" layer="21"/>
<wire x1="10" y1="-9.3" x2="10" y2="-10" width="0.2" layer="21"/>
<wire x1="10" y1="-10" x2="9.3" y2="-10" width="0.2" layer="21"/>
<wire x1="-9.3" y1="-10" x2="-10" y2="-10" width="0.2" layer="21"/>
<wire x1="-10" y1="-10" x2="-10" y2="-9.3" width="0.2" layer="21"/>
<circle x="-8.6" y="8.6" radius="0.4" width="0.2" layer="51"/>
<text x="-10.1" y="10.1" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<wire x1="-11.3" y1="7" x2="-10.1" y2="7" width="0.25" layer="41"/>
<wire x1="-11.3" y1="6.5" x2="-10.1" y2="6.5" width="0.25" layer="41"/>
<wire x1="-11.3" y1="3" x2="-10.1" y2="3" width="0.25" layer="41"/>
<wire x1="-11.3" y1="2.5" x2="-10.1" y2="2.5" width="0.25" layer="41"/>
<wire x1="-11.3" y1="2" x2="-10.1" y2="2" width="0.25" layer="41"/>
<wire x1="7.5" y1="-11.3" x2="7.5" y2="-10.1" width="0.25" layer="41"/>
<wire x1="7" y1="-11.3" x2="7" y2="-10.1" width="0.25" layer="41"/>
<wire x1="2.5" y1="-11.3" x2="2.5" y2="-10.1" width="0.25" layer="41"/>
<wire x1="2" y1="-11.3" x2="2" y2="-10.1" width="0.25" layer="41"/>
<wire x1="11.3" y1="-2.5" x2="10.1" y2="-2.5" width="0.25" layer="41"/>
<wire x1="11.3" y1="-2" x2="10.1" y2="-2" width="0.25" layer="41"/>
<wire x1="11.3" y1="6.5" x2="10.1" y2="6.5" width="0.25" layer="41"/>
<wire x1="11.3" y1="6" x2="10.1" y2="6" width="0.25" layer="41"/>
<wire x1="-7" y1="11.3" x2="-7" y2="10.1" width="0.25" layer="41"/>
<wire x1="-6.5" y1="11.3" x2="-6.5" y2="10.1" width="0.25" layer="41"/>
<wire x1="-3" y1="11.3" x2="-3" y2="10.1" width="0.25" layer="41"/>
<wire x1="-2.5" y1="11.3" x2="-2.5" y2="10.1" width="0.25" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="ICE40HX4K-TQ144">
<wire x1="17.78" y1="86.36" x2="17.78" y2="-86.36" width="0.254" layer="94"/>
<wire x1="17.78" y1="-86.36" x2="-17.78" y2="-86.36" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-86.36" x2="-17.78" y2="86.36" width="0.254" layer="94"/>
<wire x1="-17.78" y1="86.36" x2="17.78" y2="86.36" width="0.254" layer="94"/>
<text x="-5.08" y="88.9" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-88.9" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
<pin name="CDONE" x="-20.32" y="83.82" length="short" direction="in"/>
<pin name="CRESET_B" x="-20.32" y="81.28" length="short" direction="in"/>
<pin name="GND" x="-20.32" y="-83.82" length="short" direction="pwr"/>
<pin name="GNDPLL0" x="20.32" y="63.5" length="short" direction="pwr" rot="R180"/>
<pin name="GNDPLL1" x="20.32" y="60.96" length="short" direction="pwr" rot="R180"/>
<pin name="IO2_102" x="20.32" y="55.88" length="short" rot="R180"/>
<pin name="IO2_103_CBSEL0" x="-20.32" y="76.2" length="short"/>
<pin name="IO2_104_CBSEL1" x="-20.32" y="73.66" length="short"/>
<pin name="IOB_105_SDO" x="-20.32" y="71.12" length="short"/>
<pin name="IOB_106_SDI" x="-20.32" y="68.58" length="short"/>
<pin name="IOB_107_SCK" x="-20.32" y="66.04" length="short"/>
<pin name="IOB_108_SS" x="-20.32" y="63.5" length="short"/>
<pin name="IO2_56" x="-20.32" y="27.94" length="short"/>
<pin name="IO2_57" x="-20.32" y="25.4" length="short"/>
<pin name="IO2_61" x="-20.32" y="22.86" length="short"/>
<pin name="IO2_63" x="-20.32" y="20.32" length="short"/>
<pin name="IO2_64" x="-20.32" y="17.78" length="short"/>
<pin name="IO2_71" x="-20.32" y="15.24" length="short"/>
<pin name="IO2_72" x="-20.32" y="12.7" length="short"/>
<pin name="IO2_79" x="-20.32" y="7.62" length="short"/>
<pin name="IO2_73" x="-20.32" y="10.16" length="short"/>
<pin name="IO2_80" x="-20.32" y="5.08" length="short"/>
<pin name="IO2_81_GBIN5" x="-20.32" y="50.8" length="short" direction="in"/>
<pin name="IO2_82_GBIN4" x="-20.32" y="48.26" length="short" direction="in"/>
<pin name="IO2_91" x="-20.32" y="2.54" length="short"/>
<pin name="IO2_94" x="-20.32" y="0" length="short"/>
<pin name="IO2_95" x="-20.32" y="-2.54" length="short"/>
<pin name="IO2_96" x="-20.32" y="-5.08" length="short"/>
<pin name="IO3_10A" x="-20.32" y="-35.56" length="short"/>
<pin name="IO3_10B" x="-20.32" y="-38.1" length="short"/>
<pin name="IO3_12A" x="-20.32" y="-40.64" length="short"/>
<pin name="IO3_12B" x="-20.32" y="-43.18" length="short"/>
<pin name="IO3_13A" x="-20.32" y="-45.72" length="short"/>
<pin name="IO3_13B_GBIN7" x="-20.32" y="58.42" length="short" direction="in"/>
<pin name="IO3_14A_GBIN6" x="-20.32" y="55.88" length="short" direction="in"/>
<pin name="IO3_14B" x="-20.32" y="-48.26" length="short"/>
<pin name="IO3_17A" x="-20.32" y="-50.8" length="short"/>
<pin name="IO3_17B" x="-20.32" y="-53.34" length="short"/>
<pin name="IO3_18A" x="-20.32" y="-55.88" length="short"/>
<pin name="IO3_18B" x="-20.32" y="-58.42" length="short"/>
<pin name="IO3_23A" x="-20.32" y="-60.96" length="short"/>
<pin name="IO3_23B" x="-20.32" y="-63.5" length="short"/>
<pin name="IO3_24A" x="-20.32" y="-66.04" length="short"/>
<pin name="IO3_24B" x="-20.32" y="-68.58" length="short"/>
<pin name="IO3_25A" x="-20.32" y="-71.12" length="short"/>
<pin name="IO3_25B" x="-20.32" y="-73.66" length="short"/>
<pin name="IO3_2A" x="-20.32" y="-10.16" length="short"/>
<pin name="IO3_2B" x="-20.32" y="-12.7" length="short"/>
<pin name="IO3_3A" x="-20.32" y="-15.24" length="short"/>
<pin name="IO3_3B" x="-20.32" y="-17.78" length="short"/>
<pin name="IO3_4A" x="-20.32" y="-20.32" length="short"/>
<pin name="IO3_4B" x="-20.32" y="-22.86" length="short"/>
<pin name="IO3_5A" x="-20.32" y="-25.4" length="short"/>
<pin name="IO3_5B" x="-20.32" y="-27.94" length="short"/>
<pin name="IO3_8A" x="-20.32" y="-30.48" length="short"/>
<pin name="IO3_8B" x="-20.32" y="-33.02" length="short"/>
<pin name="IO1_109" x="20.32" y="53.34" length="short" rot="R180"/>
<pin name="IO1_110" x="20.32" y="50.8" length="short" rot="R180"/>
<pin name="IO1_111" x="20.32" y="48.26" length="short" rot="R180"/>
<pin name="IO1_112" x="20.32" y="45.72" length="short" rot="R180"/>
<pin name="IO1_114" x="20.32" y="43.18" length="short" rot="R180"/>
<pin name="IO1_115" x="20.32" y="40.64" length="short" rot="R180"/>
<pin name="IO1_116" x="20.32" y="38.1" length="short" rot="R180"/>
<pin name="IO1_117" x="20.32" y="35.56" length="short" rot="R180"/>
<pin name="IO1_118" x="20.32" y="33.02" length="short" rot="R180"/>
<pin name="IO1_119" x="20.32" y="30.48" length="short" rot="R180"/>
<pin name="IO1_120" x="20.32" y="27.94" length="short" rot="R180"/>
<pin name="IO1_128" x="20.32" y="25.4" length="short" rot="R180"/>
<pin name="IO1_136" x="20.32" y="22.86" length="short" rot="R180"/>
<pin name="IO1_137" x="20.32" y="20.32" length="short" rot="R180"/>
<pin name="IO1_138" x="20.32" y="17.78" length="short" rot="R180"/>
<pin name="IO1_139" x="20.32" y="15.24" length="short" rot="R180"/>
<pin name="IO1_140_GBIN3" x="-20.32" y="43.18" length="short" direction="in"/>
<pin name="IO1_141_GBIN2" x="-20.32" y="40.64" length="short" direction="in"/>
<pin name="IO1_144" x="20.32" y="12.7" length="short" rot="R180"/>
<pin name="IO1_146" x="20.32" y="10.16" length="short" rot="R180"/>
<pin name="IO1_147" x="20.32" y="7.62" length="short" rot="R180"/>
<pin name="IO1_148" x="20.32" y="5.08" length="short" rot="R180"/>
<pin name="IO1_152" x="20.32" y="2.54" length="short" rot="R180"/>
<pin name="IO1_160" x="20.32" y="0" length="short" rot="R180"/>
<pin name="IO1_161" x="20.32" y="-2.54" length="short" rot="R180"/>
<pin name="IO1_164" x="20.32" y="-5.08" length="short" rot="R180"/>
<pin name="IO1_165" x="20.32" y="-7.62" length="short" rot="R180"/>
<pin name="IO1_166" x="20.32" y="-10.16" length="short" rot="R180"/>
<pin name="IO1_167" x="20.32" y="-12.7" length="short" rot="R180"/>
<pin name="IO0_168" x="20.32" y="-22.86" length="short" rot="R180"/>
<pin name="IO0_169" x="20.32" y="-25.4" length="short" rot="R180"/>
<pin name="IO0_170" x="20.32" y="-27.94" length="short" rot="R180"/>
<pin name="IO0_171" x="20.32" y="-30.48" length="short" rot="R180"/>
<pin name="IO0_172" x="20.32" y="-33.02" length="short" rot="R180"/>
<pin name="IO0_173" x="20.32" y="-35.56" length="short" rot="R180"/>
<pin name="IO0_174" x="20.32" y="-38.1" length="short" rot="R180"/>
<pin name="IO0_177" x="20.32" y="-40.64" length="short" rot="R180"/>
<pin name="IO0_178" x="20.32" y="-43.18" length="short" rot="R180"/>
<pin name="IO0_179" x="20.32" y="-45.72" length="short" rot="R180"/>
<pin name="IO0_181" x="20.32" y="-48.26" length="short" rot="R180"/>
<pin name="IO0_190" x="20.32" y="-50.8" length="short" rot="R180"/>
<pin name="IO0_191" x="20.32" y="-53.34" length="short" rot="R180"/>
<pin name="IO0_192" x="20.32" y="-55.88" length="short" rot="R180"/>
<pin name="IO0_197_GBIN1" x="-20.32" y="35.56" length="short" direction="in"/>
<pin name="IO0_198_GBIN0" x="-20.32" y="33.02" length="short" direction="in"/>
<pin name="IO0_206" x="20.32" y="-58.42" length="short" rot="R180"/>
<pin name="IO0_212" x="20.32" y="-60.96" length="short" rot="R180"/>
<pin name="IO0_213" x="20.32" y="-63.5" length="short" rot="R180"/>
<pin name="IO0_214" x="20.32" y="-66.04" length="short" rot="R180"/>
<pin name="IO0_215" x="20.32" y="-68.58" length="short" rot="R180"/>
<pin name="IO0_216" x="20.32" y="-71.12" length="short" rot="R180"/>
<pin name="IO0_217" x="20.32" y="-73.66" length="short" rot="R180"/>
<pin name="IO0_219" x="20.32" y="-76.2" length="short" rot="R180"/>
<pin name="IO0_220" x="20.32" y="-78.74" length="short" rot="R180"/>
<pin name="IO0_221" x="20.32" y="-81.28" length="short" rot="R180"/>
<pin name="IO0_222" x="20.32" y="-83.82" length="short" rot="R180"/>
<pin name="VCC" x="20.32" y="83.82" length="short" direction="pwr" rot="R180"/>
<pin name="VCC_SPI" x="20.32" y="81.28" length="short" direction="pwr" rot="R180"/>
<pin name="VCCIO_0" x="20.32" y="78.74" length="short" direction="pwr" rot="R180"/>
<pin name="VCCIO_1" x="20.32" y="76.2" length="short" direction="pwr" rot="R180"/>
<pin name="VCCIO_2" x="20.32" y="73.66" length="short" direction="pwr" rot="R180"/>
<pin name="VCCIO_3" x="20.32" y="71.12" length="short" direction="pwr" rot="R180"/>
<pin name="VCCPLL0" x="20.32" y="68.58" length="short" direction="pwr" rot="R180"/>
<pin name="VCCPLL1" x="20.32" y="66.04" length="short" direction="pwr" rot="R180"/>
<pin name="VPP_2V5" x="-20.32" y="-78.74" length="short" direction="pwr"/>
<pin name="VPP_FAST" x="-20.32" y="-81.28" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ICE40HX4K-TQ144" prefix="U">
<description>HX Series 3520 LUTs 95 I/O 80 kBit RAM 1.2 V Surface Mount FPGA - TQFP-144 &lt;a href="https://pricing.snapeda.com/parts/ICE40HX4K-TQ144/Lattice%20Semiconductor/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ICE40HX4K-TQ144" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P2200X2200X160-144N">
<connects>
<connect gate="G$1" pin="CDONE" pad="65"/>
<connect gate="G$1" pin="CRESET_B" pad="66"/>
<connect gate="G$1" pin="GND" pad="5 13 14 59 69 86 103 132 140"/>
<connect gate="G$1" pin="GNDPLL0" pad="53"/>
<connect gate="G$1" pin="GNDPLL1" pad="127"/>
<connect gate="G$1" pin="IO0_168" pad="110"/>
<connect gate="G$1" pin="IO0_169" pad="112"/>
<connect gate="G$1" pin="IO0_170" pad="113"/>
<connect gate="G$1" pin="IO0_171" pad="114"/>
<connect gate="G$1" pin="IO0_172" pad="115"/>
<connect gate="G$1" pin="IO0_173" pad="116"/>
<connect gate="G$1" pin="IO0_174" pad="117"/>
<connect gate="G$1" pin="IO0_177" pad="118"/>
<connect gate="G$1" pin="IO0_178" pad="119"/>
<connect gate="G$1" pin="IO0_179" pad="120"/>
<connect gate="G$1" pin="IO0_181" pad="121"/>
<connect gate="G$1" pin="IO0_190" pad="122"/>
<connect gate="G$1" pin="IO0_191" pad="124"/>
<connect gate="G$1" pin="IO0_192" pad="125"/>
<connect gate="G$1" pin="IO0_197_GBIN1" pad="128"/>
<connect gate="G$1" pin="IO0_198_GBIN0" pad="129"/>
<connect gate="G$1" pin="IO0_206" pad="130"/>
<connect gate="G$1" pin="IO0_212" pad="134"/>
<connect gate="G$1" pin="IO0_213" pad="135"/>
<connect gate="G$1" pin="IO0_214" pad="136"/>
<connect gate="G$1" pin="IO0_215" pad="137"/>
<connect gate="G$1" pin="IO0_216" pad="138"/>
<connect gate="G$1" pin="IO0_217" pad="139"/>
<connect gate="G$1" pin="IO0_219" pad="141"/>
<connect gate="G$1" pin="IO0_220" pad="142"/>
<connect gate="G$1" pin="IO0_221" pad="143"/>
<connect gate="G$1" pin="IO0_222" pad="144"/>
<connect gate="G$1" pin="IO1_109" pad="73"/>
<connect gate="G$1" pin="IO1_110" pad="74"/>
<connect gate="G$1" pin="IO1_111" pad="75"/>
<connect gate="G$1" pin="IO1_112" pad="76"/>
<connect gate="G$1" pin="IO1_114" pad="78"/>
<connect gate="G$1" pin="IO1_115" pad="79"/>
<connect gate="G$1" pin="IO1_116" pad="80"/>
<connect gate="G$1" pin="IO1_117" pad="81"/>
<connect gate="G$1" pin="IO1_118" pad="82"/>
<connect gate="G$1" pin="IO1_119" pad="83"/>
<connect gate="G$1" pin="IO1_120" pad="84"/>
<connect gate="G$1" pin="IO1_128" pad="85"/>
<connect gate="G$1" pin="IO1_136" pad="87"/>
<connect gate="G$1" pin="IO1_137" pad="88"/>
<connect gate="G$1" pin="IO1_138" pad="90"/>
<connect gate="G$1" pin="IO1_139" pad="91"/>
<connect gate="G$1" pin="IO1_140_GBIN3" pad="93"/>
<connect gate="G$1" pin="IO1_141_GBIN2" pad="94"/>
<connect gate="G$1" pin="IO1_144" pad="95"/>
<connect gate="G$1" pin="IO1_146" pad="96"/>
<connect gate="G$1" pin="IO1_147" pad="97"/>
<connect gate="G$1" pin="IO1_148" pad="98"/>
<connect gate="G$1" pin="IO1_152" pad="99"/>
<connect gate="G$1" pin="IO1_160" pad="101"/>
<connect gate="G$1" pin="IO1_161" pad="102"/>
<connect gate="G$1" pin="IO1_164" pad="104"/>
<connect gate="G$1" pin="IO1_165" pad="105"/>
<connect gate="G$1" pin="IO1_166" pad="106"/>
<connect gate="G$1" pin="IO1_167" pad="107"/>
<connect gate="G$1" pin="IO2_102" pad="62"/>
<connect gate="G$1" pin="IO2_103_CBSEL0" pad="63"/>
<connect gate="G$1" pin="IO2_104_CBSEL1" pad="64"/>
<connect gate="G$1" pin="IO2_56" pad="37"/>
<connect gate="G$1" pin="IO2_57" pad="38"/>
<connect gate="G$1" pin="IO2_61" pad="39"/>
<connect gate="G$1" pin="IO2_63" pad="41"/>
<connect gate="G$1" pin="IO2_64" pad="42"/>
<connect gate="G$1" pin="IO2_71" pad="43"/>
<connect gate="G$1" pin="IO2_72" pad="44"/>
<connect gate="G$1" pin="IO2_73" pad="45"/>
<connect gate="G$1" pin="IO2_79" pad="47"/>
<connect gate="G$1" pin="IO2_80" pad="48"/>
<connect gate="G$1" pin="IO2_81_GBIN5" pad="49"/>
<connect gate="G$1" pin="IO2_82_GBIN4" pad="52"/>
<connect gate="G$1" pin="IO2_91" pad="55"/>
<connect gate="G$1" pin="IO2_94" pad="56"/>
<connect gate="G$1" pin="IO2_95" pad="60"/>
<connect gate="G$1" pin="IO2_96" pad="61"/>
<connect gate="G$1" pin="IO3_10A" pad="15"/>
<connect gate="G$1" pin="IO3_10B" pad="16"/>
<connect gate="G$1" pin="IO3_12A" pad="17"/>
<connect gate="G$1" pin="IO3_12B" pad="18"/>
<connect gate="G$1" pin="IO3_13A" pad="19"/>
<connect gate="G$1" pin="IO3_13B_GBIN7" pad="20"/>
<connect gate="G$1" pin="IO3_14A_GBIN6" pad="21"/>
<connect gate="G$1" pin="IO3_14B" pad="22"/>
<connect gate="G$1" pin="IO3_17A" pad="23"/>
<connect gate="G$1" pin="IO3_17B" pad="24"/>
<connect gate="G$1" pin="IO3_18A" pad="25"/>
<connect gate="G$1" pin="IO3_18B" pad="26"/>
<connect gate="G$1" pin="IO3_23A" pad="28"/>
<connect gate="G$1" pin="IO3_23B" pad="29"/>
<connect gate="G$1" pin="IO3_24A" pad="31"/>
<connect gate="G$1" pin="IO3_24B" pad="32"/>
<connect gate="G$1" pin="IO3_25A" pad="33"/>
<connect gate="G$1" pin="IO3_25B" pad="34"/>
<connect gate="G$1" pin="IO3_2A" pad="1"/>
<connect gate="G$1" pin="IO3_2B" pad="2"/>
<connect gate="G$1" pin="IO3_3A" pad="3"/>
<connect gate="G$1" pin="IO3_3B" pad="4"/>
<connect gate="G$1" pin="IO3_4A" pad="7"/>
<connect gate="G$1" pin="IO3_4B" pad="8"/>
<connect gate="G$1" pin="IO3_5A" pad="9"/>
<connect gate="G$1" pin="IO3_5B" pad="10"/>
<connect gate="G$1" pin="IO3_8A" pad="11"/>
<connect gate="G$1" pin="IO3_8B" pad="12"/>
<connect gate="G$1" pin="IOB_105_SDO" pad="67"/>
<connect gate="G$1" pin="IOB_106_SDI" pad="68"/>
<connect gate="G$1" pin="IOB_107_SCK" pad="70"/>
<connect gate="G$1" pin="IOB_108_SS" pad="71"/>
<connect gate="G$1" pin="VCC" pad="27 40 92 111"/>
<connect gate="G$1" pin="VCCIO_0" pad="123 131"/>
<connect gate="G$1" pin="VCCIO_1" pad="89 100"/>
<connect gate="G$1" pin="VCCIO_2" pad="46 57"/>
<connect gate="G$1" pin="VCCIO_3" pad="6 30"/>
<connect gate="G$1" pin="VCCPLL0" pad="54"/>
<connect gate="G$1" pin="VCCPLL1" pad="126"/>
<connect gate="G$1" pin="VCC_SPI" pad="72"/>
<connect gate="G$1" pin="VPP_2V5" pad="108"/>
<connect gate="G$1" pin="VPP_FAST" pad="109"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value=" iCE40™ HX Field Programmable Gate Array (FPGA) IC 107 81920 3520 144-LQFP "/>
<attribute name="MF" value="Lattice Semiconductor"/>
<attribute name="MP" value="ICE40HX4K-TQ144"/>
<attribute name="PACKAGE" value="TQFP-144 Lattice Semiconductor"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search?q=ICE40HX4K-TQ144&amp;ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="zx_spectrum2">
<packages>
<package name="ZX_SMD">
<text x="-1.5" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.5" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="36" y1="2.2" x2="36" y2="-2.2" width="0.2" layer="51"/>
<wire x1="36" y1="-2.2" x2="-36" y2="-2.2" width="0.2" layer="51"/>
<wire x1="-36" y1="-2.2" x2="-36" y2="2.2" width="0.2" layer="51"/>
<wire x1="-36" y1="2.2" x2="36" y2="2.2" width="0.2" layer="51"/>
<wire x1="-36" y1="-2.2" x2="-36" y2="-13" width="0.2" layer="51"/>
<wire x1="-36" y1="-13" x2="36" y2="-13" width="0.2" layer="51"/>
<wire x1="36" y1="-13" x2="36" y2="-2.2" width="0.2" layer="51"/>
<smd name="B1" x="-34.29" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B2" x="-31.75" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B3" x="-29.21" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B4" x="-26.67" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B28" x="34.29" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B6" x="-21.59" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B7" x="-19.05" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B8" x="-16.51" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B9" x="-13.97" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B10" x="-11.43" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B11" x="-8.89" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B12" x="-6.35" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B13" x="-3.81" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B14" x="-1.27" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B15" x="1.27" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B16" x="3.81" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B17" x="6.35" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B18" x="8.89" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B19" x="11.43" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B20" x="13.97" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B21" x="16.51" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B22" x="19.05" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B23" x="21.59" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B24" x="24.13" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B25" x="26.67" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B26" x="29.21" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="B27" x="31.75" y="0" dx="3.6" dy="1.7" layer="16" rot="R90" cream="no"/>
<smd name="A1" x="-34.29" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A2" x="-31.75" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A3" x="-29.21" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A4" x="-26.67" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A6" x="-21.59" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A7" x="-19.05" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A8" x="-16.51" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A9" x="-13.97" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A10" x="-11.43" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A11" x="-8.89" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A12" x="-6.35" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A13" x="-3.81" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A14" x="-1.27" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A15" x="1.27" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A16" x="3.81" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A17" x="6.35" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A18" x="8.89" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A19" x="11.43" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A20" x="13.97" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A21" x="16.51" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A22" x="19.05" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A23" x="21.59" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A24" x="24.13" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A25" x="26.67" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A26" x="29.21" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A27" x="31.75" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<smd name="A28" x="34.29" y="0" dx="3.6" dy="1.7" layer="1" rot="R90" cream="no"/>
<wire x1="-36" y1="-1.8" x2="-36" y2="2.2" width="0.2" layer="21"/>
<wire x1="-36" y1="2.2" x2="36" y2="2.2" width="0.2" layer="21"/>
<wire x1="36" y1="2.2" x2="36" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-36" y1="-1.8" x2="-36" y2="2.2" width="0.2" layer="22"/>
<wire x1="-36" y1="2.2" x2="36" y2="2.2" width="0.2" layer="22"/>
<wire x1="36" y1="2.2" x2="36" y2="-1.8" width="0.2" layer="22"/>
<wire x1="-36" y1="2.2" x2="36" y2="2.2" width="0.2" layer="52"/>
<wire x1="36" y1="2.2" x2="36" y2="-2.2" width="0.2" layer="52"/>
<wire x1="36" y1="-2.2" x2="36" y2="-13" width="0.2" layer="52"/>
<wire x1="36" y1="-13" x2="-36" y2="-13" width="0.2" layer="52"/>
<wire x1="-36" y1="-13" x2="-36" y2="-2.2" width="0.2" layer="52"/>
<wire x1="-36" y1="-2.2" x2="-36" y2="2.2" width="0.2" layer="52"/>
<wire x1="-36" y1="-2.2" x2="36" y2="-2.2" width="0.2" layer="52"/>
<text x="1.5" y="-0.4" size="0.8128" layer="52" font="vector" rot="MR0">&gt;Name</text>
<text x="1.5" y="2.7" size="0.8128" layer="26" font="vector" rot="MR0">&gt;Name</text>
<polygon width="0.2" layer="41">
<vertex x="-35.4" y="1.8"/>
<vertex x="-25.6" y="1.8"/>
<vertex x="-25.6" y="-1.8"/>
<vertex x="-35.4" y="-1.8"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-22.7" y="1.8"/>
<vertex x="35.4" y="1.8"/>
<vertex x="35.4" y="-1.8"/>
<vertex x="-22.7" y="-1.8"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-35.4" y="-1.8"/>
<vertex x="-25.6" y="-1.8"/>
<vertex x="-25.6" y="1.8"/>
<vertex x="-35.4" y="1.8"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-2.1" y="1.8"/>
<vertex x="-2.1" y="-1.8"/>
<vertex x="-18.2" y="-1.8"/>
<vertex x="-18.2" y="1.8"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-0.4" y="1.8"/>
<vertex x="-0.4" y="-1.8"/>
<vertex x="35.4" y="-1.8"/>
<vertex x="35.4" y="1.8"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-22.7" y="-1.8"/>
<vertex x="-22.5" y="-1.8"/>
<vertex x="-22.5" y="1.8"/>
<vertex x="-22.7" y="1.8"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-20.7" y="-1.8"/>
<vertex x="-19.9" y="-1.8"/>
<vertex x="-19.9" y="1.8"/>
<vertex x="-20.7" y="1.8"/>
</polygon>
<polygon width="0.2" layer="42">
<vertex x="-35.4" y="-1.7"/>
<vertex x="35.4" y="-1.7"/>
<vertex x="35.4" y="-2"/>
<vertex x="-35.4" y="-2"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-35.4" y="-1.7"/>
<vertex x="35.4" y="-1.7"/>
<vertex x="35.4" y="-2"/>
<vertex x="-35.4" y="-2"/>
</polygon>
</package>
<package name="ZX">
<pad name="B1" x="3.81" y="-34.29" drill="1.2" shape="long"/>
<pad name="B2" x="3.81" y="-31.75" drill="1.2" shape="long"/>
<pad name="B3" x="3.81" y="-29.21" drill="1.2" shape="long"/>
<pad name="B4" x="3.81" y="-26.67" drill="1.2" shape="long"/>
<pad name="B6" x="3.81" y="-21.59" drill="1.2" shape="long"/>
<pad name="B7" x="3.81" y="-19.05" drill="1.2" shape="long"/>
<pad name="B8" x="3.81" y="-16.51" drill="1.2" shape="long"/>
<pad name="B9" x="3.81" y="-13.97" drill="1.2" shape="long"/>
<pad name="B10" x="3.81" y="-11.43" drill="1.2" shape="long"/>
<pad name="B11" x="3.81" y="-8.89" drill="1.2" shape="long"/>
<pad name="B12" x="3.81" y="-6.35" drill="1.2" shape="long"/>
<pad name="B13" x="3.81" y="-3.81" drill="1.2" shape="long"/>
<pad name="B14" x="3.81" y="-1.27" drill="1.2" shape="long"/>
<pad name="B15" x="3.81" y="1.27" drill="1.2" shape="long"/>
<pad name="B16" x="3.81" y="3.81" drill="1.2" shape="long"/>
<pad name="B17" x="3.81" y="6.35" drill="1.2" shape="long"/>
<pad name="B18" x="3.81" y="8.89" drill="1.2" shape="long"/>
<pad name="B19" x="3.81" y="11.43" drill="1.2" shape="long"/>
<pad name="B20" x="3.81" y="13.97" drill="1.2" shape="long"/>
<pad name="B21" x="3.81" y="16.51" drill="1.2" shape="long"/>
<pad name="B22" x="3.81" y="19.05" drill="1.2" shape="long"/>
<pad name="B23" x="3.81" y="21.59" drill="1.2" shape="long"/>
<pad name="B24" x="3.81" y="24.13" drill="1.2" shape="long"/>
<pad name="B25" x="3.81" y="26.67" drill="1.2" shape="long"/>
<pad name="B26" x="3.81" y="29.21" drill="1.2" shape="long"/>
<pad name="B27" x="3.81" y="31.75" drill="1.2" shape="long"/>
<pad name="B28" x="3.81" y="34.29" drill="1.2" shape="long"/>
<pad name="A1" x="-1.27" y="-34.29" drill="0.6" shape="long"/>
<pad name="A2" x="-1.27" y="-31.75" drill="0.6" shape="long"/>
<pad name="A3" x="-1.27" y="-29.21" drill="0.6" shape="long"/>
<pad name="A4" x="-1.27" y="-26.67" drill="0.6" shape="long"/>
<pad name="A6" x="-1.27" y="-21.59" drill="0.6" shape="long"/>
<pad name="A7" x="-1.27" y="-19.05" drill="0.6" shape="long"/>
<pad name="A8" x="-1.27" y="-16.51" drill="0.6" shape="long"/>
<pad name="A9" x="-1.27" y="-13.97" drill="0.6" shape="long"/>
<pad name="A10" x="-1.27" y="-11.43" drill="0.6" shape="long"/>
<pad name="A11" x="-1.27" y="-8.89" drill="0.6" shape="long"/>
<pad name="A12" x="-1.27" y="-6.35" drill="0.6" shape="long"/>
<pad name="A13" x="-1.27" y="-3.81" drill="0.6" shape="long"/>
<pad name="A14" x="-1.27" y="-1.27" drill="0.6" shape="long"/>
<pad name="A15" x="-1.27" y="1.27" drill="0.6" shape="long"/>
<pad name="A16" x="-1.27" y="3.81" drill="0.6" shape="long"/>
<pad name="A17" x="-1.27" y="6.35" drill="0.6" shape="long"/>
<pad name="A18" x="-1.27" y="8.89" drill="0.6" shape="long"/>
<pad name="A19" x="-1.27" y="11.43" drill="0.6" shape="long"/>
<pad name="A20" x="-1.27" y="13.97" drill="0.6" shape="long"/>
<pad name="A21" x="-1.27" y="16.51" drill="0.6" shape="long"/>
<pad name="A22" x="-1.27" y="19.05" drill="0.6" shape="long"/>
<pad name="A23" x="-1.27" y="21.59" drill="0.6" shape="long"/>
<pad name="A24" x="-1.27" y="24.13" drill="0.6" shape="long"/>
<pad name="A25" x="-1.27" y="26.67" drill="0.6" shape="long"/>
<pad name="A26" x="-1.27" y="29.21" drill="0.6" shape="long"/>
<pad name="A27" x="-1.27" y="31.75" drill="0.6" shape="long"/>
<pad name="A28" x="-1.27" y="34.29" drill="0.6" shape="long"/>
<text x="-3.5" y="-2.87" size="0.8128" layer="21" font="vector" rot="R90">&gt;Name</text>
<text x="0.4" y="-2.77" size="0.8128" layer="51" font="vector" rot="R90">&gt;Name</text>
<wire x1="-3" y1="36" x2="6" y2="36" width="0.2" layer="51"/>
<wire x1="6" y1="36" x2="6" y2="-36" width="0.2" layer="51"/>
<wire x1="6" y1="-36" x2="-3" y2="-36" width="0.2" layer="51"/>
<wire x1="-3" y1="-36" x2="-3" y2="36" width="0.2" layer="51"/>
<wire x1="6" y1="-36" x2="16.8" y2="-36" width="0.2" layer="51"/>
<wire x1="16.8" y1="-36" x2="16.8" y2="36" width="0.2" layer="51"/>
<wire x1="16.8" y1="36" x2="6" y2="36" width="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ZX">
<pin name="A14" x="12.7" y="-33.02" visible="pin" length="short" rot="R180"/>
<pin name="A12" x="12.7" y="-30.48" visible="pin" length="short" rot="R180"/>
<pin name="+5V" x="12.7" y="-27.94" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="+9V" x="12.7" y="-25.4" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="GND@1" x="12.7" y="-20.32" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="GND@2" x="12.7" y="-17.78" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="/CLK" x="12.7" y="-15.24" visible="pin" length="short" rot="R180"/>
<pin name="A0" x="12.7" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="A1" x="12.7" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="A2" x="12.7" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="A3" x="12.7" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="IORQGE" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND@3" x="12.7" y="0" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VIDEO" x="12.7" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="Y" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="V" x="12.7" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="U" x="12.7" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="/BUSRQ" x="12.7" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="/RST" x="12.7" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="A7" x="12.7" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="A6" x="12.7" y="20.32" visible="pin" length="short" rot="R180"/>
<pin name="A5" x="12.7" y="22.86" visible="pin" length="short" rot="R180"/>
<pin name="A4" x="12.7" y="25.4" visible="pin" length="short" rot="R180"/>
<pin name="/ROMCS" x="12.7" y="27.94" visible="pin" length="short" rot="R180"/>
<pin name="/BUSACK" x="12.7" y="30.48" visible="pin" length="short" rot="R180"/>
<pin name="A9" x="12.7" y="33.02" visible="pin" length="short" rot="R180"/>
<pin name="A11" x="12.7" y="35.56" visible="pin" length="short" rot="R180"/>
<pin name="A15" x="-12.7" y="-33.02" visible="pin" length="short"/>
<pin name="A13" x="-12.7" y="-30.48" visible="pin" length="short"/>
<pin name="D7" x="-12.7" y="-27.94" visible="pin" length="short"/>
<pin name="D0" x="-12.7" y="-20.32" visible="pin" length="short"/>
<pin name="D1" x="-12.7" y="-17.78" visible="pin" length="short"/>
<pin name="D2" x="-12.7" y="-15.24" visible="pin" length="short"/>
<pin name="D6" x="-12.7" y="-12.7" visible="pin" length="short"/>
<pin name="D5" x="-12.7" y="-10.16" visible="pin" length="short"/>
<pin name="D3" x="-12.7" y="-7.62" visible="pin" length="short"/>
<pin name="D4" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="/INT" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="/NMI" x="-12.7" y="0" visible="pin" length="short"/>
<pin name="/HALT" x="-12.7" y="2.54" visible="pin" length="short"/>
<pin name="/MREQ" x="-12.7" y="5.08" visible="pin" length="short"/>
<pin name="/IORQ" x="-12.7" y="7.62" visible="pin" length="short"/>
<pin name="/RD" x="-12.7" y="10.16" visible="pin" length="short"/>
<pin name="/WR" x="-12.7" y="12.7" visible="pin" length="short"/>
<pin name="-5V" x="-12.7" y="15.24" visible="pin" length="short" direction="pwr"/>
<pin name="/WAIT" x="-12.7" y="17.78" visible="pin" length="short"/>
<pin name="+12V" x="-12.7" y="20.32" visible="pin" length="short" direction="pwr"/>
<pin name="12VAC" x="-12.7" y="22.86" visible="pin" length="short" direction="pwr"/>
<pin name="M1" x="-12.7" y="25.4" visible="pin" length="short"/>
<pin name="/RFSH" x="-12.7" y="27.94" visible="pin" length="short"/>
<pin name="A8" x="-12.7" y="30.48" visible="pin" length="short"/>
<pin name="A10" x="-12.7" y="33.02" visible="pin" length="short"/>
<wire x1="-10.16" y1="38.1" x2="10.16" y2="38.1" width="0.254" layer="94"/>
<wire x1="10.16" y1="38.1" x2="10.16" y2="-35.56" width="0.254" layer="94"/>
<wire x1="10.16" y1="-35.56" x2="-10.16" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-35.56" x2="-10.16" y2="38.1" width="0.254" layer="94"/>
<text x="-5.08" y="40.64" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-38.1" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
<pin name="/OE" x="-12.7" y="-25.4" visible="pin" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ZX">
<description>ZX Spectrum Expansion Bus</description>
<gates>
<gate name="G$1" symbol="ZX" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="-THT" package="ZX">
<connects>
<connect gate="G$1" pin="+12V" pad="A22"/>
<connect gate="G$1" pin="+5V" pad="B3"/>
<connect gate="G$1" pin="+9V" pad="B4"/>
<connect gate="G$1" pin="-5V" pad="A20"/>
<connect gate="G$1" pin="/BUSACK" pad="B26"/>
<connect gate="G$1" pin="/BUSRQ" pad="B19"/>
<connect gate="G$1" pin="/CLK" pad="B8"/>
<connect gate="G$1" pin="/HALT" pad="A15"/>
<connect gate="G$1" pin="/INT" pad="A13"/>
<connect gate="G$1" pin="/IORQ" pad="A17"/>
<connect gate="G$1" pin="/MREQ" pad="A16"/>
<connect gate="G$1" pin="/NMI" pad="A14"/>
<connect gate="G$1" pin="/OE" pad="A4"/>
<connect gate="G$1" pin="/RD" pad="A18"/>
<connect gate="G$1" pin="/RFSH" pad="A25"/>
<connect gate="G$1" pin="/ROMCS" pad="B25"/>
<connect gate="G$1" pin="/RST" pad="B20"/>
<connect gate="G$1" pin="/WAIT" pad="A21"/>
<connect gate="G$1" pin="/WR" pad="A19"/>
<connect gate="G$1" pin="12VAC" pad="A23"/>
<connect gate="G$1" pin="A0" pad="B9"/>
<connect gate="G$1" pin="A1" pad="B10"/>
<connect gate="G$1" pin="A10" pad="A27"/>
<connect gate="G$1" pin="A11" pad="B28"/>
<connect gate="G$1" pin="A12" pad="B2"/>
<connect gate="G$1" pin="A13" pad="A2"/>
<connect gate="G$1" pin="A14" pad="B1"/>
<connect gate="G$1" pin="A15" pad="A1"/>
<connect gate="G$1" pin="A2" pad="B11"/>
<connect gate="G$1" pin="A3" pad="B12"/>
<connect gate="G$1" pin="A4" pad="B24"/>
<connect gate="G$1" pin="A5" pad="B23"/>
<connect gate="G$1" pin="A6" pad="B22"/>
<connect gate="G$1" pin="A7" pad="B21"/>
<connect gate="G$1" pin="A8" pad="A26"/>
<connect gate="G$1" pin="A9" pad="B27"/>
<connect gate="G$1" pin="D0" pad="A6"/>
<connect gate="G$1" pin="D1" pad="A7"/>
<connect gate="G$1" pin="D2" pad="A8"/>
<connect gate="G$1" pin="D3" pad="A11"/>
<connect gate="G$1" pin="D4" pad="A12"/>
<connect gate="G$1" pin="D5" pad="A10"/>
<connect gate="G$1" pin="D6" pad="A9"/>
<connect gate="G$1" pin="D7" pad="A3"/>
<connect gate="G$1" pin="GND@1" pad="B6"/>
<connect gate="G$1" pin="GND@2" pad="B7"/>
<connect gate="G$1" pin="GND@3" pad="B14"/>
<connect gate="G$1" pin="IORQGE" pad="B13"/>
<connect gate="G$1" pin="M1" pad="A24"/>
<connect gate="G$1" pin="U" pad="B18"/>
<connect gate="G$1" pin="V" pad="B17"/>
<connect gate="G$1" pin="VIDEO" pad="B15"/>
<connect gate="G$1" pin="Y" pad="B16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD" package="ZX_SMD">
<connects>
<connect gate="G$1" pin="+12V" pad="A22"/>
<connect gate="G$1" pin="+5V" pad="B3"/>
<connect gate="G$1" pin="+9V" pad="B4"/>
<connect gate="G$1" pin="-5V" pad="A20"/>
<connect gate="G$1" pin="/BUSACK" pad="B26"/>
<connect gate="G$1" pin="/BUSRQ" pad="B19"/>
<connect gate="G$1" pin="/CLK" pad="B8"/>
<connect gate="G$1" pin="/HALT" pad="A15"/>
<connect gate="G$1" pin="/INT" pad="A13"/>
<connect gate="G$1" pin="/IORQ" pad="A17"/>
<connect gate="G$1" pin="/MREQ" pad="A16"/>
<connect gate="G$1" pin="/NMI" pad="A14"/>
<connect gate="G$1" pin="/OE" pad="A4"/>
<connect gate="G$1" pin="/RD" pad="A18"/>
<connect gate="G$1" pin="/RFSH" pad="A25"/>
<connect gate="G$1" pin="/ROMCS" pad="B25"/>
<connect gate="G$1" pin="/RST" pad="B20"/>
<connect gate="G$1" pin="/WAIT" pad="A21"/>
<connect gate="G$1" pin="/WR" pad="A19"/>
<connect gate="G$1" pin="12VAC" pad="A23"/>
<connect gate="G$1" pin="A0" pad="B9"/>
<connect gate="G$1" pin="A1" pad="B10"/>
<connect gate="G$1" pin="A10" pad="A27"/>
<connect gate="G$1" pin="A11" pad="B28"/>
<connect gate="G$1" pin="A12" pad="B2"/>
<connect gate="G$1" pin="A13" pad="A2"/>
<connect gate="G$1" pin="A14" pad="B1"/>
<connect gate="G$1" pin="A15" pad="A1"/>
<connect gate="G$1" pin="A2" pad="B11"/>
<connect gate="G$1" pin="A3" pad="B12"/>
<connect gate="G$1" pin="A4" pad="B24"/>
<connect gate="G$1" pin="A5" pad="B23"/>
<connect gate="G$1" pin="A6" pad="B22"/>
<connect gate="G$1" pin="A7" pad="B21"/>
<connect gate="G$1" pin="A8" pad="A26"/>
<connect gate="G$1" pin="A9" pad="B27"/>
<connect gate="G$1" pin="D0" pad="A6"/>
<connect gate="G$1" pin="D1" pad="A7"/>
<connect gate="G$1" pin="D2" pad="A8"/>
<connect gate="G$1" pin="D3" pad="A11"/>
<connect gate="G$1" pin="D4" pad="A12"/>
<connect gate="G$1" pin="D5" pad="A10"/>
<connect gate="G$1" pin="D6" pad="A9"/>
<connect gate="G$1" pin="D7" pad="A3"/>
<connect gate="G$1" pin="GND@1" pad="B6"/>
<connect gate="G$1" pin="GND@2" pad="B7"/>
<connect gate="G$1" pin="GND@3" pad="B14"/>
<connect gate="G$1" pin="IORQGE" pad="B13"/>
<connect gate="G$1" pin="M1" pad="A24"/>
<connect gate="G$1" pin="U" pad="B18"/>
<connect gate="G$1" pin="V" pad="B17"/>
<connect gate="G$1" pin="VIDEO" pad="B15"/>
<connect gate="G$1" pin="Y" pad="B16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ptn3366">
<description>Generated from &lt;b&gt;ptn3366.sch&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="SOT617-3">
<description>&lt;li&gt;&lt;b&gt;SOT617-3&lt;/b&gt;&lt;hr&gt;
&lt;ul&gt;&lt;li&gt;HVQFN32: plastic thermal enhanced very thin quad flat package; no leads; 32 terminals; body 5 x 5 x 0.85 mm
&lt;li&gt;&lt;u&gt;JEDEC&lt;/u&gt;: MO-220
&lt;li&gt;&lt;u&gt;IEC&lt;/u&gt;: --
&lt;li&gt;&lt;u&gt;JEITA&lt;/u&gt;: -- &lt;/ul&gt;</description>
<circle x="0" y="0" radius="0.3" width="0" layer="31"/>
<circle x="-1.125" y="1.125" radius="0.3" width="0" layer="31"/>
<circle x="0" y="1.125" radius="0.3" width="0" layer="31"/>
<circle x="1.125" y="1.125" radius="0.3" width="0" layer="31"/>
<circle x="1.125" y="0" radius="0.3" width="0" layer="31"/>
<circle x="1.125" y="-1.125" radius="0.3" width="0" layer="31"/>
<circle x="0" y="-1.125" radius="0.3" width="0" layer="31"/>
<circle x="-1.125" y="-1.125" radius="0.3" width="0" layer="31"/>
<circle x="-1.125" y="0" radius="0.3" width="0" layer="31"/>
<smd name="1" x="-2.55" y="1.75" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="2" x="-2.55" y="1.25" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="3" x="-2.55" y="0.75" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="4" x="-2.55" y="0.25" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="5" x="-2.55" y="-0.25" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="6" x="-2.55" y="-0.75" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="7" x="-2.55" y="-1.25" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="8" x="-2.55" y="-1.75" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="9" x="-1.75" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="10" x="-1.25" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="11" x="-0.75" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="12" x="-0.25" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="13" x="0.25" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="14" x="0.75" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="15" x="1.25" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="16" x="1.75" y="-2.55" dx="0.27" dy="0.85" layer="1" rot="R180"/>
<smd name="17" x="2.55" y="-1.75" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="18" x="2.55" y="-1.25" dx="0.27" dy="0.85" layer="1" rot="R90"/>
<smd name="19" x="2.55" y="-0.75" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="20" x="2.55" y="-0.25" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="21" x="2.55" y="0.25" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="22" x="2.55" y="0.75" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="23" x="2.55" y="1.25" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="24" x="2.55" y="1.75" dx="0.27" dy="0.85" layer="1" rot="R270"/>
<smd name="25" x="1.75" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="26" x="1.25" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="27" x="0.75" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="28" x="0.25" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="29" x="-0.25" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="30" x="-0.75" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="31" x="-1.25" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="32" x="-1.75" y="2.55" dx="0.27" dy="0.85" layer="1"/>
<smd name="33" x="0" y="0" dx="3.75" dy="3.75" layer="1" cream="no"/>
<text x="-1.6" y="3.4" size="0.8128" layer="25" font="vector" rot="SR0">&gt;Name</text>
<wire x1="-2.7" y1="2.7" x2="2.7" y2="2.7" width="0.2" layer="51"/>
<wire x1="2.7" y1="2.7" x2="2.7" y2="-2.7" width="0.2" layer="51"/>
<wire x1="2.7" y1="-2.7" x2="-2.7" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-2.7" y1="-2.7" x2="-2.7" y2="2.7" width="0.2" layer="51"/>
<wire x1="-2.2" y1="2.7" x2="-2.7" y2="2.7" width="0.2" layer="21"/>
<wire x1="-2.7" y1="2.7" x2="-2.7" y2="2.2" width="0.2" layer="21"/>
<wire x1="-2.7" y1="2.2" x2="-2.2" y2="2.7" width="0.2" layer="21"/>
<wire x1="2.2" y1="2.7" x2="2.7" y2="2.7" width="0.2" layer="21"/>
<wire x1="2.7" y1="2.7" x2="2.7" y2="2.2" width="0.2" layer="21"/>
<wire x1="2.7" y1="-2.2" x2="2.7" y2="-2.7" width="0.2" layer="21"/>
<wire x1="2.7" y1="-2.7" x2="2.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-2.7" x2="-2.7" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-2.7" y1="-2.7" x2="-2.7" y2="-2.2" width="0.2" layer="21"/>
<text x="-2.8" y="2.8" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<text x="-1.6" y="-0.4" size="0.8128" layer="51" font="vector" rot="SR0">&gt;Name</text>
<wire x1="-1.9" y1="1.9" x2="1.9" y2="1.9" width="0.2" layer="41"/>
<wire x1="1.9" y1="1.9" x2="1.9" y2="-1.9" width="0.2" layer="41"/>
<wire x1="1.9" y1="-1.9" x2="-1.9" y2="-1.9" width="0.2" layer="41"/>
<wire x1="-1.9" y1="-1.9" x2="-1.9" y2="1.9" width="0.2" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="PTN3366">
<wire x1="-12.7" y1="33.02" x2="12.7" y2="33.02" width="0.254" layer="94"/>
<wire x1="12.7" y1="33.02" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="-12.7" y2="33.02" width="0.254" layer="94"/>
<pin name="DDC_EN" x="-15.24" y="-15.24" length="short" direction="in"/>
<pin name="EQ0" x="-15.24" y="-10.16" length="short" direction="in"/>
<pin name="EQ1" x="-15.24" y="-12.7" length="short" direction="in"/>
<pin name="GND" x="-15.24" y="-25.4" length="short" direction="pwr"/>
<pin name="HPD_SNK" x="15.24" y="-7.62" length="short" direction="in" rot="R180"/>
<pin name="HPD_SRC" x="-15.24" y="-7.62" length="short" direction="out"/>
<pin name="IN_D1+" x="-15.24" y="10.16" length="short" direction="in"/>
<pin name="IN_D1-" x="-15.24" y="-2.54" length="short" direction="in"/>
<pin name="IN_D2+" x="-15.24" y="12.7" length="short" direction="in"/>
<pin name="IN_D2-" x="-15.24" y="0" length="short" direction="in"/>
<pin name="IN_D3+" x="-15.24" y="15.24" length="short" direction="in"/>
<pin name="IN_D3-" x="-15.24" y="2.54" length="short" direction="in"/>
<pin name="IN_D4+" x="-15.24" y="17.78" length="short" direction="in"/>
<pin name="IN_D4-" x="-15.24" y="5.08" length="short" direction="in"/>
<pin name="NC1" x="15.24" y="-10.16" length="short" direction="nc" rot="R180"/>
<pin name="NC2" x="15.24" y="-12.7" length="short" direction="nc" rot="R180"/>
<pin name="NC3" x="15.24" y="-15.24" length="short" direction="nc" rot="R180"/>
<pin name="OE_N" x="-15.24" y="22.86" length="short" direction="in"/>
<pin name="OUT_D1+" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<pin name="OUT_D1-" x="15.24" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="OUT_D2+" x="15.24" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="OUT_D2-" x="15.24" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="OUT_D3+" x="15.24" y="15.24" length="short" direction="out" rot="R180"/>
<pin name="OUT_D3-" x="15.24" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="OUT_D4+" x="15.24" y="22.86" length="short" direction="out" rot="R180"/>
<pin name="OUT_D4-" x="15.24" y="20.32" length="short" direction="out" rot="R180"/>
<pin name="REXT" x="15.24" y="27.94" length="short" rot="R180"/>
<pin name="SCL_SNK" x="15.24" y="-20.32" length="short" rot="R180"/>
<pin name="SCL_SRC" x="-15.24" y="-20.32" length="short"/>
<pin name="SDA_SNK" x="15.24" y="-22.86" length="short" rot="R180"/>
<pin name="SDA_SRC" x="-15.24" y="-22.86" length="short"/>
<pin name="VDD1" x="-15.24" y="27.94" length="short" direction="pwr"/>
<pin name="VDD2" x="-15.24" y="30.48" length="short" direction="pwr"/>
<text x="-2.54" y="35.56" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-30.48" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PTN3366">
<description>PTN3366 - Low power HDMI/DVI level shifter with active DDC buffer, supporting 3Gbit/s operation.</description>
<gates>
<gate name="G$1" symbol="PTN3366" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT617-3">
<connects>
<connect gate="G$1" pin="DDC_EN" pad="22"/>
<connect gate="G$1" pin="EQ0" pad="8"/>
<connect gate="G$1" pin="EQ1" pad="2"/>
<connect gate="G$1" pin="GND" pad="33"/>
<connect gate="G$1" pin="HPD_SNK" pad="21"/>
<connect gate="G$1" pin="HPD_SRC" pad="5"/>
<connect gate="G$1" pin="IN_D1+" pad="26"/>
<connect gate="G$1" pin="IN_D1-" pad="25"/>
<connect gate="G$1" pin="IN_D2+" pad="28"/>
<connect gate="G$1" pin="IN_D2-" pad="27"/>
<connect gate="G$1" pin="IN_D3+" pad="30"/>
<connect gate="G$1" pin="IN_D3-" pad="29"/>
<connect gate="G$1" pin="IN_D4+" pad="32"/>
<connect gate="G$1" pin="IN_D4-" pad="31"/>
<connect gate="G$1" pin="NC1" pad="3"/>
<connect gate="G$1" pin="NC2" pad="23"/>
<connect gate="G$1" pin="NC3" pad="24"/>
<connect gate="G$1" pin="OE_N" pad="17"/>
<connect gate="G$1" pin="OUT_D1+" pad="15"/>
<connect gate="G$1" pin="OUT_D1-" pad="16"/>
<connect gate="G$1" pin="OUT_D2+" pad="13"/>
<connect gate="G$1" pin="OUT_D2-" pad="14"/>
<connect gate="G$1" pin="OUT_D3+" pad="11"/>
<connect gate="G$1" pin="OUT_D3-" pad="12"/>
<connect gate="G$1" pin="OUT_D4+" pad="9"/>
<connect gate="G$1" pin="OUT_D4-" pad="10"/>
<connect gate="G$1" pin="REXT" pad="4"/>
<connect gate="G$1" pin="SCL_SNK" pad="19"/>
<connect gate="G$1" pin="SCL_SRC" pad="7"/>
<connect gate="G$1" pin="SDA_SNK" pad="20"/>
<connect gate="G$1" pin="SDA_SRC" pad="6"/>
<connect gate="G$1" pin="VDD1" pad="1"/>
<connect gate="G$1" pin="VDD2" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IP4251CZ16-8-TTL">
<packages>
<package name="SON40P135X330X55-17N">
<text x="-1.2" y="2.4" size="0.8128" layer="25" font="vector" rot="SR0">&gt;Name</text>
<smd name="1" x="-0.75" y="1.4" dx="0.6" dy="0.2" layer="1"/>
<smd name="2" x="-0.75" y="1" dx="0.6" dy="0.2" layer="1"/>
<smd name="3" x="-0.75" y="0.6" dx="0.6" dy="0.2" layer="1"/>
<smd name="4" x="-0.75" y="0.2" dx="0.6" dy="0.2" layer="1"/>
<smd name="5" x="-0.75" y="-0.2" dx="0.6" dy="0.2" layer="1"/>
<smd name="6" x="-0.75" y="-0.6" dx="0.6" dy="0.2" layer="1"/>
<smd name="7" x="-0.75" y="-1" dx="0.6" dy="0.2" layer="1"/>
<smd name="8" x="-0.75" y="-1.4" dx="0.6" dy="0.2" layer="1"/>
<smd name="9" x="0.75" y="-1.4" dx="0.6" dy="0.2" layer="1"/>
<smd name="10" x="0.75" y="-1" dx="0.6" dy="0.2" layer="1"/>
<smd name="11" x="0.75" y="-0.6" dx="0.6" dy="0.2" layer="1"/>
<smd name="12" x="0.75" y="-0.2" dx="0.6" dy="0.2" layer="1"/>
<smd name="13" x="0.75" y="0.2" dx="0.6" dy="0.2" layer="1"/>
<smd name="14" x="0.75" y="0.6" dx="0.6" dy="0.2" layer="1"/>
<smd name="15" x="0.75" y="1" dx="0.6" dy="0.2" layer="1"/>
<smd name="16" x="0.75" y="1.4" dx="0.6" dy="0.2" layer="1"/>
<smd name="17" x="0" y="0" dx="0.4" dy="2.8" layer="1"/>
<wire x1="-1.4" y1="1.9" x2="1.4" y2="1.9" width="0.2" layer="21"/>
<wire x1="1.4" y1="1.9" x2="1.4" y2="-1.9" width="0.2" layer="21"/>
<wire x1="1.4" y1="-1.9" x2="-1.4" y2="-1.9" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-1.9" x2="-1.4" y2="1.9" width="0.2" layer="21"/>
<text x="-1.5" y="2" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector" rot="SR0">&gt;Name</text>
<wire x1="-1.4" y1="1.9" x2="1.4" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.4" y1="1.9" x2="1.4" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.4" y1="-1.9" x2="-1.4" y2="-1.9" width="0.2" layer="51"/>
<wire x1="-1.4" y1="-1.9" x2="-1.4" y2="1.9" width="0.2" layer="51"/>
<circle x="-0.9" y="1.4" radius="0.2" width="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="IP4251CZ16-8-TTL">
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<text x="-5.08" y="15.24" size="1.778" layer="95" font="vector" rot="SR0">&gt;Name</text>
<text x="2.54" y="-15.24" size="1.778" layer="96" font="vector" rot="SR180">&gt;Value</text>
<pin name="CH1" x="-12.7" y="10.16" length="short" direction="pas"/>
<pin name="CH2" x="-12.7" y="7.62" length="short" direction="pas"/>
<pin name="CH3" x="-12.7" y="5.08" length="short" direction="pas"/>
<pin name="CH4" x="-12.7" y="2.54" length="short" direction="pas"/>
<pin name="CH5" x="-12.7" y="0" length="short" direction="pas"/>
<pin name="CH6" x="-12.7" y="-2.54" length="short" direction="pas"/>
<pin name="CH7" x="-12.7" y="-5.08" length="short" direction="pas"/>
<pin name="CH8" x="-12.7" y="-7.62" length="short" direction="pas"/>
<pin name="EP" x="-12.7" y="-10.16" length="short" direction="pwr"/>
<pin name="CH8_2" x="12.7" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="CH7_2" x="12.7" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="CH6_2" x="12.7" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="CH5_2" x="12.7" y="0" length="short" direction="pas" rot="R180"/>
<pin name="CH4_2" x="12.7" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="CH3_2" x="12.7" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="CH2_2" x="12.7" y="7.62" length="short" direction="pas" rot="R180"/>
<pin name="CH1_2" x="12.7" y="10.16" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IP4251CZ16-8-TTL" prefix="U">
<description>passive filter network &lt;a href="https://pricing.snapeda.com/parts/IP4251CZ16-8-TTL/NXP%20Semiconductors/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="A" symbol="IP4251CZ16-8-TTL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SON40P135X330X55-17N">
<connects>
<connect gate="A" pin="CH1" pad="1"/>
<connect gate="A" pin="CH1_2" pad="16"/>
<connect gate="A" pin="CH2" pad="2"/>
<connect gate="A" pin="CH2_2" pad="15"/>
<connect gate="A" pin="CH3" pad="3"/>
<connect gate="A" pin="CH3_2" pad="14"/>
<connect gate="A" pin="CH4" pad="4"/>
<connect gate="A" pin="CH4_2" pad="13"/>
<connect gate="A" pin="CH5" pad="5"/>
<connect gate="A" pin="CH5_2" pad="12"/>
<connect gate="A" pin="CH6" pad="6"/>
<connect gate="A" pin="CH6_2" pad="11"/>
<connect gate="A" pin="CH7" pad="7"/>
<connect gate="A" pin="CH7_2" pad="10"/>
<connect gate="A" pin="CH8" pad="8"/>
<connect gate="A" pin="CH8_2" pad="9"/>
<connect gate="A" pin="EP" pad="17"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Not in stock"/>
<attribute name="CHECK_PRICES" value="https://www.snapeda.com/parts/IP4251CZ16-8-TTL/NXP+Semiconductors/view-part/?ref=eda"/>
<attribute name="DESCRIPTION" value=" RC (Pi) EMI Filter 2nd Order Low Pass 8 Channel R = 100Ohms, C = 10pF (Total) 16-UFDFN Exposed Pad "/>
<attribute name="MF" value="NXP Semiconductors"/>
<attribute name="MP" value="IP4251CZ16-8-TTL"/>
<attribute name="PACKAGE" value="UFDFN-16 NXP Semiconductors"/>
<attribute name="PRICE" value="None"/>
<attribute name="SNAPEDA_LINK" value="https://www.snapeda.com/parts/IP4251CZ16-8-TTL/NXP+Semiconductors/view-part/?ref=snap"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ENC424J600">
<description>Generated from &lt;b&gt;untitled.sch&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="TQFP-44">
<smd name="1" x="-4" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="2" x="-3.2" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="3" x="-2.4" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="4" x="-1.6" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="5" x="-0.8" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="6" x="0" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="7" x="0.8" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="8" x="1.6" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="9" x="2.4" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="10" x="3.2" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="11" x="4" y="-5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="12" x="5.7" y="-4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="13" x="5.7" y="-3.2" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="14" x="5.7" y="-2.4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="15" x="5.7" y="-1.6" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="16" x="5.7" y="-0.8" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="17" x="5.7" y="0" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="18" x="5.7" y="0.8" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="19" x="5.7" y="1.6" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="20" x="5.7" y="2.4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="21" x="5.7" y="3.2" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="22" x="5.7" y="4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="23" x="4" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="24" x="3.2" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="25" x="2.4" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="26" x="1.6" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="27" x="0.8" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="28" x="0" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="29" x="-0.8" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="30" x="-1.6" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="31" x="-2.4" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="32" x="-3.2" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="33" x="-4" y="5.7" dx="0.55" dy="1.5" layer="1"/>
<smd name="34" x="-5.7" y="4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="35" x="-5.7" y="3.2" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="36" x="-5.7" y="2.4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="37" x="-5.7" y="1.6" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="38" x="-5.7" y="0.8" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="39" x="-5.7" y="0" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="40" x="-5.7" y="-0.8" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="41" x="-5.7" y="-1.6" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="42" x="-5.7" y="-2.4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="43" x="-5.7" y="-3.2" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<smd name="44" x="-5.7" y="-4" dx="0.55" dy="1.5" layer="1" rot="R90"/>
<text x="-1.9" y="6.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.5" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-5.5" y1="4.7" x2="-5.5" y2="5.5" width="0.2" layer="21"/>
<wire x1="-5.5" y1="5.5" x2="-4.7" y2="5.5" width="0.2" layer="21"/>
<wire x1="4.7" y1="5.5" x2="5.5" y2="5.5" width="0.2" layer="21"/>
<wire x1="5.5" y1="5.5" x2="5.5" y2="4.7" width="0.2" layer="21"/>
<wire x1="5.5" y1="-4.7" x2="5.5" y2="-5.5" width="0.2" layer="21"/>
<wire x1="5.5" y1="-5.5" x2="4.7" y2="-5.5" width="0.2" layer="21"/>
<wire x1="-4.7" y1="-5.5" x2="-5.5" y2="-5.5" width="0.2" layer="21"/>
<wire x1="-5.5" y1="-5.5" x2="-5.5" y2="-4.7" width="0.2" layer="21"/>
<wire x1="-5.5" y1="-4.7" x2="-4.7" y2="-5.5" width="0.2" layer="21"/>
<text x="-5.6" y="-5.6" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
<wire x1="-5.5" y1="5.5" x2="-5.5" y2="-5.5" width="0.2" layer="51"/>
<wire x1="-5.5" y1="-5.5" x2="5.5" y2="-5.5" width="0.2" layer="51"/>
<wire x1="5.5" y1="-5.5" x2="5.5" y2="5.5" width="0.2" layer="51"/>
<wire x1="5.5" y1="5.5" x2="-5.5" y2="5.5" width="0.2" layer="51"/>
<circle x="-4.3" y="-4.3" radius="0.3" width="0.2" layer="51"/>
<wire x1="-4.3" y1="-5.1" x2="-4.3" y2="-6.3" width="0.2" layer="41"/>
<wire x1="-3.7" y1="-5.1" x2="-3.7" y2="-6.3" width="0.2" layer="41"/>
<wire x1="-4.4" y1="-5.1" x2="-4.4" y2="-6.3" width="0.2" layer="41"/>
<wire x1="5.1" y1="-3.5" x2="6.3" y2="-3.5" width="0.2" layer="41"/>
<wire x1="5.1" y1="-2.9" x2="6.3" y2="-2.9" width="0.2" layer="41"/>
<wire x1="5.1" y1="-2.7" x2="6.3" y2="-2.7" width="0.2" layer="41"/>
<wire x1="5.1" y1="-2.1" x2="6.3" y2="-2.1" width="0.2" layer="41"/>
<wire x1="5.1" y1="4.3" x2="6.3" y2="4.3" width="0.2" layer="41"/>
<wire x1="5.1" y1="4.4" x2="6.3" y2="4.4" width="0.2" layer="41"/>
<wire x1="-4.3" y1="5.1" x2="-4.3" y2="6.3" width="0.2" layer="41"/>
<wire x1="-4.4" y1="5.1" x2="-4.4" y2="6.3" width="0.2" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="ENC424J600">
<wire x1="12.7" y1="43.18" x2="12.7" y2="-38.1" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-38.1" x2="-12.7" y2="-38.1" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-38.1" x2="-12.7" y2="43.18" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="43.18" x2="12.7" y2="43.18" width="0.4064" layer="94"/>
<pin name="AD0" x="15.24" y="20.32" length="short" rot="R180"/>
<pin name="AD1" x="15.24" y="17.78" length="short" direction="in" rot="R180"/>
<pin name="AD2" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="AD3" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="AD4" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="AD5" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="AD6" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="AD7" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="AD8" x="15.24" y="0" length="short" rot="R180"/>
<pin name="AD9" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="AD10" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="AD11" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="AD12" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="AD13" x="15.24" y="-12.7" length="short" rot="R180"/>
<pin name="AD14" x="15.24" y="-15.24" length="short" rot="R180"/>
<pin name="CLKOUT" x="15.24" y="25.4" length="short" direction="out" function="clk" rot="R180"/>
<pin name="CS" x="15.24" y="38.1" length="short" direction="in" rot="R180"/>
<pin name="INT/SPISEL" x="15.24" y="27.94" length="short" rot="R180"/>
<pin name="LEDA" x="15.24" y="-33.02" length="short" direction="out" rot="R180"/>
<pin name="LEDB" x="15.24" y="-35.56" length="short" direction="out" rot="R180"/>
<pin name="OSC1" x="-15.24" y="2.54" length="short" direction="in"/>
<pin name="OSC2" x="-15.24" y="0" length="short" direction="out"/>
<pin name="PSPCFG0" x="15.24" y="30.48" length="short" direction="in" rot="R180"/>
<pin name="RBIAS" x="-15.24" y="12.7" length="short" direction="in"/>
<pin name="SCK/AL" x="15.24" y="40.64" length="short" direction="in" rot="R180"/>
<pin name="SI/RD/RW" x="15.24" y="33.02" length="short" direction="in" rot="R180"/>
<pin name="SO/WR/EN" x="15.24" y="35.56" length="short" rot="R180"/>
<pin name="TPIN+" x="15.24" y="-27.94" length="short" direction="in" rot="R180"/>
<pin name="TPIN-" x="15.24" y="-25.4" length="short" direction="in" rot="R180"/>
<pin name="TPOUT+" x="15.24" y="-22.86" length="short" direction="out" rot="R180"/>
<pin name="TPOUT-" x="15.24" y="-20.32" length="short" direction="out" rot="R180"/>
<pin name="VCAP" x="-15.24" y="17.78" length="short" direction="in"/>
<pin name="VDD" x="-15.24" y="40.64" length="short" direction="in"/>
<pin name="VDDOSC" x="-15.24" y="27.94" length="short" direction="in"/>
<pin name="VDDPLL" x="-15.24" y="35.56" length="short" direction="in"/>
<pin name="VDDRX" x="-15.24" y="30.48" length="short" direction="in"/>
<pin name="VDDTX" x="-15.24" y="33.02" length="short" direction="in"/>
<pin name="VSS1" x="-15.24" y="-33.02" length="short" direction="in"/>
<pin name="VSS2" x="-15.24" y="-35.56" length="short" direction="in"/>
<pin name="VSSOSC" x="-15.24" y="-17.78" length="short" direction="in"/>
<pin name="VSSPLL" x="-15.24" y="-27.94" length="short" direction="in"/>
<pin name="VSSRX" x="-15.24" y="-20.32" length="short" direction="in"/>
<pin name="VSSTX1" x="-15.24" y="-22.86" length="short" direction="in"/>
<pin name="VSSTX2" x="-15.24" y="-25.4" length="short" direction="in"/>
<text x="-5.08" y="45.72" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="7.62" y="-40.64" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ENC424J600" prefix="U">
<description>Stand-Alone 10/100 Ethernet Controller with SPI or Parallel Interface

Source: http://ww1.microchip.com/downloads/en/DeviceDoc/39935c.pdf</description>
<gates>
<gate name="G$1" symbol="ENC424J600" x="0" y="0"/>
</gates>
<devices>
<device name="-TQFP44" package="TQFP-44">
<connects>
<connect gate="G$1" pin="AD0" pad="38"/>
<connect gate="G$1" pin="AD1" pad="39"/>
<connect gate="G$1" pin="AD10" pad="27"/>
<connect gate="G$1" pin="AD11" pad="28"/>
<connect gate="G$1" pin="AD12" pad="29"/>
<connect gate="G$1" pin="AD13" pad="30"/>
<connect gate="G$1" pin="AD14" pad="31"/>
<connect gate="G$1" pin="AD2" pad="40"/>
<connect gate="G$1" pin="AD3" pad="41"/>
<connect gate="G$1" pin="AD4" pad="5"/>
<connect gate="G$1" pin="AD5" pad="6"/>
<connect gate="G$1" pin="AD6" pad="7"/>
<connect gate="G$1" pin="AD7" pad="8"/>
<connect gate="G$1" pin="AD8" pad="25"/>
<connect gate="G$1" pin="AD9" pad="26"/>
<connect gate="G$1" pin="CLKOUT" pad="23"/>
<connect gate="G$1" pin="CS" pad="34"/>
<connect gate="G$1" pin="INT/SPISEL" pad="24"/>
<connect gate="G$1" pin="LEDA" pad="10"/>
<connect gate="G$1" pin="LEDB" pad="9"/>
<connect gate="G$1" pin="OSC1" pad="3"/>
<connect gate="G$1" pin="OSC2" pad="2"/>
<connect gate="G$1" pin="PSPCFG0" pad="32"/>
<connect gate="G$1" pin="RBIAS" pad="11"/>
<connect gate="G$1" pin="SCK/AL" pad="37"/>
<connect gate="G$1" pin="SI/RD/RW" pad="36"/>
<connect gate="G$1" pin="SO/WR/EN" pad="35"/>
<connect gate="G$1" pin="TPIN+" pad="16"/>
<connect gate="G$1" pin="TPIN-" pad="17"/>
<connect gate="G$1" pin="TPOUT+" pad="20"/>
<connect gate="G$1" pin="TPOUT-" pad="21"/>
<connect gate="G$1" pin="VCAP" pad="43"/>
<connect gate="G$1" pin="VDD" pad="44"/>
<connect gate="G$1" pin="VDDOSC" pad="4"/>
<connect gate="G$1" pin="VDDPLL" pad="12"/>
<connect gate="G$1" pin="VDDRX" pad="15"/>
<connect gate="G$1" pin="VDDTX" pad="18"/>
<connect gate="G$1" pin="VSS1" pad="42"/>
<connect gate="G$1" pin="VSS2" pad="33"/>
<connect gate="G$1" pin="VSSOSC" pad="1"/>
<connect gate="G$1" pin="VSSPLL" pad="13"/>
<connect gate="G$1" pin="VSSRX" pad="14"/>
<connect gate="G$1" pin="VSSTX1" pad="19"/>
<connect gate="G$1" pin="VSSTX2" pad="22"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_HEADER_THT_2.54X1_MALE">
<packages>
<package name="HEADER_THT_2.54X1_MALE-01">
<pad name="1" x="0" y="0" drill="1" shape="square"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.2" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.2" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.2" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.2" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2" layer="51"/>
<wire x1="-0.63" y1="-0.63" x2="0.63" y2="-0.63" width="0.2" layer="51"/>
<wire x1="0.63" y1="-0.63" x2="0.63" y2="0.63" width="0.2" layer="51"/>
<wire x1="0.63" y1="0.63" x2="-0.63" y2="0.63" width="0.2" layer="51"/>
<wire x1="-0.63" y1="0.63" x2="-0.63" y2="-0.63" width="0.2" layer="51"/>
<text x="-1" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1" y="-0.5" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="HEADER_THT_2.54X1_MALE-01">
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-0.72" size="1.4224" layer="95" font="vector">1</text>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="0" y="-5.08" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER_THT_2.54X1_MALE-01" prefix="JP" uservalue="yes">
<description>"HEADER_THT_2.54X1_MALE-01"</description>
<gates>
<gate name="JP" symbol="HEADER_THT_2.54X1_MALE-01" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="HEADER_THT_2.54X1_MALE-01">
<connects>
<connect gate="JP" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="-2.54"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J4" library="zx_spectrum2" deviceset="ZX" device="-SMD"/>
<part name="HOLE1" library="REBANE" deviceset="HOLE" device="-M2.5"/>
<part name="HOLE2" library="REBANE" deviceset="HOLE" device="-M2.5"/>
<part name="HOLE3" library="REBANE" deviceset="HOLE" device="-M2.5"/>
<part name="HOLE4" library="REBANE" deviceset="HOLE" device="-M2.5"/>
<part name="J2" library="1747981-1" deviceset="1747981-1" device=""/>
<part name="SW1" library="REBANE" deviceset="TACTILE-SWITCH-GND" device="-TH-90"/>
<part name="J3" library="REBANE" deviceset="MICRO_SD" device=""/>
<part name="J5" library="we-rj45_2" deviceset="WE-RJ45" device="" value="7498111120R"/>
<part name="GND_1" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_2" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_3" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_4" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_5" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_6" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_7" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_8" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_9" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_10" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_11" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="LED1" library="REBANE" deviceset="LED" device="-3MM" value="GREEN"/>
<part name="LED2" library="REBANE" deviceset="LED" device="-3MM" value="YELLOW"/>
<part name="LED3" library="REBANE" deviceset="LED" device="-3MM" value="RED"/>
<part name="U5" library="REBANE" deviceset="SPI_FLASH" device="-SOIC8" value="N25Q032A13ESC40F"/>
<part name="U9" library="SN74LXC8T245" deviceset="SN74LXC8T245" device=""/>
<part name="U8" library="SN74LXC8T245" deviceset="SN74LXC8T245" device=""/>
<part name="U10" library="SN74LXC8T245" deviceset="SN74LXC8T245" device=""/>
<part name="U11" library="SN74LXC8T245" deviceset="SN74LXC8T245" device=""/>
<part name="U12" library="SN74LXC8T245" deviceset="SN74LXC8T245" device=""/>
<part name="+5V_1" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="GND_12" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_13" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_14" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_15" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_1" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_16" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_17" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_18" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_19" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_20" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_21" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_2" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_22" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_23" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_24" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_25" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_3" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_26" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_27" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_28" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_29" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_30" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_31" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_4" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_5" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_35" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_36" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_37" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_38" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_39" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_40" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_6" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="JP5" library="REBANE_HEADER_THT_2.54X1N_MALE" deviceset="HEADER_THT_2.54X1N_MALE-02" device="-2.54" technology="-2.54"/>
<part name="+9V_1" library="REBANE_POWER" deviceset="+9V" device=""/>
<part name="JP4" library="REBANE_HEADER_THT_2.54X2_MALE" deviceset="HEADER_THT_2.54X2_MALE-02" device="-2.54" technology="-2.54"/>
<part name="GND_33" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="U6" library="REBANE" deviceset="SOT-X-5" device="-SOT23-5" value="TLV75512PDBVR">
<attribute name="P1" value="IN"/>
<attribute name="P2" value="GND"/>
<attribute name="P3" value="EN"/>
<attribute name="P4" value="NC"/>
<attribute name="P5" value="OUT"/>
</part>
<part name="C18" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="4.7uF"/>
<part name="C19" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="4.7uF"/>
<part name="GND_34" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_41" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_42" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_7" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+1V2_1" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="T2" library="REBANE" deviceset="TRANSISTOR-NPN" device="-SOT-23"/>
<part name="GND_43" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R34" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="100k"/>
<part name="R35" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="R33" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="U14" library="ICE40HX4K-TQ144" deviceset="ICE40HX4K-TQ144" device=""/>
<part name="U3" library="SN74LXC8T245" deviceset="SN74LXC8T245" device=""/>
<part name="+3V3_8" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_9" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_32" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_44" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="J1" library="REBANE" deviceset="PROG_SPI_AVR" device="-1.27_IDC90"/>
<part name="R24" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="GND_45" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_46" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_10" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="U1" library="ptn3366" deviceset="PTN3366" device=""/>
<part name="+1V2_3" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="+1V2_4" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="+5V_14" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="GND_47" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_48" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_49" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R10" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="100"/>
<part name="R5" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="N/A"/>
<part name="+3V3_11" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_12" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R12" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="12k4"/>
<part name="GND_50" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R8" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="4k7"/>
<part name="R9" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="4k7"/>
<part name="R6" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="N/A"/>
<part name="+5V_15" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="C8" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C7" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C6" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C5" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_54" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_56" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C4" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C3" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C2" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C1" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_55" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+5V_16" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V3_13" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="U2" library="IP4251CZ16-8-TTL" deviceset="IP4251CZ16-8-TTL" device=""/>
<part name="U4" library="IP4251CZ16-8-TTL" deviceset="IP4251CZ16-8-TTL" device=""/>
<part name="GND_57" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_58" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+5V0A_1" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_2" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_3" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_4" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_5" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_6" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_7" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_8" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_9" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_10" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="R1" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_14" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R4" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_15" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R11" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="GND_51" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R3" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="4k7"/>
<part name="R2" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="4k7"/>
<part name="+3V3_16" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_17" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_52" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_53" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="T1" library="REBANE" deviceset="MOSFET-P" device="-SOT-23"/>
<part name="+3V3_18" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R13" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_19" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C13" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="4.7uF"/>
<part name="+3V3_20" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R23" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="100"/>
<part name="C14" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_59" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R14" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R15" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R16" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R17" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R18" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R19" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R20" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_21" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_22" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_23" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_24" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_25" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_26" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_27" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_29" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C53" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_60" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C54" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_61" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_34" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C55" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_62" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_35" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C56" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_63" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_36" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C57" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_64" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_37" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C58" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_65" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C59" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_66" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_39" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C60" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_67" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C61" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_68" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_41" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C62" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_69" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_42" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C63" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_70" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C10" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="+3V3_44" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_71" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C9" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="+3V3_45" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND_72" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C64" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_73" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C65" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_74" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C51" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C49" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="R7" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="100"/>
<part name="GND_77" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R25" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="R26" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="R27" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="+5V0A_11" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="+5V0A_12" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="C22" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C27" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_78" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C23" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C28" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C24" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C29" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C25" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C30" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C26" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C31" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_79" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_80" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_81" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_82" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+5V0A_13" library="REBANE_POWER" deviceset="+5V0A" device=""/>
<part name="R28" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="GND_83" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R30" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+1V2_5" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="+1V2_6" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="+1V2_9" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="+1V2_10" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="+1V2_11" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="L2" library="REBANE" deviceset="FERRITE/INDUCTOR" device="-0402[1005-METRIC]"/>
<part name="L1" library="REBANE" deviceset="FERRITE/INDUCTOR" device="-0402[1005-METRIC]"/>
<part name="+3V3_33" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_38" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C50" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="C48" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="+3V3_28" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="D1" library="REBANE" deviceset="DIODE" device="-SOD-323" value="1N4148WX-TP"/>
<part name="R22" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_30" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R21" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_31" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R48" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_32" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3_43" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="JP3" library="REBANE_HEADER_THT_2.54X1N_MALE" deviceset="HEADER_THT_2.54X1N_MALE-02" device="-2.54" technology="-2.54"/>
<part name="U7" library="REBANE" deviceset="REGULATOR_EN_BYP" device="-TPS7933" value="XC6210B332MR"/>
<part name="C16" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="4.7uF"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="C17" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="4.7uF"/>
<part name="P+8" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V6" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C12" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_75" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C11" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_76" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME3" library="frames" deviceset="A4L-LOC" device=""/>
<part name="C15" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_84" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_46" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="Q2" library="REBANE" deviceset="OSCILLATOR" device="-3.2X2.5" value="11.0592"/>
<part name="GND_85" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_40" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C52" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_86" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="U13" library="ENC424J600" deviceset="ENC424J600" device="-TQFP44"/>
<part name="GND_87" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C66" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_88" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_47" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C67" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_89" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+1V2_2" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="C68" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_90" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_48" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C69" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_91" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+1V2_7" library="REBANE_POWER" deviceset="+1V2" device=""/>
<part name="C41" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_92" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R39" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="12k4"/>
<part name="GND_93" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C43" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="18pF"/>
<part name="C44" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="18pF"/>
<part name="GND_94" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_95" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="Q1" library="REBANE" deviceset="CRYSTAL-GND" device="-3.2X2.5" value="25MHz"/>
<part name="GND_96" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R38" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="GND_98" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R42" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="R47" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="1k"/>
<part name="T3" library="REBANE" deviceset="MOSFET-P" device="-SOT-23"/>
<part name="+3V3_49" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R36" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="+3V3_50" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C32" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="4.7uF"/>
<part name="+3V3_51" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R37" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="100"/>
<part name="C34" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="C35" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C36" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.01uF"/>
<part name="C37" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C38" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C39" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="C40" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.1uF"/>
<part name="GND_97" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_99" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_100" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_101" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_102" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_103" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_104" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C33" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_105" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C47" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="6.8nF"/>
<part name="C46" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="6.8nF"/>
<part name="R43" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="49.9"/>
<part name="R44" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="49.9"/>
<part name="R45" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="49.9"/>
<part name="R46" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="49.9"/>
<part name="R40" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="N/A"/>
<part name="C45" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="0.01uF"/>
<part name="GND_106" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R41" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10"/>
<part name="C42" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="1uF"/>
<part name="GND_107" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V3_52" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R31" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="N/A"/>
<part name="R32" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="0"/>
<part name="P+1" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="C20" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="C21" library="REBANE" deviceset="CAPACITOR" device="-0402[1005-METRIC]" value="10uF"/>
<part name="GND_108" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_109" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME4" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME5" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FIDUCIAL1" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL2" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL3" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL4" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="JP1" library="REBANE_HEADER_THT_2.54X1_MALE" deviceset="HEADER_THT_2.54X1_MALE-01" device="-2.54" technology="-2.54"/>
<part name="JP2" library="REBANE_HEADER_THT_2.54X1_MALE" deviceset="HEADER_THT_2.54X1_MALE-01" device="-2.54" technology="-2.54"/>
<part name="GND_110" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND_111" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="D2" library="REBANE" deviceset="DIODE" device="-SOD-323" value="1N4148WX-TP"/>
<part name="D3" library="REBANE" deviceset="DIODE" device="-SOD-323" value="1N4148WX-TP"/>
<part name="D4" library="REBANE" deviceset="DIODE" device="-SOD-323" value="1N4148WX-TP"/>
<part name="D5" library="REBANE" deviceset="DIODE" device="-SOD-323" value="1N4148WX-TP"/>
<part name="GND_112" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="D6" library="REBANE" deviceset="DIODE" device="-SOD-323" value="1N4148WX-TP"/>
<part name="+3V3_53" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R29" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="10k"/>
<part name="R49" library="REBANE" deviceset="RESISTOR" device="-0402[1005-METRIC]" value="4k7"/>
<part name="GND_113" library="REBANE_POWER" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="7.62" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="170.18" x2="251.46" y2="53.34" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="53.34" x2="157.48" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">HDMI/DEBUG</text>
</plain>
<instances>
<instance part="J2" gate="G$1" x="124.46" y="106.68" smashed="yes">
<attribute name="NAME" x="122.936" y="135.128" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="131.064" y="77.978" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND_10" gate="1" x="114.3" y="55.88"/>
<instance part="J1" gate="G$1" x="157.48" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="159.258" y="117.348" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="U1" gate="G$1" x="66.04" y="104.14" smashed="yes">
<attribute name="NAME" x="64.008" y="137.668" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="72.136" y="75.692" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="R10" gate="R" x="96.52" y="86.36"/>
<instance part="R5" gate="R" x="86.36" y="142.24" rot="R90"/>
<instance part="+3V3_11" gate="G$1" x="86.36" y="160.02"/>
<instance part="+3V3_12" gate="G$1" x="48.26" y="160.02"/>
<instance part="R12" gate="R" x="83.82" y="66.04" rot="R90"/>
<instance part="GND_50" gate="1" x="83.82" y="55.88"/>
<instance part="R8" gate="R" x="96.52" y="96.52"/>
<instance part="R9" gate="R" x="96.52" y="93.98"/>
<instance part="R6" gate="R" x="88.9" y="142.24" rot="R90"/>
<instance part="+5V_15" gate="1" x="88.9" y="160.02"/>
<instance part="C8" gate="C" x="38.1" y="101.6"/>
<instance part="C7" gate="C" x="38.1" y="104.14"/>
<instance part="C6" gate="C" x="38.1" y="106.68"/>
<instance part="C5" gate="C" x="38.1" y="109.22"/>
<instance part="GND_54" gate="1" x="33.02" y="55.88"/>
<instance part="GND_56" gate="1" x="48.26" y="55.88"/>
<instance part="C4" gate="C" x="38.1" y="114.3"/>
<instance part="C3" gate="C" x="38.1" y="116.84"/>
<instance part="C2" gate="C" x="38.1" y="119.38"/>
<instance part="C1" gate="C" x="38.1" y="121.92"/>
<instance part="GND_55" gate="1" x="134.62" y="55.88"/>
<instance part="+5V_16" gate="1" x="172.72" y="160.02"/>
<instance part="+3V3_13" gate="G$1" x="175.26" y="160.02"/>
<instance part="U2" gate="A" x="210.82" y="104.14" smashed="yes">
<attribute name="NAME" x="208.534" y="117.602" size="1.778" layer="95" font="vector" rot="SR0"/>
<attribute name="VALUE" x="223.52" y="90.678" size="1.778" layer="96" font="vector" rot="SR180"/>
</instance>
<instance part="GND_57" gate="1" x="195.58" y="55.88"/>
<instance part="R1" gate="R" x="228.6" y="142.24" rot="R90"/>
<instance part="+3V3_14" gate="G$1" x="228.6" y="160.02"/>
<instance part="R4" gate="R" x="40.64" y="142.24" rot="R90"/>
<instance part="+3V3_15" gate="G$1" x="40.64" y="160.02"/>
<instance part="R11" gate="R" x="40.64" y="66.04" rot="R90"/>
<instance part="GND_51" gate="1" x="40.64" y="55.88"/>
<instance part="R3" gate="R" x="30.48" y="142.24" rot="R90"/>
<instance part="R2" gate="R" x="27.94" y="142.24" rot="R90"/>
<instance part="+3V3_16" gate="G$1" x="30.48" y="160.02"/>
<instance part="+3V3_17" gate="G$1" x="27.94" y="160.02"/>
<instance part="C10" gate="C" x="25.4" y="66.04" rot="R90"/>
<instance part="+3V3_44" gate="G$1" x="25.4" y="81.28"/>
<instance part="GND_71" gate="1" x="25.4" y="55.88"/>
<instance part="C9" gate="C" x="20.32" y="66.04" rot="R90"/>
<instance part="+3V3_45" gate="G$1" x="20.32" y="81.28"/>
<instance part="GND_72" gate="1" x="20.32" y="55.88"/>
<instance part="R7" gate="R" x="228.6" y="124.46" rot="R90"/>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="U3" gate="G$1" x="66.04" y="30.48" smashed="yes">
<attribute name="NAME" x="63.754" y="48.768" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="76.2" y="11.938" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="+3V3_8" gate="G$1" x="53.34" y="58.42"/>
<instance part="+3V3_9" gate="G$1" x="78.74" y="58.42"/>
<instance part="GND_32" gate="1" x="53.34" y="10.16"/>
<instance part="GND_44" gate="1" x="78.74" y="10.16"/>
<instance part="C12" gate="C" x="86.36" y="43.18"/>
<instance part="GND_75" gate="1" x="96.52" y="43.18" rot="R90"/>
<instance part="C11" gate="C" x="86.36" y="45.72"/>
<instance part="GND_76" gate="1" x="96.52" y="45.72" rot="R90"/>
<instance part="HOLE1" gate="G$1" x="193.04" y="45.72"/>
<instance part="HOLE2" gate="G$1" x="200.66" y="45.72"/>
<instance part="HOLE3" gate="G$1" x="208.28" y="45.72"/>
<instance part="HOLE4" gate="G$1" x="215.9" y="45.72"/>
<instance part="FIDUCIAL1" gate="G$1" x="193.04" y="38.1"/>
<instance part="FIDUCIAL2" gate="G$1" x="200.66" y="38.1"/>
<instance part="FIDUCIAL3" gate="G$1" x="208.28" y="38.1"/>
<instance part="FIDUCIAL4" gate="G$1" x="215.9" y="38.1"/>
<instance part="JP1" gate="JP" x="226.06" y="45.72" smashed="yes">
<attribute name="NAME" x="223.012" y="43.434" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="226.06" y="40.64" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="JP2" gate="JP" x="226.06" y="38.1" smashed="yes">
<attribute name="NAME" x="223.012" y="35.814" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="226.06" y="33.02" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND_110" gate="1" x="233.68" y="45.72" rot="R90"/>
<instance part="GND_111" gate="1" x="233.68" y="38.1" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="SHIELD"/>
<pinref part="GND_10" gate="1" pin="GND"/>
<wire x1="114.3" y1="58.42" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<wire x1="114.3" y1="81.28" x2="116.84" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="116.84" y1="129.54" x2="114.3" y2="129.54" width="0.1524" layer="91"/>
<wire x1="114.3" y1="129.54" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="5"/>
<wire x1="114.3" y1="121.92" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<wire x1="114.3" y1="114.3" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<wire x1="114.3" y1="106.68" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<wire x1="116.84" y1="121.92" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<junction x="114.3" y="121.92"/>
<pinref part="J2" gate="G$1" pin="8"/>
<wire x1="116.84" y1="114.3" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="11"/>
<wire x1="116.84" y1="106.68" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<junction x="114.3" y="114.3"/>
<junction x="114.3" y="106.68"/>
<pinref part="J2" gate="G$1" pin="17"/>
<wire x1="116.84" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<junction x="114.3" y="91.44"/>
<junction x="114.3" y="81.28"/>
</segment>
<segment>
<pinref part="R12" gate="R" pin="P$1"/>
<pinref part="GND_50" gate="1" pin="GND"/>
<wire x1="83.82" y1="60.96" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="C" pin="1"/>
<pinref part="GND_54" gate="1" pin="GND"/>
<wire x1="33.02" y1="58.42" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
<wire x1="33.02" y1="101.6" x2="35.56" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C7" gate="C" pin="1"/>
<wire x1="33.02" y1="104.14" x2="35.56" y2="104.14" width="0.1524" layer="91"/>
<wire x1="33.02" y1="104.14" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C6" gate="C" pin="1"/>
<wire x1="33.02" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<wire x1="33.02" y1="106.68" x2="33.02" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C5" gate="C" pin="1"/>
<wire x1="33.02" y1="109.22" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<wire x1="33.02" y1="109.22" x2="33.02" y2="106.68" width="0.1524" layer="91"/>
<junction x="33.02" y="106.68"/>
<junction x="33.02" y="104.14"/>
<junction x="33.02" y="101.6"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="EQ1"/>
<pinref part="GND_56" gate="1" pin="GND"/>
<wire x1="48.26" y1="58.42" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<wire x1="48.26" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="EQ0"/>
<wire x1="50.8" y1="93.98" x2="48.26" y2="93.98" width="0.1524" layer="91"/>
<wire x1="48.26" y1="93.98" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<junction x="48.26" y="91.44"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="50.8" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<junction x="48.26" y="78.74"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<pinref part="GND_55" gate="1" pin="GND"/>
<wire x1="147.32" y1="114.3" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="134.62" y1="114.3" x2="134.62" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="EP"/>
<pinref part="GND_57" gate="1" pin="GND"/>
<wire x1="198.12" y1="93.98" x2="195.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="195.58" y1="93.98" x2="195.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="R" pin="P$1"/>
<pinref part="GND_51" gate="1" pin="GND"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="C" pin="1"/>
<pinref part="GND_71" gate="1" pin="GND"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="C" pin="1"/>
<pinref part="GND_72" gate="1" pin="GND"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND@11"/>
<pinref part="GND_32" gate="1" pin="GND"/>
<wire x1="55.88" y1="17.78" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<wire x1="53.34" y1="17.78" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND@12"/>
<wire x1="53.34" y1="15.24" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="15.24" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<junction x="53.34" y="15.24"/>
<pinref part="U3" gate="G$1" pin="DIR"/>
<wire x1="55.88" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<wire x1="53.34" y1="40.64" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<junction x="53.34" y="17.78"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND@25"/>
<pinref part="GND_44" gate="1" pin="GND"/>
<wire x1="76.2" y1="15.24" x2="78.74" y2="15.24" width="0.1524" layer="91"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND@13"/>
<wire x1="76.2" y1="17.78" x2="78.74" y2="17.78" width="0.1524" layer="91"/>
<wire x1="78.74" y1="17.78" x2="78.74" y2="15.24" width="0.1524" layer="91"/>
<junction x="78.74" y="15.24"/>
<pinref part="U3" gate="G$1" pin="B5"/>
<wire x1="76.2" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="B6"/>
<wire x1="78.74" y1="25.4" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<wire x1="78.74" y1="22.86" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<wire x1="78.74" y1="20.32" x2="78.74" y2="17.78" width="0.1524" layer="91"/>
<wire x1="76.2" y1="25.4" x2="78.74" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="B7"/>
<wire x1="76.2" y1="22.86" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="B8"/>
<wire x1="76.2" y1="20.32" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<junction x="78.74" y="17.78"/>
<junction x="78.74" y="20.32"/>
<junction x="78.74" y="22.86"/>
<junction x="78.74" y="25.4"/>
</segment>
<segment>
<pinref part="C12" gate="C" pin="2"/>
<pinref part="GND_75" gate="1" pin="GND"/>
<wire x1="93.98" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="C" pin="2"/>
<pinref part="GND_76" gate="1" pin="GND"/>
<wire x1="88.9" y1="45.72" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="JP" pin="1"/>
<pinref part="GND_110" gate="1" pin="GND"/>
<wire x1="231.14" y1="45.72" x2="228.6" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="JP" pin="1"/>
<pinref part="GND_111" gate="1" pin="GND"/>
<wire x1="231.14" y1="38.1" x2="228.6" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="+5V_15" gate="1" pin="+5V"/>
<pinref part="R6" gate="R" pin="P$2"/>
<wire x1="88.9" y1="152.4" x2="88.9" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="5V"/>
<pinref part="+5V_16" gate="1" pin="+5V"/>
<wire x1="167.64" y1="106.68" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="172.72" y1="106.68" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R5" gate="R" pin="P$2"/>
<pinref part="+3V3_11" gate="G$1" pin="+3V3"/>
<wire x1="86.36" y1="152.4" x2="86.36" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD1"/>
<pinref part="+3V3_12" gate="G$1" pin="+3V3"/>
<wire x1="50.8" y1="132.08" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<wire x1="48.26" y1="132.08" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD2"/>
<wire x1="48.26" y1="134.62" x2="48.26" y2="152.4" width="0.1524" layer="91"/>
<wire x1="50.8" y1="134.62" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<junction x="48.26" y="134.62"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3V3"/>
<pinref part="+3V3_13" gate="G$1" pin="+3V3"/>
<wire x1="167.64" y1="104.14" x2="175.26" y2="104.14" width="0.1524" layer="91"/>
<wire x1="175.26" y1="104.14" x2="175.26" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3_14" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="R" pin="P$2"/>
<wire x1="228.6" y1="152.4" x2="228.6" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3_15" gate="G$1" pin="+3V3"/>
<pinref part="R4" gate="R" pin="P$2"/>
<wire x1="40.64" y1="152.4" x2="40.64" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3_17" gate="G$1" pin="+3V3"/>
<pinref part="R2" gate="R" pin="P$2"/>
<wire x1="27.94" y1="152.4" x2="27.94" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3_16" gate="G$1" pin="+3V3"/>
<pinref part="R3" gate="R" pin="P$2"/>
<wire x1="30.48" y1="152.4" x2="30.48" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="C" pin="2"/>
<pinref part="+3V3_44" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="73.66" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="C" pin="2"/>
<pinref part="+3V3_45" gate="G$1" pin="+3V3"/>
<wire x1="20.32" y1="73.66" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VCCA"/>
<pinref part="+3V3_8" gate="G$1" pin="+3V3"/>
<wire x1="55.88" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="45.72" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VCCB@24"/>
<pinref part="+3V3_9" gate="G$1" pin="+3V3"/>
<wire x1="76.2" y1="45.72" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="45.72" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VCCB@23"/>
<wire x1="76.2" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="43.18" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<junction x="78.74" y="45.72"/>
<pinref part="C12" gate="C" pin="1"/>
<wire x1="83.82" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<junction x="78.74" y="43.18"/>
<pinref part="C11" gate="C" pin="1"/>
<wire x1="83.82" y1="45.72" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EXT_SCK" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH5_2"/>
<wire x1="223.52" y1="104.14" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<label x="248.92" y="104.14" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="B3"/>
<wire x1="76.2" y1="33.02" x2="96.52" y2="33.02" width="0.1524" layer="91"/>
<label x="96.52" y="33.02" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="EXT_MOSI" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH6_2"/>
<wire x1="223.52" y1="101.6" x2="248.92" y2="101.6" width="0.1524" layer="91"/>
<label x="248.92" y="101.6" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="B2"/>
<wire x1="76.2" y1="35.56" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<label x="96.52" y="35.56" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="EXT_CS" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH7_2"/>
<wire x1="223.52" y1="99.06" x2="248.92" y2="99.06" width="0.1524" layer="91"/>
<label x="248.92" y="99.06" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="B1"/>
<wire x1="76.2" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<label x="96.52" y="38.1" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="EXT_MISO" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH4_2"/>
<wire x1="223.52" y1="106.68" x2="248.92" y2="106.68" width="0.1524" layer="91"/>
<label x="248.92" y="106.68" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="A4"/>
<wire x1="55.88" y1="30.48" x2="40.64" y2="30.48" width="0.1524" layer="91"/>
<label x="40.64" y="30.48" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH8_2"/>
<wire x1="223.52" y1="96.52" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<label x="248.92" y="96.52" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="/OE"/>
<wire x1="55.88" y1="43.18" x2="40.64" y2="43.18" width="0.1524" layer="91"/>
<label x="40.64" y="43.18" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="116.84" y1="132.08" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D1+"/>
<wire x1="81.28" y1="104.14" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="104.14" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="116.84" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D1-"/>
<wire x1="81.28" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<wire x1="99.06" y1="101.6" x2="99.06" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="116.84" y1="124.46" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<wire x1="101.6" y1="124.46" x2="101.6" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D2+"/>
<wire x1="101.6" y1="111.76" x2="81.28" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="6"/>
<wire x1="116.84" y1="119.38" x2="104.14" y2="119.38" width="0.1524" layer="91"/>
<wire x1="104.14" y1="119.38" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D2-"/>
<wire x1="104.14" y1="109.22" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="7"/>
<wire x1="116.84" y1="116.84" x2="93.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="93.98" y1="116.84" x2="93.98" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D3+"/>
<wire x1="93.98" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="91.44" y1="114.3" x2="91.44" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D3-"/>
<wire x1="91.44" y1="116.84" x2="81.28" y2="116.84" width="0.1524" layer="91"/>
<wire x1="91.44" y1="114.3" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="9"/>
<wire x1="111.76" y1="114.3" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
<wire x1="111.76" y1="111.76" x2="116.84" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="106.68" y1="109.22" x2="106.68" y2="129.54" width="0.1524" layer="91"/>
<wire x1="106.68" y1="129.54" x2="93.98" y2="129.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="129.54" x2="93.98" y2="127" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D4+"/>
<wire x1="93.98" y1="127" x2="81.28" y2="127" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="10"/>
<wire x1="106.68" y1="109.22" x2="116.84" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="109.22" y1="104.14" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="109.22" y1="121.92" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="93.98" y1="121.92" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT_D4-"/>
<wire x1="93.98" y1="124.46" x2="81.28" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="12"/>
<wire x1="109.22" y1="104.14" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="HPD_SNK"/>
<pinref part="R10" gate="R" pin="P$1"/>
<wire x1="81.28" y1="96.52" x2="86.36" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R5" gate="R" pin="P$1"/>
<wire x1="86.36" y1="96.52" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
<wire x1="86.36" y1="86.36" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<wire x1="86.36" y1="137.16" x2="86.36" y2="96.52" width="0.1524" layer="91"/>
<junction x="86.36" y="96.52"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="REXT"/>
<pinref part="R12" gate="R" pin="P$2"/>
<wire x1="81.28" y1="132.08" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<wire x1="83.82" y1="132.08" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="19"/>
<wire x1="116.84" y1="86.36" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R10" gate="R" pin="P$2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL_SNK"/>
<wire x1="81.28" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="106.68" y1="83.82" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="15"/>
<wire x1="106.68" y1="96.52" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R8" gate="R" pin="P$2"/>
<wire x1="101.6" y1="96.52" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<junction x="106.68" y="96.52"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDA_SNK"/>
<wire x1="81.28" y1="81.28" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="81.28" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="16"/>
<wire x1="109.22" y1="93.98" x2="116.84" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="P$2"/>
<wire x1="101.6" y1="93.98" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<junction x="109.22" y="93.98"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="18"/>
<wire x1="116.84" y1="88.9" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="88.9" y1="88.9" x2="88.9" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R8" gate="R" pin="P$1"/>
<wire x1="88.9" y1="93.98" x2="88.9" y2="96.52" width="0.1524" layer="91"/>
<wire x1="88.9" y1="96.52" x2="91.44" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="P$1"/>
<wire x1="91.44" y1="93.98" x2="88.9" y2="93.98" width="0.1524" layer="91"/>
<junction x="88.9" y="93.98"/>
<pinref part="R6" gate="R" pin="P$1"/>
<wire x1="88.9" y1="137.16" x2="88.9" y2="96.52" width="0.1524" layer="91"/>
<junction x="88.9" y="96.52"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="13"/>
<wire x1="116.84" y1="101.6" x2="111.76" y2="101.6" width="0.1524" layer="91"/>
<wire x1="111.76" y1="101.6" x2="111.76" y2="73.66" width="0.1524" layer="91"/>
<wire x1="111.76" y1="73.66" x2="132.08" y2="73.66" width="0.1524" layer="91"/>
<wire x1="132.08" y1="73.66" x2="132.08" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH1"/>
<wire x1="198.12" y1="114.3" x2="195.58" y2="114.3" width="0.1524" layer="91"/>
<wire x1="195.58" y1="114.3" x2="195.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="195.58" y1="121.92" x2="132.08" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D1-"/>
<wire x1="50.8" y1="101.6" x2="40.64" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C8" gate="C" pin="2"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D2-"/>
<pinref part="C7" gate="C" pin="2"/>
<wire x1="40.64" y1="104.14" x2="50.8" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D3-"/>
<pinref part="C6" gate="C" pin="2"/>
<wire x1="40.64" y1="106.68" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D4-"/>
<pinref part="C5" gate="C" pin="2"/>
<wire x1="40.64" y1="109.22" x2="50.8" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D4+"/>
<pinref part="C1" gate="C" pin="2"/>
<wire x1="40.64" y1="121.92" x2="50.8" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D3+"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="40.64" y1="119.38" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D2+"/>
<pinref part="C3" gate="C" pin="2"/>
<wire x1="40.64" y1="116.84" x2="50.8" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN_D1+"/>
<pinref part="C4" gate="C" pin="2"/>
<wire x1="40.64" y1="114.3" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="RX"/>
<wire x1="147.32" y1="104.14" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
<wire x1="144.78" y1="104.14" x2="144.78" y2="99.06" width="0.1524" layer="91"/>
<wire x1="144.78" y1="99.06" x2="180.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="180.34" y1="99.06" x2="180.34" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH2"/>
<wire x1="180.34" y1="111.76" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="TX"/>
<wire x1="147.32" y1="106.68" x2="142.24" y2="106.68" width="0.1524" layer="91"/>
<wire x1="142.24" y1="106.68" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
<wire x1="142.24" y1="96.52" x2="182.88" y2="96.52" width="0.1524" layer="91"/>
<wire x1="182.88" y1="96.52" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH3"/>
<wire x1="182.88" y1="109.22" x2="198.12" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="MISO"/>
<wire x1="147.32" y1="109.22" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="139.7" y1="109.22" x2="139.7" y2="93.98" width="0.1524" layer="91"/>
<wire x1="139.7" y1="93.98" x2="185.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="185.42" y1="93.98" x2="185.42" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH4"/>
<wire x1="185.42" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SCK"/>
<wire x1="147.32" y1="111.76" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<wire x1="137.16" y1="111.76" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="137.16" y1="91.44" x2="187.96" y2="91.44" width="0.1524" layer="91"/>
<wire x1="187.96" y1="91.44" x2="187.96" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH5"/>
<wire x1="187.96" y1="104.14" x2="198.12" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="CS"/>
<wire x1="167.64" y1="111.76" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<wire x1="177.8" y1="111.76" x2="177.8" y2="114.3" width="0.1524" layer="91"/>
<wire x1="177.8" y1="114.3" x2="190.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="190.5" y1="114.3" x2="190.5" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH7"/>
<wire x1="190.5" y1="99.06" x2="198.12" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH6"/>
<wire x1="198.12" y1="101.6" x2="193.04" y2="101.6" width="0.1524" layer="91"/>
<wire x1="193.04" y1="101.6" x2="193.04" y2="116.84" width="0.1524" layer="91"/>
<wire x1="193.04" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="170.18" y1="116.84" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="MOSI"/>
<wire x1="170.18" y1="114.3" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="RST"/>
<wire x1="167.64" y1="109.22" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="177.8" y1="109.22" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<wire x1="177.8" y1="88.9" x2="190.5" y2="88.9" width="0.1524" layer="91"/>
<wire x1="190.5" y1="88.9" x2="190.5" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CH8"/>
<wire x1="190.5" y1="96.52" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH1_2"/>
<pinref part="R7" gate="R" pin="P$1"/>
<wire x1="228.6" y1="119.38" x2="228.6" y2="114.3" width="0.1524" layer="91"/>
<wire x1="228.6" y1="114.3" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HDMI_OE_N" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OE_N"/>
<wire x1="50.8" y1="127" x2="40.64" y2="127" width="0.1524" layer="91"/>
<pinref part="R4" gate="R" pin="P$1"/>
<wire x1="40.64" y1="127" x2="10.16" y2="127" width="0.1524" layer="91"/>
<wire x1="40.64" y1="137.16" x2="40.64" y2="127" width="0.1524" layer="91"/>
<junction x="40.64" y="127"/>
<label x="10.16" y="127" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_HPD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="HPD_SRC"/>
<wire x1="50.8" y1="96.52" x2="10.16" y2="96.52" width="0.1524" layer="91"/>
<label x="10.16" y="96.52" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_DDC_EN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DDC_EN"/>
<wire x1="50.8" y1="88.9" x2="40.64" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R11" gate="R" pin="P$2"/>
<wire x1="40.64" y1="88.9" x2="10.16" y2="88.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="71.12" x2="40.64" y2="88.9" width="0.1524" layer="91"/>
<junction x="40.64" y="88.9"/>
<label x="10.16" y="88.9" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL_SRC"/>
<wire x1="50.8" y1="83.82" x2="30.48" y2="83.82" width="0.1524" layer="91"/>
<wire x1="30.48" y1="83.82" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R3" gate="R" pin="P$1"/>
<wire x1="30.48" y1="86.36" x2="10.16" y2="86.36" width="0.1524" layer="91"/>
<wire x1="30.48" y1="137.16" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<junction x="30.48" y="86.36"/>
<label x="10.16" y="86.36" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDA_SRC"/>
<wire x1="50.8" y1="81.28" x2="27.94" y2="81.28" width="0.1524" layer="91"/>
<wire x1="27.94" y1="81.28" x2="27.94" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R2" gate="R" pin="P$1"/>
<wire x1="27.94" y1="83.82" x2="10.16" y2="83.82" width="0.1524" layer="91"/>
<wire x1="27.94" y1="137.16" x2="27.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="27.94" y="83.82"/>
<label x="10.16" y="83.82" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_CLKP" class="0">
<segment>
<pinref part="C1" gate="C" pin="1"/>
<wire x1="35.56" y1="121.92" x2="10.16" y2="121.92" width="0.1524" layer="91"/>
<label x="10.16" y="121.92" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_D0P" class="0">
<segment>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="35.56" y1="119.38" x2="10.16" y2="119.38" width="0.1524" layer="91"/>
<label x="10.16" y="119.38" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_D1P" class="0">
<segment>
<pinref part="C3" gate="C" pin="1"/>
<wire x1="35.56" y1="116.84" x2="10.16" y2="116.84" width="0.1524" layer="91"/>
<label x="10.16" y="116.84" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_D2P" class="0">
<segment>
<pinref part="C4" gate="C" pin="1"/>
<wire x1="35.56" y1="114.3" x2="10.16" y2="114.3" width="0.1524" layer="91"/>
<label x="10.16" y="114.3" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="HDMI_CEC" class="0">
<segment>
<pinref part="R7" gate="R" pin="P$2"/>
<wire x1="228.6" y1="129.54" x2="228.6" y2="134.62" width="0.1524" layer="91"/>
<label x="248.92" y="134.62" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="R1" gate="R" pin="P$1"/>
<wire x1="228.6" y1="134.62" x2="248.92" y2="134.62" width="0.1524" layer="91"/>
<wire x1="228.6" y1="137.16" x2="228.6" y2="134.62" width="0.1524" layer="91"/>
<junction x="228.6" y="134.62"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH2_2"/>
<wire x1="223.52" y1="111.76" x2="248.92" y2="111.76" width="0.1524" layer="91"/>
<label x="248.92" y="111.76" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U2" gate="A" pin="CH3_2"/>
<wire x1="223.52" y1="109.22" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<label x="248.92" y="109.22" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A3"/>
<wire x1="55.88" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
<label x="40.64" y="33.02" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A1"/>
<wire x1="55.88" y1="38.1" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<label x="40.64" y="38.1" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="B4"/>
<wire x1="76.2" y1="30.48" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<label x="96.52" y="30.48" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A2"/>
<wire x1="55.88" y1="35.56" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<label x="40.64" y="35.56" size="1.778" layer="95" font="vector"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="7.62" y1="170.18" x2="147.32" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="81.28" x2="7.62" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">SD</text>
<wire x1="147.32" y1="81.28" x2="147.32" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="81.28" x2="147.32" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<wire x1="152.4" y1="81.28" x2="152.4" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="170.18" x2="152.4" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="81.28" x2="152.4" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="170.18" x2="251.46" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<text x="154.94" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">FLASH</text>
<text x="48.006" y="51.308" size="1.778" layer="95" font="vector">I</text>
<text x="50.546" y="51.308" size="1.778" layer="95" font="vector">I</text>
<text x="68.326" y="51.308" size="1.778" layer="95" font="vector">I</text>
<text x="63.246" y="33.274" size="1.778" layer="95" font="vector">I</text>
<text x="53.086" y="51.308" size="1.778" layer="95" font="vector">O</text>
<text x="55.626" y="51.308" size="1.778" layer="95" font="vector">O</text>
<text x="58.166" y="51.308" size="1.778" layer="95" font="vector">O</text>
<text x="60.706" y="51.308" size="1.778" layer="95" font="vector">O</text>
<text x="63.246" y="51.308" size="1.778" layer="95" font="vector">O</text>
<text x="81.026" y="33.274" size="1.778" layer="95" font="vector">O</text>
<text x="75.946" y="51.308" size="1.778" layer="95" font="vector">O</text>
<text x="78.486" y="51.308" size="1.778" layer="95" font="vector">X</text>
<text x="35.306" y="33.274" size="1.778" layer="95" font="vector">O</text>
<text x="78.486" y="33.274" size="1.778" layer="95" font="vector">I</text>
<text x="48.006" y="33.274" size="1.778" layer="95" font="vector">X</text>
<text x="53.086" y="33.274" size="1.778" layer="95" font="vector">X</text>
<text x="55.626" y="33.274" size="1.778" layer="95" font="vector">X</text>
<text x="58.166" y="33.274" size="1.778" layer="95" font="vector">X</text>
<text x="60.706" y="33.274" size="1.778" layer="95" font="vector">X</text>
<wire x1="7.62" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="73.66" size="5.08" layer="94" font="vector" rot="MR180">ZX INTERFACE</text>
<wire x1="7.62" y1="10.16" x2="96.52" y2="10.16" width="0.1524" layer="94" style="longdash"/>
<wire x1="96.52" y1="10.16" x2="96.52" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="10.16" x2="101.6" y2="10.16" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="76.2" x2="129.54" y2="10.16" width="0.1524" layer="94" style="longdash"/>
<text x="104.14" y="73.66" size="5.08" layer="94" font="vector" rot="MR180">LEDS</text>
<wire x1="134.62" y1="76.2" x2="134.62" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="76.2" x2="195.58" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="27.94" x2="195.58" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="195.58" y1="27.94" x2="195.58" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="200.66" y1="27.94" x2="200.66" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="76.2" x2="200.66" y2="76.2" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="27.94" x2="200.66" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="76.2" x2="251.46" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<text x="137.16" y="73.66" size="5.08" layer="94" font="vector" rot="MR180">3V3</text>
<text x="203.2" y="73.66" size="5.08" layer="94" font="vector" rot="MR180">1V2</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="SW1" gate="SW" x="63.5" y="106.68" rot="MR0"/>
<instance part="J3" gate="G$1" x="25.4" y="114.3" smashed="yes" rot="MR0">
<attribute name="NAME" x="27.178" y="113.538" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="27.94" y="114.3" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="GND_3" gate="1" x="17.78" y="86.36"/>
<instance part="GND_4" gate="1" x="15.24" y="86.36"/>
<instance part="GND_5" gate="1" x="17.78" y="134.62" rot="R180"/>
<instance part="GND_6" gate="1" x="15.24" y="134.62" rot="R180"/>
<instance part="SW1" gate="GND" x="63.5" y="104.14" rot="R90"/>
<instance part="GND_9" gate="1" x="63.5" y="86.36"/>
<instance part="U4" gate="A" x="86.36" y="114.3" smashed="yes">
<attribute name="NAME" x="84.074" y="127.508" size="1.778" layer="95" font="vector" rot="SR0"/>
<attribute name="VALUE" x="99.314" y="100.838" size="1.778" layer="96" font="vector" rot="SR180"/>
</instance>
<instance part="GND_58" gate="1" x="71.12" y="86.36"/>
<instance part="GND_52" gate="1" x="45.72" y="86.36"/>
<instance part="GND_53" gate="1" x="55.88" y="86.36"/>
<instance part="T1" gate="T" x="43.18" y="132.08" smashed="yes" rot="MR0">
<attribute name="NAME" x="42.672" y="134.62" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="40.64" y="127" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="+3V3_18" gate="G$1" x="43.18" y="160.02"/>
<instance part="R13" gate="R" x="50.8" y="142.24" rot="R90"/>
<instance part="+3V3_19" gate="G$1" x="50.8" y="160.02"/>
<instance part="C13" gate="C" x="55.88" y="142.24" rot="R90"/>
<instance part="+3V3_20" gate="G$1" x="55.88" y="160.02"/>
<instance part="R23" gate="R" x="66.04" y="132.08"/>
<instance part="C14" gate="C" x="43.18" y="93.98" rot="R90"/>
<instance part="GND_59" gate="1" x="43.18" y="86.36"/>
<instance part="R14" gate="R" x="104.14" y="142.24" rot="R90"/>
<instance part="R15" gate="R" x="106.68" y="142.24" rot="R90"/>
<instance part="R16" gate="R" x="109.22" y="142.24" rot="R90"/>
<instance part="R17" gate="R" x="111.76" y="142.24" rot="R90"/>
<instance part="R18" gate="R" x="114.3" y="142.24" rot="R90"/>
<instance part="R19" gate="R" x="116.84" y="142.24" rot="R90"/>
<instance part="R20" gate="R" x="119.38" y="142.24" rot="R90"/>
<instance part="+3V3_21" gate="G$1" x="104.14" y="160.02"/>
<instance part="+3V3_22" gate="G$1" x="106.68" y="160.02"/>
<instance part="+3V3_23" gate="G$1" x="109.22" y="160.02"/>
<instance part="+3V3_24" gate="G$1" x="111.76" y="160.02"/>
<instance part="+3V3_25" gate="G$1" x="114.3" y="160.02"/>
<instance part="+3V3_26" gate="G$1" x="116.84" y="160.02"/>
<instance part="+3V3_27" gate="G$1" x="119.38" y="160.02"/>
<instance part="U5" gate="G$1" x="205.74" y="109.22" smashed="yes">
<attribute name="NAME" x="203.962" y="114.808" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="218.948" y="101.092" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="R24" gate="R" x="172.72" y="96.52" rot="R90"/>
<instance part="GND_45" gate="1" x="172.72" y="86.36"/>
<instance part="GND_46" gate="1" x="182.88" y="86.36"/>
<instance part="+3V3_10" gate="G$1" x="172.72" y="160.02"/>
<instance part="R22" gate="R" x="233.68" y="137.16" rot="R90"/>
<instance part="+3V3_30" gate="G$1" x="233.68" y="160.02"/>
<instance part="R21" gate="R" x="182.88" y="137.16" rot="R90"/>
<instance part="+3V3_31" gate="G$1" x="182.88" y="160.02"/>
<instance part="+3V3_43" gate="G$1" x="228.6" y="160.02"/>
<instance part="JP3" gate="JP" x="167.64" y="137.16" smashed="yes" rot="MR180">
<attribute name="NAME" x="168.656" y="140.208" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="167.64" y="142.24" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="C15" gate="C" x="228.6" y="93.98" rot="R90"/>
<instance part="GND_84" gate="1" x="228.6" y="86.36"/>
<instance part="U7" gate="U" x="172.72" y="45.72" smashed="yes">
<attribute name="NAME" x="162.56" y="51.308" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="170.434" y="51.308" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C16" gate="C" x="154.94" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="154.432" y="36.83" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="154.432" y="39.37" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="154.94" y="30.48" smashed="yes"/>
<instance part="GND13" gate="1" x="172.72" y="30.48" smashed="yes"/>
<instance part="GND14" gate="1" x="190.5" y="30.48" smashed="yes"/>
<instance part="C17" gate="C" x="190.5" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="189.992" y="36.83" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="189.992" y="39.37" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="P+8" gate="1" x="154.94" y="66.04"/>
<instance part="+3V6" gate="G$1" x="190.5" y="66.04"/>
<instance part="J4" gate="G$1" x="50.8" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="40.64" y="43.688" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="46.736" y="42.672" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND_7" gate="1" x="33.02" y="15.24"/>
<instance part="GND_8" gate="1" x="30.48" y="15.24"/>
<instance part="GND_11" gate="1" x="50.8" y="15.24"/>
<instance part="+5V_1" gate="1" x="22.86" y="15.24" rot="R180"/>
<instance part="JP5" gate="JP" x="58.42" y="20.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="60.96" y="25.654" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="58.42" y="15.24" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="+9V_1" gate="G$1" x="25.4" y="15.24" rot="R180"/>
<instance part="U6" gate="G$1" x="226.06" y="48.26" smashed="yes">
<attribute name="P1" x="219.202" y="49.784" size="1.778" layer="95" font="vector"/>
<attribute name="P2" x="219.202" y="47.244" size="1.778" layer="95" font="vector"/>
<attribute name="P3" x="219.202" y="44.704" size="1.778" layer="95" font="vector"/>
<attribute name="P4" x="232.918" y="44.704" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="P5" x="232.918" y="49.784" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="NAME" x="224.536" y="53.848" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="235.458" y="42.672" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="C18" gate="C" x="208.28" y="38.1" rot="R90"/>
<instance part="C19" gate="C" x="241.3" y="38.1" rot="R90"/>
<instance part="GND_34" gate="1" x="208.28" y="30.48"/>
<instance part="GND_41" gate="1" x="241.3" y="30.48"/>
<instance part="GND_42" gate="1" x="213.36" y="30.48"/>
<instance part="+3V3_7" gate="G$1" x="208.28" y="66.04"/>
<instance part="+1V2_1" gate="G$1" x="241.3" y="66.04"/>
<instance part="JP4" gate="JP" x="147.32" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="146.304" y="48.768" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="147.32" y="50.8" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND_33" gate="1" x="142.24" y="30.48"/>
<instance part="+5V_14" gate="1" x="142.24" y="66.04"/>
<instance part="LED1" gate="LED" x="114.3" y="25.4"/>
<instance part="LED2" gate="LED" x="119.38" y="25.4"/>
<instance part="LED3" gate="LED" x="124.46" y="25.4"/>
<instance part="GND_47" gate="1" x="114.3" y="15.24"/>
<instance part="GND_48" gate="1" x="119.38" y="15.24"/>
<instance part="GND_49" gate="1" x="124.46" y="15.24"/>
<instance part="R25" gate="R" x="114.3" y="50.8" rot="R90"/>
<instance part="R26" gate="R" x="119.38" y="50.8" rot="R90"/>
<instance part="R27" gate="R" x="124.46" y="50.8" rot="R90"/>
<instance part="+3V3_46" gate="G$1" x="114.3" y="66.04"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="G1"/>
<pinref part="GND_3" gate="1" pin="GND"/>
<wire x1="17.78" y1="88.9" x2="17.78" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="G2"/>
<pinref part="GND_4" gate="1" pin="GND"/>
<wire x1="15.24" y1="88.9" x2="15.24" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="G4"/>
<pinref part="GND_5" gate="1" pin="GND"/>
<wire x1="17.78" y1="132.08" x2="17.78" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="G3"/>
<pinref part="GND_6" gate="1" pin="GND"/>
<wire x1="15.24" y1="132.08" x2="15.24" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW1" gate="GND" pin="GND"/>
<pinref part="GND_9" gate="1" pin="GND"/>
<wire x1="63.5" y1="88.9" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="A" pin="EP"/>
<pinref part="GND_58" gate="1" pin="GND"/>
<wire x1="73.66" y1="104.14" x2="71.12" y2="104.14" width="0.1524" layer="91"/>
<wire x1="71.12" y1="104.14" x2="71.12" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="VSS"/>
<pinref part="GND_52" gate="1" pin="GND"/>
<wire x1="40.64" y1="111.76" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<wire x1="45.72" y1="111.76" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND_53" gate="1" pin="GND"/>
<wire x1="55.88" y1="106.68" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SW1" gate="SW" pin="B"/>
<wire x1="58.42" y1="106.68" x2="55.88" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="C" pin="1"/>
<pinref part="GND_59" gate="1" pin="GND"/>
<wire x1="43.18" y1="88.9" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R24" gate="R" pin="P$1"/>
<pinref part="GND_45" gate="1" pin="GND"/>
<wire x1="172.72" y1="88.9" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND"/>
<pinref part="GND_46" gate="1" pin="GND"/>
<wire x1="195.58" y1="104.14" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<wire x1="182.88" y1="104.14" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="C" pin="1"/>
<pinref part="GND_84" gate="1" pin="GND"/>
<wire x1="228.6" y1="91.44" x2="228.6" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="C" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="154.94" y1="33.02" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="U" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="172.72" y1="33.02" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C17" gate="C" pin="1"/>
<wire x1="190.5" y1="33.02" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="GND@2"/>
<pinref part="GND_7" gate="1" pin="GND"/>
<wire x1="33.02" y1="17.78" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="GND@1"/>
<pinref part="GND_8" gate="1" pin="GND"/>
<wire x1="30.48" y1="17.78" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="GND@3"/>
<pinref part="GND_11" gate="1" pin="GND"/>
<wire x1="50.8" y1="17.78" x2="50.8" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="C" pin="1"/>
<pinref part="GND_34" gate="1" pin="GND"/>
<wire x1="208.28" y1="33.02" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="C" pin="1"/>
<pinref part="GND_41" gate="1" pin="GND"/>
<wire x1="241.3" y1="33.02" x2="241.3" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="2"/>
<pinref part="GND_42" gate="1" pin="GND"/>
<wire x1="215.9" y1="48.26" x2="213.36" y2="48.26" width="0.1524" layer="91"/>
<wire x1="213.36" y1="48.26" x2="213.36" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP4" gate="JP" pin="1"/>
<pinref part="GND_33" gate="1" pin="GND"/>
<wire x1="144.78" y1="43.18" x2="142.24" y2="43.18" width="0.1524" layer="91"/>
<wire x1="142.24" y1="43.18" x2="142.24" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="LED" pin="C"/>
<pinref part="GND_47" gate="1" pin="GND"/>
<wire x1="114.3" y1="17.78" x2="114.3" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED2" gate="LED" pin="C"/>
<pinref part="GND_48" gate="1" pin="GND"/>
<wire x1="119.38" y1="17.78" x2="119.38" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED3" gate="LED" pin="C"/>
<pinref part="GND_49" gate="1" pin="GND"/>
<wire x1="124.46" y1="17.78" x2="124.46" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="T1" gate="T" pin="S"/>
<pinref part="+3V3_18" gate="G$1" pin="+3V3"/>
<wire x1="43.18" y1="152.4" x2="43.18" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3_19" gate="G$1" pin="+3V3"/>
<pinref part="R13" gate="R" pin="P$2"/>
<wire x1="50.8" y1="152.4" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="C" pin="2"/>
<pinref part="+3V3_20" gate="G$1" pin="+3V3"/>
<wire x1="55.88" y1="152.4" x2="55.88" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R14" gate="R" pin="P$2"/>
<pinref part="+3V3_21" gate="G$1" pin="+3V3"/>
<wire x1="104.14" y1="152.4" x2="104.14" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R15" gate="R" pin="P$2"/>
<pinref part="+3V3_22" gate="G$1" pin="+3V3"/>
<wire x1="106.68" y1="152.4" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R16" gate="R" pin="P$2"/>
<pinref part="+3V3_23" gate="G$1" pin="+3V3"/>
<wire x1="109.22" y1="152.4" x2="109.22" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R17" gate="R" pin="P$2"/>
<pinref part="+3V3_24" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="152.4" x2="111.76" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R18" gate="R" pin="P$2"/>
<pinref part="+3V3_25" gate="G$1" pin="+3V3"/>
<wire x1="114.3" y1="152.4" x2="114.3" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R19" gate="R" pin="P$2"/>
<pinref part="+3V3_26" gate="G$1" pin="+3V3"/>
<wire x1="116.84" y1="152.4" x2="116.84" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="R" pin="P$2"/>
<pinref part="+3V3_27" gate="G$1" pin="+3V3"/>
<wire x1="119.38" y1="152.4" x2="119.38" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3_10" gate="G$1" pin="+3V3"/>
<wire x1="170.18" y1="137.16" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
<wire x1="172.72" y1="137.16" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<pinref part="JP3" gate="JP" pin="2"/>
</segment>
<segment>
<pinref part="R22" gate="R" pin="P$2"/>
<pinref part="+3V3_30" gate="G$1" pin="+3V3"/>
<wire x1="233.68" y1="152.4" x2="233.68" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="R" pin="P$2"/>
<pinref part="+3V3_31" gate="G$1" pin="+3V3"/>
<wire x1="182.88" y1="152.4" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VCC"/>
<pinref part="+3V3_43" gate="G$1" pin="+3V3"/>
<wire x1="215.9" y1="111.76" x2="228.6" y2="111.76" width="0.1524" layer="91"/>
<wire x1="228.6" y1="111.76" x2="228.6" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="HLD"/>
<wire x1="215.9" y1="109.22" x2="228.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="228.6" y1="109.22" x2="228.6" y2="111.76" width="0.1524" layer="91"/>
<junction x="228.6" y="111.76"/>
<pinref part="C15" gate="C" pin="2"/>
<wire x1="228.6" y1="96.52" x2="228.6" y2="109.22" width="0.1524" layer="91"/>
<junction x="228.6" y="109.22"/>
</segment>
<segment>
<pinref part="U7" gate="U" pin="VOUT"/>
<pinref part="C17" gate="C" pin="2"/>
<wire x1="182.88" y1="48.26" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<wire x1="190.5" y1="48.26" x2="190.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="190.5" y1="58.42" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<junction x="190.5" y="48.26"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="1"/>
<pinref part="C18" gate="C" pin="2"/>
<wire x1="215.9" y1="50.8" x2="208.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="208.28" y1="50.8" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="3"/>
<wire x1="208.28" y1="45.72" x2="208.28" y2="40.64" width="0.1524" layer="91"/>
<wire x1="215.9" y1="45.72" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<junction x="208.28" y="45.72"/>
<pinref part="+3V3_7" gate="G$1" pin="+3V3"/>
<wire x1="208.28" y1="58.42" x2="208.28" y2="50.8" width="0.1524" layer="91"/>
<junction x="208.28" y="50.8"/>
</segment>
<segment>
<pinref part="R25" gate="R" pin="P$2"/>
<pinref part="+3V3_46" gate="G$1" pin="+3V3"/>
<wire x1="114.3" y1="58.42" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH1"/>
<pinref part="J3" gate="G$1" pin="DAT2"/>
<wire x1="73.66" y1="124.46" x2="40.64" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="DAT3"/>
<wire x1="40.64" y1="121.92" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="CH2"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="CMD"/>
<pinref part="U4" gate="A" pin="CH3"/>
<wire x1="40.64" y1="119.38" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="CLK"/>
<wire x1="40.64" y1="114.3" x2="45.72" y2="114.3" width="0.1524" layer="91"/>
<wire x1="45.72" y1="114.3" x2="45.72" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="CH4"/>
<wire x1="45.72" y1="116.84" x2="73.66" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="DAT0"/>
<wire x1="40.64" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<wire x1="48.26" y1="109.22" x2="48.26" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="CH5"/>
<wire x1="48.26" y1="114.3" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="DAT1"/>
<wire x1="40.64" y1="106.68" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<wire x1="50.8" y1="106.68" x2="50.8" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="CH6"/>
<wire x1="50.8" y1="111.76" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="/CD"/>
<wire x1="40.64" y1="104.14" x2="53.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="53.34" y1="104.14" x2="53.34" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="CH7"/>
<wire x1="53.34" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="SW1" gate="SW" pin="A"/>
<pinref part="U4" gate="A" pin="CH8"/>
<wire x1="68.58" y1="106.68" x2="73.66" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="VDD"/>
<pinref part="T1" gate="T" pin="D"/>
<wire x1="40.64" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="43.18" y1="116.84" x2="43.18" y2="127" width="0.1524" layer="91"/>
<pinref part="C14" gate="C" pin="2"/>
<wire x1="43.18" y1="96.52" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<junction x="43.18" y="116.84"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="T1" gate="T" pin="G"/>
<pinref part="R13" gate="R" pin="P$1"/>
<wire x1="48.26" y1="132.08" x2="50.8" y2="132.08" width="0.1524" layer="91"/>
<wire x1="50.8" y1="132.08" x2="50.8" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C13" gate="C" pin="1"/>
<wire x1="55.88" y1="139.7" x2="55.88" y2="132.08" width="0.1524" layer="91"/>
<wire x1="55.88" y1="132.08" x2="50.8" y2="132.08" width="0.1524" layer="91"/>
<junction x="50.8" y="132.08"/>
<pinref part="R23" gate="R" pin="P$1"/>
<wire x1="60.96" y1="132.08" x2="55.88" y2="132.08" width="0.1524" layer="91"/>
<junction x="55.88" y="132.08"/>
</segment>
</net>
<net name="SD_D2" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH1_2"/>
<pinref part="R14" gate="R" pin="P$1"/>
<wire x1="99.06" y1="124.46" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="137.16" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<junction x="104.14" y="124.46"/>
<label x="137.16" y="124.46" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_D3" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH2_2"/>
<pinref part="R15" gate="R" pin="P$1"/>
<wire x1="99.06" y1="121.92" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
<wire x1="106.68" y1="121.92" x2="106.68" y2="137.16" width="0.1524" layer="91"/>
<wire x1="106.68" y1="121.92" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
<junction x="106.68" y="121.92"/>
<label x="137.16" y="121.92" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_CMD" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH3_2"/>
<pinref part="R16" gate="R" pin="P$1"/>
<wire x1="99.06" y1="119.38" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<wire x1="109.22" y1="119.38" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<junction x="109.22" y="119.38"/>
<label x="137.16" y="119.38" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_D0" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH5_2"/>
<pinref part="R17" gate="R" pin="P$1"/>
<wire x1="99.06" y1="114.3" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
<wire x1="111.76" y1="114.3" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="111.76" y1="114.3" x2="137.16" y2="114.3" width="0.1524" layer="91"/>
<junction x="111.76" y="114.3"/>
<label x="137.16" y="114.3" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_D1" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH6_2"/>
<pinref part="R18" gate="R" pin="P$1"/>
<wire x1="99.06" y1="111.76" x2="114.3" y2="111.76" width="0.1524" layer="91"/>
<wire x1="114.3" y1="111.76" x2="114.3" y2="137.16" width="0.1524" layer="91"/>
<wire x1="114.3" y1="111.76" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<junction x="114.3" y="111.76"/>
<label x="137.16" y="111.76" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_CD" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH7_2"/>
<pinref part="R19" gate="R" pin="P$1"/>
<wire x1="99.06" y1="109.22" x2="116.84" y2="109.22" width="0.1524" layer="91"/>
<wire x1="116.84" y1="109.22" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<wire x1="116.84" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<junction x="116.84" y="109.22"/>
<label x="137.16" y="109.22" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="BUTTON" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH8_2"/>
<pinref part="R20" gate="R" pin="P$1"/>
<wire x1="99.06" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<wire x1="119.38" y1="106.68" x2="119.38" y2="137.16" width="0.1524" layer="91"/>
<wire x1="119.38" y1="106.68" x2="137.16" y2="106.68" width="0.1524" layer="91"/>
<junction x="119.38" y="106.68"/>
<label x="137.16" y="106.68" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_CLK" class="0">
<segment>
<pinref part="U4" gate="A" pin="CH4_2"/>
<wire x1="99.06" y1="116.84" x2="137.16" y2="116.84" width="0.1524" layer="91"/>
<label x="137.16" y="116.84" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SD_PWR" class="0">
<segment>
<pinref part="R23" gate="R" pin="P$2"/>
<wire x1="71.12" y1="132.08" x2="137.16" y2="132.08" width="0.1524" layer="91"/>
<label x="137.16" y="132.08" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="WP"/>
<pinref part="R24" gate="R" pin="P$2"/>
<wire x1="195.58" y1="106.68" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="172.72" y1="106.68" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<wire x1="170.18" y1="134.62" x2="172.72" y2="134.62" width="0.1524" layer="91"/>
<wire x1="172.72" y1="134.62" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<junction x="172.72" y="106.68"/>
<pinref part="JP3" gate="JP" pin="1"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SCK"/>
<pinref part="R22" gate="R" pin="P$1"/>
<wire x1="215.9" y1="106.68" x2="233.68" y2="106.68" width="0.1524" layer="91"/>
<wire x1="233.68" y1="106.68" x2="233.68" y2="132.08" width="0.1524" layer="91"/>
<wire x1="233.68" y1="106.68" x2="246.38" y2="106.68" width="0.1524" layer="91"/>
<junction x="233.68" y="106.68"/>
<label x="246.38" y="106.68" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="CS"/>
<pinref part="R21" gate="R" pin="P$1"/>
<wire x1="195.58" y1="111.76" x2="182.88" y2="111.76" width="0.1524" layer="91"/>
<wire x1="182.88" y1="111.76" x2="182.88" y2="132.08" width="0.1524" layer="91"/>
<wire x1="182.88" y1="111.76" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<junction x="182.88" y="111.76"/>
<label x="157.48" y="111.76" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="MISO"/>
<wire x1="195.58" y1="109.22" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<label x="157.48" y="109.22" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="MOSI"/>
<wire x1="215.9" y1="104.14" x2="246.38" y2="104.14" width="0.1524" layer="91"/>
<label x="246.38" y="104.14" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U7" gate="U" pin="EN"/>
<wire x1="162.56" y1="43.18" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
<wire x1="154.94" y1="43.18" x2="154.94" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U7" gate="U" pin="VIN"/>
<wire x1="154.94" y1="48.26" x2="154.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="162.56" y1="48.26" x2="154.94" y2="48.26" width="0.1524" layer="91"/>
<junction x="154.94" y="48.26"/>
<pinref part="C16" gate="C" pin="2"/>
<wire x1="154.94" y1="40.64" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
<junction x="154.94" y="43.18"/>
<pinref part="P+8" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="+5V"/>
<pinref part="+5V_1" gate="1" pin="+5V"/>
<wire x1="22.86" y1="22.86" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+5V_14" gate="1" pin="+5V"/>
<pinref part="JP4" gate="JP" pin="2"/>
<wire x1="144.78" y1="45.72" x2="142.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="142.24" y1="45.72" x2="142.24" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ZX_D7" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D7"/>
<wire x1="22.86" y1="55.88" x2="22.86" y2="66.04" width="0.1524" layer="91"/>
<label x="22.86" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D0" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D0"/>
<wire x1="30.48" y1="55.88" x2="30.48" y2="66.04" width="0.1524" layer="91"/>
<label x="30.48" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D1" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D1"/>
<wire x1="33.02" y1="55.88" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<label x="33.02" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D2" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D2"/>
<wire x1="35.56" y1="55.88" x2="35.56" y2="66.04" width="0.1524" layer="91"/>
<label x="35.56" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D6" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D6"/>
<wire x1="38.1" y1="55.88" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
<label x="38.1" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D5" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D5"/>
<wire x1="40.64" y1="55.88" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<label x="40.64" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D3" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D3"/>
<wire x1="43.18" y1="55.88" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<label x="43.18" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_D4" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D4"/>
<wire x1="45.72" y1="55.88" x2="45.72" y2="66.04" width="0.1524" layer="91"/>
<label x="45.72" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_A15" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A15"/>
<wire x1="17.78" y1="55.88" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<label x="17.78" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_A13" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A13"/>
<wire x1="20.32" y1="55.88" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<label x="20.32" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_A12" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A12"/>
<wire x1="20.32" y1="30.48" x2="20.32" y2="15.24" width="0.1524" layer="91"/>
<label x="20.32" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A14" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A14"/>
<wire x1="17.78" y1="30.48" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<label x="17.78" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_CLK" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/CLK"/>
<wire x1="35.56" y1="30.48" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<label x="35.56" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A0" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A0"/>
<wire x1="38.1" y1="30.48" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<label x="38.1" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A1" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A1"/>
<wire x1="40.64" y1="30.48" x2="40.64" y2="15.24" width="0.1524" layer="91"/>
<label x="40.64" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A2" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A2"/>
<wire x1="43.18" y1="30.48" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<label x="43.18" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A3" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A3"/>
<wire x1="45.72" y1="30.48" x2="45.72" y2="15.24" width="0.1524" layer="91"/>
<label x="45.72" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_BUSRQ" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/BUSRQ"/>
<wire x1="63.5" y1="30.48" x2="63.5" y2="15.24" width="0.1524" layer="91"/>
<label x="63.5" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_RST" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/RST"/>
<wire x1="66.04" y1="30.48" x2="66.04" y2="15.24" width="0.1524" layer="91"/>
<label x="66.04" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A7" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A7"/>
<wire x1="68.58" y1="30.48" x2="68.58" y2="15.24" width="0.1524" layer="91"/>
<label x="68.58" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A6" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A6"/>
<wire x1="71.12" y1="30.48" x2="71.12" y2="15.24" width="0.1524" layer="91"/>
<label x="71.12" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A5" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A5"/>
<wire x1="73.66" y1="30.48" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<label x="73.66" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A4" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A4"/>
<wire x1="76.2" y1="30.48" x2="76.2" y2="15.24" width="0.1524" layer="91"/>
<label x="76.2" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_ROMCS" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/ROMCS"/>
<wire x1="78.74" y1="30.48" x2="78.74" y2="15.24" width="0.1524" layer="91"/>
<label x="78.74" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_BUSACK" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/BUSACK"/>
<wire x1="81.28" y1="30.48" x2="81.28" y2="15.24" width="0.1524" layer="91"/>
<label x="81.28" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A9" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A9"/>
<wire x1="83.82" y1="30.48" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<label x="83.82" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A11" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A11"/>
<wire x1="86.36" y1="30.48" x2="86.36" y2="15.24" width="0.1524" layer="91"/>
<label x="86.36" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_NMI" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/NMI"/>
<wire x1="50.8" y1="55.88" x2="50.8" y2="66.04" width="0.1524" layer="91"/>
<label x="50.8" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_HALT" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/HALT"/>
<wire x1="53.34" y1="55.88" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<label x="53.34" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_MREQ" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/MREQ"/>
<wire x1="55.88" y1="55.88" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
<label x="55.88" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_IORQ" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/IORQ"/>
<wire x1="58.42" y1="55.88" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<label x="58.42" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_RD" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/RD"/>
<wire x1="60.96" y1="55.88" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
<label x="60.96" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_WR" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/WR"/>
<wire x1="63.5" y1="55.88" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<label x="63.5" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_WAIT" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/WAIT"/>
<wire x1="68.58" y1="55.88" x2="68.58" y2="66.04" width="0.1524" layer="91"/>
<label x="68.58" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_M1" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="M1"/>
<wire x1="76.2" y1="55.88" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<label x="76.2" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_A8" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A8"/>
<wire x1="81.28" y1="55.88" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<label x="81.28" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_A10" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="A10"/>
<wire x1="83.82" y1="55.88" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<label x="83.82" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="VIDEO"/>
<wire x1="53.34" y1="22.86" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="JP5" gate="JP" pin="1"/>
<wire x1="55.88" y1="22.86" x2="53.34" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ZX_OE2" class="0">
<segment>
<wire x1="53.34" y1="20.32" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<label x="53.34" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
<pinref part="JP5" gate="JP" pin="2"/>
<wire x1="55.88" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+9V" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="+9V"/>
<pinref part="+9V_1" gate="G$1" pin="+9V"/>
<wire x1="25.4" y1="22.86" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+1V2" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="5"/>
<pinref part="C19" gate="C" pin="2"/>
<wire x1="236.22" y1="50.8" x2="241.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="241.3" y1="50.8" x2="241.3" y2="40.64" width="0.1524" layer="91"/>
<pinref part="+1V2_1" gate="G$1" pin="+1V2"/>
<wire x1="241.3" y1="58.42" x2="241.3" y2="50.8" width="0.1524" layer="91"/>
<junction x="241.3" y="50.8"/>
</segment>
</net>
<net name="CDONE" class="0">
<segment>
<pinref part="LED1" gate="LED" pin="A"/>
<pinref part="R25" gate="R" pin="P$1"/>
<wire x1="114.3" y1="27.94" x2="114.3" y2="40.64" width="0.1524" layer="91"/>
<wire x1="114.3" y1="40.64" x2="114.3" y2="45.72" width="0.1524" layer="91"/>
<wire x1="114.3" y1="40.64" x2="109.22" y2="40.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="40.64" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<junction x="114.3" y="40.64"/>
<label x="109.22" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LED2" gate="LED" pin="A"/>
<pinref part="R26" gate="R" pin="P$1"/>
<wire x1="119.38" y1="27.94" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="LED3" gate="LED" pin="A"/>
<pinref part="R27" gate="R" pin="P$1"/>
<wire x1="124.46" y1="27.94" x2="124.46" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="R26" gate="R" pin="P$2"/>
<wire x1="119.38" y1="55.88" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<label x="119.38" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<pinref part="R27" gate="R" pin="P$2"/>
<wire x1="124.46" y1="55.88" x2="124.46" y2="66.04" width="0.1524" layer="91"/>
<label x="124.46" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_OE" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/OE"/>
<wire x1="25.4" y1="55.88" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<label x="25.4" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_IORQGE" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="IORQGE"/>
<wire x1="48.26" y1="30.48" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
<label x="48.26" y="15.24" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_INT" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/INT"/>
<wire x1="48.26" y1="55.88" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<label x="48.26" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ZX_RFSH" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="/RFSH"/>
<wire x1="78.74" y1="55.88" x2="78.74" y2="66.04" width="0.1524" layer="91"/>
<label x="78.74" y="66.04" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>connector</description>
<plain>
<wire x1="7.62" y1="157.48" x2="254" y2="157.48" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="27.94" x2="7.62" y2="157.48" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="154.94" size="5.08" layer="94" font="vector" rot="MR180">LEVEL SHIFTER</text>
<wire x1="7.62" y1="27.94" x2="254" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="254" y1="27.94" x2="254" y2="157.48" width="0.1524" layer="94" style="longdash"/>
</plain>
<instances>
<instance part="U9" gate="G$1" x="71.12" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="69.596" y="86.614" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="80.518" y="86.106" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U8" gate="G$1" x="27.94" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="26.416" y="86.614" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="37.338" y="86.106" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U10" gate="G$1" x="114.3" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="112.776" y="86.614" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="123.698" y="86.106" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U11" gate="G$1" x="157.48" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="155.956" y="86.614" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="166.878" y="86.106" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U12" gate="G$1" x="228.6" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="227.076" y="86.614" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="237.998" y="86.106" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND_12" gate="1" x="58.42" y="30.48"/>
<instance part="GND_13" gate="1" x="55.88" y="30.48"/>
<instance part="GND_14" gate="1" x="55.88" y="119.38" rot="R180"/>
<instance part="GND_15" gate="1" x="58.42" y="119.38" rot="R180"/>
<instance part="+3V3_1" gate="G$1" x="86.36" y="119.38"/>
<instance part="GND_16" gate="1" x="15.24" y="30.48"/>
<instance part="GND_17" gate="1" x="12.7" y="30.48"/>
<instance part="GND_18" gate="1" x="12.7" y="119.38" rot="R180"/>
<instance part="GND_19" gate="1" x="15.24" y="119.38" rot="R180"/>
<instance part="GND_20" gate="1" x="40.64" y="119.38" rot="R180"/>
<instance part="GND_21" gate="1" x="38.1" y="119.38" rot="R180"/>
<instance part="+3V3_2" gate="G$1" x="43.18" y="119.38"/>
<instance part="GND_22" gate="1" x="99.06" y="30.48"/>
<instance part="GND_23" gate="1" x="101.6" y="30.48"/>
<instance part="GND_24" gate="1" x="99.06" y="119.38" rot="R180"/>
<instance part="GND_25" gate="1" x="101.6" y="119.38" rot="R180"/>
<instance part="+3V3_3" gate="G$1" x="129.54" y="119.38"/>
<instance part="GND_26" gate="1" x="124.46" y="119.38" rot="R180"/>
<instance part="GND_27" gate="1" x="127" y="119.38" rot="R180"/>
<instance part="GND_28" gate="1" x="142.24" y="30.48"/>
<instance part="GND_29" gate="1" x="144.78" y="30.48"/>
<instance part="GND_30" gate="1" x="142.24" y="119.38" rot="R180"/>
<instance part="GND_31" gate="1" x="144.78" y="119.38" rot="R180"/>
<instance part="+3V3_4" gate="G$1" x="172.72" y="119.38"/>
<instance part="+3V3_5" gate="G$1" x="167.64" y="119.38"/>
<instance part="GND_35" gate="1" x="213.36" y="119.38" rot="R180"/>
<instance part="GND_36" gate="1" x="215.9" y="119.38" rot="R180"/>
<instance part="GND_37" gate="1" x="213.36" y="30.48"/>
<instance part="GND_38" gate="1" x="215.9" y="30.48"/>
<instance part="GND_39" gate="1" x="241.3" y="119.38" rot="R180"/>
<instance part="GND_40" gate="1" x="238.76" y="119.38" rot="R180"/>
<instance part="+3V3_6" gate="G$1" x="243.84" y="119.38"/>
<instance part="T2" gate="T" x="195.58" y="63.5" smashed="yes">
<attribute name="VALUE" x="200.66" y="58.42" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="198.628" y="66.04" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND_43" gate="1" x="198.12" y="30.48"/>
<instance part="R34" gate="R" x="198.12" y="91.44" rot="R90"/>
<instance part="R35" gate="R" x="190.5" y="73.66" rot="R270"/>
<instance part="R33" gate="R" x="185.42" y="91.44" rot="R90"/>
<instance part="+5V0A_1" gate="G$1" x="170.18" y="30.48" rot="R180"/>
<instance part="+5V0A_2" gate="G$1" x="172.72" y="30.48" rot="R180"/>
<instance part="+5V0A_3" gate="G$1" x="241.3" y="30.48" rot="R180"/>
<instance part="+5V0A_4" gate="G$1" x="243.84" y="30.48" rot="R180"/>
<instance part="+5V0A_5" gate="G$1" x="40.64" y="30.48" rot="R180"/>
<instance part="+5V0A_6" gate="G$1" x="43.18" y="30.48" rot="R180"/>
<instance part="+5V0A_7" gate="G$1" x="127" y="30.48" rot="R180"/>
<instance part="+5V0A_8" gate="G$1" x="129.54" y="30.48" rot="R180"/>
<instance part="+5V0A_9" gate="G$1" x="83.82" y="30.48" rot="R180"/>
<instance part="+5V0A_10" gate="G$1" x="86.36" y="30.48" rot="R180"/>
<instance part="+5V0A_11" gate="G$1" x="185.42" y="119.38"/>
<instance part="+5V0A_12" gate="G$1" x="198.12" y="119.38"/>
<instance part="C22" gate="C" x="48.26" y="99.06" rot="R90"/>
<instance part="C27" gate="C" x="48.26" y="73.66" rot="R90"/>
<instance part="GND_78" gate="1" x="50.8" y="30.48"/>
<instance part="C23" gate="C" x="91.44" y="99.06" rot="R90"/>
<instance part="C28" gate="C" x="91.44" y="73.66" rot="R90"/>
<instance part="C24" gate="C" x="134.62" y="99.06" rot="R90"/>
<instance part="C29" gate="C" x="134.62" y="73.66" rot="R90"/>
<instance part="C25" gate="C" x="177.8" y="99.06" rot="R90"/>
<instance part="C30" gate="C" x="177.8" y="73.66" rot="R90"/>
<instance part="C26" gate="C" x="248.92" y="99.06" rot="R90"/>
<instance part="C31" gate="C" x="248.92" y="73.66" rot="R90"/>
<instance part="GND_79" gate="1" x="93.98" y="30.48"/>
<instance part="GND_80" gate="1" x="137.16" y="30.48"/>
<instance part="GND_81" gate="1" x="180.34" y="30.48"/>
<instance part="GND_82" gate="1" x="251.46" y="30.48"/>
<instance part="+5V0A_13" gate="G$1" x="177.8" y="30.48" rot="R180"/>
<instance part="R28" gate="R" x="81.28" y="134.62" rot="R90"/>
<instance part="GND_83" gate="1" x="81.28" y="149.86" rot="R180"/>
<instance part="R30" gate="R" x="170.18" y="134.62" rot="R90"/>
<instance part="+3V3_52" gate="G$1" x="190.5" y="149.86"/>
<instance part="R31" gate="R" x="190.5" y="134.62" rot="R90"/>
<instance part="R32" gate="R" x="203.2" y="134.62" rot="R90"/>
<instance part="P+1" gate="1" x="203.2" y="149.86"/>
<instance part="C20" gate="C" x="165.1" y="134.62" rot="R90"/>
<instance part="C21" gate="C" x="185.42" y="134.62" rot="R90"/>
<instance part="GND_108" gate="1" x="165.1" y="127"/>
<instance part="GND_109" gate="1" x="185.42" y="127"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="D2" gate="D" x="162.56" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="162.052" y="51.562" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="162.052" y="55.118" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="D3" gate="D" x="154.94" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="154.432" y="51.562" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="154.432" y="55.118" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="D4" gate="D" x="157.48" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="156.972" y="51.562" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="156.972" y="55.118" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="D5" gate="D" x="147.32" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="146.812" y="51.562" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="146.812" y="55.118" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND_112" gate="1" x="83.82" y="119.38" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="GND@13"/>
<pinref part="GND_12" gate="1" pin="GND"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="GND@25"/>
<pinref part="GND_13" gate="1" pin="GND"/>
<wire x1="55.88" y1="33.02" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="GND@12"/>
<pinref part="GND_14" gate="1" pin="GND"/>
<wire x1="55.88" y1="116.84" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="GND@11"/>
<pinref part="GND_15" gate="1" pin="GND"/>
<wire x1="58.42" y1="116.84" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND@13"/>
<pinref part="GND_16" gate="1" pin="GND"/>
<wire x1="15.24" y1="33.02" x2="15.24" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND@25"/>
<pinref part="GND_17" gate="1" pin="GND"/>
<wire x1="12.7" y1="33.02" x2="12.7" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND@12"/>
<pinref part="GND_18" gate="1" pin="GND"/>
<wire x1="12.7" y1="116.84" x2="12.7" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND@11"/>
<pinref part="GND_19" gate="1" pin="GND"/>
<wire x1="15.24" y1="116.84" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="/OE"/>
<pinref part="GND_20" gate="1" pin="GND"/>
<wire x1="40.64" y1="116.84" x2="40.64" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="DIR"/>
<pinref part="GND_21" gate="1" pin="GND"/>
<wire x1="38.1" y1="116.84" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND@25"/>
<pinref part="GND_22" gate="1" pin="GND"/>
<wire x1="99.06" y1="33.02" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND@13"/>
<pinref part="GND_23" gate="1" pin="GND"/>
<wire x1="101.6" y1="33.02" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND@12"/>
<pinref part="GND_24" gate="1" pin="GND"/>
<wire x1="99.06" y1="116.84" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND@11"/>
<pinref part="GND_25" gate="1" pin="GND"/>
<wire x1="101.6" y1="116.84" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="DIR"/>
<pinref part="GND_26" gate="1" pin="GND"/>
<wire x1="124.46" y1="116.84" x2="124.46" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="/OE"/>
<pinref part="GND_27" gate="1" pin="GND"/>
<wire x1="127" y1="116.84" x2="127" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="GND@25"/>
<pinref part="GND_28" gate="1" pin="GND"/>
<wire x1="142.24" y1="33.02" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="GND@13"/>
<pinref part="GND_29" gate="1" pin="GND"/>
<wire x1="144.78" y1="33.02" x2="144.78" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="GND@12"/>
<pinref part="GND_30" gate="1" pin="GND"/>
<wire x1="142.24" y1="116.84" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="GND@11"/>
<pinref part="GND_31" gate="1" pin="GND"/>
<wire x1="144.78" y1="116.84" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="GND@12"/>
<pinref part="GND_35" gate="1" pin="GND"/>
<wire x1="213.36" y1="116.84" x2="213.36" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="GND@11"/>
<pinref part="GND_36" gate="1" pin="GND"/>
<wire x1="215.9" y1="116.84" x2="215.9" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="GND@25"/>
<pinref part="GND_37" gate="1" pin="GND"/>
<wire x1="213.36" y1="33.02" x2="213.36" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="GND@13"/>
<pinref part="GND_38" gate="1" pin="GND"/>
<wire x1="215.9" y1="33.02" x2="215.9" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="/OE"/>
<pinref part="GND_39" gate="1" pin="GND"/>
<wire x1="241.3" y1="116.84" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="DIR"/>
<pinref part="GND_40" gate="1" pin="GND"/>
<wire x1="238.76" y1="116.84" x2="238.76" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="T" pin="E"/>
<pinref part="GND_43" gate="1" pin="GND"/>
<wire x1="198.12" y1="58.42" x2="198.12" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND_78" gate="1" pin="GND"/>
<pinref part="C22" gate="C" pin="1"/>
<wire x1="50.8" y1="33.02" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<wire x1="50.8" y1="86.36" x2="48.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="48.26" y1="86.36" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C27" gate="C" pin="2"/>
<wire x1="48.26" y1="76.2" x2="48.26" y2="86.36" width="0.1524" layer="91"/>
<junction x="48.26" y="86.36"/>
</segment>
<segment>
<pinref part="C26" gate="C" pin="1"/>
<pinref part="C31" gate="C" pin="2"/>
<wire x1="248.92" y1="96.52" x2="248.92" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND_82" gate="1" pin="GND"/>
<wire x1="248.92" y1="86.36" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<wire x1="251.46" y1="33.02" x2="251.46" y2="86.36" width="0.1524" layer="91"/>
<wire x1="251.46" y1="86.36" x2="248.92" y2="86.36" width="0.1524" layer="91"/>
<junction x="248.92" y="86.36"/>
</segment>
<segment>
<pinref part="C30" gate="C" pin="2"/>
<pinref part="C25" gate="C" pin="1"/>
<wire x1="177.8" y1="76.2" x2="177.8" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND_81" gate="1" pin="GND"/>
<wire x1="177.8" y1="86.36" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<wire x1="180.34" y1="33.02" x2="180.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="180.34" y1="86.36" x2="177.8" y2="86.36" width="0.1524" layer="91"/>
<junction x="177.8" y="86.36"/>
</segment>
<segment>
<pinref part="C24" gate="C" pin="1"/>
<pinref part="C29" gate="C" pin="2"/>
<wire x1="134.62" y1="96.52" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND_80" gate="1" pin="GND"/>
<wire x1="134.62" y1="86.36" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="137.16" y1="33.02" x2="137.16" y2="86.36" width="0.1524" layer="91"/>
<wire x1="137.16" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<junction x="134.62" y="86.36"/>
</segment>
<segment>
<pinref part="C23" gate="C" pin="1"/>
<pinref part="C28" gate="C" pin="2"/>
<wire x1="91.44" y1="96.52" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND_79" gate="1" pin="GND"/>
<wire x1="91.44" y1="86.36" x2="91.44" y2="76.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="33.02" x2="93.98" y2="86.36" width="0.1524" layer="91"/>
<wire x1="93.98" y1="86.36" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<junction x="91.44" y="86.36"/>
</segment>
<segment>
<pinref part="R28" gate="R" pin="P$2"/>
<pinref part="GND_83" gate="1" pin="GND"/>
<wire x1="81.28" y1="147.32" x2="81.28" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C20" gate="C" pin="1"/>
<pinref part="GND_108" gate="1" pin="GND"/>
<wire x1="165.1" y1="129.54" x2="165.1" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="C" pin="1"/>
<pinref part="GND_109" gate="1" pin="GND"/>
<wire x1="185.42" y1="129.54" x2="185.42" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="/OE"/>
<pinref part="GND_112" gate="1" pin="GND"/>
<wire x1="83.82" y1="116.84" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ZX_D7" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B8"/>
<wire x1="60.96" y1="76.2" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<label x="60.96" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D0" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B7"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<label x="63.5" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D1" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B6"/>
<wire x1="66.04" y1="76.2" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<label x="66.04" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D2" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B5"/>
<wire x1="68.58" y1="76.2" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<label x="68.58" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D6" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B4"/>
<wire x1="71.12" y1="76.2" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<label x="71.12" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D5" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B3"/>
<wire x1="73.66" y1="76.2" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<label x="73.66" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D3" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B2"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="30.48" width="0.1524" layer="91"/>
<label x="76.2" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_D4" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="B1"/>
<wire x1="78.74" y1="76.2" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<label x="78.74" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R32" gate="R" pin="P$2"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="203.2" y1="139.7" x2="203.2" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="VCCA"/>
<pinref part="+3V3_2" gate="G$1" pin="+3V3"/>
<wire x1="43.18" y1="111.76" x2="43.18" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C22" gate="C" pin="2"/>
<wire x1="43.18" y1="109.22" x2="43.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="48.26" y1="101.6" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<wire x1="48.26" y1="109.22" x2="43.18" y2="109.22" width="0.1524" layer="91"/>
<junction x="43.18" y="109.22"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="VCCA"/>
<pinref part="+3V3_3" gate="G$1" pin="+3V3"/>
<wire x1="129.54" y1="111.76" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C24" gate="C" pin="2"/>
<wire x1="129.54" y1="109.22" x2="129.54" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="101.6" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<wire x1="134.62" y1="109.22" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<junction x="129.54" y="109.22"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="VCCA"/>
<pinref part="+3V3_1" gate="G$1" pin="+3V3"/>
<wire x1="86.36" y1="111.76" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C23" gate="C" pin="2"/>
<wire x1="91.44" y1="101.6" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<wire x1="91.44" y1="109.22" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<junction x="86.36" y="109.22"/>
<wire x1="86.36" y1="109.22" x2="86.36" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="VCCA"/>
<pinref part="+3V3_4" gate="G$1" pin="+3V3"/>
<wire x1="172.72" y1="111.76" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C25" gate="C" pin="2"/>
<wire x1="172.72" y1="109.22" x2="172.72" y2="96.52" width="0.1524" layer="91"/>
<wire x1="177.8" y1="101.6" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="177.8" y1="109.22" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<junction x="172.72" y="109.22"/>
<pinref part="R30" gate="R" pin="P$2"/>
<wire x1="170.18" y1="139.7" x2="170.18" y2="149.86" width="0.1524" layer="91"/>
<wire x1="170.18" y1="149.86" x2="177.8" y2="149.86" width="0.1524" layer="91"/>
<wire x1="177.8" y1="149.86" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<junction x="177.8" y="109.22"/>
<pinref part="C20" gate="C" pin="2"/>
<wire x1="165.1" y1="137.16" x2="165.1" y2="149.86" width="0.1524" layer="91"/>
<wire x1="170.18" y1="149.86" x2="165.1" y2="149.86" width="0.1524" layer="91"/>
<junction x="170.18" y="149.86"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="DIR"/>
<pinref part="+3V3_5" gate="G$1" pin="+3V3"/>
<wire x1="167.64" y1="111.76" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="VCCA"/>
<pinref part="+3V3_6" gate="G$1" pin="+3V3"/>
<wire x1="243.84" y1="111.76" x2="243.84" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C26" gate="C" pin="2"/>
<wire x1="243.84" y1="109.22" x2="243.84" y2="96.52" width="0.1524" layer="91"/>
<wire x1="248.92" y1="101.6" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<wire x1="248.92" y1="109.22" x2="243.84" y2="109.22" width="0.1524" layer="91"/>
<junction x="243.84" y="109.22"/>
</segment>
<segment>
<pinref part="R31" gate="R" pin="P$2"/>
<pinref part="+3V3_52" gate="G$1" pin="+3V3"/>
<wire x1="190.5" y1="139.7" x2="190.5" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ZX_A15" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B8"/>
<wire x1="17.78" y1="76.2" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<label x="17.78" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A13" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B7"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<label x="20.32" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A12" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B5"/>
<wire x1="25.4" y1="76.2" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<label x="25.4" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A14" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B6"/>
<wire x1="22.86" y1="76.2" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
<label x="22.86" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_CLK" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B4"/>
<wire x1="27.94" y1="76.2" x2="27.94" y2="30.48" width="0.1524" layer="91"/>
<label x="27.94" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A0" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B3"/>
<wire x1="30.48" y1="76.2" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<label x="30.48" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A1" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B2"/>
<wire x1="33.02" y1="76.2" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<label x="33.02" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A2" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="B1"/>
<wire x1="35.56" y1="76.2" x2="35.56" y2="30.48" width="0.1524" layer="91"/>
<label x="35.56" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A3" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B8"/>
<wire x1="104.14" y1="76.2" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<label x="104.14" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_BUSRQ" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B6"/>
<wire x1="152.4" y1="76.2" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
<label x="152.4" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_RST" class="0">
<segment>
<pinref part="T2" gate="T" pin="C"/>
<pinref part="R34" gate="R" pin="P$1"/>
<wire x1="198.12" y1="86.36" x2="198.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="198.12" y1="81.28" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="198.12" y1="81.28" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="198.12" y="81.28"/>
<label x="208.28" y="81.28" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="ZX_A7" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B2"/>
<wire x1="119.38" y1="76.2" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<label x="119.38" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A6" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B1"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="30.48" width="0.1524" layer="91"/>
<label x="121.92" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A5" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B8"/>
<wire x1="218.44" y1="76.2" x2="218.44" y2="30.48" width="0.1524" layer="91"/>
<label x="218.44" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A4" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B6"/>
<wire x1="223.52" y1="76.2" x2="223.52" y2="30.48" width="0.1524" layer="91"/>
<label x="223.52" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_BUSACK" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B5"/>
<wire x1="226.06" y1="76.2" x2="226.06" y2="30.48" width="0.1524" layer="91"/>
<label x="226.06" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A9" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B4"/>
<wire x1="228.6" y1="76.2" x2="228.6" y2="30.48" width="0.1524" layer="91"/>
<label x="228.6" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A11" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B3"/>
<wire x1="231.14" y1="76.2" x2="231.14" y2="30.48" width="0.1524" layer="91"/>
<label x="231.14" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="N$472" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B8"/>
<wire x1="147.32" y1="76.2" x2="147.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D5" gate="D" pin="A"/>
</segment>
</net>
<net name="ZX_NMI" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B7"/>
<wire x1="149.86" y1="76.2" x2="149.86" y2="30.48" width="0.1524" layer="91"/>
<label x="149.86" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_HALT" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B7"/>
<wire x1="106.68" y1="76.2" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<label x="106.68" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_MREQ" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B6"/>
<wire x1="109.22" y1="76.2" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<label x="109.22" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_IORQ" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B5"/>
<wire x1="111.76" y1="76.2" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
<label x="111.76" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_RD" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B4"/>
<wire x1="114.3" y1="76.2" x2="114.3" y2="30.48" width="0.1524" layer="91"/>
<label x="114.3" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_WR" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="B3"/>
<wire x1="116.84" y1="76.2" x2="116.84" y2="30.48" width="0.1524" layer="91"/>
<label x="116.84" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_WAIT" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B3"/>
<wire x1="160.02" y1="76.2" x2="160.02" y2="30.48" width="0.1524" layer="91"/>
<label x="160.02" y="30.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ZX_M1" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B7"/>
<wire x1="220.98" y1="76.2" x2="220.98" y2="30.48" width="0.1524" layer="91"/>
<label x="220.98" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A8" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B2"/>
<wire x1="233.68" y1="76.2" x2="233.68" y2="30.48" width="0.1524" layer="91"/>
<label x="233.68" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_A10" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="B1"/>
<wire x1="236.22" y1="76.2" x2="236.22" y2="30.48" width="0.1524" layer="91"/>
<label x="236.22" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="N$470" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B5"/>
<wire x1="154.94" y1="76.2" x2="154.94" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D3" gate="D" pin="A"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="T2" gate="T" pin="B"/>
<pinref part="R35" gate="R" pin="P$2"/>
<wire x1="190.5" y1="68.58" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="190.5" y1="63.5" x2="193.04" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R35" gate="R" pin="P$1"/>
<pinref part="R33" gate="R" pin="P$1"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="83.82" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="185.42" y1="83.82" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="B1"/>
<wire x1="165.1" y1="76.2" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="165.1" y1="63.5" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="185.42" y1="63.5" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<junction x="185.42" y="83.82"/>
</segment>
</net>
<net name="+5V0A" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="VCCB@23"/>
<wire x1="170.18" y1="38.1" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+5V0A_1" gate="G$1" pin="+5V0A"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="VCCB@24"/>
<wire x1="172.72" y1="38.1" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+5V0A_2" gate="G$1" pin="+5V0A"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="VCCB@23"/>
<wire x1="241.3" y1="38.1" x2="241.3" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+5V0A_3" gate="G$1" pin="+5V0A"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="VCCB@24"/>
<wire x1="243.84" y1="38.1" x2="243.84" y2="66.04" width="0.1524" layer="91"/>
<pinref part="+5V0A_4" gate="G$1" pin="+5V0A"/>
<pinref part="C31" gate="C" pin="1"/>
<wire x1="243.84" y1="66.04" x2="243.84" y2="76.2" width="0.1524" layer="91"/>
<wire x1="248.92" y1="71.12" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<wire x1="248.92" y1="66.04" x2="243.84" y2="66.04" width="0.1524" layer="91"/>
<junction x="243.84" y="66.04"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VCCB@23"/>
<wire x1="40.64" y1="38.1" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+5V0A_5" gate="G$1" pin="+5V0A"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VCCB@24"/>
<wire x1="43.18" y1="38.1" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<pinref part="+5V0A_6" gate="G$1" pin="+5V0A"/>
<pinref part="C27" gate="C" pin="1"/>
<wire x1="43.18" y1="66.04" x2="43.18" y2="76.2" width="0.1524" layer="91"/>
<wire x1="48.26" y1="71.12" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<wire x1="48.26" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<junction x="43.18" y="66.04"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="VCCB@23"/>
<wire x1="127" y1="38.1" x2="127" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+5V0A_7" gate="G$1" pin="+5V0A"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="VCCB@24"/>
<wire x1="129.54" y1="38.1" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<pinref part="+5V0A_8" gate="G$1" pin="+5V0A"/>
<pinref part="C29" gate="C" pin="1"/>
<wire x1="129.54" y1="66.04" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="134.62" y1="71.12" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="134.62" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="129.54" y="66.04"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="VCCB@23"/>
<wire x1="83.82" y1="38.1" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+5V0A_9" gate="G$1" pin="+5V0A"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="VCCB@24"/>
<wire x1="86.36" y1="38.1" x2="86.36" y2="66.04" width="0.1524" layer="91"/>
<pinref part="+5V0A_10" gate="G$1" pin="+5V0A"/>
<pinref part="C28" gate="C" pin="1"/>
<wire x1="86.36" y1="66.04" x2="86.36" y2="76.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="71.12" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
<wire x1="91.44" y1="66.04" x2="86.36" y2="66.04" width="0.1524" layer="91"/>
<junction x="86.36" y="66.04"/>
</segment>
<segment>
<pinref part="R33" gate="R" pin="P$2"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="101.6" width="0.1524" layer="91"/>
<pinref part="+5V0A_11" gate="G$1" pin="+5V0A"/>
<pinref part="R31" gate="R" pin="P$1"/>
<wire x1="185.42" y1="101.6" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
<wire x1="190.5" y1="129.54" x2="190.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="190.5" y1="101.6" x2="185.42" y2="101.6" width="0.1524" layer="91"/>
<junction x="185.42" y="101.6"/>
<wire x1="185.42" y1="101.6" x2="180.34" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C21" gate="C" pin="2"/>
<wire x1="185.42" y1="137.16" x2="185.42" y2="149.86" width="0.1524" layer="91"/>
<wire x1="180.34" y1="101.6" x2="180.34" y2="149.86" width="0.1524" layer="91"/>
<wire x1="180.34" y1="149.86" x2="185.42" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R34" gate="R" pin="P$2"/>
<wire x1="198.12" y1="96.52" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<pinref part="+5V0A_12" gate="G$1" pin="+5V0A"/>
<pinref part="R32" gate="R" pin="P$1"/>
<wire x1="198.12" y1="101.6" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="203.2" y1="129.54" x2="203.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="203.2" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<junction x="198.12" y="101.6"/>
</segment>
<segment>
<pinref part="C30" gate="C" pin="1"/>
<pinref part="+5V0A_13" gate="G$1" pin="+5V0A"/>
<wire x1="177.8" y1="71.12" x2="177.8" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="F_A3" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A8"/>
<wire x1="104.14" y1="96.52" x2="104.14" y2="119.38" width="0.1524" layer="91"/>
<label x="104.14" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_HALT" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A7"/>
<wire x1="106.68" y1="96.52" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="106.68" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_MREQ" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A6"/>
<wire x1="109.22" y1="96.52" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<label x="109.22" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_IORQ" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A5"/>
<wire x1="111.76" y1="96.52" x2="111.76" y2="119.38" width="0.1524" layer="91"/>
<label x="111.76" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_RD" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A4"/>
<wire x1="114.3" y1="96.52" x2="114.3" y2="119.38" width="0.1524" layer="91"/>
<label x="114.3" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_WR" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A3"/>
<wire x1="116.84" y1="96.52" x2="116.84" y2="119.38" width="0.1524" layer="91"/>
<label x="116.84" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A7" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A2"/>
<wire x1="119.38" y1="96.52" x2="119.38" y2="119.38" width="0.1524" layer="91"/>
<label x="119.38" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A6" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A1"/>
<wire x1="121.92" y1="96.52" x2="121.92" y2="119.38" width="0.1524" layer="91"/>
<label x="121.92" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D_DIR" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="DIR"/>
<pinref part="R28" gate="R" pin="P$1"/>
<wire x1="81.28" y1="96.52" x2="81.28" y2="129.54" width="0.1524" layer="91"/>
<label x="81.28" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_O_OE" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="/OE"/>
<pinref part="R30" gate="R" pin="P$1"/>
<wire x1="170.18" y1="96.52" x2="170.18" y2="129.54" width="0.1524" layer="91"/>
<label x="170.18" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D4" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A1"/>
<wire x1="78.74" y1="96.52" x2="78.74" y2="119.38" width="0.1524" layer="91"/>
<label x="78.74" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D3" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A2"/>
<wire x1="76.2" y1="96.52" x2="76.2" y2="119.38" width="0.1524" layer="91"/>
<label x="76.2" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_CLK" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A4"/>
<wire x1="27.94" y1="96.52" x2="27.94" y2="119.38" width="0.1524" layer="91"/>
<label x="27.94" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A15" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A8"/>
<wire x1="17.78" y1="96.52" x2="17.78" y2="119.38" width="0.1524" layer="91"/>
<label x="17.78" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A13" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A7"/>
<wire x1="20.32" y1="96.52" x2="20.32" y2="119.38" width="0.1524" layer="91"/>
<label x="20.32" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A14" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A6"/>
<wire x1="22.86" y1="96.52" x2="22.86" y2="119.38" width="0.1524" layer="91"/>
<label x="22.86" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A12" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A5"/>
<wire x1="25.4" y1="96.52" x2="25.4" y2="119.38" width="0.1524" layer="91"/>
<label x="25.4" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A0" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A3"/>
<wire x1="30.48" y1="96.52" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
<label x="30.48" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A1" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A2"/>
<wire x1="33.02" y1="96.52" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<label x="33.02" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A2" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="A1"/>
<wire x1="35.56" y1="96.52" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D7" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A8"/>
<wire x1="60.96" y1="96.52" x2="60.96" y2="119.38" width="0.1524" layer="91"/>
<label x="60.96" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D0" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A7"/>
<wire x1="63.5" y1="96.52" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<label x="63.5" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D5" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A3"/>
<wire x1="73.66" y1="96.52" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
<label x="73.66" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D6" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A4"/>
<wire x1="71.12" y1="96.52" x2="71.12" y2="119.38" width="0.1524" layer="91"/>
<label x="71.12" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D2" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A5"/>
<wire x1="68.58" y1="96.52" x2="68.58" y2="119.38" width="0.1524" layer="91"/>
<label x="68.58" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_D1" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A6"/>
<wire x1="66.04" y1="96.52" x2="66.04" y2="119.38" width="0.1524" layer="91"/>
<label x="66.04" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_IORQGE" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A8"/>
<wire x1="147.32" y1="96.52" x2="147.32" y2="119.38" width="0.1524" layer="91"/>
<label x="147.32" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_NMI" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A7"/>
<wire x1="149.86" y1="96.52" x2="149.86" y2="119.38" width="0.1524" layer="91"/>
<label x="149.86" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_BUSRQ" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A6"/>
<wire x1="152.4" y1="96.52" x2="152.4" y2="119.38" width="0.1524" layer="91"/>
<label x="152.4" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_OE" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A5"/>
<wire x1="154.94" y1="96.52" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<label x="154.94" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_OE2" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A4"/>
<wire x1="157.48" y1="96.52" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<label x="157.48" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_WAIT" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A3"/>
<wire x1="160.02" y1="96.52" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
<label x="160.02" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_ROMCS" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A2"/>
<wire x1="162.56" y1="96.52" x2="162.56" y2="119.38" width="0.1524" layer="91"/>
<label x="162.56" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_RST" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="A1"/>
<wire x1="165.1" y1="96.52" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<label x="165.1" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A5" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A8"/>
<wire x1="218.44" y1="96.52" x2="218.44" y2="119.38" width="0.1524" layer="91"/>
<label x="218.44" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_M1" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A7"/>
<wire x1="220.98" y1="96.52" x2="220.98" y2="119.38" width="0.1524" layer="91"/>
<label x="220.98" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A4" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A6"/>
<wire x1="223.52" y1="96.52" x2="223.52" y2="119.38" width="0.1524" layer="91"/>
<label x="223.52" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_BUSACK" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A5"/>
<wire x1="226.06" y1="96.52" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
<label x="226.06" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A9" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A4"/>
<wire x1="228.6" y1="96.52" x2="228.6" y2="119.38" width="0.1524" layer="91"/>
<label x="228.6" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A11" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A3"/>
<wire x1="231.14" y1="96.52" x2="231.14" y2="119.38" width="0.1524" layer="91"/>
<label x="231.14" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A8" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A2"/>
<wire x1="233.68" y1="96.52" x2="233.68" y2="119.38" width="0.1524" layer="91"/>
<label x="233.68" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A10" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="A1"/>
<wire x1="236.22" y1="96.52" x2="236.22" y2="119.38" width="0.1524" layer="91"/>
<label x="236.22" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B2"/>
<pinref part="D2" gate="D" pin="A"/>
<wire x1="162.56" y1="55.88" x2="162.56" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ZX_ROMCS" class="0">
<segment>
<pinref part="D2" gate="D" pin="C"/>
<wire x1="162.56" y1="50.8" x2="162.56" y2="30.48" width="0.1524" layer="91"/>
<label x="162.56" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_OE" class="0">
<segment>
<pinref part="D3" gate="D" pin="C"/>
<wire x1="154.94" y1="50.8" x2="154.94" y2="30.48" width="0.1524" layer="91"/>
<label x="154.94" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="N$471" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="B4"/>
<wire x1="157.48" y1="76.2" x2="157.48" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D4" gate="D" pin="A"/>
</segment>
</net>
<net name="ZX_OE2" class="0">
<segment>
<pinref part="D4" gate="D" pin="C"/>
<wire x1="157.48" y1="50.8" x2="157.48" y2="30.48" width="0.1524" layer="91"/>
<label x="157.48" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_IORQGE" class="0">
<segment>
<pinref part="D5" gate="D" pin="C"/>
<wire x1="147.32" y1="50.8" x2="147.32" y2="30.48" width="0.1524" layer="91"/>
<label x="147.32" y="30.48" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="7.62" y1="170.18" x2="254" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="30.48" x2="7.62" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">ETHERNET</text>
<wire x1="7.62" y1="30.48" x2="254" y2="30.48" width="0.1524" layer="94" style="longdash"/>
<wire x1="254" y1="30.48" x2="254" y2="170.18" width="0.1524" layer="94" style="longdash"/>
</plain>
<instances>
<instance part="J5" gate="G$1" x="200.66" y="66.04" smashed="yes">
<attribute name="NAME" x="205.232" y="84.328" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="205.232" y="45.212" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND_1" gate="1" x="205.74" y="38.1"/>
<instance part="GND_2" gate="1" x="205.74" y="91.44" rot="R180"/>
<instance part="U13" gate="G$1" x="81.28" y="78.74" smashed="yes">
<attribute name="NAME" x="78.994" y="122.428" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="95.25" y="39.878" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND_87" gate="1" x="63.5" y="38.1"/>
<instance part="C41" gate="C" x="50.8" y="96.52"/>
<instance part="GND_92" gate="1" x="40.64" y="96.52" rot="R270"/>
<instance part="R39" gate="R" x="50.8" y="91.44"/>
<instance part="GND_93" gate="1" x="40.64" y="91.44" rot="R270"/>
<instance part="C43" gate="C" x="40.64" y="71.12" rot="R90"/>
<instance part="C44" gate="C" x="60.96" y="71.12" rot="R90"/>
<instance part="GND_94" gate="1" x="40.64" y="63.5"/>
<instance part="GND_95" gate="1" x="60.96" y="63.5"/>
<instance part="Q1" gate="Q" x="50.8" y="78.74" smashed="yes">
<attribute name="NAME" x="49.276" y="79.248" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="52.324" y="79.248" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND_96" gate="1" x="50.8" y="63.5"/>
<instance part="R38" gate="R" x="109.22" y="106.68"/>
<instance part="GND_98" gate="1" x="172.72" y="38.1"/>
<instance part="R42" gate="R" x="182.88" y="78.74"/>
<instance part="R47" gate="R" x="182.88" y="48.26"/>
<instance part="T3" gate="T" x="63.5" y="129.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="62.992" y="132.08" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="60.96" y="124.46" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="+3V3_49" gate="G$1" x="63.5" y="157.48"/>
<instance part="R36" gate="R" x="71.12" y="139.7" rot="R90"/>
<instance part="+3V3_50" gate="G$1" x="71.12" y="157.48"/>
<instance part="C32" gate="C" x="76.2" y="139.7" rot="R90"/>
<instance part="+3V3_51" gate="G$1" x="76.2" y="157.48"/>
<instance part="R37" gate="R" x="86.36" y="129.54"/>
<instance part="C34" gate="C" x="50.8" y="121.92"/>
<instance part="C35" gate="C" x="50.8" y="119.38"/>
<instance part="C36" gate="C" x="50.8" y="116.84"/>
<instance part="C37" gate="C" x="50.8" y="114.3"/>
<instance part="C38" gate="C" x="50.8" y="111.76"/>
<instance part="C39" gate="C" x="50.8" y="109.22"/>
<instance part="C40" gate="C" x="50.8" y="106.68"/>
<instance part="GND_97" gate="1" x="40.64" y="106.68" rot="R270"/>
<instance part="GND_99" gate="1" x="40.64" y="109.22" rot="R270"/>
<instance part="GND_100" gate="1" x="40.64" y="111.76" rot="R270"/>
<instance part="GND_101" gate="1" x="40.64" y="114.3" rot="R270"/>
<instance part="GND_102" gate="1" x="40.64" y="116.84" rot="R270"/>
<instance part="GND_103" gate="1" x="40.64" y="119.38" rot="R270"/>
<instance part="GND_104" gate="1" x="40.64" y="121.92" rot="R270"/>
<instance part="C33" gate="C" x="50.8" y="137.16"/>
<instance part="GND_105" gate="1" x="40.64" y="137.16" rot="R270"/>
<instance part="C47" gate="C" x="149.86" y="50.8"/>
<instance part="C46" gate="C" x="160.02" y="53.34"/>
<instance part="R43" gate="R" x="134.62" y="71.12" rot="R90"/>
<instance part="R44" gate="R" x="137.16" y="71.12" rot="R90"/>
<instance part="R45" gate="R" x="139.7" y="71.12" rot="R90"/>
<instance part="R46" gate="R" x="142.24" y="71.12" rot="R90"/>
<instance part="R40" gate="R" x="142.24" y="91.44" rot="R90"/>
<instance part="C45" gate="C" x="147.32" y="71.12" rot="R90"/>
<instance part="GND_106" gate="1" x="147.32" y="60.96"/>
<instance part="R41" gate="R" x="154.94" y="91.44" rot="R90"/>
<instance part="C42" gate="C" x="149.86" y="91.44" rot="R90"/>
<instance part="GND_107" gate="1" x="149.86" y="60.96"/>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="CHS@1"/>
<pinref part="GND_1" gate="1" pin="GND"/>
<wire x1="205.74" y1="40.64" x2="205.74" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="CHS@2"/>
<pinref part="GND_2" gate="1" pin="GND"/>
<wire x1="205.74" y1="88.9" x2="205.74" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U13" gate="G$1" pin="VSSOSC"/>
<pinref part="GND_87" gate="1" pin="GND"/>
<wire x1="66.04" y1="60.96" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<wire x1="63.5" y1="60.96" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VSSRX"/>
<wire x1="63.5" y1="58.42" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="63.5" y1="55.88" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="53.34" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="63.5" y1="50.8" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<wire x1="63.5" y1="45.72" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VSSTX1"/>
<wire x1="66.04" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VSSTX2"/>
<wire x1="66.04" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VSSPLL"/>
<wire x1="66.04" y1="50.8" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VSS1"/>
<wire x1="66.04" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VSS2"/>
<wire x1="66.04" y1="43.18" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<junction x="63.5" y="43.18"/>
<junction x="63.5" y="45.72"/>
<junction x="63.5" y="50.8"/>
<junction x="63.5" y="53.34"/>
<junction x="63.5" y="55.88"/>
<junction x="63.5" y="58.42"/>
</segment>
<segment>
<pinref part="GND_92" gate="1" pin="GND"/>
<pinref part="C41" gate="C" pin="1"/>
<wire x1="48.26" y1="96.52" x2="43.18" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R39" gate="R" pin="P$1"/>
<pinref part="GND_93" gate="1" pin="GND"/>
<wire x1="43.18" y1="91.44" x2="45.72" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C43" gate="C" pin="1"/>
<pinref part="GND_94" gate="1" pin="GND"/>
<wire x1="40.64" y1="68.58" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C44" gate="C" pin="1"/>
<pinref part="GND_95" gate="1" pin="GND"/>
<wire x1="60.96" y1="68.58" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="Q" pin="GND"/>
<pinref part="GND_96" gate="1" pin="GND"/>
<wire x1="50.8" y1="76.2" x2="50.8" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="LEDY-"/>
<pinref part="GND_98" gate="1" pin="GND"/>
<wire x1="193.04" y1="50.8" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="177.8" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="172.72" y1="50.8" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="LEDG-"/>
<wire x1="193.04" y1="81.28" x2="190.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="190.5" y1="81.28" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<wire x1="190.5" y1="58.42" x2="177.8" y2="58.42" width="0.1524" layer="91"/>
<wire x1="177.8" y1="58.42" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<junction x="177.8" y="50.8"/>
<pinref part="J5" gate="G$1" pin="GND"/>
<wire x1="177.8" y1="53.34" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="193.04" y1="53.34" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<junction x="177.8" y="53.34"/>
</segment>
<segment>
<pinref part="C40" gate="C" pin="1"/>
<pinref part="GND_97" gate="1" pin="GND"/>
<wire x1="43.18" y1="106.68" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C39" gate="C" pin="1"/>
<pinref part="GND_99" gate="1" pin="GND"/>
<wire x1="43.18" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C38" gate="C" pin="1"/>
<pinref part="GND_100" gate="1" pin="GND"/>
<wire x1="43.18" y1="111.76" x2="48.26" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C37" gate="C" pin="1"/>
<pinref part="GND_101" gate="1" pin="GND"/>
<wire x1="43.18" y1="114.3" x2="48.26" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C36" gate="C" pin="1"/>
<pinref part="GND_102" gate="1" pin="GND"/>
<wire x1="43.18" y1="116.84" x2="48.26" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C35" gate="C" pin="1"/>
<pinref part="GND_103" gate="1" pin="GND"/>
<wire x1="43.18" y1="119.38" x2="48.26" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="C" pin="1"/>
<pinref part="GND_104" gate="1" pin="GND"/>
<wire x1="43.18" y1="121.92" x2="48.26" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND_105" gate="1" pin="GND"/>
<pinref part="C33" gate="C" pin="1"/>
<wire x1="43.18" y1="137.16" x2="48.26" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C45" gate="C" pin="1"/>
<pinref part="GND_106" gate="1" pin="GND"/>
<wire x1="147.32" y1="68.58" x2="147.32" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C42" gate="C" pin="1"/>
<pinref part="GND_107" gate="1" pin="GND"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="T3" gate="T" pin="S"/>
<pinref part="+3V3_49" gate="G$1" pin="+3V3"/>
<wire x1="63.5" y1="149.86" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C33" gate="C" pin="2"/>
<wire x1="63.5" y1="137.16" x2="63.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="53.34" y1="137.16" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<junction x="63.5" y="137.16"/>
</segment>
<segment>
<pinref part="+3V3_50" gate="G$1" pin="+3V3"/>
<pinref part="R36" gate="R" pin="P$2"/>
<wire x1="71.12" y1="149.86" x2="71.12" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="C" pin="2"/>
<pinref part="+3V3_51" gate="G$1" pin="+3V3"/>
<wire x1="76.2" y1="149.86" x2="76.2" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="OSC1"/>
<wire x1="66.04" y1="81.28" x2="40.64" y2="81.28" width="0.1524" layer="91"/>
<wire x1="40.64" y1="81.28" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C43" gate="C" pin="2"/>
<wire x1="40.64" y1="73.66" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<junction x="40.64" y="78.74"/>
<pinref part="Q1" gate="Q" pin="1"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="OSC2"/>
<wire x1="53.34" y1="78.74" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C44" gate="C" pin="2"/>
<wire x1="60.96" y1="78.74" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<wire x1="60.96" y1="73.66" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<junction x="60.96" y="78.74"/>
<pinref part="Q1" gate="Q" pin="2"/>
</segment>
</net>
<net name="ETH_CLK" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="CLKOUT"/>
<wire x1="96.52" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
<label x="129.54" y="104.14" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="VCAP"/>
<pinref part="C41" gate="C" pin="2"/>
<wire x1="53.34" y1="96.52" x2="66.04" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R39" gate="R" pin="P$2"/>
<pinref part="U13" gate="G$1" pin="RBIAS"/>
<wire x1="55.88" y1="91.44" x2="66.04" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="INT/SPISEL"/>
<pinref part="R38" gate="R" pin="P$1"/>
<wire x1="104.14" y1="106.68" x2="96.52" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ETH_INT" class="0">
<segment>
<pinref part="R38" gate="R" pin="P$2"/>
<wire x1="114.3" y1="106.68" x2="129.54" y2="106.68" width="0.1524" layer="91"/>
<label x="129.54" y="106.68" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="R47" gate="R" pin="P$2"/>
<pinref part="J5" gate="G$1" pin="LEDY+"/>
<wire x1="187.96" y1="48.26" x2="193.04" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R42" gate="R" pin="P$2"/>
<pinref part="J5" gate="G$1" pin="LEDG+"/>
<wire x1="187.96" y1="78.74" x2="193.04" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LEDG" class="0">
<segment>
<pinref part="R42" gate="R" pin="P$1"/>
<wire x1="177.8" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<label x="160.02" y="78.74" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="LEDY" class="0">
<segment>
<pinref part="R47" gate="R" pin="P$1"/>
<wire x1="177.8" y1="48.26" x2="160.02" y2="48.26" width="0.1524" layer="91"/>
<label x="160.02" y="48.26" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="T3" gate="T" pin="G"/>
<pinref part="R36" gate="R" pin="P$1"/>
<wire x1="68.58" y1="129.54" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
<wire x1="71.12" y1="129.54" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C32" gate="C" pin="1"/>
<wire x1="76.2" y1="137.16" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<wire x1="76.2" y1="129.54" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
<junction x="71.12" y="129.54"/>
<pinref part="R37" gate="R" pin="P$1"/>
<wire x1="81.28" y1="129.54" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<junction x="76.2" y="129.54"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="VDDOSC"/>
<pinref part="T3" gate="T" pin="D"/>
<wire x1="66.04" y1="106.68" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<wire x1="63.5" y1="106.68" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VDDRX"/>
<wire x1="63.5" y1="109.22" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="63.5" y2="116.84" width="0.1524" layer="91"/>
<wire x1="63.5" y1="116.84" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<wire x1="63.5" y1="119.38" x2="63.5" y2="121.92" width="0.1524" layer="91"/>
<wire x1="63.5" y1="121.92" x2="63.5" y2="124.46" width="0.1524" layer="91"/>
<wire x1="66.04" y1="109.22" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VDDTX"/>
<wire x1="66.04" y1="111.76" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VDDPLL"/>
<wire x1="66.04" y1="114.3" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VDD"/>
<wire x1="66.04" y1="119.38" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<junction x="63.5" y="119.38"/>
<junction x="63.5" y="114.3"/>
<junction x="63.5" y="111.76"/>
<junction x="63.5" y="109.22"/>
<pinref part="C34" gate="C" pin="2"/>
<wire x1="53.34" y1="121.92" x2="63.5" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C35" gate="C" pin="2"/>
<wire x1="53.34" y1="119.38" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C36" gate="C" pin="2"/>
<wire x1="53.34" y1="116.84" x2="63.5" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C37" gate="C" pin="2"/>
<wire x1="53.34" y1="114.3" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C38" gate="C" pin="2"/>
<wire x1="53.34" y1="111.76" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C39" gate="C" pin="2"/>
<wire x1="53.34" y1="109.22" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C40" gate="C" pin="2"/>
<wire x1="53.34" y1="106.68" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<junction x="63.5" y="106.68"/>
<junction x="63.5" y="116.84"/>
<junction x="63.5" y="121.92"/>
<wire x1="63.5" y1="121.92" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<wire x1="66.04" y1="121.92" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R44" gate="R" pin="P$2"/>
<wire x1="137.16" y1="76.2" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R40" gate="R" pin="P$2"/>
<wire x1="137.16" y1="101.6" x2="139.7" y2="101.6" width="0.1524" layer="91"/>
<wire x1="139.7" y1="101.6" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="142.24" y1="101.6" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R43" gate="R" pin="P$2"/>
<wire x1="134.62" y1="76.2" x2="134.62" y2="101.6" width="0.1524" layer="91"/>
<wire x1="134.62" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<junction x="137.16" y="101.6"/>
<wire x1="66.04" y1="124.46" x2="139.7" y2="124.46" width="0.1524" layer="91"/>
<wire x1="139.7" y1="124.46" x2="139.7" y2="101.6" width="0.1524" layer="91"/>
<junction x="139.7" y="101.6"/>
<pinref part="R41" gate="R" pin="P$2"/>
<wire x1="154.94" y1="96.52" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="154.94" y1="101.6" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<junction x="142.24" y="101.6"/>
<pinref part="C42" gate="C" pin="2"/>
<wire x1="149.86" y1="101.6" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="149.86" y1="93.98" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<junction x="149.86" y="101.6"/>
</segment>
</net>
<net name="ETH_PWR" class="0">
<segment>
<pinref part="R37" gate="R" pin="P$2"/>
<wire x1="91.44" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<label x="129.54" y="129.54" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_SCK_AL" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="SCK/AL"/>
<wire x1="96.52" y1="119.38" x2="129.54" y2="119.38" width="0.1524" layer="91"/>
<label x="129.54" y="119.38" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_CS" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="CS"/>
<wire x1="96.52" y1="116.84" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
<label x="129.54" y="116.84" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_MISO_WR" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="SO/WR/EN"/>
<wire x1="96.52" y1="114.3" x2="129.54" y2="114.3" width="0.1524" layer="91"/>
<label x="129.54" y="114.3" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_MOSI_RD" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="SI/RD/RW"/>
<wire x1="96.52" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<label x="129.54" y="111.76" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_PSPCFG0" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="PSPCFG0"/>
<wire x1="96.52" y1="109.22" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<label x="129.54" y="109.22" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD0" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD0"/>
<wire x1="96.52" y1="99.06" x2="129.54" y2="99.06" width="0.1524" layer="91"/>
<label x="129.54" y="99.06" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD1" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD1"/>
<wire x1="96.52" y1="96.52" x2="129.54" y2="96.52" width="0.1524" layer="91"/>
<label x="129.54" y="96.52" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD2" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD2"/>
<wire x1="96.52" y1="93.98" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
<label x="129.54" y="93.98" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD3" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD3"/>
<wire x1="96.52" y1="91.44" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
<label x="129.54" y="91.44" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD4" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD4"/>
<wire x1="96.52" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<label x="129.54" y="88.9" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD5" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD5"/>
<wire x1="96.52" y1="86.36" x2="129.54" y2="86.36" width="0.1524" layer="91"/>
<label x="129.54" y="86.36" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD6" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD6"/>
<wire x1="96.52" y1="83.82" x2="129.54" y2="83.82" width="0.1524" layer="91"/>
<label x="129.54" y="83.82" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD7" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD7"/>
<wire x1="96.52" y1="81.28" x2="129.54" y2="81.28" width="0.1524" layer="91"/>
<label x="129.54" y="81.28" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD8" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD8"/>
<wire x1="96.52" y1="78.74" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<label x="129.54" y="78.74" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD9" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD9"/>
<wire x1="96.52" y1="76.2" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<label x="129.54" y="76.2" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD10" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD10"/>
<wire x1="96.52" y1="73.66" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<label x="129.54" y="73.66" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD11" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD11"/>
<wire x1="96.52" y1="71.12" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<label x="129.54" y="71.12" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD12" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD12"/>
<wire x1="96.52" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<label x="129.54" y="68.58" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD13" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD13"/>
<wire x1="96.52" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<label x="129.54" y="66.04" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_AD14" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="AD14"/>
<wire x1="96.52" y1="63.5" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<label x="129.54" y="63.5" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_LEDA" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="LEDA"/>
<wire x1="96.52" y1="45.72" x2="129.54" y2="45.72" width="0.1524" layer="91"/>
<label x="129.54" y="45.72" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ETH_LEDB" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="LEDB"/>
<wire x1="96.52" y1="43.18" x2="129.54" y2="43.18" width="0.1524" layer="91"/>
<label x="129.54" y="43.18" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="TPIN+"/>
<pinref part="C47" gate="C" pin="1"/>
<wire x1="96.52" y1="50.8" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R46" gate="R" pin="P$1"/>
<wire x1="142.24" y1="50.8" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="142.24" y1="66.04" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<junction x="142.24" y="50.8"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="TPIN-"/>
<pinref part="C46" gate="C" pin="1"/>
<wire x1="96.52" y1="53.34" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R45" gate="R" pin="P$1"/>
<wire x1="139.7" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="139.7" y1="66.04" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<junction x="139.7" y="53.34"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="C47" gate="C" pin="2"/>
<wire x1="152.4" y1="50.8" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="170.18" y1="50.8" x2="170.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="TRD1+"/>
<wire x1="170.18" y1="71.12" x2="193.04" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="C46" gate="C" pin="2"/>
<wire x1="162.56" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="172.72" y1="53.34" x2="172.72" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="TRD1-"/>
<wire x1="172.72" y1="68.58" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="TPOUT+"/>
<wire x1="96.52" y1="55.88" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<wire x1="137.16" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="TRD0+"/>
<wire x1="165.1" y1="76.2" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R44" gate="R" pin="P$1"/>
<wire x1="137.16" y1="66.04" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<junction x="137.16" y="55.88"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="TPOUT-"/>
<wire x1="96.52" y1="58.42" x2="134.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="134.62" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="58.42" x2="167.64" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="TRD0-"/>
<wire x1="167.64" y1="73.66" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R43" gate="R" pin="P$1"/>
<wire x1="134.62" y1="66.04" x2="134.62" y2="58.42" width="0.1524" layer="91"/>
<junction x="134.62" y="58.42"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="R45" gate="R" pin="P$2"/>
<wire x1="139.7" y1="76.2" x2="139.7" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R40" gate="R" pin="P$1"/>
<wire x1="139.7" y1="81.28" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="142.24" y1="81.28" x2="142.24" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R46" gate="R" pin="P$2"/>
<wire x1="142.24" y1="83.82" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="142.24" y1="76.2" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
<junction x="142.24" y="81.28"/>
<pinref part="C45" gate="C" pin="2"/>
<wire x1="147.32" y1="73.66" x2="147.32" y2="83.82" width="0.1524" layer="91"/>
<wire x1="147.32" y1="83.82" x2="142.24" y2="83.82" width="0.1524" layer="91"/>
<junction x="142.24" y="83.82"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="VCC"/>
<wire x1="193.04" y1="55.88" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="175.26" y1="55.88" x2="175.26" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R41" gate="R" pin="P$1"/>
<wire x1="175.26" y1="66.04" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
<wire x1="154.94" y1="66.04" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="426.72" y1="152.4" x2="673.1" y2="152.4" width="0.1524" layer="94" style="longdash"/>
<wire x1="426.72" y1="-10.16" x2="426.72" y2="152.4" width="0.1524" layer="94" style="longdash"/>
<text x="429.26" y="149.86" size="5.08" layer="94" font="vector" rot="MR180">FPGA</text>
<wire x1="673.1" y1="10.16" x2="673.1" y2="152.4" width="0.1524" layer="94" style="longdash"/>
<wire x1="426.72" y1="-10.16" x2="576.58" y2="-10.16" width="0.1524" layer="94" style="longdash"/>
<wire x1="576.58" y1="-10.16" x2="576.58" y2="10.16" width="0.1524" layer="94" style="longdash"/>
<wire x1="576.58" y1="10.16" x2="673.1" y2="10.16" width="0.1524" layer="94" style="longdash"/>
</plain>
<instances>
<instance part="U14" gate="G$1" x="553.72" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="552.45" y="79.248" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="566.166" y="77.978" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="+1V2_3" gate="G$1" x="474.98" y="149.86"/>
<instance part="+1V2_4" gate="G$1" x="477.52" y="149.86"/>
<instance part="+3V3_29" gate="G$1" x="472.44" y="149.86"/>
<instance part="C53" gate="C" x="472.44" y="2.54" rot="R90"/>
<instance part="GND_60" gate="1" x="472.44" y="-5.08"/>
<instance part="C54" gate="C" x="477.52" y="2.54" rot="R90"/>
<instance part="GND_61" gate="1" x="477.52" y="-5.08"/>
<instance part="+3V3_34" gate="G$1" x="477.52" y="17.78"/>
<instance part="C55" gate="C" x="482.6" y="2.54" rot="R90"/>
<instance part="GND_62" gate="1" x="482.6" y="-5.08"/>
<instance part="+3V3_35" gate="G$1" x="482.6" y="17.78"/>
<instance part="C56" gate="C" x="487.68" y="2.54" rot="R90"/>
<instance part="GND_63" gate="1" x="487.68" y="-5.08"/>
<instance part="+3V3_36" gate="G$1" x="487.68" y="17.78"/>
<instance part="C57" gate="C" x="492.76" y="2.54" rot="R90"/>
<instance part="GND_64" gate="1" x="492.76" y="-5.08"/>
<instance part="+3V3_37" gate="G$1" x="492.76" y="17.78"/>
<instance part="C58" gate="C" x="497.84" y="2.54" rot="R90"/>
<instance part="GND_65" gate="1" x="497.84" y="-5.08"/>
<instance part="C59" gate="C" x="502.92" y="2.54" rot="R90"/>
<instance part="GND_66" gate="1" x="502.92" y="-5.08"/>
<instance part="+3V3_39" gate="G$1" x="502.92" y="17.78"/>
<instance part="C60" gate="C" x="508" y="2.54" rot="R90"/>
<instance part="GND_67" gate="1" x="508" y="-5.08"/>
<instance part="C61" gate="C" x="513.08" y="2.54" rot="R90"/>
<instance part="GND_68" gate="1" x="513.08" y="-5.08"/>
<instance part="+3V3_41" gate="G$1" x="513.08" y="17.78"/>
<instance part="C62" gate="C" x="518.16" y="2.54" rot="R90"/>
<instance part="GND_69" gate="1" x="518.16" y="-5.08"/>
<instance part="+3V3_42" gate="G$1" x="518.16" y="17.78"/>
<instance part="C63" gate="C" x="523.24" y="2.54" rot="R90"/>
<instance part="GND_70" gate="1" x="523.24" y="-5.08"/>
<instance part="C64" gate="C" x="535.94" y="2.54" rot="R90"/>
<instance part="GND_73" gate="1" x="535.94" y="-5.08"/>
<instance part="C65" gate="C" x="541.02" y="2.54" rot="R90"/>
<instance part="GND_74" gate="1" x="541.02" y="-5.08"/>
<instance part="C51" gate="C" x="482.6" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="481.33" y="112.268" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="483.87" y="112.268" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C49" gate="C" x="485.14" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="483.87" y="122.428" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="486.41" y="122.428" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND_77" gate="1" x="637.54" y="25.4"/>
<instance part="+1V2_5" gate="G$1" x="469.9" y="149.86"/>
<instance part="+1V2_6" gate="G$1" x="508" y="17.78"/>
<instance part="+1V2_9" gate="G$1" x="497.84" y="17.78"/>
<instance part="+1V2_10" gate="G$1" x="472.44" y="17.78"/>
<instance part="+1V2_11" gate="G$1" x="523.24" y="17.78"/>
<instance part="L2" gate="L" x="474.98" y="124.46" rot="R90"/>
<instance part="L1" gate="L" x="477.52" y="134.62" rot="R90"/>
<instance part="+3V3_33" gate="G$1" x="535.94" y="17.78"/>
<instance part="+3V3_38" gate="G$1" x="541.02" y="17.78"/>
<instance part="C50" gate="C" x="482.6" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="481.33" y="117.348" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="483.87" y="117.348" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C48" gate="C" x="485.14" y="127" smashed="yes" rot="R180">
<attribute name="NAME" x="483.87" y="127.508" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="486.41" y="127.508" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="+3V3_28" gate="G$1" x="632.46" y="25.4" rot="R180"/>
<instance part="D1" gate="D" x="632.46" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="631.952" y="36.322" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="631.952" y="39.878" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R48" gate="R" x="464.82" y="124.46" rot="R90"/>
<instance part="+3V3_32" gate="G$1" x="464.82" y="149.86"/>
<instance part="Q2" gate="G$1" x="444.5" y="10.16" smashed="yes">
<attribute name="NAME" x="442.976" y="15.748" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="449.834" y="4.572" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND_85" gate="1" x="457.2" y="-5.08"/>
<instance part="+3V3_40" gate="G$1" x="431.8" y="22.86"/>
<instance part="C52" gate="C" x="431.8" y="2.54" rot="R90"/>
<instance part="GND_86" gate="1" x="431.8" y="-5.08"/>
<instance part="C66" gate="C" x="553.72" y="2.54" rot="R90"/>
<instance part="GND_88" gate="1" x="553.72" y="-5.08"/>
<instance part="+3V3_47" gate="G$1" x="553.72" y="17.78"/>
<instance part="C67" gate="C" x="558.8" y="2.54" rot="R90"/>
<instance part="GND_89" gate="1" x="558.8" y="-5.08"/>
<instance part="+1V2_2" gate="G$1" x="558.8" y="17.78"/>
<instance part="C68" gate="C" x="563.88" y="2.54" rot="R90"/>
<instance part="GND_90" gate="1" x="563.88" y="-5.08"/>
<instance part="+3V3_48" gate="G$1" x="563.88" y="17.78"/>
<instance part="C69" gate="C" x="568.96" y="2.54" rot="R90"/>
<instance part="GND_91" gate="1" x="568.96" y="-5.08"/>
<instance part="+1V2_7" gate="G$1" x="568.96" y="17.78"/>
<instance part="FRAME5" gate="G$1" x="419.1" y="-17.78"/>
<instance part="D6" gate="D" x="523.24" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="522.732" y="36.322" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="522.732" y="39.878" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="+3V3_53" gate="G$1" x="523.24" y="25.4" rot="R180"/>
<instance part="R29" gate="R" x="556.26" y="48.26" rot="R90"/>
<instance part="R49" gate="R" x="561.34" y="48.26" rot="R90"/>
<instance part="GND_113" gate="1" x="556.26" y="25.4"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C53" gate="C" pin="1"/>
<pinref part="GND_60" gate="1" pin="GND"/>
<wire x1="472.44" y1="-2.54" x2="472.44" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C54" gate="C" pin="1"/>
<pinref part="GND_61" gate="1" pin="GND"/>
<wire x1="477.52" y1="-2.54" x2="477.52" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C55" gate="C" pin="1"/>
<pinref part="GND_62" gate="1" pin="GND"/>
<wire x1="482.6" y1="-2.54" x2="482.6" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C56" gate="C" pin="1"/>
<pinref part="GND_63" gate="1" pin="GND"/>
<wire x1="487.68" y1="-2.54" x2="487.68" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C57" gate="C" pin="1"/>
<pinref part="GND_64" gate="1" pin="GND"/>
<wire x1="492.76" y1="-2.54" x2="492.76" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C58" gate="C" pin="1"/>
<pinref part="GND_65" gate="1" pin="GND"/>
<wire x1="497.84" y1="-2.54" x2="497.84" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C59" gate="C" pin="1"/>
<pinref part="GND_66" gate="1" pin="GND"/>
<wire x1="502.92" y1="-2.54" x2="502.92" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C60" gate="C" pin="1"/>
<pinref part="GND_67" gate="1" pin="GND"/>
<wire x1="508" y1="-2.54" x2="508" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C61" gate="C" pin="1"/>
<pinref part="GND_68" gate="1" pin="GND"/>
<wire x1="513.08" y1="-2.54" x2="513.08" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C62" gate="C" pin="1"/>
<pinref part="GND_69" gate="1" pin="GND"/>
<wire x1="518.16" y1="-2.54" x2="518.16" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C63" gate="C" pin="1"/>
<pinref part="GND_70" gate="1" pin="GND"/>
<wire x1="523.24" y1="-2.54" x2="523.24" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C64" gate="C" pin="1"/>
<pinref part="GND_73" gate="1" pin="GND"/>
<wire x1="535.94" y1="-2.54" x2="535.94" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C65" gate="C" pin="1"/>
<pinref part="GND_74" gate="1" pin="GND"/>
<wire x1="541.02" y1="-2.54" x2="541.02" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="GND"/>
<pinref part="GND_77" gate="1" pin="GND"/>
<wire x1="637.54" y1="58.42" x2="637.54" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="GND"/>
<pinref part="GND_85" gate="1" pin="GND"/>
<wire x1="454.66" y1="7.62" x2="457.2" y2="7.62" width="0.1524" layer="91"/>
<wire x1="457.2" y1="7.62" x2="457.2" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C52" gate="C" pin="1"/>
<pinref part="GND_86" gate="1" pin="GND"/>
<wire x1="431.8" y1="0" x2="431.8" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C66" gate="C" pin="1"/>
<pinref part="GND_88" gate="1" pin="GND"/>
<wire x1="553.72" y1="-2.54" x2="553.72" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C67" gate="C" pin="1"/>
<pinref part="GND_89" gate="1" pin="GND"/>
<wire x1="558.8" y1="-2.54" x2="558.8" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C68" gate="C" pin="1"/>
<pinref part="GND_90" gate="1" pin="GND"/>
<wire x1="563.88" y1="-2.54" x2="563.88" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C69" gate="C" pin="1"/>
<pinref part="GND_91" gate="1" pin="GND"/>
<wire x1="568.96" y1="-2.54" x2="568.96" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND_113" gate="1" pin="GND"/>
<pinref part="R29" gate="R" pin="P$1"/>
<wire x1="556.26" y1="43.18" x2="556.26" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="VCC_SPI"/>
<pinref part="+3V3_29" gate="G$1" pin="+3V3"/>
<wire x1="472.44" y1="142.24" x2="472.44" y2="104.14" width="0.1524" layer="91"/>
<wire x1="472.44" y1="104.14" x2="472.44" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="VCCIO_0"/>
<wire x1="474.98" y1="104.14" x2="474.98" y2="99.06" width="0.1524" layer="91"/>
<wire x1="472.44" y1="104.14" x2="474.98" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="VCCIO_1"/>
<wire x1="477.52" y1="104.14" x2="477.52" y2="99.06" width="0.1524" layer="91"/>
<wire x1="474.98" y1="104.14" x2="477.52" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="VCCIO_2"/>
<wire x1="480.06" y1="99.06" x2="480.06" y2="104.14" width="0.1524" layer="91"/>
<wire x1="477.52" y1="104.14" x2="480.06" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="VCCIO_3"/>
<wire x1="482.6" y1="99.06" x2="482.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="480.06" y1="104.14" x2="482.6" y2="104.14" width="0.1524" layer="91"/>
<junction x="480.06" y="104.14"/>
<junction x="477.52" y="104.14"/>
<junction x="474.98" y="104.14"/>
<junction x="472.44" y="104.14"/>
</segment>
<segment>
<pinref part="C54" gate="C" pin="2"/>
<pinref part="+3V3_34" gate="G$1" pin="+3V3"/>
<wire x1="477.52" y1="10.16" x2="477.52" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C55" gate="C" pin="2"/>
<pinref part="+3V3_35" gate="G$1" pin="+3V3"/>
<wire x1="482.6" y1="10.16" x2="482.6" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C56" gate="C" pin="2"/>
<pinref part="+3V3_36" gate="G$1" pin="+3V3"/>
<wire x1="487.68" y1="10.16" x2="487.68" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C57" gate="C" pin="2"/>
<pinref part="+3V3_37" gate="G$1" pin="+3V3"/>
<wire x1="492.76" y1="10.16" x2="492.76" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C59" gate="C" pin="2"/>
<pinref part="+3V3_39" gate="G$1" pin="+3V3"/>
<wire x1="502.92" y1="10.16" x2="502.92" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C61" gate="C" pin="2"/>
<pinref part="+3V3_41" gate="G$1" pin="+3V3"/>
<wire x1="513.08" y1="10.16" x2="513.08" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C62" gate="C" pin="2"/>
<pinref part="+3V3_42" gate="G$1" pin="+3V3"/>
<wire x1="518.16" y1="10.16" x2="518.16" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C64" gate="C" pin="2"/>
<wire x1="535.94" y1="10.16" x2="535.94" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+3V3_33" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C65" gate="C" pin="2"/>
<wire x1="541.02" y1="10.16" x2="541.02" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+3V3_38" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V3_28" gate="G$1" pin="+3V3"/>
<wire x1="632.46" y1="33.02" x2="632.46" y2="35.56" width="0.1524" layer="91"/>
<pinref part="D1" gate="D" pin="A"/>
</segment>
<segment>
<pinref part="R48" gate="R" pin="P$2"/>
<pinref part="+3V3_32" gate="G$1" pin="+3V3"/>
<wire x1="464.82" y1="142.24" x2="464.82" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="VCC"/>
<pinref part="+3V3_40" gate="G$1" pin="+3V3"/>
<wire x1="434.34" y1="12.7" x2="431.8" y2="12.7" width="0.1524" layer="91"/>
<wire x1="431.8" y1="12.7" x2="431.8" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C52" gate="C" pin="2"/>
<wire x1="431.8" y1="5.08" x2="431.8" y2="12.7" width="0.1524" layer="91"/>
<junction x="431.8" y="12.7"/>
</segment>
<segment>
<pinref part="C66" gate="C" pin="2"/>
<pinref part="+3V3_47" gate="G$1" pin="+3V3"/>
<wire x1="553.72" y1="10.16" x2="553.72" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C68" gate="C" pin="2"/>
<pinref part="+3V3_48" gate="G$1" pin="+3V3"/>
<wire x1="563.88" y1="10.16" x2="563.88" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D6" gate="D" pin="C"/>
<pinref part="+3V3_53" gate="G$1" pin="+3V3"/>
<wire x1="523.24" y1="35.56" x2="523.24" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+1V2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="VCC"/>
<wire x1="469.9" y1="142.24" x2="469.9" y2="99.06" width="0.1524" layer="91"/>
<pinref part="+1V2_5" gate="G$1" pin="+1V2"/>
</segment>
<segment>
<pinref part="C60" gate="C" pin="2"/>
<wire x1="508" y1="10.16" x2="508" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+1V2_6" gate="G$1" pin="+1V2"/>
</segment>
<segment>
<pinref part="C58" gate="C" pin="2"/>
<wire x1="497.84" y1="10.16" x2="497.84" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+1V2_9" gate="G$1" pin="+1V2"/>
</segment>
<segment>
<pinref part="C53" gate="C" pin="2"/>
<wire x1="472.44" y1="10.16" x2="472.44" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+1V2_10" gate="G$1" pin="+1V2"/>
</segment>
<segment>
<pinref part="C63" gate="C" pin="2"/>
<wire x1="523.24" y1="10.16" x2="523.24" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+1V2_11" gate="G$1" pin="+1V2"/>
</segment>
<segment>
<pinref part="+1V2_3" gate="G$1" pin="+1V2"/>
<pinref part="L2" gate="L" pin="P$2"/>
<wire x1="474.98" y1="129.54" x2="474.98" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+1V2_4" gate="G$1" pin="+1V2"/>
<pinref part="L1" gate="L" pin="P$2"/>
<wire x1="477.52" y1="139.7" x2="477.52" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C67" gate="C" pin="2"/>
<wire x1="558.8" y1="10.16" x2="558.8" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+1V2_2" gate="G$1" pin="+1V2"/>
</segment>
<segment>
<pinref part="C69" gate="C" pin="2"/>
<wire x1="568.96" y1="10.16" x2="568.96" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+1V2_7" gate="G$1" pin="+1V2"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CRESET_B"/>
<pinref part="R48" gate="R" pin="P$1"/>
<wire x1="472.44" y1="58.42" x2="472.44" y2="50.8" width="0.1524" layer="91"/>
<wire x1="472.44" y1="50.8" x2="464.82" y2="50.8" width="0.1524" layer="91"/>
<wire x1="464.82" y1="50.8" x2="464.82" y2="119.38" width="0.1524" layer="91"/>
<wire x1="472.44" y1="50.8" x2="472.44" y2="25.4" width="0.1524" layer="91"/>
<junction x="472.44" y="50.8"/>
<label x="472.44" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="HDMI_OE_N" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_206"/>
<wire x1="612.14" y1="99.06" x2="612.14" y2="149.86" width="0.1524" layer="91"/>
<label x="612.14" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="HDMI_HPD" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_220"/>
<wire x1="632.46" y1="99.06" x2="632.46" y2="149.86" width="0.1524" layer="91"/>
<label x="632.46" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="HDMI_DDC_EN" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_191"/>
<wire x1="607.06" y1="99.06" x2="607.06" y2="149.86" width="0.1524" layer="91"/>
<label x="607.06" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="HDMI_SCL" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_222"/>
<wire x1="637.54" y1="99.06" x2="637.54" y2="149.86" width="0.1524" layer="91"/>
<label x="637.54" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="HDMI_SDA" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_221"/>
<wire x1="635" y1="99.06" x2="635" y2="149.86" width="0.1524" layer="91"/>
<label x="635" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="HDMI_CLKP" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_10A"/>
<wire x1="589.28" y1="58.42" x2="589.28" y2="25.4" width="0.1524" layer="91"/>
<label x="589.28" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="HDMI_D0P" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_12A"/>
<wire x1="594.36" y1="58.42" x2="594.36" y2="25.4" width="0.1524" layer="91"/>
<label x="594.36" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="HDMI_D1P" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_17A"/>
<wire x1="604.52" y1="58.42" x2="604.52" y2="25.4" width="0.1524" layer="91"/>
<label x="604.52" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="HDMI_D2P" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_18A"/>
<wire x1="609.6" y1="58.42" x2="609.6" y2="25.4" width="0.1524" layer="91"/>
<label x="609.6" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="SD_D2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_192"/>
<wire x1="609.6" y1="99.06" x2="609.6" y2="149.86" width="0.1524" layer="91"/>
<label x="609.6" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_D3" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_212"/>
<wire x1="614.68" y1="99.06" x2="614.68" y2="149.86" width="0.1524" layer="91"/>
<label x="614.68" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_CMD" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_213"/>
<wire x1="617.22" y1="99.06" x2="617.22" y2="149.86" width="0.1524" layer="91"/>
<label x="617.22" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_D0" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_215"/>
<wire x1="622.3" y1="99.06" x2="622.3" y2="149.86" width="0.1524" layer="91"/>
<label x="622.3" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_D1" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_217"/>
<wire x1="627.38" y1="99.06" x2="627.38" y2="149.86" width="0.1524" layer="91"/>
<label x="627.38" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_CD" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_219"/>
<wire x1="629.92" y1="99.06" x2="629.92" y2="149.86" width="0.1524" layer="91"/>
<label x="629.92" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="BUTTON" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_216"/>
<wire x1="624.84" y1="99.06" x2="624.84" y2="149.86" width="0.1524" layer="91"/>
<label x="624.84" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_CLK" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_214"/>
<wire x1="619.76" y1="99.06" x2="619.76" y2="149.86" width="0.1524" layer="91"/>
<label x="619.76" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="SD_PWR" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_164"/>
<wire x1="558.8" y1="99.06" x2="558.8" y2="149.86" width="0.1524" layer="91"/>
<label x="558.8" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A3" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_81_GBIN5"/>
<wire x1="502.92" y1="58.42" x2="502.92" y2="25.4" width="0.1524" layer="91"/>
<label x="502.92" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_HALT" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_91"/>
<wire x1="551.18" y1="58.42" x2="551.18" y2="25.4" width="0.1524" layer="91"/>
<label x="551.18" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_MREQ" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_94"/>
<wire x1="553.72" y1="58.42" x2="553.72" y2="25.4" width="0.1524" layer="91"/>
<label x="553.72" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_IORQ" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_82_GBIN4"/>
<wire x1="505.46" y1="58.42" x2="505.46" y2="25.4" width="0.1524" layer="91"/>
<label x="505.46" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_RD" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_96"/>
<wire x1="558.8" y1="58.42" x2="558.8" y2="25.4" width="0.1524" layer="91"/>
<label x="558.8" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_WR" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_102"/>
<wire x1="497.84" y1="99.06" x2="497.84" y2="149.86" width="0.1524" layer="91"/>
<label x="497.84" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A7" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_80"/>
<wire x1="548.64" y1="58.42" x2="548.64" y2="25.4" width="0.1524" layer="91"/>
<label x="548.64" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A6" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_79"/>
<wire x1="546.1" y1="58.42" x2="546.1" y2="25.4" width="0.1524" layer="91"/>
<label x="546.1" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="CDONE" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CDONE"/>
<wire x1="469.9" y1="58.42" x2="469.9" y2="25.4" width="0.1524" layer="91"/>
<label x="469.9" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_8A"/>
<wire x1="584.2" y1="58.42" x2="584.2" y2="25.4" width="0.1524" layer="91"/>
<label x="584.2" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_5B"/>
<wire x1="581.66" y1="58.42" x2="581.66" y2="25.4" width="0.1524" layer="91"/>
<label x="581.66" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ZX_INT" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_73"/>
<wire x1="543.56" y1="58.42" x2="543.56" y2="45.72" width="0.1524" layer="91"/>
<label x="543.56" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
<pinref part="D6" gate="D" pin="A"/>
<wire x1="543.56" y1="45.72" x2="543.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="523.24" y1="40.64" x2="523.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="523.24" y1="45.72" x2="543.56" y2="45.72" width="0.1524" layer="91"/>
<junction x="543.56" y="45.72"/>
</segment>
</net>
<net name="F_D_DIR" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_61"/>
<wire x1="530.86" y1="58.42" x2="530.86" y2="25.4" width="0.1524" layer="91"/>
<label x="530.86" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_O_OE" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_116"/>
<wire x1="515.62" y1="99.06" x2="515.62" y2="149.86" width="0.1524" layer="91"/>
<label x="515.62" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="VCCPLL1"/>
<pinref part="L1" gate="L" pin="P$1"/>
<wire x1="477.52" y1="129.54" x2="477.52" y2="127" width="0.1524" layer="91"/>
<wire x1="477.52" y1="109.22" x2="487.68" y2="109.22" width="0.1524" layer="91"/>
<wire x1="487.68" y1="109.22" x2="487.68" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C49" gate="C" pin="2"/>
<wire x1="482.6" y1="121.92" x2="477.52" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C48" gate="C" pin="2"/>
<wire x1="482.6" y1="127" x2="477.52" y2="127" width="0.1524" layer="91"/>
<junction x="477.52" y="121.92"/>
<junction x="477.52" y="127"/>
<wire x1="477.52" y1="109.22" x2="477.52" y2="121.92" width="0.1524" layer="91"/>
<wire x1="477.52" y1="121.92" x2="477.52" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="C51" gate="C" pin="2"/>
<wire x1="474.98" y1="111.76" x2="480.06" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="VCCPLL0"/>
<pinref part="L2" gate="L" pin="P$1"/>
<wire x1="474.98" y1="119.38" x2="474.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="474.98" y1="106.68" x2="485.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="485.14" y1="106.68" x2="485.14" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C50" gate="C" pin="2"/>
<wire x1="480.06" y1="116.84" x2="474.98" y2="116.84" width="0.1524" layer="91"/>
<junction x="474.98" y="111.76"/>
<junction x="474.98" y="116.84"/>
<wire x1="474.98" y1="106.68" x2="474.98" y2="111.76" width="0.1524" layer="91"/>
<wire x1="474.98" y1="111.76" x2="474.98" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="C51" gate="C" pin="1"/>
<wire x1="490.22" y1="111.76" x2="485.14" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="GNDPLL0"/>
<wire x1="490.22" y1="99.06" x2="490.22" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C50" gate="C" pin="1"/>
<wire x1="485.14" y1="116.84" x2="490.22" y2="116.84" width="0.1524" layer="91"/>
<wire x1="490.22" y1="116.84" x2="490.22" y2="111.76" width="0.1524" layer="91"/>
<junction x="490.22" y="111.76"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GNDPLL1"/>
<pinref part="C49" gate="C" pin="1"/>
<wire x1="492.76" y1="99.06" x2="492.76" y2="121.92" width="0.1524" layer="91"/>
<wire x1="492.76" y1="121.92" x2="487.68" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C48" gate="C" pin="1"/>
<wire x1="487.68" y1="127" x2="492.76" y2="127" width="0.1524" layer="91"/>
<wire x1="492.76" y1="127" x2="492.76" y2="121.92" width="0.1524" layer="91"/>
<junction x="492.76" y="121.92"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="VPP_2V5"/>
<wire x1="632.46" y1="40.64" x2="632.46" y2="58.42" width="0.1524" layer="91"/>
<pinref part="D1" gate="D" pin="C"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IOB_107_SCK"/>
<wire x1="487.68" y1="58.42" x2="487.68" y2="25.4" width="0.1524" layer="91"/>
<label x="487.68" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IOB_108_SS"/>
<wire x1="490.22" y1="58.42" x2="490.22" y2="25.4" width="0.1524" layer="91"/>
<label x="490.22" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IOB_106_SDI"/>
<wire x1="485.14" y1="58.42" x2="485.14" y2="25.4" width="0.1524" layer="91"/>
<label x="485.14" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IOB_105_SDO"/>
<wire x1="482.6" y1="58.42" x2="482.6" y2="25.4" width="0.1524" layer="91"/>
<label x="482.6" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D4" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_57"/>
<wire x1="528.32" y1="58.42" x2="528.32" y2="25.4" width="0.1524" layer="91"/>
<label x="528.32" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D3" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_56"/>
<wire x1="525.78" y1="58.42" x2="525.78" y2="25.4" width="0.1524" layer="91"/>
<label x="525.78" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_2A"/>
<wire x1="563.88" y1="58.42" x2="563.88" y2="25.4" width="0.1524" layer="91"/>
<label x="563.88" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_2B"/>
<wire x1="566.42" y1="58.42" x2="566.42" y2="25.4" width="0.1524" layer="91"/>
<label x="566.42" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="EXT_MISO" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_3A"/>
<wire x1="568.96" y1="58.42" x2="568.96" y2="25.4" width="0.1524" layer="91"/>
<label x="568.96" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="EXT_SCK" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_3B"/>
<wire x1="571.5" y1="58.42" x2="571.5" y2="25.4" width="0.1524" layer="91"/>
<label x="571.5" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="EXT_CS" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_4A"/>
<wire x1="574.04" y1="58.42" x2="574.04" y2="25.4" width="0.1524" layer="91"/>
<label x="574.04" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="EXT_MOSI" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_4B"/>
<wire x1="576.58" y1="58.42" x2="576.58" y2="25.4" width="0.1524" layer="91"/>
<label x="576.58" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="HDMI_CEC" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_5A"/>
<wire x1="579.12" y1="58.42" x2="579.12" y2="25.4" width="0.1524" layer="91"/>
<label x="579.12" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="OUT"/>
<wire x1="454.66" y1="12.7" x2="464.82" y2="12.7" width="0.1524" layer="91"/>
<label x="464.82" y="12.7" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="IO3_13B_GBIN7"/>
<wire x1="495.3" y1="58.42" x2="495.3" y2="25.4" width="0.1524" layer="91"/>
<label x="495.3" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_CLK" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_14A_GBIN6"/>
<wire x1="497.84" y1="58.42" x2="497.84" y2="25.4" width="0.1524" layer="91"/>
<label x="497.84" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A15" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_8B"/>
<wire x1="586.74" y1="58.42" x2="586.74" y2="25.4" width="0.1524" layer="91"/>
<label x="586.74" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A13" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_13A"/>
<wire x1="599.44" y1="58.42" x2="599.44" y2="25.4" width="0.1524" layer="91"/>
<label x="599.44" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A14" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_23A"/>
<wire x1="614.68" y1="58.42" x2="614.68" y2="25.4" width="0.1524" layer="91"/>
<label x="614.68" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A12" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_14B"/>
<wire x1="601.98" y1="58.42" x2="601.98" y2="25.4" width="0.1524" layer="91"/>
<label x="601.98" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A0" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_23B"/>
<wire x1="617.22" y1="58.42" x2="617.22" y2="25.4" width="0.1524" layer="91"/>
<label x="617.22" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A1" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_24A"/>
<wire x1="619.76" y1="58.42" x2="619.76" y2="25.4" width="0.1524" layer="91"/>
<label x="619.76" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_A2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_24B"/>
<wire x1="622.3" y1="58.42" x2="622.3" y2="25.4" width="0.1524" layer="91"/>
<label x="622.3" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D7" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_25A"/>
<wire x1="624.84" y1="58.42" x2="624.84" y2="25.4" width="0.1524" layer="91"/>
<label x="624.84" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D0" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO3_25B"/>
<wire x1="627.38" y1="58.42" x2="627.38" y2="25.4" width="0.1524" layer="91"/>
<label x="627.38" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D5" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_63"/>
<wire x1="533.4" y1="58.42" x2="533.4" y2="25.4" width="0.1524" layer="91"/>
<label x="533.4" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D6" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_64"/>
<wire x1="535.94" y1="58.42" x2="535.94" y2="25.4" width="0.1524" layer="91"/>
<label x="535.94" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_71"/>
<wire x1="538.48" y1="58.42" x2="538.48" y2="25.4" width="0.1524" layer="91"/>
<label x="538.48" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_D1" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_72"/>
<wire x1="541.02" y1="58.42" x2="541.02" y2="25.4" width="0.1524" layer="91"/>
<label x="541.02" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_IORQGE" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_103_CBSEL0"/>
<wire x1="477.52" y1="58.42" x2="477.52" y2="25.4" width="0.1524" layer="91"/>
<label x="477.52" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_NMI" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_104_CBSEL1"/>
<wire x1="480.06" y1="58.42" x2="480.06" y2="25.4" width="0.1524" layer="91"/>
<label x="480.06" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="F_BUSRQ" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_109"/>
<wire x1="500.38" y1="99.06" x2="500.38" y2="149.86" width="0.1524" layer="91"/>
<label x="500.38" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_OE" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_110"/>
<wire x1="502.92" y1="99.06" x2="502.92" y2="149.86" width="0.1524" layer="91"/>
<label x="502.92" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_OE2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_111"/>
<wire x1="505.46" y1="99.06" x2="505.46" y2="149.86" width="0.1524" layer="91"/>
<label x="505.46" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_WAIT" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_112"/>
<wire x1="508" y1="99.06" x2="508" y2="149.86" width="0.1524" layer="91"/>
<label x="508" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_ROMCS" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_114"/>
<wire x1="510.54" y1="99.06" x2="510.54" y2="149.86" width="0.1524" layer="91"/>
<label x="510.54" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_RST" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_115"/>
<wire x1="513.08" y1="99.06" x2="513.08" y2="149.86" width="0.1524" layer="91"/>
<label x="513.08" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_CLK" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_141_GBIN2"/>
<wire x1="513.08" y1="58.42" x2="513.08" y2="25.4" width="0.1524" layer="91"/>
<label x="513.08" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ETH_INT" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_166"/>
<wire x1="563.88" y1="99.06" x2="563.88" y2="149.86" width="0.1524" layer="91"/>
<label x="563.88" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="LEDG" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_167"/>
<wire x1="566.42" y1="99.06" x2="566.42" y2="149.86" width="0.1524" layer="91"/>
<label x="566.42" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="LEDY" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_171"/>
<wire x1="584.2" y1="99.06" x2="584.2" y2="149.86" width="0.1524" layer="91"/>
<label x="584.2" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_PWR" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_170"/>
<wire x1="581.66" y1="99.06" x2="581.66" y2="149.86" width="0.1524" layer="91"/>
<label x="581.66" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_SCK_AL" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_146"/>
<wire x1="543.56" y1="99.06" x2="543.56" y2="149.86" width="0.1524" layer="91"/>
<label x="543.56" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_CS" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_139"/>
<wire x1="538.48" y1="99.06" x2="538.48" y2="149.86" width="0.1524" layer="91"/>
<label x="538.48" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_MISO_WR" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_140_GBIN3"/>
<wire x1="510.54" y1="58.42" x2="510.54" y2="25.4" width="0.1524" layer="91"/>
<label x="510.54" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ETH_MOSI_RD" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_144"/>
<wire x1="541.02" y1="99.06" x2="541.02" y2="149.86" width="0.1524" layer="91"/>
<label x="541.02" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_PSPCFG0" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_198_GBIN0"/>
<wire x1="520.7" y1="58.42" x2="520.7" y2="25.4" width="0.1524" layer="91"/>
<label x="520.7" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ETH_AD0" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_147"/>
<wire x1="546.1" y1="99.06" x2="546.1" y2="149.86" width="0.1524" layer="91"/>
<label x="546.1" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD1" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_148"/>
<wire x1="548.64" y1="99.06" x2="548.64" y2="149.86" width="0.1524" layer="91"/>
<label x="548.64" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD2" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_152"/>
<wire x1="551.18" y1="99.06" x2="551.18" y2="149.86" width="0.1524" layer="91"/>
<label x="551.18" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD3" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_160"/>
<wire x1="553.72" y1="99.06" x2="553.72" y2="149.86" width="0.1524" layer="91"/>
<label x="553.72" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD4" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_190"/>
<wire x1="604.52" y1="99.06" x2="604.52" y2="149.86" width="0.1524" layer="91"/>
<label x="604.52" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD5" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_178"/>
<wire x1="596.9" y1="99.06" x2="596.9" y2="149.86" width="0.1524" layer="91"/>
<label x="596.9" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD6" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_177"/>
<wire x1="594.36" y1="99.06" x2="594.36" y2="149.86" width="0.1524" layer="91"/>
<label x="594.36" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD7" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_174"/>
<wire x1="591.82" y1="99.06" x2="591.82" y2="149.86" width="0.1524" layer="91"/>
<label x="591.82" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD8" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_165"/>
<wire x1="561.34" y1="99.06" x2="561.34" y2="149.86" width="0.1524" layer="91"/>
<label x="561.34" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD9" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_161"/>
<wire x1="556.26" y1="99.06" x2="556.26" y2="149.86" width="0.1524" layer="91"/>
<label x="556.26" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD10" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_168"/>
<wire x1="576.58" y1="99.06" x2="576.58" y2="149.86" width="0.1524" layer="91"/>
<label x="576.58" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD11" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_169"/>
<wire x1="579.12" y1="99.06" x2="579.12" y2="149.86" width="0.1524" layer="91"/>
<label x="579.12" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD12" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_179"/>
<wire x1="599.44" y1="99.06" x2="599.44" y2="149.86" width="0.1524" layer="91"/>
<label x="599.44" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD13" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_181"/>
<wire x1="601.98" y1="99.06" x2="601.98" y2="149.86" width="0.1524" layer="91"/>
<label x="601.98" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_AD14" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_197_GBIN1"/>
<wire x1="518.16" y1="58.42" x2="518.16" y2="25.4" width="0.1524" layer="91"/>
<label x="518.16" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ETH_LEDA" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_172"/>
<wire x1="586.74" y1="99.06" x2="586.74" y2="149.86" width="0.1524" layer="91"/>
<label x="586.74" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="ETH_LEDB" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO0_173"/>
<wire x1="589.28" y1="99.06" x2="589.28" y2="149.86" width="0.1524" layer="91"/>
<label x="589.28" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A5" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_117"/>
<wire x1="518.16" y1="99.06" x2="518.16" y2="149.86" width="0.1524" layer="91"/>
<label x="518.16" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_M1" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_118"/>
<wire x1="520.7" y1="99.06" x2="520.7" y2="149.86" width="0.1524" layer="91"/>
<label x="520.7" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A4" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_119"/>
<wire x1="523.24" y1="99.06" x2="523.24" y2="149.86" width="0.1524" layer="91"/>
<label x="523.24" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_BUSACK" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_120"/>
<wire x1="525.78" y1="99.06" x2="525.78" y2="149.86" width="0.1524" layer="91"/>
<label x="525.78" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A9" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_128"/>
<wire x1="528.32" y1="99.06" x2="528.32" y2="149.86" width="0.1524" layer="91"/>
<label x="528.32" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A11" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_136"/>
<wire x1="530.86" y1="99.06" x2="530.86" y2="149.86" width="0.1524" layer="91"/>
<label x="530.86" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A8" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_137"/>
<wire x1="533.4" y1="99.06" x2="533.4" y2="149.86" width="0.1524" layer="91"/>
<label x="533.4" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="F_A10" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO1_138"/>
<wire x1="535.94" y1="99.06" x2="535.94" y2="149.86" width="0.1524" layer="91"/>
<label x="535.94" y="149.86" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="IO2_95"/>
<wire x1="556.26" y1="58.42" x2="556.26" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R29" gate="R" pin="P$2"/>
<pinref part="R49" gate="R" pin="P$2"/>
<wire x1="556.26" y1="55.88" x2="556.26" y2="53.34" width="0.1524" layer="91"/>
<wire x1="561.34" y1="53.34" x2="561.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="561.34" y1="55.88" x2="556.26" y2="55.88" width="0.1524" layer="91"/>
<junction x="556.26" y="55.88"/>
</segment>
</net>
<net name="ZX_RFSH" class="0">
<segment>
<pinref part="R49" gate="R" pin="P$1"/>
<wire x1="561.34" y1="43.18" x2="561.34" y2="25.4" width="0.1524" layer="91"/>
<label x="561.34" y="25.4" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
