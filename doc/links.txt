http://sindik.at/data/vram/

Z80 T1-T4 cycles:
http://www.piclist.com/techref/mem/dram/slide4.html

https://sinclair.wiki.zxnet.co.uk/wiki/ZX_Spectrum_16K/48K_edge_connector

HDMI
https://github.com/mattvenn/fpga-dvid-ice
https://blackmesalabs.wordpress.com/2017/12/15/bml-hdmi-video-for-fpgas-over-pmod/
https://hackaday.io/page/5702-dvi-hdmi-pmod-for-an-ice40-fpga
https://github.com/splinedrive/my_hdmi_device
https://github.com/Wren6991/DVI-PMOD

M1 signal
http://blog.tynemouthsoftware.co.uk/2017/07/z80-m1-signals.html

ZX pinout
http://thomasloven.com/blog/2013/01/Z80-Information/

zx basic floating point numbers:
http://www.worldofspectrum.org/ZXBasicManual/zxmanappc.html

color information:
http://www.worldofspectrum.org/ZXBasicManual/zxmanchap16.html

tape audio format:
http://faqwiki.zxnet.co.uk/wiki/Spectrum_tape_interface

kuidas originaalne snapshoti tegemine käis:
http://faqwiki.zxnet.co.uk/wiki/Mirage_Microdriver

http://www.pyroelectro.com/tutorials/fpga_vga_resistor_dac/schematic.html

interrupts:
http://www.animatez.co.uk/computers/zx-spectrum/interrupts/
http://wordpress.animatez.co.uk/computers/zx-spectrum/interrupts/
https://chuntey.wordpress.com/2013/10/02/how-to-write-zx-spectrum-games-chapter-17/

stack:
http://www.worldofspectrum.org/Z80.html#5

ULA port & hardware info:
http://www.worldofspectrum.org/faq/reference/48kreference.htm

http://www.worldofspectrum.org/ZXSpectrum128Manual/sp128p04.html 
http://www.alanflavell.org.uk/unicode/unidata25.html
http://www.oryx-embedded.com/doc/enc624j600_8c_source.html
https://chuntey.wordpress.com/2012/12/19/how-to-write-zx-spectrum-games-chapter-2/

http://8bit.yarek.pl/upgrade/zx.roms/index-de.html

snow effect test:
http://www.zxdesign.info/harlequinSnow.shtml

