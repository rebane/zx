void main() __naked{
	__asm

	// disable interrupts
	di

	// clear attrs
	ld hl, #0x5800
	ld (hl), #0x38
	ld de, #0x5801
	ld bc, #((32 * 24) - 1)
	ldir

	// clear pixels
	ld hl, #0x4000
	ld (hl), #0x00
	ld de, #0x4001
	ld bc, #((32 * 8 * 24) - 1)
	ldir

	// set pixels for first 8 rows
	ld hl, #0x4000
	ld (hl), #0xFF
	ld de, #0x4001
	ld bc, #((32 * 8 * 8) - 1)
	ldir

	halt

loop:
	jp loop

	__endasm;
}

