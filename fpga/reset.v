module reset (input clk125, input clk_rdy, input button, output rstn);
	reg [19:0] counter = 19'b0;
	reg [7:0] debounce = 8'b0;
	reg r = 1'b0;

	always @(posedge clk125) begin
		counter <= counter + 1;
		if (counter == 0) begin
			debounce <= { debounce[6:0], clk_rdy && button };
		end
		if (debounce == 0) begin
			r = 1'b0;
		end else if (debounce == 8'b11111111) begin
			r = 1'b1;
		end
	end

	assign rstn = r;
endmodule

