`include "zx_bus.vh"

module io_test (input rstn, input clk125, input [5:0] zx_rq, input [15:0] zx_a, input [7:0] zx_d_in, output io_d_rq, output [7:0] io_d_out);
	localparam STATE_IDLE   = 3'b000;
	localparam STATE_WRITE1 = 3'b001;
	localparam STATE_WRITE2 = 3'b010;
	localparam STATE_READ1  = 3'b011;
	localparam STATE_READ2  = 3'b100;
	localparam STATE_READ3  = 3'b101;

	reg [2:0] state = STATE_IDLE;
	reg [2:0] state_next = STATE_IDLE;

	reg we = 1'b0;
	reg re = 1'b0;

	wire [7:0] unused;
	wire [15:0] io_test_ram1_d;

	always @(posedge clk125) begin
		if (!rstn) begin
			state <= STATE_IDLE;
		end else begin
			state <= state_next;
		end
		if ((state_next == STATE_WRITE1) || (state_next == STATE_WRITE2)) begin
			we <= 1'b1;
		end else begin
			we <= 1'b0;
		end
		if ((state_next == STATE_READ1) || (state_next == STATE_READ2) || (state_next == STATE_READ3)) begin
			re <= 1'b1;
		end else begin
			re <= 1'b0;
		end
	end

	always @(*) begin
		state_next = state;
		if (state == STATE_IDLE) begin
			if ((zx_rq == ZX_BUS_WR_IO) && zx_a[0] && (zx_a[15:1] < 512)) begin
				state_next = STATE_WRITE1;
			end else if ((zx_rq == ZX_BUS_RD_IO) && zx_a[0] && (zx_a[15:1] < 512)) begin
				state_next = STATE_READ1;
			end
		end else if (state == STATE_WRITE1) begin
			state_next = STATE_WRITE2;
		end else if (state == STATE_WRITE2) begin
			state_next = STATE_IDLE;
		end else if (state == STATE_READ1) begin
			state_next = STATE_READ2;
		end else if (state == STATE_READ2) begin
			state_next = STATE_READ3;
		end else if (state == STATE_READ3) begin
			state_next = STATE_IDLE;
		end
	end

	assign io_d_rq = (state_next == STATE_READ3);
	assign io_d_out = zx_a[1] ? io_test_ram1_d[15:8] : io_test_ram1_d[7:0];

	SB_RAM40_4K #(
		.WRITE_MODE(0),
		.READ_MODE(0),
//		.INIT_0(256'h00000000000000000000000000000000000000000000000000000000000000FF)
		.INIT_FILE("bootstrap.hex")
	) io_test_ram1 (
		.WCLK(clk125),
		.WCLKE(we),
		.WE(we),
		.WADDR({3'b000, zx_a[9:2]}),
		.WDATA({zx_d_in, zx_d_in}),
		.MASK(zx_a[1] ? 16'b0000000011111111 : 16'b1111111100000000),
		.RCLK(clk125),
		.RCLKE(re),
		.RE(re),
		.RADDR({3'b000, zx_a[9:2]}),
		.RDATA(io_test_ram1_d)
	);
endmodule

