module zx_bus(input rstn, input clk125, input [15:0] zx_a, input [7:0] zx_d, input zx_wr, input zx_rd, input zx_mreq, input zx_iorq,
		output [5:0] zx_rq, input zx_d_rq, output zx_d_dir);

	localparam ZX_BUS_STATE_IDLE             = 3'b000;
	localparam ZX_BUS_STATE_HIT_WRITE        = 3'b001;
	localparam ZX_BUS_STATE_HIT_READ         = 3'b010;
	localparam ZX_BUS_STATE_WAIT_WRITE       = 3'b011;
	localparam ZX_BUS_STATE_WAIT_READ        = 3'b100;
	localparam ZX_BUS_STATE_WAIT_READ_ACTIVE = 3'b101;

	reg [2:0] state = ZX_BUS_STATE_IDLE;
	reg [2:0] state_next = ZX_BUS_STATE_IDLE;

	always @(posedge clk125) begin
		if (!rstn) begin
			state <= ZX_BUS_STATE_IDLE;
		end else begin
			state <= state_next;
		end
	end

	always @(*) begin
		state_next = state;
		if (state == ZX_BUS_STATE_IDLE) begin
			if (!zx_wr) begin
				state_next = ZX_BUS_STATE_HIT_WRITE;
			end else if (!zx_rd) begin
				state_next = ZX_BUS_STATE_HIT_READ;
			end
		end else if (state == ZX_BUS_STATE_HIT_WRITE) begin
			if (zx_wr) begin
				state_next = ZX_BUS_STATE_IDLE;
			end else begin
				state_next = ZX_BUS_STATE_WAIT_WRITE;
			end
		end else if (state == ZX_BUS_STATE_HIT_READ) begin
			if (zx_rd) begin
				state_next = ZX_BUS_STATE_IDLE;
			end else if (zx_d_rq) begin
				state_next = ZX_BUS_STATE_WAIT_READ_ACTIVE;
			end else begin
				state_next = ZX_BUS_STATE_WAIT_READ;
			end
		end else if (state == ZX_BUS_STATE_WAIT_WRITE) begin
			if (zx_wr) begin
				state_next = ZX_BUS_STATE_IDLE;
			end
		end else if (state == ZX_BUS_STATE_WAIT_READ) begin
			if (zx_rd) begin
				state_next = ZX_BUS_STATE_IDLE;
			end else if (zx_d_rq) begin
				state_next = ZX_BUS_STATE_WAIT_READ_ACTIVE;
			end
		end else if (state == ZX_BUS_STATE_WAIT_READ_ACTIVE) begin
			if (zx_rd) begin
				state_next = ZX_BUS_STATE_IDLE;
			end
		end
	end

	assign zx_rq = { ((state == ZX_BUS_STATE_HIT_WRITE) || (state == ZX_BUS_STATE_HIT_READ)), (state != ZX_BUS_STATE_IDLE), !zx_wr, !zx_rd, !zx_mreq, !zx_iorq };
	assign zx_d_dir = (state == ZX_BUS_STATE_WAIT_READ_ACTIVE);
endmodule

// https://discourse.tinyfpga.com/t/resolved-understanding-the-sb-io-primitive/1357/3

