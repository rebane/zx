module sample ();
	reg clk;
	reg rstn;
	reg [15:0] zx_a;
	reg [7:0] zx_d;
	reg zx_wr;
	reg zx_rd;
	reg zx_mreq;
	reg zx_iorq;
	reg zx_d_rq;

	wire [5:0] zx_rq;
	wire zx_d_dir;

	zx_bus ZX_BUS(rstn, clk, zx_a, zx_d, zx_wr, zx_rd, zx_mreq, zx_iorq, zx_rq, zx_d_rq, zx_d_dir);

	initial begin
		$dumpfile("sample.vcd");
		$dumpvars(0, sample);
		force rstn = 0;
		force clk = 0;
		force zx_rd = 1;
		force zx_wr = 1;
		force zx_iorq = 1;
		force zx_mreq = 1;
		#30;
		force rstn = 1;
		#20;
		force zx_a = 64;
		force zx_d = 16;
		force zx_wr = 0;
		force zx_iorq = 0;
		#100;
		force zx_wr = 1;
		force zx_iorq = 1;
		#200;
		force zx_rd = 0;
		force zx_iorq = 0;
		#40;
		force zx_d_rq = 1;
		#20
		force zx_d_rq = 0;
		#100;
		force zx_rd = 1;
		force zx_iorq = 1;
		#1000 $finish();
	end

	always begin
		#10;
		force clk = 1;
		#10;
		force clk = 0;
	end
endmodule

