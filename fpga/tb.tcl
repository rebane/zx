proc add_signal { signal data color } {
	set signals [list]
	lappend signals $signal
	gtkwave::addSignalsFromList $signals
	set signals [list]
	lappend signals [lindex [gtkwave::getDisplayedSignals] end]
	gtkwave::/Edit/UnHighlight_All
	gtkwave::highlightSignalsFromList $signals
	gtkwave::/Edit/Data_Format/$data
	gtkwave::/Edit/Color_Format/$color
	gtkwave::/Edit/Insert_Blank
	gtkwave::/Edit/UnHighlight_All
}

add_signal "clk" Hex Blue
add_signal "rstn" Hex Red
add_signal "sample.ZX_BUS.a" Hex Normal

gtkwave::/Time/Zoom/Zoom_Best_Fit

