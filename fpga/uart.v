module uart(input clk125, output tx);

	reg [10:0] brr = 11'b0;
	reg [19:0] b = 20'b01000001011111111111;

	always @(posedge clk125) begin
		brr <= brr + 1;
		if (brr == 1091) begin
			brr <= 0;
			b[19:0] <= { b[18:0], b[19] };
		end
	end

	assign tx = b[19];

endmodule

