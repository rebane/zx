`include "zx_bus.vh"

module main (input clk11, input button, output led2, output led3, output tx,
		output zx_iorqge, output zx_nmi, output zx_busrq, output zx_oe, output zx_oe2, output zx_wait, output zx_romcs, output zx_rst, output zx_o_oe,
		input [15:0] zx_a, inout [7:0] zx_d, output zx_d_oe, output zx_d_dir, input zx_rd, input zx_wr, input zx_iorq, input zx_mreq, input zx_halt,
		output hdmi_oe_n, output hdmi_ddc_en, output [3:0] hdmi_p);

	wire clk125;
	wire pll125_lock;
	wire rstn;
	wire [5:0] zx_rq;
	wire [7:0] zx_d_in;

	wire bootstrap_d_rq;
	wire [7:0] bootstrap_d_out;

	wire io_d_rq;
	wire [7:0] io_d_out;

	reg zx_d_rq = 1'b0;
	reg [7:0] zx_d_out = 8'b00000000;

	reg [31:0] counter = 32'b0;
	reg led;

	reset RESET(clk125, pll125_lock, button, rstn);
	hdmi HDMI(clk125, hdmi_p);
	uart UART(clk125, tx);
	zx_bus ZX_BUS(rstn, clk125, zx_a, zx_d_in, zx_wr, zx_rd, zx_mreq, zx_iorq, zx_rq, zx_d_rq, zx_d_dir);
	bootstrap BOOTSTRAP(rstn, clk125, zx_rq, zx_a, zx_d_in, bootstrap_d_rq, bootstrap_d_out);
	io_test IO_TEST(rstn, clk125, zx_rq, zx_a, zx_d_in, io_d_rq, io_d_out);

	always @(posedge clk125) begin
		counter <= counter + 1;
		if ((zx_rq == ZX_BUS_WR_IO) && (zx_a == 16'b0000000000000001)) begin
			led <= !led;
		end
		if (bootstrap_d_rq && zx_romcs) begin
			zx_d_rq <= 1'b1;
			zx_d_out <= bootstrap_d_out;
		end else if (io_d_rq) begin
			zx_d_rq <= 1'b1;
			zx_d_out <= io_d_out;
		end else begin
			zx_d_rq <= 1'b0;
		end
	end

	assign zx_iorqge = 1'b0;
	assign zx_nmi = 1'b1;
	assign zx_busrq = 1'b1;
	assign zx_oe = 1'b0;
	assign zx_oe2 = 1'b0;
	assign zx_wait = 1'b1;
	assign zx_romcs = 1'b1;
	assign zx_rst = !rstn;
	assign zx_o_oe = 1'b0;
	assign zx_d_oe = 1'b0;

	assign hdmi_oe_n = 1'b0;
	assign hdmi_ddc_en = 1'b1;

	assign led3 = zx_halt;
	assign led2 = counter[25];

	SB_IO #( 
		.PIN_TYPE(6'b101001), 
		.PULLUP(1'b0) 
	) zx_d_io_buf [7:0] ( 
		.PACKAGE_PIN(zx_d), 
		.OUTPUT_ENABLE(zx_d_dir), 
		.D_OUT_0(zx_d_out), 
		.D_IN_0(zx_d_in) 
	);

	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.DIVR(4'b0000),         // DIVR =  0
		.DIVF(7'b1011010),      // DIVF = 90
		.DIVQ(3'b011),          // DIVQ =  3
		.FILTER_RANGE(3'b001)   // FILTER_RANGE = 1
	) pll125 (
		.RESETB(1'b1),
		.BYPASS(1'b0),
		.REFERENCECLK(clk11),
		.PLLOUTCORE(clk125),
		.LOCK(pll125_lock)
	);
endmodule

