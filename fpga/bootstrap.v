`include "zx_bus.vh"

module bootstrap (input rstn, input clk125, input [5:0] zx_rq, input [15:0] zx_a, input [7:0] zx_d_in, output bootstrap_d_rq, output [7:0] bootstrap_d_out);
	localparam STATE_IDLE  = 2'b00;
	localparam STATE_READ1 = 2'b01;
	localparam STATE_READ2 = 2'b10;
	localparam STATE_READ3 = 2'b11;

	reg [1:0] state = STATE_IDLE;
	reg [1:0] state_next = STATE_IDLE;

	reg re = 1'b0;

	wire [7:0] unused;
	wire [15:0] bootstrap_rom1_d;

	always @(posedge clk125) begin
		if (!rstn) begin
			state <= STATE_IDLE;
		end else begin
			state <= state_next;
		end
		if ((state_next == STATE_READ1) || (state_next == STATE_READ2) || (state_next == STATE_READ3)) begin
			re <= 1'b1;
		end else begin
			re <= 1'b0;
		end
	end

	always @(*) begin
		state_next = state;
		if (state == STATE_IDLE) begin
			if ((zx_rq == ZX_BUS_RD_MEM) && (zx_a < 512)) begin
				state_next = STATE_READ1;
			end
		end else if (state == STATE_READ1) begin
			state_next = STATE_READ2;
		end else if (state == STATE_READ2) begin
			state_next = STATE_READ3;
		end else if (state == STATE_READ3) begin
			state_next = STATE_IDLE;
		end
	end

	assign bootstrap_d_rq = (state_next == STATE_READ3);
	assign bootstrap_d_out = zx_a[0] ? bootstrap_rom1_d[15:8] : bootstrap_rom1_d[7:0];

	SB_RAM40_4K #(
		.WRITE_MODE(0),
		.READ_MODE(0),
		.INIT_FILE("bootstrap.hex")
	) bootstrap_rom1 (
		.WCLK(clk125),
		.WCLKE(1'b0),
		.WE(1'b0),
		.WADDR(11'b00000000000),
		.WDATA(16'b0000000000000000),
		.MASK(16'b0000000000000000),
		.RCLK(clk125),
		.RCLKE(re),
		.RE(re),
		.RADDR({3'b000, zx_a[8:1]}),
		.RDATA(bootstrap_rom1_d)
	);
endmodule

